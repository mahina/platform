#!/usr/bin/env bash

set -e
echo "" > coverage.txt

for d in $(go list ./... | grep -v vendor); do
    TEST_DATABASE=$1 go test -tags 'unit integration' -race -coverprofile=profile.out -covermode=atomic $d
    if [ -f profile.out ]; then
        cat profile.out >> coverage.txt
        rm profile.out
    fi
done

bash <(curl -s https://codecov.io/bash) -t 2145637a-f91f-49eb-a9c1-57f409488073