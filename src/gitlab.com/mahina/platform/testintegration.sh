#!/bin/bash
if [[ -z "${TEST_DATABASE}" ]]; then
  echo "Warning, no TEST_DATABASE envvar found, setting to default 'market'"
  export TEST_DATABASE="market"
fi
if [[ -z "${TEST_DATABASE_USER}" ]]; then
  echo "Warning, no TEST_DATABASE_USER envvar found, setting to default 'mahina'"
  export TEST_DATABASE_USER="mahina"
fi
if [[ -z "${TEST_DATABASE_PASSWORD}" ]]; then
  echo "Warning, no TEST_DATABASE_PASSWORD envvar found, setting to 'default'"
  export TEST_DATABASE_PASSWORD="m@hInA"
fi
if [[ -z "${TEST_DATABASE_HOST}" ]]; then
  echo "Warning, no TEST_DATABASE_HOST envvar found, setting to default 'localhost'"
  export TEST_DATABASE_HOST="localhost"
fi

go test -v $(go list ./... | grep -v /vendor/) -tags integration
