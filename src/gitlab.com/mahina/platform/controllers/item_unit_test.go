// +build unit all

package controllers

import (
	"net/http"
	"strings"
	"testing"

	"net/http/httptest"

	"github.com/stretchr/testify/assert"
	"github.com/gin-gonic/gin"
)

func TestItemPostNullContent(t *testing.T) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("POST", version+"/items", nil)
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusBadRequest, resp.Code)
}

func TestItemPostEmptyContent(t *testing.T) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("POST", version+"/items", strings.NewReader(""))
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusBadRequest, resp.Code)
}

func TestItemPostInvalidContent(t *testing.T) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("POST", version+"/items", strings.NewReader(`{"foo":"bar"}`))
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusBadRequest, resp.Code)
}

func TestItemGetItemBadParam(t *testing.T) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("GET", version+"/items/foo", nil)
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusBadRequest, resp.Code)
}
