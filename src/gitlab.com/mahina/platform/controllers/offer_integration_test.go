// +build integration all

package controllers

import (
	"bytes"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"strconv"
	"testing"
	"time"

	"gitlab.com/mahina/platform/models"
	"gitlab.com/mahina/platform/util"

	"strings"

	"encoding/json"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestSearchOffers(t *testing.T) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("GET", version+"/offers?expand=false&attributes=organic", nil)
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code)
}

func TestCreateOffer(t *testing.T) {

	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	// simulate posting an image before the offer is created
	path := "test_fixtures/smallimage.png"
	file, err := os.Open(path)
	require.NoError(t, err, "Expected to find test fixture image")
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filepath.Base(path))
	require.NoError(t, err, "Expected to create form file writer")
	_, err = io.Copy(part, file)

	err = writer.Close()
	require.NoError(t, err, "Expected to close the writer")

	req, err := http.NewRequest("POST", version+"/item-images", body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusCreated, resp.Code)

	image, err := models.NewItemImage(nil, resp.Body.Bytes(), "market")
	require.NoError(t, err, "Expected to create the image object")

	// Post the OFFER details
	offerBody := fmt.Sprintf(offerJSON, "active", image.ImageUUID.String)
	req, err = http.NewRequest("POST", version+"/offers", strings.NewReader(offerBody))
	require.NoError(t, err, "Expected to be able to create offer POST url")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	require.Equal(t, http.StatusCreated, resp.Code, string(resp.Body.Bytes()))

	// Instantiate the offer from the response
	entity := SupplierOffer{}
	err = json.Unmarshal(resp.Body.Bytes(), &entity)
	assert.NoError(t, err, "Expected to be able to create the rest offer object")

	// Get the offer from REST
	req, err = http.NewRequest("GET", version+"/offers/"+entity.OfferID, nil)
	assert.NoError(t, err, "Expected to create offer GET url")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code, string(resp.Body.Bytes()))

	// Test the offer is correct
	offer := SupplierOffer{}
	err = json.Unmarshal(resp.Body.Bytes(), &offer)
	assert.True(t, util.IsUUID(offer.OfferID), "Expected valid UUID, got: "+offer.OfferID)
	assert.Equal(t, 1, len(offer.ImageUUIDs), "Expected to find one imageUUID")

	// Get the expanded offer from REST
	req, err = http.NewRequest("GET", version+"/offers/"+entity.OfferID+"?expand=true&encodeImages=true", nil)
	assert.NoError(t, err, "Expected to create offer GET url")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code, string(resp.Body.Bytes()))

	// Test the offer is correct
	offer = SupplierOffer{}
	err = json.Unmarshal(resp.Body.Bytes(), &offer)
	assert.True(t, util.IsUUID(offer.OfferID), "Expected valid UUID, got: "+offer.OfferID)
	assert.Equal(t, 1, len(offer.Images), "Expected to find one image entry")

	// Get the raw offer image
	req, err = http.NewRequest("GET", version+"/offers/"+entity.OfferID+"/images/"+offer.Images[0].ImageUUID.String, nil)
	assert.NoError(t, err, "Expected creation of get offer image url")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code, string(resp.Body.Bytes()))
	assert.True(t, resp.Body.Len() > 1, "Expected to get image bytes")

	// Find the offer from REST via search
	req, err = http.NewRequest("GET", version+"/offers?q=cabbage&attributes=organic", nil)
	assert.NoError(t, err, "Expected to create the request for the offer")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code, string(resp.Body.Bytes()))
	assert.NotEqual(t, resp.HeaderMap.Get("X-Total-Count"), "0", "expected at least 1 result")

	req, err = http.NewRequest("GET", version+"/offers?q=tigers", nil)
	assert.NoError(t, err, "Expected to execute an offer search")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code, string(resp.Body.Bytes()))
	assert.Equal(t, "0", resp.HeaderMap.Get("X-Total-Count"), "expected 0 results, result:"+string(resp.Body.Bytes()))

	// Update the offer as INACTIVE
	offerBody = fmt.Sprintf(offerJSON, "inactive", image.ImageUUID.String)
	req, err = http.NewRequest("PUT", version+"/offers/"+offer.OfferID, strings.NewReader(offerBody))
	require.NoError(t, err, "Expected to be able to create offer PUT url")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	require.Equal(t, http.StatusOK, resp.Code, string(resp.Body.Bytes()))

	req, err = http.NewRequest("GET", version+"/offers?status=inactive", nil)
	assert.NoError(t, err, "Expected to be able to create offer GET url")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code, string(resp.Body.Bytes()))
	assert.NotEqual(t, resp.HeaderMap.Get("X-Total-Count"), "0", "expected at least 1 result")

	// Clean up

	// Delete the OFFER
	req, err = http.NewRequest("DELETE", version+"/offers/"+entity.OfferID, nil)
	assert.NoError(t, err, "Expected to create offer DELETE url")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusNoContent, resp.Code, string(resp.Body.Bytes()))
}

func TestCreateOfferOngoing(t *testing.T) {

	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	// simulate posting an image before the offer is created
	path := "test_fixtures/smallimage.png"
	file, err := os.Open(path)
	require.NoError(t, err, "Expected to find test fixture image")
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filepath.Base(path))
	require.NoError(t, err, "Expected to create form file writer")
	_, err = io.Copy(part, file)

	err = writer.Close()
	require.NoError(t, err, "Expected to close the writer")

	req, err := http.NewRequest("POST", version+"/item-images", body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusCreated, resp.Code)

	image, err := models.NewItemImage(nil, resp.Body.Bytes(), "market")
	require.NoError(t, err, "Expected to create the image object")

	// Post the OFFER details
	offerBody := fmt.Sprintf(ongoingOfferJSON, "active", image.ImageUUID.String)
	req, err = http.NewRequest("POST", version+"/offers", strings.NewReader(offerBody))
	require.NoError(t, err, "Expected to be able to create offer POST url")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	require.Equal(t, http.StatusCreated, resp.Code, string(resp.Body.Bytes()))

	// Instantiate the offer from the response
	entity := SupplierOffer{}
	err = json.Unmarshal(resp.Body.Bytes(), &entity)
	assert.NoError(t, err, "Expected to be able to create the rest offer object")

	// Get the expanded offer from REST
	req, err = http.NewRequest("GET", version+"/offers/"+entity.OfferID+"?expand=true", nil)
	assert.NoError(t, err, "Expected to create offer GET url")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code, string(resp.Body.Bytes()))

	// Test the offer is correct
	offer := SupplierOffer{}
	err = json.Unmarshal(resp.Body.Bytes(), &offer)
	assert.True(t, util.IsUUID(offer.OfferID), "Expected valid UUID, got: "+offer.OfferID)
	assert.Equal(t, 1, len(offer.Images), "Expected to find one image entry")
	assert.Nil(t, offer.WillSupply.LastDate, "Expected last date to be nil for an ongoing offer")

	// Clean up

	// Delete the OFFER
	req, err = http.NewRequest("DELETE", version+"/offers/"+entity.OfferID, nil)
	assert.NoError(t, err, "Expected to create offer DELETE url")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusNoContent, resp.Code, string(resp.Body.Bytes()))
}

func TestFindOfferAndOrder(t *testing.T) {

	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	// simulate posting an image before the offer is created
	path := "test_fixtures/smallimage.png"
	file, err := os.Open(path)
	require.NoError(t, err, "Expected to find test fixture image")
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filepath.Base(path))
	require.NoError(t, err, "Expected to create form file writer")
	_, err = io.Copy(part, file)

	err = writer.Close()
	require.NoError(t, err, "Expected to close the writer")

	req, err := http.NewRequest("POST", version+"/item-images", body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusCreated, resp.Code)

	image, err := models.NewItemImage(nil, resp.Body.Bytes(), "market")
	require.NoError(t, err, "Expected to create the image object")

	// Post the first OFFER
	offerBody := fmt.Sprintf(offerJSON, "active", image.ImageUUID.String)
	req, err = http.NewRequest("POST", version+"/offers", strings.NewReader(offerBody))
	require.NoError(t, err, "Expected to be able to create offer POST url")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	require.Equal(t, http.StatusCreated, resp.Code, string(resp.Body.Bytes()))

	// Instantiate the offer from the response
	entity := SupplierOffer{}
	err = json.Unmarshal(resp.Body.Bytes(), &entity)
	assert.NoError(t, err, "Expected to be able to create the rest offer object")

	// Post the second OFFER
	offerBody = fmt.Sprintf(offerJSON2, "active", image.ImageUUID.String)
	req, err = http.NewRequest("POST", version+"/offers", strings.NewReader(offerBody))
	require.NoError(t, err, "Expected to be able to create offer POST url")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	require.Equal(t, http.StatusCreated, resp.Code, string(resp.Body.Bytes()))

	// Instantiate the offer from the response
	entity2 := SupplierOffer{}
	err = json.Unmarshal(resp.Body.Bytes(), &entity2)
	assert.NoError(t, err, "Expected to be able to create the rest offer object")

	// Find offers from REST, orderBy category
	req, err = http.NewRequest("GET", version+"/offers?orderBy=category:desc", nil)
	assert.NoError(t, err, "Expected to create the get request for the offer")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code, string(resp.Body.Bytes()))
	assert.NotEqual(t, resp.HeaderMap.Get("X-Total-Count"), "0", "expected at least 1 result")

	results := []SupplierOffer{}
	err = json.Unmarshal(resp.Body.Bytes(), &results)
	assert.NoError(t, err, "Expected to be able to create the rest []offer object")
	assert.Equal(t, "001.013.014", results[0].Category)

	// Find offers from REST, orderBy startDate
	req, err = http.NewRequest("GET", version+"/offers?orderBy=startDate:desc", nil)
	assert.NoError(t, err, "Expected to create the get request for the offer")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code, string(resp.Body.Bytes()))
	assert.NotEqual(t, resp.HeaderMap.Get("X-Total-Count"), "0", "expected at least 1 result")

	results = []SupplierOffer{}
	err = json.Unmarshal(resp.Body.Bytes(), &results)
	assert.NoError(t, err, "Expected to be able to create the rest []offer object")
	assert.Equal(t, time.Date(2018, time.December, 1, 0, 0, 0, 0, time.UTC), results[0].WillSupply.FirstDate.Time)

	// Find offers from REST, orderBy title
	req, err = http.NewRequest("GET", version+"/offers?orderBy=title", nil)
	assert.NoError(t, err, "Expected to create the get request for the offer")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code, string(resp.Body.Bytes()))
	assert.NotEqual(t, resp.HeaderMap.Get("X-Total-Count"), "0", "expected at least 1 result")

	results = []SupplierOffer{}
	err = json.Unmarshal(resp.Body.Bytes(), &results)
	assert.NoError(t, err, "Expected to be able to create the rest []offer object")
	assert.Equal(t, "Cabbages", results[0].Title)

	// Find offers from REST, orderBy quantity
	req, err = http.NewRequest("GET", version+"/offers?orderBy=quantity:desc", nil)
	assert.NoError(t, err, "Expected to create the get request for the offer")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code, string(resp.Body.Bytes()))
	assert.NotEqual(t, resp.HeaderMap.Get("X-Total-Count"), "0", "expected at least 1 result")

	results = []SupplierOffer{}
	err = json.Unmarshal(resp.Body.Bytes(), &results)
	assert.NoError(t, err, "Expected to be able to create the rest []offer object")
	assert.Equal(t, 33.5, results[0].WillSupply.Quantity)

	// Find offers from REST, orderBy price
	req, err = http.NewRequest("GET", version+"/offers?orderBy=price:desc", nil)
	assert.NoError(t, err, "Expected to create the get request for the offer")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code, string(resp.Body.Bytes()))
	assert.NotEqual(t, resp.HeaderMap.Get("X-Total-Count"), "0", "expected at least 1 result")

	results = []SupplierOffer{}
	err = json.Unmarshal(resp.Body.Bytes(), &results)
	assert.NoError(t, err, "Expected to be able to create the rest []offer object")
	assert.Equal(t, 10.5, results[0].Pricing[0].Price)

	// Clean up

	// Delete the OFFERs
	req, err = http.NewRequest("DELETE", version+"/offers/"+entity.OfferID, nil)
	assert.NoError(t, err, "Expected to create offer DELETE url")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusNoContent, resp.Code, string(resp.Body.Bytes()))
	req, err = http.NewRequest("DELETE", version+"/offers/"+entity2.OfferID, nil)
	assert.NoError(t, err, "Expected to create offer DELETE url")
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusNoContent, resp.Code, string(resp.Body.Bytes()))
}

var offerJSON = `
{
	"title": "Cabbages",
	"description": "These cabbages are amazing.",
	"status": "%s",
	"attributes": [
		"organic"
	],
	"imageUUIDs": [
		"%s"
	],
	"category": "001.006",
	"willSupply": {
		"quantity": 33.5,
		"uom": "pound",
		"firstDate": "2018-06-09",
		"lastDate": "2018-08-30"
	},
	"pricing": [
		{
			"min": 1,
			"max": 20,
			"price": 1.94
		},
		{
			"min": 21,
			"max": 40,
			"price": 1.78			
		}
	],
	"deliveryDays": [
		"tuesday",
		"friday"
	],
	"notes": "here are some notes to the market manager"
}
`

var offerJSON2 = `
{
	"title": "Clover Honey",
	"description": "This honey is amazing.",
	"status": "%s",
	"attributes": [
		"organic"
	],
	"imageUUIDs": [
		"%s"
	],
	"category": "001.013.014",
	"willSupply": {
		"quantity": 10.5,
		"uom": "pound",
		"firstDate": "2018-12-01",
		"lastDate": "2018-12-31"
	},
	"pricing": [
		{
			"min": 1,
			"max": 20,
			"price": 10.50
		},
		{
			"min": 21,
			"max": 40,
			"price": 8.78			
		}
	],
	"deliveryDays": [
		"tuesday",
		"friday"
	],
	"notes": "here are some notes to the market manager"
}
`

var ongoingOfferJSON = `
{
	"title": "Cabbages",
	"description": "These cabbages are amazing.",
	"status": "%s",
	"attributes": [
		"organic"
	],
	"imageUUIDs": [
		"%s"
	],
	"category": "001.006",
	"willSupply": {
		"quantity": 33.5,
		"uom": "pound",
		"firstDate": "2018-04-01"
	},
	"pricing": [
		{
			"min": 1,
			"max": 20,
			"price": 1.94
		},
		{
			"min": 21,
			"max": 40,
			"price": 1.78			
		}
	],
	"deliveryDays": [
		"friday"
	],
	"notes": "here are some notes to the market manager"
}
`

var offerJSONArray = []string{
	`{
		"supplierUUID": "%s",
		"itemUUID": "%s",
		"status": "active",
		"type": "willsupply",
		"firstDate": "2017-06-09",
		"lastDate": "2500-01-01",
		"quantity": 5,
		"deliveryDays": 2
	}`,
	`{
		"supplierUUID": "%s",
		"itemUUID": "%s",
		"status": "active",
		"type": "willsupply",
		"firstDate": "2017-08-01",
		"lastDate": "2017-11-30",
		"quantity": 20,
		"deliveryDays": 2
	}`,
}

const offerPricingJSON = `{
	"offerUUID": "%s",
	"quantityMin": %f,
	"quantityMax": %f,
	"price": %f
}`

func insertItemOffers(tx *sqlx.Tx) error {
	// 359 items, 144 fruits, 40 onions, 170 vegetables, 1 herbs, 4 ornamentals
	// 122 organic items
	schema := "market"

	var totalCount uint64
	results, err := models.GetAllItems(nil, 100, 1, schema, "active",
		"", "", "", nil, true, true, &totalCount)

	if len(results) == 0 {
		fmt.Println("No items found, returning")
		return nil
	}
	if err != nil {
		return err
	}
	for i := range results {
		item := results[i]
		n := i % 2
		offerString := fmt.Sprintf(offerJSONArray[n], item.SupplierUUID.String, item.ItemUUID.String)
		offer, err := models.NewOffer(tx, []byte(offerString), schema)
		if err != nil {
			return err
		}
		err = offer.Insert(tx)
		if err != nil {
			return err
		}
		if n == 1 {
			offerPricingString := fmt.Sprintf(offerPricingJSON, offer.OfferUUID.String, 1.0, 20.0, 3.15)
			offerPricing, err := models.NewOfferPricing(tx, []byte(offerPricingString), schema)
			if err != nil {
				return err
			}
			err = offerPricing.Insert(tx)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

var testOfferJSON = `
{
	"title": "%s",
	"description": "%s",
	"status": "%s",
	"attributes": [
		"%s",
		"%s"
	],
	"imageIDs": [
		"%s"
	],
	"category": "%s",
	"willSupply": {
		"quantity": 33.5,
		"uom": "pound",
		"firstDate": "2017-06-09",
		"lastDateXXX": "2017-08-30"
	},
	"pricing": [
		{
			"min": 1,
			"max": 20,
			"price": 1.94
		},
		{
			"min": 21,
			"max": 40,
			"price": 1.78			
		}
	],
	"deliveryDays": [
		"tuesday",
		"friday"
	],
	"notes": "here are some notes to the market manager"
}
`

func TestSearchOffersExtensive(t *testing.T) {

	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	// simulate posting an image before the offer is created
	path := "test_fixtures/smallimage.png"
	file, err := os.Open(path)
	require.NoError(t, err, "Expected to find test fixture image")
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filepath.Base(path))
	require.NoError(t, err, "Expected to create form file writer")
	_, err = io.Copy(part, file)

	err = writer.Close()
	require.NoError(t, err, "Expected to close the writer")

	req, err := http.NewRequest("POST", version+"/item-images", body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusCreated, resp.Code)

	image, err := models.NewItemImage(nil, resp.Body.Bytes(), "market")
	require.NoError(t, err, "Expected to create the image object")

	tempAttribute := "temp" + strconv.Itoa(int(time.Now().Unix()))

	data := [][]string{
		{"White Peaches", "Amazing White Peaches from our orchards", "organic", tempAttribute, "001.002"},
		{"Oyster Mushrooms", "Creamy mushrooms, great for sauteing", "none", tempAttribute, "001.006.009"},
		{"Plover Eggs", "Small and delicate", "organic", tempAttribute, "001.011.012"},
		{"Clover Honey", "Honey primarily from clover", "none", tempAttribute, "001.013.014"},
		{"Yellow Peaches", "The best of the small fruit from our orchards", "organic", tempAttribute, "001.002"},
	}

	offers := []SupplierOffer{}
	for i := range data {
		// Post the OFFER details
		offerBody := fmt.Sprintf(testOfferJSON, data[i][0], data[i][1], "active", data[i][2], data[i][3], image.ImageUUID.String, data[i][4])
		req, err = http.NewRequest("POST", version+"/offers", strings.NewReader(offerBody))
		require.NoError(t, err, "Expected to be able to create offer POST url")
		resp = httptest.NewRecorder()
		router.ServeHTTP(resp, req)
		require.Equal(t, http.StatusCreated, resp.Code, string(resp.Body.Bytes()))
		entity := SupplierOffer{}
		err = json.Unmarshal(resp.Body.Bytes(), &entity)
		assert.NoError(t, err, "Expected to be able to create the rest offer object")
		offers = append(offers, entity)
	}

	type foil struct {
		query string
		count string
	}
	allOffersCount := strconv.Itoa(len(data))
	searchData := []foil{
		{version + "/offers?status=active&attributes=" + tempAttribute, allOffersCount},                                 // search for all
		{version + "/offers?orderBy=price&q=mushroom&attributes=" + tempAttribute, "1"},                                 // search for just mushrooms (and add orderBy just for kicks)
		{version + "/offers?category=001.013&attributes=" + tempAttribute, "1"},                                         // search for just artisanals
		{version + "/offers?category=001&attributes=" + tempAttribute, allOffersCount},                                  // search for all by upper category
		{version + "/offers?q=honey+default&attributes=" + tempAttribute, "1"},                                          // search for all by eggs and supplier name
		{version + "/offers?q=orchard&attributes=" + tempAttribute, "2"},                                                // search for orchards in the description
		{version + "/offers?q=orchard&limit=1&attributes=" + tempAttribute, "2"},                                        // test limit
		{version + "/offers?supplier=ffffffff-ffff-ffff-ffff-ffffffffffff&attributes=" + tempAttribute, allOffersCount}, // search by supplier
		{version + "/offers?supplier=eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee&attributes=" + tempAttribute, "0"},            // search by non-existent supplier
		{version + "/offers?q=saute&attributes=" + tempAttribute, "1"},                                                  // search for stem word in description
		{version + "/offers?q=saute+honey&attributes=" + tempAttribute, "0"},                                            // search for stem word in title, description
		{version + "/offers?q=saute+creamy&attributes=" + tempAttribute, "1"},                                           // search for stem word in title, description
		{version + "/offers?q=eggs+small+marketplace&attributes=" + tempAttribute, "1"},                                 // search for stem word in title, description and supplier
		{version + "/offers?q=small&attributes=" + tempAttribute, "2"},                                                  // search for stem word in description
		{version + "/offers?attributes=organic&attributes=" + tempAttribute, "3"},                                       // search for multiple attributes
	}

	for i := range searchData {
		req, err = http.NewRequest("GET", searchData[i].query, nil)
		assert.NoError(t, err, "Expected to be able to create offer GET url")
		resp = httptest.NewRecorder()
		router.ServeHTTP(resp, req)
		assert.Equal(t, http.StatusOK, resp.Code, string(resp.Body.Bytes()))
		assert.Equal(t, searchData[i].count, resp.HeaderMap.Get("X-Total-Count"), fmt.Sprintf("expected %s results for %s", searchData[i].count, searchData[i].query))
	}

	// CLEAN UP
	for _, offer := range offers {
		req, err = http.NewRequest("DELETE", version+"/offers/"+offer.OfferID, nil)
		assert.NoError(t, err, "Expected to create offer DELETE url")
		resp = httptest.NewRecorder()
		router.ServeHTTP(resp, req)
		assert.Equal(t, http.StatusNoContent, resp.Code, string(resp.Body.Bytes()))
	}

}
