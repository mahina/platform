package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type APIErrorMessage struct {
	Description string `json:"description"`
	Link        string `json:"link"`
}

var errorsMap = map[int]string{
	http.StatusBadRequest:          "The request was unacceptable",
	http.StatusUnauthorized:        "Either the presented credentials are incorrect, or missing altogether",
	http.StatusForbidden:           "The presented credentials are correct, but not sufficent to access the requested resource",
	http.StatusInternalServerError: "An unexpected exception occured",
	http.StatusNotFound:            "The requested resource was not found",
}

/*
In summary, a 401 Unauthorized response should be used for missing or bad authentication, and a 403 Forbidden response should be used afterwards, when the user is authenticated but isn’t authorized to perform the requested operation on the given resource.
*/
func ErrorMessage(c *gin.Context, status int, message string, err error) {
	msg := errorsMap[status]
	if message != "" {
		msg += ". Detail: " + message + "."
	}
	if err != nil {
		msg += "Error: " + err.Error()
	}
	c.IndentedJSON(status, &APIErrorMessage{msg, "https://gitlab.com/mahina/platform/wikis/api-error-detail"})
}
