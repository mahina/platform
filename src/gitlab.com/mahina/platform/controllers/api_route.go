package controllers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/mahina/platform/util"

	log "github.com/Sirupsen/logrus"
)

const V1 = "/v1"

// InitializeAPIRoutes creates the routing paths for the REST API
func InitializeAPIRoutes(router *gin.Engine) {

	key, err := util.GetPrivateRSAKey()
	if err != nil {
		log.Error("API service reports error getting private key. Aborting", err)
		return
	}

	v1 := router.Group(V1)
	v1.Use(ErrorWriter)
	if gin.Mode() != gin.TestMode {
		v1.Use(Auth(key))
	}
	{
		v1.GET("/mktconfig", marketConfigGet)

		v1.GET("/users", userGetAll)
		v1.GET("/users/:id", userGet)
		v1.GET("/users/:id/roles", userRolesGet)
		v1.GET("/users/:id/organizations", userOrganizationsGet)
		v1.POST("/users", userCreate)
		v1.PUT("/users/:id", userUpdate)
		v1.DELETE("/users/:id", userDelete)
		v1.POST("/users/:id/authenticate", userAuthenticate)

		v1.GET("/registrations/:id", registrationGet)
		v1.POST("/registrations", registrationCreate)
		v1.DELETE("/registrations/:id", registrationDelete)
		v1.GET("/registrations/:id/confirm-email", registrationConfirmEmail)
		v1.POST("/registrations/:id/confirm-email", registrationConfirmEmail)
		v1.POST("/registrations/:id/process", registrationProcess)

		v1.GET("/items/:id", itemGet)
		v1.GET("/items", itemGetAll)
		v1.POST("/items", itemCreate)
		v1.PUT("/items/:id", itemUpdate)
		v1.DELETE("/items/:id", itemDelete)
		v1.POST("/items/:id/images", itemImageCreate)
		v1.GET("/items/:id/images/:imageID", itemImageGet)

		v1.POST("/item-images", itemImageCreate)
		v1.DELETE("/item-images/:id", itemImageDelete)

		v1.GET("/item-categories", itemCategoriesGet)

		v1.GET("/offers/:id", offerGet)
		v1.GET("/offers", offerGetAll)
		v1.POST("/offers", offerCreate)
		v1.PUT("/offers/:id", offerUpdate)
		v1.DELETE("/offers/:id", offerDelete)
		v1.POST("/offers/:id/images", offerImageCreate)
		v1.GET("/offers/:id/images/:imageID", offerImageGet)

		v1.GET("/standing-orders/:id", standingOrderGet)
		v1.GET("/standing-orders", standingOrderGetAll)
		v1.POST("/standing-orders", standingOrderCreate)
		v1.PUT("/standing-orders/:id", standingOrderUpdate)
		v1.DELETE("/standing-orders/:id", standingOrderDelete)

		v1.GET("/orders/:id", standingOrderGet)
		v1.GET("/orders", standingOrderGetAll)
		v1.POST("/orders", standingOrderCreate)
		v1.PUT("/orders/:id", standingOrderUpdate)
		v1.DELETE("/orders/:id", standingOrderDelete)

	}
}
