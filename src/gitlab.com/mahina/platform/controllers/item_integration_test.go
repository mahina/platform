// +build integration all

package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strconv"
	"testing"

	"gitlab.com/mahina/platform/models"
	"gitlab.com/mahina/platform/util"

	"encoding/csv"

	"strings"

	"github.com/gin-gonic/gin"
	"github.com/guregu/null"
	"github.com/guregu/null/zero"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestItemCategoriesGet(t *testing.T) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("GET", version+"/item-categories", nil)
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code)

	categories := categoryEntry{}
	err := json.Unmarshal(resp.Body.Bytes(), &categories)
	assert.NoError(t, err, "Expected to unmarshal categories")
	assert.True(t, len(categories.Children) > 0, "Expected to find children categories")
}

func NOTTestItemCreate(t *testing.T) {
	tx, err := models.BeginTX()
	require.NoError(t, err, "Expected new transaction object")
	err = insertProduceItems(tx)
	require.NoError(t, err, "Expected inserted items")

	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("POST", version+"/items", strings.NewReader(`{"foo":"bar"}`))
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusBadRequest, resp.Code)

}

var itemJSON = `{
		"supplierUUID": "ffffffff-ffff-ffff-ffff-ffffffffffff",
		"uom": "pound",
		"status": "active",
		"title": "Green Beans",
		"description": "These are great beans",
		"category": "001.006",
		"pricing": ,
}
`

func NOTTestSearchItems(t *testing.T) {
	tx, err := models.BeginTX()
	require.NoError(t, err, "Expected new transaction object")
	err = insertProduceItems(tx)
	require.NoError(t, err, "Expected inserted items")

	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("GET", version+"/items", nil)
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code)

	// Clean up
	err = tx.Rollback()
	//err = tx.Commit()
	assert.NoError(t, err, "Expected transactional rollback to complete without error")
}

var imageRecordMap map[string]BingImageSearchResults
var imageCacheMap map[string][]byte

func insertProduceItems(tx *sqlx.Tx) error {
	// 359 items, 144 fruits, 40 onions, 170 vegetables, 1 herbs, 4 ornamentals
	// 122 organic items
	imageRecordMap = make(map[string]BingImageSearchResults)
	imageCacheMap = make(map[string][]byte)
	file, err := os.Open("test_fixtures/produce_price_list.csv")
	if err != nil {
		return err
	}
	defer file.Close()
	r := csv.NewReader(file)
	lines, err := r.ReadAll()

	count := 0
	for n, v := range lines {
		if n == 0 {
			continue
		}
		item := &models.Item{}
		item.Schema = "market"

		switch v[4] {
		case "per pound":
			item.UOM = models.UOMTypePound
		default:
			item.UOM = models.UOMTypeEach
		}
		switch v[0] {
		case "FRUITS":
			item.Category = "001.002"
		case "ONIONS AND POTATOES":
			item.Category = "001.006.008"
		case "VEGETABLES":
			item.Category = "001.006"
		case "HERBS":
			item.Category = "001.006.010"
		case "ORNAMENTALS":
			item.Category = "001.013"
		default:
			fmt.Println("Unknown category", v[0])
			continue
		}
		item.SupplierID = 1
		item.SupplierUUID = null.StringFrom("ffffffff-ffff-ffff-ffff-ffffffffffff")
		item.Title = null.StringFrom(strings.Title(strings.ToLower(v[1])))
		item.Description = zero.StringFrom(strings.ToLower(v[2]))
		item.Status = zero.StringFrom("active")

		err = item.Insert(tx)
		if err != nil {
			return err
		}
		if v[3] == "Y" {
			err = item.AddAttribute(tx, "organic")
			if err != nil {
				return err
			}
		}
		pricing := &models.ItemPricing{}
		pricing.Schema = "market"
		pricing.ItemID = item.ItemID
		pricing.ItemUUID = null.StringFrom(item.ItemUUID.String)
		pricing.QuantityMin = 1
		pricing.QuantityMax = 100

		pricing.Price, err = strconv.ParseFloat(v[5], 64)
		if err != nil {
			return err
		}
		err := pricing.Insert(tx)
		if err != nil {
			return err
		}

		searchTerm := item.Title.String
		imageBytes := imageCacheMap[searchTerm]
		if imageBytes == nil {
			BingAccessKey := os.Getenv("BING_ACCESS_KEY")
			if BingAccessKey == "" {
				BingAccessKey = "8b568700d51c45dbab80c4679f6bd426"
			}
			url := fmt.Sprintf("https://api.cognitive.microsoft.com/bing/v5.0/images/search?q=%s", url.QueryEscape(searchTerm))
			req, err := http.NewRequest("GET", url, nil)
			if err != nil {
				return err
			}
			client := &http.Client{}
			req.Header.Add("Content-Type", "multipart/form-data")
			req.Header.Add("Ocp-Apim-Subscription-Key", BingAccessKey)
			resp, err := client.Do(req)
			if err != nil {
				return err
			}
			defer resp.Body.Close()

			var record BingImageSearchResults
			if err := json.NewDecoder(resp.Body).Decode(&record); err != nil {
				return err
			}

			imageURL := record.Value[0].ContentURL
			imageBytes, err = util.GetImageBytesFromURL(imageURL)
			if err != nil {
				return err
			}
			imageCacheMap[searchTerm] = imageBytes
			imageRecordMap[searchTerm] = record
		}
		image := &models.ItemImage{}
		image.Schema = item.Schema
		image.Image = imageBytes
		image.Name = zero.StringFrom(searchTerm)
		image.ItemID = item.ItemID
		image.ItemUUID = item.ItemUUID
		record := imageRecordMap[searchTerm]
		image.Width = int64(record.Value[0].Width)
		image.Height = int64(record.Value[0].Height)
		image.Format = null.StringFrom("image/" + record.Value[0].EncodingFormat)
		err = image.Insert(tx)
		if err != nil {
			return err
		}
		count++
	}
	fmt.Println("Inserted", count, "items")
	return nil
}

type BingImageSearchResults struct {
	Type                         string `json:"_type"`
	DisplayRecipeSourcesBadges   bool   `json:"displayRecipeSourcesBadges"`
	DisplayShoppingSourcesBadges bool   `json:"displayShoppingSourcesBadges"`
	Instrumentation              struct {
		PageLoadPingURL string `json:"pageLoadPingUrl"`
	} `json:"instrumentation"`
	NextOffsetAddCount int `json:"nextOffsetAddCount"`
	PivotSuggestions   []struct {
		Pivot       string        `json:"pivot"`
		Suggestions []interface{} `json:"suggestions"`
	} `json:"pivotSuggestions"`
	QueryExpansions       interface{} `json:"queryExpansions"`
	SimilarTerms          interface{} `json:"similarTerms"`
	TotalEstimatedMatches int         `json:"totalEstimatedMatches"`
	Value                 []struct {
		AccentColor            string      `json:"accentColor"`
		ContentSize            string      `json:"contentSize"`
		ContentURL             string      `json:"contentUrl"`
		DatePublished          string      `json:"datePublished"`
		EncodingFormat         string      `json:"encodingFormat"`
		Height                 int         `json:"height"`
		HomePageURL            interface{} `json:"homePageUrl"`
		HostPageDisplayURL     string      `json:"hostPageDisplayUrl"`
		ImageID                string      `json:"imageId"`
		ImageInsightsToken     string      `json:"imageInsightsToken"`
		InsightsSourcesSummary struct {
			RecipeSourcesCount   int `json:"recipeSourcesCount"`
			ShoppingSourcesCount int `json:"shoppingSourcesCount"`
		} `json:"insightsSourcesSummary"`
		Name      string `json:"name"`
		Thumbnail struct {
			Height int `json:"height"`
			Width  int `json:"width"`
		} `json:"thumbnail"`
		ThumbnailURL string `json:"thumbnailUrl"`
		WebSearchURL string `json:"webSearchUrl"`
		Width        int    `json:"width"`
	} `json:"value"`
	WebSearchURL string `json:"webSearchUrl"`
}
