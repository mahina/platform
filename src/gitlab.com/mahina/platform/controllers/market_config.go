package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/mahina/platform/util"
)

// MarketConfig defines static configuration information about the market.
// This information comes from the mahina.toml file that is read by the config system.
type MarketConfig struct {
	Version        string   `json:"version"`
	Name           string   `json:"name"`
	AddressLine1   string   `json:"addressLine1"`
	AddressLine2   string   `json:"addressLine2,omitempty"`
	CityStateZip   string   `json:"cityStateZip"`
	CommissionRate float64  `json:"commissionRate"`
	DeliveryDays   []string `json:"deliveryDays"`
	ManagerName    string   `json:"managerName"`
	ManagerEmail   string   `json:"managerEmail"`
	ManagerPhone   string   `json:"managerPhone"`
	SupportEmail   string   `json:"supportEmail"`
}

const version = "0.1.0"

var config *MarketConfig

func marketConfigGet(c *gin.Context) {
	if config == nil {
		config = &MarketConfig{}
		config.Version = version
		config.Name = util.ConfigGetString("marketplace.full_name")
		config.AddressLine1 = util.ConfigGetString("marketplace.address_line1")
		config.AddressLine2 = util.ConfigGetString("marketplace.address_line2")
		config.CityStateZip = util.ConfigGetString("marketplace.address_city_state_zip")
		config.CommissionRate = util.ConfigGetFloat64("marketplace.marketplace_commission")
		config.DeliveryDays = util.ConfigGetStringSlice("marketplace.delivery_days")
		config.ManagerName = util.ConfigGetString("marketplace.manager_name")
		config.ManagerEmail = util.ConfigGetString("marketplace.manager_email")
		config.ManagerPhone = util.ConfigGetString("marketplace.phone")
		config.SupportEmail = util.ConfigGetString("marketplace.support_email")
	}
	c.IndentedJSON(http.StatusOK, config)
}
