package controllers

import (
	"fmt"
	"net/http"
	"strings"

	"io/ioutil"

	"github.com/gin-gonic/gin"
	"gitlab.com/mahina/platform/models"
	"gitlab.com/mahina/platform/util"
)

type UserRolesRecord struct {
	User  *models.User `json:"user"`
	Roles []string     `json:"roles"`
}

func isAdmin(roles []string) bool {
	for _, v := range roles {
		if v == "admin" {
			return true
		}
	}
	return false
}

func userGetAll(c *gin.Context) {
	user, roles, err := getUserAndRolesFromClaims(c)
	go InsertAuditAction(c, "entity", "access", "", "user", nil, nil)
	if err != nil {
		return
	}
	if user != "admin" && !isAdmin(roles) {
		ErrorMessage(c, http.StatusForbidden, "", nil)
	} else { // return all users
		users, err := models.GetAllUsers(-1, -1)
		if err != nil {
			ErrorMessage(c, http.StatusInternalServerError, "", err)
			return
		}
		c.IndentedJSON(http.StatusOK, users)
	}
}

func userGet(c *gin.Context) {
	var user *models.User
	var err error

	id := c.Param("id")
	if util.IsUUID(id) {
		user, err = models.GetUserByUUID(nil, id)
	} else {
		user, err = models.GetUserByUsername(nil, id)
	}
	uuid := ""
	if user != nil {
		uuid = user.UserUUID.String
	}
	go InsertAuditAction(c, "entity", "access", uuid, "user", nil, nil)
	if user == nil {
		ErrorMessage(c, http.StatusNotFound, "", nil)
		return

	}
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	c.IndentedJSON(http.StatusOK, user)
}

func userRolesGet(c *gin.Context) {
	record := &UserRolesRecord{}
	var err error

	id := c.Param("id")
	if util.IsUUID(id) {
		record.User, err = models.GetUserByUUID(nil, id)
	} else {
		record.User, err = models.GetUserByUsername(nil, id)
	}
	uuid := ""
	if record.User != nil {
		uuid = record.User.UserUUID.String
	}
	go InsertAuditAction(c, "entity", "access", uuid, "user", nil, nil)
	if record.User == nil {
		ErrorMessage(c, http.StatusNotFound, "", nil)
		return
	}
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	go InsertAuditAction(c, "entity", "access", uuid, "role", nil, nil)
	roles, err := models.GetAllRolesForUserUUIDOrName(nil, record.User.Username.String)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	for _, v := range roles {
		record.Roles = append(record.Roles, v.RoleID)
	}
	c.IndentedJSON(http.StatusOK, record)
}

type userOrganizationsRecord struct {
	User                *models.User         `json:"user"`
	OrganizationRecords []organizationRecord `json:"organizations"`
}

type organizationRecord struct {
	Organization *models.Organization `json:"organization"`
	Locations    []models.Location    `json:"locations"`
}

func userOrganizationsGet(c *gin.Context) {
	record := &userOrganizationsRecord{}
	var err error
	var orgs []models.UserOrganization

	id := c.Param("id")
	if util.IsUUID(id) {
		record.User, err = models.GetUserByUUID(nil, id)
	} else {
		record.User, err = models.GetUserByUsername(nil, id)
	}
	uuid := ""
	if record.User != nil {
		uuid = record.User.UserUUID.String
	}
	go InsertAuditAction(c, "entity", "access", uuid, "user", nil, nil)
	if record.User == nil {
		ErrorMessage(c, http.StatusNotFound, "", nil)
		return
	}
	if err != nil {
		goto internalError
	}
	orgs, err = models.GetAllOrganizationsForUserUUIDOrName(nil, record.User.Username.String)
	if err != nil {
		goto internalError
	}
	for _, v := range orgs {
		orgRecord := organizationRecord{}
		org, err := models.GetOrganizationByOrgID(nil, v.OrgID)
		if err != nil {
			goto internalError
		}
		orgRecord.Organization = org
		orgRecord.Locations, err = models.GetLocationsByOrgID(nil, v.OrgID)
		if err != nil {
			goto internalError
		}
		record.OrganizationRecords = append(record.OrganizationRecords, orgRecord)
	}
	c.IndentedJSON(http.StatusOK, record)
	return

internalError:

	ErrorMessage(c, http.StatusInternalServerError, "", err)
	return
}

func userCreate(c *gin.Context) {
	if c.Request.ContentLength == 0 {
		ErrorMessage(c, http.StatusBadRequest, "", nil)
		return
	}
	user := &models.User{}
	err := c.BindJSON(&user)
	if err != nil || !user.Valid() {
		ErrorMessage(c, http.StatusBadRequest, "", err)
		return
	}
	go InsertAuditAction(c, "entity", "insert", "", "user", nil, nil)
	err = user.Insert(nil)
	if err != nil {
		if strings.Contains(err.Error(), "user_username_key") {
			ErrorMessage(c, http.StatusBadRequest, "The supplied username is a duplicate", nil)
		} else {
			ErrorMessage(c, http.StatusInternalServerError, "", err)
		}
		return
	}
	go InsertAuditAction(c, "entity", "access", user.UserUUID.String, "user", nil, nil)
	c.IndentedJSON(http.StatusCreated, user)
}

func userUpdate(c *gin.Context) {
	var user *models.User
	var err error

	if c.Request.ContentLength == 0 {
		ErrorMessage(c, http.StatusBadRequest, "", nil)
		return
	}
	id := c.Param("id")
	if util.IsUUID(id) {
		user, err = models.GetUserByUUID(nil, id)
	} else {
		user, err = models.GetUserByUsername(nil, id)
	}
	if user == nil {
		ErrorMessage(c, http.StatusNotFound, "", nil)
		return
	}
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	updatedUser := &models.User{}
	err = c.BindJSON(&updatedUser)
	go InsertAuditAction(c, "entity", "update", user.UserUUID.String, "user", user.Bytes(), updatedUser.Bytes())
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "", err)
		return
	}
	if !updatedUser.Valid() {
		ErrorMessage(c, http.StatusBadRequest, "", nil)
		return
	}
	if user.UserUUID.String != updatedUser.UserUUID.String {
		ErrorMessage(c, http.StatusBadRequest, "user UUID mismatch", nil)
		return
	}
	err = updatedUser.Update(nil)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	c.IndentedJSON(http.StatusCreated, updatedUser)
}

func userDelete(c *gin.Context) {
	var user *models.User
	var err error

	id := c.Param("id")
	if util.IsUUID(id) {
		user, err = models.GetUserByUUID(nil, id)
	} else {
		user, err = models.GetUserByUsername(nil, id)
	}
	if user == nil {
		go InsertAuditAction(c, "entity", "delete", "", "user", nil, nil)
		ErrorMessage(c, http.StatusNotFound, "", nil)
		return
	}
	go InsertAuditAction(c, "entity", "delete", user.UserUUID.String, "user", nil, nil)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	err = user.Delete(nil)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	c.Status(http.StatusNoContent)
}

func userAuthenticate(c *gin.Context) {
	var user *models.User
	var err error

	id := c.Param("id")
	if util.IsUUID(id) {
		user, err = models.GetUserByUUID(nil, id)
	} else {
		user, err = models.GetUserByUsername(nil, id)
	}
	if err != nil {
		fmt.Println("ERROR:", err)
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	if user == nil {
		ErrorMessage(c, http.StatusUnauthorized, "Username not found", nil)
		return
	}
	password, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	user, err = models.VerifyCredential(nil, id, models.CredentialTypePassword, []byte(user.Username.String), []byte(password))
	if err != nil {
		ErrorMessage(c, http.StatusUnauthorized, "Password mismatch", nil)
		return
	}
	tokenString, err := generateSignedToken(user.UserUUID.String)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}

	cookie := &http.Cookie{}
	cookie.Path = "/"
	cookie.Name = "platform-token"
	cookie.Value = tokenString
	cookie.Secure = true
	//cookie.HttpOnly = true
	http.SetCookie(c.Writer, cookie)

	userOrganizationsGet(c)
}
