package controllers

import (
	"bytes"
	"net/http"

	"github.com/gin-contrib/static"

	"github.com/gin-gonic/gin"
	"gitlab.com/mahina/platform/defines"
	"gitlab.com/mahina/platform/util"

	"io/ioutil"

	log "github.com/Sirupsen/logrus"
)

// InitializeWebRoutes creates the routing paths for the web application
func InitializeWebRoutes(router *gin.Engine) {
	//router.LoadHTMLGlob(util.ConfigGetString(defines.WebAppLocationFlag) + "/templates/*")

	// public routes
	public := router.Group("/api")
	public.POST("/authenticate", authenticate)

	middle := static.Serve("/", static.LocalFile(util.ConfigGetString(defines.WebAppLocationFlag), true))
	router.Use(middle)
	//router.NoRoute(static.Serve("/index.html", static.LocalFile(util.ConfigGetString(defines.WebAppLocationFlag), true)))

	key, err := util.GetPrivateRSAKey()
	if err != nil {
		log.Warn("web service reports error getting private key", err)
		return
	}
	private := router.Group("/app")
	private.GET("/dashboard", serveDashboard)
	private.Use(Auth(key))
}

func authenticate(c *gin.Context) {
	username := c.PostForm("username")
	password := c.PostForm("password")
	if username == "" || password == "" {
		c.String(http.StatusBadRequest, "Missing fields")
		return
	}
	c.Params = append(c.Params, gin.Param{Key: "id", Value: username})
	c.Request.Body = ioutil.NopCloser(bytes.NewBuffer([]byte(password)))
	userAuthenticate(c)
}

func serveDashboard(c *gin.Context) {
	obj, _ := c.Get("auth-claims")
	claims := obj.(*AuthClaims)

	//c.IndentedJSON(http.StatusOK, token)
	//c.String(http.StatusOK, "Welcome to the dashboard")
	c.HTML(http.StatusOK, "dashboard.tmpl", gin.H{
		"name": claims.Id,
	})
}
