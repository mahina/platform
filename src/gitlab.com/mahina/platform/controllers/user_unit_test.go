// +build unit all

package controllers

import (
	"net/http"
	"strings"
	"testing"

	"net/http/httptest"

	"github.com/stretchr/testify/assert"
	"github.com/gin-gonic/gin"
)

func TestUserPostNullContent(t *testing.T) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("POST", version+"/users", nil)
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusBadRequest, resp.Code)
}

func TestUserPostEmptyContent(t *testing.T) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("POST", version+"/users", strings.NewReader(""))
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusBadRequest, resp.Code)
}

func TestUserPostInvalidContent(t *testing.T) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("POST", version+"/users", strings.NewReader(`{"foo":"bar"}`))
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusBadRequest, resp.Code)
}
