// +build integration all

package controllers

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/mahina/platform/models"

	"github.com/stretchr/testify/assert"
	"github.com/gin-gonic/gin"
)

func TestRegistrationAPIPostContent(t *testing.T) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("POST", version+"/registrations", strings.NewReader(regJSON))
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusCreated, resp.Code, string(resp.Body.Bytes()))

	reg, err := models.NewRegistration(resp.Body.Bytes())
	assert.NoError(t, err, "Failed converting response to new Registration object")
	assert.True(t, reg.Valid(), "Expected valid registration object")

	uuid := reg.RegUUID.String

	req, err = http.NewRequest("GET", version+"/registrations/"+uuid, nil)
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code)

	// Clean up
	req, _ = http.NewRequest("DELETE", version+"/registrations/"+reg.RegUUID.String, nil)
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusNoContent, resp.Code)
}

func TestRegistrationAPIPostInvalidContent(t *testing.T) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("POST", version+"/registrations", strings.NewReader(invalidRegJSON))
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusBadRequest, resp.Code, "Expected failure posting incomplete registration")
}

func TestRegistrationLookupNonExistent(t *testing.T) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("GET", version+"/registrations/clearlynotavalidid", nil)
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusBadRequest, resp.Code)

	req, _ = http.NewRequest("GET", version+"/registrations/794e1b0a-0f48-11e7-a620-532f2a44dd6a", nil)
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusNotFound, resp.Code)

}

const regJSON = `{
    "user": {
        "username": "vpotus@whitehouse.gov",
        "firstName": "Nelson",
        "lastName": "Rockefeller",
        "title": "Vice President",
        "email": "vpotus@whitehouse.gov"
    },
    "password": "이념의 암호",
    "passwordConfirm": "이념의 암호",
    "roles": [
        "buyer",
        "supplier"
    ],
    "organization": {
	    "legalName": "Kruger Industrial Smoothing",
	    "shortName": "Kruger",
	    "orgType": "corp"
    },
    "location": {
        "name": "uptown",
        "addressLine1": "123 Main Street",
        "addressLine2": "Unit 21",
        "locality": "Fairmount",
        "region": "New Hampshire",
        "postcode": "03042",
        "country": "USA",
        "phone": "603-555-1212"
    }
}`

// Missing Lastname
const invalidRegJSON = `{
    "user": {
        "username": "vpotus@whitehouse.gov",
        "firstName": "Nelson",
        "title": "Vice President",
        "email": "vpotus@whitehouse.gov"
    },
    "password": "이념의 암호",
    "passwordConfirm": "이념의 암호",
    "roles": [
        "buyer",
        "supplier"
    ],
    "organization": {
	    "legalName": "Kruger Industrial Smoothing",
	    "shortName": "Kruger",
	    "orgType": "corp"
    },
    "location": {
        "name": "uptown",
        "addressLine1": "123 Main Street",
        "addressLine2": "Unit 21",
        "locality": "Fairmount",
        "region": "New Hampshire",
        "postcode": "03042",
        "country": "USA",
        "phone": "603-555-1212"
    }
}`
