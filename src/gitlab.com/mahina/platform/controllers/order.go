package controllers

import (
	"net/http"

	"github.com/guregu/null"

	"gitlab.com/mahina/platform/models"
	"gitlab.com/mahina/platform/util"

	"github.com/guregu/null/zero"
	"github.com/lib/pq"
	"github.com/gin-gonic/gin"
	validator "gopkg.in/validator.v2"
)

type quantityByDOW struct {
	DOW      string  `json:"dayOfWeek"`
	Quantity float64 `json:"quantity"`
}

type V1StandingOrder struct {
	OrderUUID  string          `json:"orderUUID"`
	OfferUUID  string          `json:"offerUUID"`
	ItemUUID   string          `json:"itemUUID"`
	BuyerUUID  string          `json:"buyerUUID" validate:"nonzero"`
	FirstDate  util.ZeroDate   `json:"firstDate"`
	LastDate   util.NullDate   `json:"lastDate"`
	Quantities []quantityByDOW `json:"quantities" validate:"min=1"`
	Frequency  string          `json:"frequency"`
	Price      float64         `json:"price" validate:"nonzero"`
	Notes      string          `json:"notes"`
}

func (order *V1StandingOrder) Validate() error {
	return validator.Validate(order)
}

func (order *V1StandingOrder) GetQuantitiesArray() pq.Float64Array {
	array := pq.Float64Array{0, 0, 0, 0, 0, 0, 0}
	for _, v := range order.Quantities {
		var dow models.DeliveryDay
		dow = dow.DOW(v.DOW)
		array[int(dow)] = v.Quantity
	}
	return array
}

func standingOrderGet(c *gin.Context) {
	claims := ExtractClaims(c)
	if claims == nil {
		ErrorMessage(c, http.StatusForbidden, "unexpected empty claims", nil)
		return
	}
	id := c.Param("id")
	order, err := models.GetOrderByUUID(nil, id, claims.Market)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	if order == nil {
		ErrorMessage(c, http.StatusNotFound, "", nil)
		return
	}
	items, err := models.GetAllStandingOrderItemsForOrder(nil, id, claims.Market)
	if len(items) != 1 {
		ErrorMessage(c, http.StatusInternalServerError, "unexpected number of standing order items", nil)
		return
	}
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	orderItem := items[0]

	standingOrder := &V1StandingOrder{}
	standingOrder.OrderUUID = order.OrderUUID.String
	standingOrder.OfferUUID = orderItem.OfferUUID.String
	standingOrder.ItemUUID = orderItem.ItemUUID.String
	standingOrder.BuyerUUID = order.BuyerUUID.String
	standingOrder.FirstDate = order.DeliveryDate
	standingOrder.LastDate = order.LastDeliveryDate
	for n, v := range orderItem.QuantityByDOW {
		if v > 0.0 {
			day := models.DeliveryDay(n)
			qbd := quantityByDOW{day.String(), v}
			standingOrder.Quantities = append(standingOrder.Quantities, qbd)
		}
	}
	standingOrder.Frequency = orderItem.Frequency.String
	standingOrder.Price = orderItem.Price
	standingOrder.Notes = orderItem.Notes.String

	c.IndentedJSON(http.StatusOK, order)
}

func standingOrderGetAll(c *gin.Context) {
}

func standingOrderCreate(c *gin.Context) {
	claims := ExtractClaims(c)
	if claims == nil {
		ErrorMessage(c, http.StatusForbidden, "unexpected empty claims", nil)
		return
	}
	if c.Request.ContentLength == 0 {
		ErrorMessage(c, http.StatusBadRequest, "", nil)
		return
	}
	orderReq := &V1StandingOrder{}
	err := c.BindJSON(&orderReq)
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "unable to process submitted standing order", err)
		return
	}
	err = orderReq.Validate()
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "unable to process submitted standing order", err)
		return
	}
	tx, err := models.BeginTX()
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	orgs, err := models.GetAllOrganizationsForUserUUIDOrName(tx, claims.Id)
	if orgs == nil || err != nil {
		tx.Rollback()
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	orgID := orgs[0].OrgID
	org, err := models.GetOrganizationByOrgID(tx, orgID)
	if org == nil || err != nil {
		tx.Rollback()
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}

	buyer, err := models.GetOrganizationByUUID(tx, orderReq.BuyerUUID)
	if buyer == nil || err != nil {
		tx.Rollback()
		ErrorMessage(c, http.StatusInternalServerError, "unable to find buyer from supplied UUID", err)
		return
	}
	user, err := models.GetUserByUUID(tx, claims.Id)
	if user == nil || err != nil {
		tx.Rollback()
		ErrorMessage(c, http.StatusInternalServerError, "unable to find user from supplied token", err)
		return
	}

	// Create a Standing Order
	order := models.Order{}
	order.Schema = claims.Market
	order.BuyerID = buyer.OrgID
	order.BuyerUUID = null.StringFrom(orderReq.BuyerUUID)
	order.BuyerUserID = user.UserID
	order.BuyerUserUUID = null.StringFrom("ffffffff-ffff-ffff-ffff-ffffffffffff")
	order.Type = null.StringFrom("standing")
	order.DeliveryDate = orderReq.FirstDate
	order.LastDeliveryDate = orderReq.LastDate
	order.Notes = zero.StringFrom(orderReq.Notes)

	err = order.Insert(tx)
	if err != nil {
		tx.Rollback()
		ErrorMessage(c, http.StatusInternalServerError, "error adding order", err)
		return
	}

	offer, err := models.GetOfferByUUID(tx, orderReq.OfferUUID, order.Schema)
	if offer == nil || err != nil {
		tx.Rollback()
		ErrorMessage(c, http.StatusInternalServerError, "error locating offer", err)
		return
	}

	item, err := models.GetItemByUUID(tx, offer.ItemUUID.String, order.Schema)
	if item == nil || err != nil {
		tx.Rollback()
		ErrorMessage(c, http.StatusInternalServerError, "error locating item", err)
		return
	}

	// Create a Single Item for the Standing Order
	orderItem := models.StandingOrderItem{}
	orderItem.Schema = order.Schema
	orderItem.OrderID = order.OrderID
	orderItem.OrderUUID = order.OrderUUID
	orderItem.StandingOrderItemUUID = order.OrderUUID
	orderItem.SupplierID = item.SupplierID
	orderItem.SupplierUUID = item.SupplierUUID
	orderItem.ItemID = item.ItemID
	orderItem.ItemUUID = item.ItemUUID
	orderItem.OfferID = null.IntFrom(offer.OfferID)
	orderItem.OfferUUID = zero.StringFrom(offer.OfferUUID.String)
	orderItem.QuantityByDOW = orderReq.GetQuantitiesArray()
	orderItem.Frequency = null.StringFrom(orderReq.Frequency)
	orderItem.Price = orderReq.Price
	orderItem.Notes = zero.StringFrom(orderReq.Notes)

	err = orderItem.Insert(tx)
	if err != nil {
		tx.Rollback()
		ErrorMessage(c, http.StatusInternalServerError, "error adding item", err)
		return
	}

	err = tx.Commit()
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}

	c.IndentedJSON(http.StatusCreated, item)
}

func standingOrderUpdate(c *gin.Context) {
}

func standingOrderDelete(c *gin.Context) {
}
