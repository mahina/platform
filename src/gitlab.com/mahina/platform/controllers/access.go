package controllers

type RouteAccessEntry struct {
	Roles []string
	Owner bool
	Open  bool
}

var RouteAccessControlMap = map[string]RouteAccessEntry{
	V1 + "/registration":    RouteAccessEntry{Open: true},
	V1 + "/item_categories": RouteAccessEntry{Open: true},
}
