// +build unit all

package controllers

import (
	"bytes"
	"encoding/json"
	"net/http"
	"strings"
	"testing"

	"gitlab.com/mahina/platform/models"

	"net/http/httptest"

	"github.com/stretchr/testify/assert"
	"github.com/gin-gonic/gin"
)

func TestOrderPostNullContent(t *testing.T) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("POST", version+"/orders", nil)
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusBadRequest, resp.Code)
}

func TestOrderPostEmptyContent(t *testing.T) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("POST", version+"/orders", strings.NewReader(""))
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusBadRequest, resp.Code)
}

func TestOrderPostInvalidContent(t *testing.T) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("POST", version+"/orders", strings.NewReader(`{"foo":"bar"}`))
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusBadRequest, resp.Code)
}

func TestOrderQuantitiesUnmarshal(t *testing.T) {
	orderReq := &V1StandingOrder{}
	err := json.Unmarshal([]byte(standingOrderJSON), &orderReq)
	assert.NoError(t, err, "Expected to be able to unmarshal order")

	quantityArray := orderReq.GetQuantitiesArray()
	assert.Len(t, quantityArray, 7, "Expected quantity array of certain size")
	assert.Equal(t, 10.0, quantityArray[models.Tuesday], "Expected tuesday quantities to be a certain value")
	assert.Equal(t, 20.5, quantityArray[models.Friday], "Expected friday quantities to be a certain value")
	assert.Equal(t, 0.0, quantityArray[models.Thursday], "Expected thursday quantities to be a certain value")
}

func TestOrderQuantitiesMarshal(t *testing.T) {
	orderReq := &V1StandingOrder{}
	err := json.Unmarshal([]byte(standingOrderJSON), &orderReq)
	assert.NoError(t, err, "Expected to be able to unmarshal order")

	var jsonBytes bytes.Buffer
	b, err := json.Marshal(orderReq)
	err = json.Indent(&jsonBytes, b, "", "\t")
	assert.NoError(t, err, "Expected to be able to encode order")
	jsonString := string(jsonBytes.Bytes())
	assert.NotEmpty(t, jsonString, "Expected an encoded string")
	assert.True(t, strings.Contains(jsonString, "20.5"), "Expected to find value in string")
}

var standingOrderJSON = `{
	"orderUUID": "foo",
	"supplierUUID": "foo",
	"itemUUID": "foo",
	"offerUUID": "foo",
	"quantities": [
            {
                "dayOfWeek": "tuesday",
                "quantity": 10
            },
            {
                "dayOfWeek": "friday",
                "quantity": 20.5
            }		
	],
	"frequency": "weekly",
	"price": 1.25,
	"firstDate": "2018-01-01",
	"lastDate": "2019-01-01"
}`
