package controllers

import (
	"bytes"
	"encoding/json"
	"errors"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"net/http"
	"strconv"

	"gitlab.com/mahina/platform/models"
	"gitlab.com/mahina/platform/util"

	"github.com/gin-gonic/gin"
	"github.com/guregu/null"
	"github.com/guregu/null/zero"
)

type offerEmbedded struct {
	Quantity  float64        `json:"quantity" validate:"nonzero"`
	UOM       string         `json:"uom" validate:"nonzero"`
	FirstDate *util.NullDate `json:"firstDate" validate:"nonzero"`
	LastDate  *util.NullDate `json:"lastDate,omitempty"`
}

type pricingEmbedded struct {
	Min   float64 `json:"min" validate:"nonzero"`
	Max   float64 `json:"max" validate:"nonzero"`
	Price float64 `json:"price" validate:"nonzero"`
}

// SupplierOffer is the REST-specific object used by clients to manage supplier offers
type SupplierOffer struct {
	OfferID      string             `json:"offerID"`
	Status       string             `json:"status" validate:"nonzero"`
	Title        string             `json:"title" validate:"nonzero"`
	Description  string             `json:"description"`
	Attributes   []string           `json:"attributes"`
	SupplierID   string             `json:"supplierID,omitempty"`
	Supplier     string             `json:"supplier,omitempty"`
	ImageUUIDs   []string           `json:"imageUUIDs" validate:"nonzero"`
	Images       []models.ItemImage `json:"images,omitempty"`
	Category     string             `json:"category" validate:"nonzero"`
	WillSupply   *offerEmbedded     `json:"willSupply,omitempty"`
	CouldSupply  *offerEmbedded     `json:"couldSupply,omitempty"`
	Pricing      []pricingEmbedded  `json:"pricing" validate:"nonzero"`
	DeliveryDays []string           `json:"deliveryDays" validate:"nonzero"`
	Orders       []string           `json:"orders,omitempty"`
	Notes        string             `json:"notes"`
}

func (entity *SupplierOffer) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(entity)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

type initConfig struct {
	market       string
	userUUID     string
	expand       bool
	encodeImages bool
}

func (so *SupplierOffer) InitFromOffer(offer *models.SupplierOffer, config *initConfig) error {
	if config == nil {
		config = &initConfig{market: "market"}
	}
	item, err := models.GetItemByItemID(nil, offer.ItemID, config.market)
	if err != nil {
		return err
	}
	if item == nil {
		return errors.New("Offer item not found")
	}
	so.OfferID = offer.OfferUUID.String
	so.Status = offer.Status.String
	so.Title = item.Title.String
	so.Description = item.Description.String
	so.Attributes, err = models.GetAllItemAttributesByItemID(nil, item.ItemID, config.market)
	if err != nil {
		return err
	}
	so.ImageUUIDs, err = models.GetAllItemImageUUIDsByItem(nil, item.ItemID, config.market)
	if err != nil {
		return err
	}
	so.Category = item.Category
	offerEmbedded := &offerEmbedded{}
	offerEmbedded.Quantity = offer.Quantity
	offerEmbedded.UOM = item.UOM.String()
	offerEmbedded.FirstDate = offer.FirstDate
	if offer.LastDate != nil && !offer.LastDate.Time.Equal(models.Infinity) {
		offerEmbedded.LastDate = offer.LastDate
	}
	if offer.Type.String == "willsupply" {
		so.WillSupply = offerEmbedded
	} else {
		so.CouldSupply = offerEmbedded
	}
	pricing, err := models.GetAllOfferPricingForOffer(nil, offer.OfferID, config.market)
	so.Pricing = make([]pricingEmbedded, 0)
	for _, v := range pricing {
		embeddedPricing := pricingEmbedded{Min: v.QuantityMin, Max: v.QuantityMax, Price: v.Price}
		so.Pricing = append(so.Pricing, embeddedPricing)
	}
	if err != nil {
		return err
	}
	so.DeliveryDays = offer.GetDeliveryDays()
	so.Notes = offer.Notes.String
	if config.expand {
		so.Images, err = models.GetAllItemImagesByItemUUID(nil, offer.ItemUUID.String, config.encodeImages, config.market)
		if err != nil {
			return err
		}
		so.SupplierID = offer.SupplierUUID.String
		supplier, err := models.GetOrganizationByUUID(nil, so.SupplierID)
		if err != nil {
			return err
		}
		so.Supplier = supplier.LegalName.String
		access, err := models.UserInOrganization(nil, config.userUUID, offer.SupplierUUID.String)
		if err != nil {
			return err
		}
		if access {
			so.Orders, err = offer.GetAllStandingOrderUUIDs(nil, config.market)
			if err != nil {
				return err
			}
		}
	}
	return err
}

func offerGet(c *gin.Context) {
	claims := ExtractClaims(c)
	if claims == nil {
		ErrorMessage(c, http.StatusForbidden, "unexpected empty claims", nil)
		return
	}
	id := c.Param("id")
	val, expand := c.GetQuery("expand")
	if val == "false" || val == "f" {
		expand = false
	}
	encode := c.DefaultQuery("encodeImages", "false")
	offer, err := models.GetOfferByUUID(nil, id, claims.Market)
	go InsertAuditAction(c, claims.Market, "access", id, "offer", nil, nil)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "Error getting offer "+id, err)
		return
	}
	if offer == nil {
		ErrorMessage(c, http.StatusNotFound, "Offer not found", nil)
		return
	}
	config := &initConfig{market: claims.Market, expand: expand, encodeImages: (encode == "true"), userUUID: claims.Id}
	restOffer := &SupplierOffer{}
	err = restOffer.InitFromOffer(offer, config)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "Error getting offer "+id, err)
		return
	}
	c.IndentedJSON(http.StatusOK, restOffer)
}

func offerGetAll(c *gin.Context) {
	claims := ExtractClaims(c)
	if claims == nil {
		ErrorMessage(c, http.StatusForbidden, "unexpected empty claims", nil)
		return
	}
	limitQP := c.DefaultQuery("limit", "20")
	pageQP := c.DefaultQuery("page", "1")
	limit, err := strconv.Atoi(limitQP)
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "invalid limit query parameter", nil)
		return
	}
	page, err := strconv.Atoi(pageQP)
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "invalid page query parameter", nil)
		return
	}
	status := c.DefaultQuery("status", "active")
	orgID := c.Query("supplier")
	category := c.Query("category")
	searchTerm := c.Query("q")
	attributes := c.QueryArray("attributes")
	val, expand := c.GetQuery("expand")
	if val == "false" || val == "f" {
		expand = false
	}
	encode := c.DefaultQuery("encodeImages", "false")
	var totalCount uint64
	offers, err := models.GetAllOffers(nil, uint64(limit), uint64(page), claims.Market, status, orgID,
		category, searchTerm, attributes, expand, encode == "true", c.Query("orderBy"), &totalCount)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "Error getting offers", err)
		return
	}
	config := &initConfig{market: claims.Market, expand: expand, encodeImages: (encode == "true"), userUUID: claims.Id}
	restOffers := make([]*SupplierOffer, 0)
	for i := range offers {
		restOffer := &SupplierOffer{}
		err = restOffer.InitFromOffer(&offers[i], config)
		go InsertAuditAction(c, claims.Market, "access", offers[i].OfferUUID.String, "offer", nil, nil)
		if err != nil {
			ErrorMessage(c, http.StatusInternalServerError, "Error initing offers", err)
			return
		}
		restOffers = append(restOffers, restOffer)
	}
	c.Header("X-Total-Count", strconv.Itoa(int(totalCount)))
	c.IndentedJSON(http.StatusOK, restOffers)
}

func offerCreate(c *gin.Context) {
	claims := ExtractClaims(c)
	if claims == nil {
		ErrorMessage(c, http.StatusForbidden, "unexpected empty claims", nil)
		return
	}
	if c.Request.ContentLength == 0 {
		ErrorMessage(c, http.StatusBadRequest, "", nil)
		return
	}
	offer := &SupplierOffer{}
	err := c.BindJSON(&offer)
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "unable to process submitted offer", err)
		return
	}
	tx, err := models.BeginTX()
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	orgs, err := models.GetAllOrganizationsForUserUUIDOrName(tx, claims.Id)
	if orgs == nil || err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	orgID := orgs[0].OrgID // TODO: select the organization the user is current logged in under
	org, err := models.GetOrganizationByOrgID(tx, orgID)
	if org == nil || err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}

	// Create Item
	item := models.Item{}
	item.Schema = claims.Market
	item.SupplierID = orgs[0].OrgID
	item.SupplierUUID = org.OrgUUID
	if offer.WillSupply != nil {
		item.UOM, err = models.UOMTypeFromString(offer.WillSupply.UOM)
	} else {
		item.UOM, err = models.UOMTypeFromString(offer.CouldSupply.UOM)
	}
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "invalid UOM", nil)
		return
	}
	item.Status = zero.StringFrom(models.ItemStatusActive)
	item.Title = null.StringFrom(offer.Title)
	item.Description = zero.StringFrom(offer.Description)
	item.Category = offer.Category

	err = item.Insert(tx)
	if err != nil {
		tx.Rollback()
		c.AbortWithError(500, err)
		return
	}

	// Associate Item Images
	for _, v := range offer.ImageUUIDs {
		image, err := models.GetItemImageByUUID(tx, v, true, claims.Market)
		if err != nil {
			tx.Rollback()
			ErrorMessage(c, http.StatusInternalServerError, "", err)
			return
		}
		image.ItemID = item.ItemID
		image.ItemUUID = item.ItemUUID
		image.Schema = item.Schema
		err = image.Update(tx)
		if err != nil {
			tx.Rollback()
			ErrorMessage(c, http.StatusInternalServerError, "", err)
			return
		}
	}

	// Create Item Attributes and link
	for _, v := range offer.Attributes {
		err = item.AddAttribute(tx, v)
		if err != nil {
			tx.Rollback()
			ErrorMessage(c, http.StatusInternalServerError, "error adding attributes", err)
			return
		}
	}

	mo := &models.SupplierOffer{}
	mo.Schema = item.Schema
	mo.SupplierID = orgs[0].OrgID
	mo.SupplierUUID = org.OrgUUID
	mo.ItemID = item.ItemID
	mo.ItemUUID = item.ItemUUID
	mo.Status = zero.StringFrom(models.OfferStatusActive)
	embeddedOffer := offer.WillSupply
	if embeddedOffer == nil {
		embeddedOffer = offer.CouldSupply
	}
	mo.FirstDate = embeddedOffer.FirstDate
	if embeddedOffer.LastDate == nil || !embeddedOffer.LastDate.Valid {
		date := util.NullDateFrom(models.Infinity)
		mo.LastDate = &date
	} else {
		mo.LastDate = embeddedOffer.LastDate
	}
	mo.Quantity = embeddedOffer.Quantity
	mo.Notes = zero.StringFrom(offer.Notes)
	mo.SetDeliveryDays(offer.DeliveryDays)

	// Create Offer (will grow)
	if offer.WillSupply != nil {
		mo.Type = null.StringFrom(models.OfferTypeWillSupply)
	} else if offer.CouldSupply != nil { // Create Offer (could grow)
		mo.Type = null.StringFrom(models.OfferTypeCouldSupply)
	} else {
		ErrorMessage(c, http.StatusBadRequest, "missing offer details", nil)
		return
	}
	err = mo.Insert(tx)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "error creating willsupply offer", err)
		return
	}
	offer.OfferID = mo.OfferUUID.String

	// Create Offer Pricing
	for _, v := range offer.Pricing {
		pricing := &models.OfferPricing{}
		pricing.Schema = item.Schema
		pricing.OfferID = mo.OfferID
		pricing.OfferUUID = mo.OfferUUID
		pricing.QuantityMin = v.Min
		pricing.QuantityMax = v.Max
		pricing.Price = v.Price
		err = pricing.Insert(tx)
		if err != nil {
			tx.Rollback()
			ErrorMessage(c, http.StatusInternalServerError, "", err)
			return
		}
	}

	err = tx.Commit()
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	go InsertAuditAction(c, claims.Market, "insert", offer.OfferID, "offer", nil, nil)
	c.IndentedJSON(http.StatusCreated, offer)
}

func offerImageCreate(c *gin.Context) {
	claims := ExtractClaims(c)
	if claims == nil {
		ErrorMessage(c, http.StatusForbidden, "", nil)
		return
	}
	if c.Request.ContentLength == 0 {
		ErrorMessage(c, http.StatusBadRequest, "empty content", nil)
		return
	}

	file, header, err := c.Request.FormFile("file")
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "missing 'file' form-part", nil)
		return
	}

	img, format, err := image.Decode(file)
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "error decoding image file", err)
		return
	}

	image := &models.ItemImage{}
	image.Schema = claims.Market
	image.Format = null.StringFrom(format)
	image.Name = zero.StringFrom(header.Filename)
	offerUUID := c.Param("id")
	if offerUUID == "" {
		// If no offer uuid, assign temporarily to the default system item
		image.ItemID = 1
		image.ItemUUID = null.StringFrom("ffffffff-ffff-ffff-ffff-ffffffffffff")
	} else {
		offer, err := models.GetOfferByUUID(nil, offerUUID, claims.Market)
		if offer == nil {
			ErrorMessage(c, http.StatusNotFound, "offer not found", nil)
			return
		}
		if err != nil {
			ErrorMessage(c, http.StatusInternalServerError, "error getting offer", err)
			return
		}
		access, err := models.UserInOrganization(nil, claims.Id, offer.SupplierUUID.String)
		if err != nil {
			ErrorMessage(c, http.StatusInternalServerError, "Error testing offer access control ", err)
			return
		}
		if !access {
			ErrorMessage(c, http.StatusForbidden, "No access to offer", nil)
			return
		}
		item, err := models.GetItemByUUID(nil, offer.ItemUUID.String, claims.Market)
		if item == nil {
			ErrorMessage(c, http.StatusNotFound, "item not found", nil)
			return
		}
		if err != nil {
			ErrorMessage(c, http.StatusInternalServerError, "error getting item", err)
			return
		}
		image.ItemID = item.ItemID
		image.ItemUUID = item.ItemUUID
	}
	image.Width = int64(img.Bounds().Max.X)
	image.Height = int64(img.Bounds().Max.Y)

	buf := new(bytes.Buffer)
	switch format {
	case "png":
		err = png.Encode(buf, img)
	case "jpeg":
		fallthrough
	case "jpg":
		err = jpeg.Encode(buf, img, nil)
	case "gif":
		err = gif.Encode(buf, img, nil)
	default:
		err = errors.New("unknown image format: " + format)
	}
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "error encoding image file", err)
		return
	}
	image.Image = buf.Bytes()
	err = image.Insert(nil)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	c.IndentedJSON(http.StatusCreated, image)
}

func offerImageGet(c *gin.Context) {
	claims := ExtractClaims(c)
	if claims == nil {
		ErrorMessage(c, http.StatusForbidden, "", nil)
		return
	}
	id := c.Param("id")
	if !util.IsUUID(id) {
		ErrorMessage(c, http.StatusBadRequest, "invalid item UUID", nil)
		return
	}
	offer, err := models.GetOfferByUUID(nil, id, claims.Market)
	if offer == nil {
		ErrorMessage(c, http.StatusNotFound, "offer not found", nil)
		return
	}
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "error getting offer", err)
		return
	}
	imageID := c.Param("imageID")
	if !util.IsUUID(id) {
		ErrorMessage(c, http.StatusBadRequest, "invalid image UUID", nil)
		return
	}
	image, err := models.GetItemImageByUUID(nil, imageID, true, claims.Market)
	if image == nil {
		ErrorMessage(c, http.StatusNotFound, "image not found", nil)
		return
	}
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "error getting image", err)
		return
	}
	c.Data(http.StatusOK, image.Format.String, image.Image)
}

func offerUpdate(c *gin.Context) {
	claims := ExtractClaims(c)
	if claims == nil {
		ErrorMessage(c, http.StatusForbidden, "unexpected empty claims", nil)
		return
	}
	if c.Request.ContentLength == 0 {
		ErrorMessage(c, http.StatusBadRequest, "", nil)
		return
	}
	id := c.Param("id")
	offer, err := models.GetOfferByUUID(nil, id, claims.Market)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "Error getting offer "+id, err)
		return
	}
	if offer == nil {
		ErrorMessage(c, http.StatusNotFound, "Offer not found", nil)
		return
	}
	access, err := models.UserInOrganization(nil, claims.Id, offer.SupplierUUID.String)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "Error testing offer access control "+id, err)
		return
	}
	if !access {
		ErrorMessage(c, http.StatusForbidden, "No access to offer", nil)
		return
	}
	restOffer := &SupplierOffer{}
	err = c.BindJSON(&restOffer)
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "error initialing offer data", err)
		return
	}

	originalOffer := offer.Bytes()
	tx, err := models.BeginTX()
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}

	// Get the Item
	item, err := models.GetItemByItemID(tx, offer.ItemID, claims.Market)
	if restOffer.WillSupply != nil {
		item.UOM, err = models.UOMTypeFromString(restOffer.WillSupply.UOM)
	} else {
		item.UOM, err = models.UOMTypeFromString(restOffer.CouldSupply.UOM)
	}
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "incorrect UOM", nil)
		return
	}
	item.Title = null.StringFrom(restOffer.Title)
	item.Description = zero.StringFrom(restOffer.Description)
	item.Category = restOffer.Category

	err = item.Update(tx)
	if err != nil {
		tx.Rollback()
		ErrorMessage(c, http.StatusInternalServerError, "error updating item", err)
		return
	}

	// Associate Item Images
	err = item.UpdateItemImages(tx, restOffer.ImageUUIDs, claims.Market)
	if err != nil {
		tx.Rollback()
		ErrorMessage(c, http.StatusInternalServerError, "error updating images", err)
		return
	}

	// Create Item Attributes and link
	err = item.UpdateAttributes(tx, restOffer.Attributes, claims.Market)
	if err != nil {
		tx.Rollback()
		ErrorMessage(c, http.StatusInternalServerError, "error updating attributes", err)
		return
	}

	offer.Status = zero.StringFrom(restOffer.Status)
	offer.FirstDate = restOffer.WillSupply.FirstDate
	if restOffer.WillSupply.LastDate == nil || !restOffer.WillSupply.LastDate.Valid {
		date := util.NullDateFrom(models.Infinity)
		offer.LastDate = &date
	} else {
		offer.LastDate = restOffer.WillSupply.LastDate
	}
	offer.Quantity = restOffer.WillSupply.Quantity
	offer.Notes = zero.StringFrom(restOffer.Notes)
	offer.SetDeliveryDays(restOffer.DeliveryDays)
	err = offer.Update(tx)
	if err != nil {
		tx.Rollback()
		ErrorMessage(c, http.StatusInternalServerError, "error updating willsupply offer", err)
		return
	}
	go InsertAuditAction(c, claims.Market, "update", id, "offer", originalOffer, offer.Bytes())
	// Delete existing Pricing
	pricings, err := models.GetAllOfferPricingForOffer(tx, offer.OfferID, claims.Market)
	if err != nil {
		tx.Rollback()
		ErrorMessage(c, http.StatusInternalServerError, "error getting offer pricings", err)
		return
	}
	for i := range pricings {
		pricing, err := models.GetOfferPricingByPricingID(tx, pricings[i].PricingID, claims.Market)
		if err != nil {
			ErrorMessage(c, http.StatusInternalServerError, "error getting offer pricing", err)
			return
		}
		err = pricing.Delete(tx)
		if err != nil {
			tx.Rollback()
			ErrorMessage(c, http.StatusInternalServerError, "error deleting offer pricing", err)
			return
		}
	}

	// Recreate Offer Pricing
	for _, v := range restOffer.Pricing {
		pricing := &models.OfferPricing{}
		pricing.Schema = item.Schema
		pricing.OfferID = offer.OfferID
		pricing.OfferUUID = offer.OfferUUID
		pricing.QuantityMin = v.Min
		pricing.QuantityMax = v.Max
		pricing.Price = v.Price
		err = pricing.Insert(tx)
		if err != nil {
			tx.Rollback()
			ErrorMessage(c, http.StatusInternalServerError, "", err)
			return
		}
	}

	err = tx.Commit()
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "error commiting transaction", err)
		return
	}
	c.IndentedJSON(http.StatusOK, offer)
}

func offerDelete(c *gin.Context) {
	claims := ExtractClaims(c)
	if claims == nil {
		ErrorMessage(c, http.StatusForbidden, "unexpected empty claims", nil)
		return
	}
	id := c.Param("id")
	go InsertAuditAction(c, claims.Market, "delete", id, "offer", nil, nil)
	offer, err := models.GetOfferByUUID(nil, id, claims.Market)
	if err != nil || offer == nil {
		ErrorMessage(c, http.StatusInternalServerError, "error finding offer", err)
		return
	}
	access, err := models.UserInOrganization(nil, claims.Id, offer.SupplierUUID.String)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "Error validating offer access control "+id, err)
		return
	}
	if !access {
		ErrorMessage(c, http.StatusForbidden, "No access to offer", nil)
		return
	}
	err = offer.Delete(nil)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "error deleting offer", err)
		return
	}
	c.Status(http.StatusNoContent)
}
