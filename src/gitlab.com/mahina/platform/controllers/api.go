package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"github.com/gin-gonic/gin"
	"github.com/guregu/null/zero"
	diff "github.com/yudai/gojsondiff"
	"github.com/yudai/gojsondiff/formatter"
	"gitlab.com/mahina/platform/models"
	"gitlab.com/mahina/platform/util"
)

// JWT Middleware

type AuthClaims struct {
	jwt.StandardClaims
	Roles  []string `json:"roles"`
	Market string   `json:"market"`
}

func (claims *AuthClaims) isAdmin() bool {
	for _, v := range claims.Roles {
		if v == "admin" {
			return true
		}
	}
	return false
}

// ErrorWriter is middleware that handles writing JSON representations
// of errors. Typically an issue when other middleware is forced to .Abort
func ErrorWriter(c *gin.Context) {
	defer func(c *gin.Context) {
		if len(c.Errors) > 0 {
			c.IndentedJSON(-1, c.Errors)
		}
	}(c)
	c.Next()
}

// Auth is middleware that examines requests for valid JWT tokens, passed
// either in secure, http-only cookies or in the Authorization: Bearer http header
func Auth(key []byte) gin.HandlerFunc {
	return func(c *gin.Context) {
		accessControl, found := RouteAccessControlMap[c.Request.URL.Path]
		if found && accessControl.Open == true {
			return
		}
		var tokenValue string
		claims := &AuthClaims{}
		cookie, err := c.Request.Cookie("platform-token")
		if cookie == nil || err != nil {
			bearerToken, err := request.ParseFromRequestWithClaims(c.Request, request.OAuth2Extractor, claims, func(token *jwt.Token) (interface{}, error) {
				return key, nil
			})

			if bearerToken == nil || err != nil {

				fmt.Println(">>>>>>>>>>> Aborting with 401")
				c.AbortWithError(401, err)
				return
			}
			tokenValue = bearerToken.Raw
		} else {
			tokenValue = cookie.Value
		}

		//fmt.Println(">>>>>>Raw token: ", tokenValue)

		token, err := jwt.ParseWithClaims(tokenValue, claims, func(token *jwt.Token) (interface{}, error) {
			return key, nil
		})
		if !token.Valid {
			c.AbortWithError(401, errors.New("invalid token"))
			return
		}
		if err != nil {
			c.AbortWithError(401, err)
			return
		}
		c.Set("auth-claims", claims)
		if cookie != nil {
			tokenString, err := generateSignedToken(claims.Id)
			if err != nil {
				ErrorMessage(c, http.StatusInternalServerError, "error generating signed token", err)
				return
			}
			cookie := &http.Cookie{}
			cookie.Path = "/"
			cookie.Name = "platform-token"
			cookie.Value = tokenString
			cookie.Secure = true
			//cookie.HttpOnly = true
			http.SetCookie(c.Writer, cookie)
			fmt.Println(">>>>>> generated new cookie for", claims.Id)
		}
	}
}

func getAuthTokenFromContext(c *gin.Context) (token string, err error) {
	cookie, err := c.Request.Cookie("platform-token")
	if cookie == nil || err != nil {
		token = c.Request.Header.Get("Bearer")
	} else {
		token = cookie.Value
	}
	return
}

func generateSignedToken(uuid string) (string, error) {
	token, err := generateToken(uuid)
	if err != nil {
		return "", err
	}
	key, err := util.GetPrivateRSAKey()
	if err != nil {
		return "", err
	}
	ss, err := token.SignedString(key)
	return ss, err
}

func generateToken(uuid string) (*jwt.Token, error) {
	roles, err := models.GetAllRolesForUserUUIDOrName(nil, uuid)
	if err != nil {
		return nil, err
	}
	claim := &AuthClaims{}
	claim.Id = uuid
	claim.ExpiresAt = time.Now().Add(time.Hour * 1).Unix()
	for _, v := range roles {
		claim.Roles = append(claim.Roles, v.RoleID)
	}
	markets, err := models.GetMarketplacesForUser(nil, uuid)
	if err != nil {
		errorMessage := fmt.Sprintf("auth system reports error getting marketplaces for user: %s", err)
		return nil, errors.New(errorMessage)
	}
	if len(markets) == 0 {
		errorMessage := fmt.Sprintf("auth system reports finding no marketplaces for user: %s", uuid)
		return nil, errors.New(errorMessage)
	}
	if len(markets) > 1 {
		errorMessage := fmt.Sprintf("auth system reports finding more than one marketplaces for user: %s", uuid)
		return nil, errors.New(errorMessage)
	}
	claim.Market = markets[0]
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	return token, nil
}

func ExtractClaims(c *gin.Context) *AuthClaims {
	if gin.Mode() == gin.TestMode {
		claims := &AuthClaims{}
		claims.Id = "ffffffff-ffff-ffff-ffff-ffffffffffff"
		claims.Roles = []string{"admin"}
		claims.Market = "market"
		return claims
	}
	claims, found := c.Get("auth-claims")
	if !found {
		return nil
	}
	return claims.(*AuthClaims)
}

func getUserAndRolesFromClaims(c *gin.Context) (uuid string, roles []string, err error) {
	claims := ExtractClaims(c)
	if claims == nil {
		err = errors.New("no claims found in context")
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	uuid = claims.Id
	roles = claims.Roles
	return
}

// InsertAuditAction records an audit record
func InsertAuditAction(c *gin.Context, schema string, action string, entityUUID string, entityType string,
	entityBefore []byte, entityAfter []byte) (*models.Audit, error) {
	audit := models.Audit{}
	audit.Action = zero.StringFrom(action)
	if len(entityBefore) > 0 && len(entityAfter) > 0 {
		differ := diff.New()
		d, err := differ.Compare(entityBefore, entityAfter)
		if err != nil {
			return nil, err
		}
		var asciiJSON map[string]interface{}
		json.Unmarshal(entityBefore, &asciiJSON)
		config := formatter.AsciiFormatterConfig{
			ShowArrayIndex: true,
		}
		formatter := formatter.NewAsciiFormatter(asciiJSON, config)
		audit.Difference, err = formatter.Format(d)
	}
	audit.EntityUUID = entityUUID
	audit.EntityType = entityType
	if c != nil {
		audit.AuthToken, _ = getAuthTokenFromContext(c)
		claims := ExtractClaims(c)
		if claims != nil {
			audit.UserUUID = claims.Id
		}
		audit.IPAddress = c.ClientIP()
	}
	audit.Schema = schema
	err := audit.Insert(nil)
	return &audit, err
}
