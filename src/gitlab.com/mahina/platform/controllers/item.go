package controllers

import (
	"bytes"
	"errors"
	"image"
	"image/jpeg"
	"image/png"
	"net/http"
	"strconv"

	"gitlab.com/mahina/platform/models"
	"gitlab.com/mahina/platform/util"

	"image/gif"

	"strings"

	"github.com/guregu/null"
	"github.com/guregu/null/zero"
	"github.com/jmoiron/sqlx"
	"github.com/gin-gonic/gin"
)

type SupplierItem struct {
	Title       string            `json:"title"`
	Description string            `json:"description"`
	Attributes  []string          `json:"attributes"`
	ImageIDs    []string          `json:"imageIDs"`
	Category    string            `json:"category"`
	Pricing     []pricingEmbedded `json:"pricing"`
	Notes       string            `json:"notes,omitempty"`
}

func itemGet(c *gin.Context) {
	expand := c.Query("expand")
	if strings.HasPrefix(strings.ToLower(expand), "t") {
		itemGetExpanded(c)
		return
	}
	claims := ExtractClaims(c)
	if claims == nil {
		ErrorMessage(c, http.StatusForbidden, "", nil)
		return
	}
	id := c.Param("id")
	if !util.IsUUID(id) {
		ErrorMessage(c, http.StatusBadRequest, "invalid item UUID", nil)
		return
	}
	item, err := models.GetItemByUUID(nil, id, claims.Market)
	if item == nil {
		ErrorMessage(c, http.StatusNotFound, "item not found", nil)
		return
	}
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "error getting item", err)
		return
	}
	c.IndentedJSON(http.StatusOK, item)
}

func itemGetExpanded(c *gin.Context) {
	claims := ExtractClaims(c)
	if claims == nil {
		ErrorMessage(c, http.StatusForbidden, "", nil)
		return
	}
	id := c.Param("id")
	if !util.IsUUID(id) {
		ErrorMessage(c, http.StatusBadRequest, "invalid item UUID", nil)
	}
	item, err := models.GetItemExpanded(nil, id, claims.Market)
	if item == nil {
		ErrorMessage(c, http.StatusNotFound, "item not found", nil)
		return
	}
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "error getting item", err)
		return
	}
	c.IndentedJSON(http.StatusOK, item)
}

func itemGetAll(c *gin.Context) {
	claims := ExtractClaims(c)
	if claims == nil {
		ErrorMessage(c, http.StatusForbidden, "unexpected empty claims", nil)
		return
	}
	limitQP := c.DefaultQuery("limit", "20")
	pageQP := c.DefaultQuery("page", "1")
	limit, err := strconv.Atoi(limitQP)
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "invalid limit query parameter", nil)
		return
	}
	page, err := strconv.Atoi(pageQP)
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "invalid page query parameter", nil)
		return
	}
	status := c.DefaultQuery("status", "active")
	orgID := c.Query("supplier")
	category := c.Query("category")
	searchTerm := c.Query("q")
	attributes := c.QueryArray("attributes")
	expand := c.Query("expand") == "true"
	encodeImage := c.DefaultQuery("encodeImages", "false")
	var totalCount uint64
	results, err := models.GetAllItems(nil, uint64(limit), uint64(page), claims.Market, status,
		orgID, category, searchTerm, attributes, expand, encodeImage == "true", &totalCount)

	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "error getting items", err)
		return
	}
	c.Header("X-Total-Count", strconv.Itoa(int(totalCount)))
	c.IndentedJSON(http.StatusOK, results)
}

func itemCreate(c *gin.Context) {
	claims := ExtractClaims(c)
	if claims == nil {
		ErrorMessage(c, http.StatusForbidden, "unexpected empty claims", nil)
		return
	}
	if c.Request.ContentLength == 0 {
		ErrorMessage(c, http.StatusBadRequest, "", nil)
		return
	}
	item := &models.Item{}
	err := c.BindJSON(&item)
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "unable to process submitted offer", nil)
		return
	}
	err = item.Validate()
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "unable to process submitted offer", nil)
		return
	}

	// TODO
	ErrorMessage(c, http.StatusNotImplemented, "Item management not implemented", nil)
}

func itemUpdate(c *gin.Context) {
	// TODO
	ErrorMessage(c, http.StatusNotImplemented, "Item management not implemented", nil)
}

func itemDelete(c *gin.Context) {
	// TODO
	ErrorMessage(c, http.StatusNotImplemented, "Item management not implemented", nil)
}

func itemImageCreate(c *gin.Context) {
	claims := ExtractClaims(c)
	if claims == nil {
		ErrorMessage(c, http.StatusForbidden, "", nil)
		return
	}
	if c.Request.ContentLength == 0 {
		ErrorMessage(c, http.StatusBadRequest, "empty content", nil)
		return
	}

	file, header, err := c.Request.FormFile("file")
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "missing 'file' form-part", nil)
		return
	}

	img, format, err := image.Decode(file)
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "error decoding image file", err)
		return
	}

	image := &models.ItemImage{}
	image.Schema = claims.Market
	image.Format = null.StringFrom(format)
	image.Name = zero.StringFrom(header.Filename)
	itemUUID := c.Param("id")
	if itemUUID == "" {
		// If no item uuid, assign temporarily to the default system item
		image.ItemID = 1
		image.ItemUUID = null.StringFrom("ffffffff-ffff-ffff-ffff-ffffffffffff")
	} else {
		item, err := models.GetItemByUUID(nil, itemUUID, claims.Market)
		if item == nil {
			ErrorMessage(c, http.StatusNotFound, "item not found", nil)
			return
		}
		if err != nil {
			ErrorMessage(c, http.StatusInternalServerError, "error getting item", err)
			return
		}
		image.ItemID = item.ItemID
		image.ItemUUID = item.ItemUUID
	}
	image.Width = int64(img.Bounds().Max.X)
	image.Height = int64(img.Bounds().Max.Y)

	buf := new(bytes.Buffer)
	switch format {
	case "png":
		err = png.Encode(buf, img)
	case "jpeg":
		fallthrough
	case "jpg":
		err = jpeg.Encode(buf, img, nil)
	case "gif":
		err = gif.Encode(buf, img, nil)
	default:
		err = errors.New("unknown image format: " + format)
	}
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "error encoding image file", err)
		return
	}
	image.Image = buf.Bytes()
	err = image.Insert(nil)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	c.IndentedJSON(http.StatusCreated, image)
}

func itemImageGet(c *gin.Context) {
	claims := ExtractClaims(c)
	if claims == nil {
		ErrorMessage(c, http.StatusForbidden, "", nil)
		return
	}
	id := c.Param("id")
	if !util.IsUUID(id) {
		ErrorMessage(c, http.StatusBadRequest, "invalid item UUID", nil)
		return
	}
	item, err := models.GetItemByUUID(nil, id, claims.Market)
	if item == nil {
		ErrorMessage(c, http.StatusNotFound, "item not found", nil)
		return
	}
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "error getting item", err)
		return
	}
	imageID := c.Param("imageID")
	if !util.IsUUID(id) {
		ErrorMessage(c, http.StatusBadRequest, "invalid image UUID", nil)
		return
	}
	image, err := models.GetItemImageByUUID(nil, imageID, true, claims.Market)
	if image == nil {
		ErrorMessage(c, http.StatusNotFound, "image not found", nil)
		return
	}
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "error getting image", err)
		return
	}
	c.Data(http.StatusOK, image.Format.String, image.Image)
}

func itemImageDelete(c *gin.Context) {
	claims := ExtractClaims(c)
	if claims == nil {
		ErrorMessage(c, http.StatusForbidden, "", nil)
		return
	}

	var image *models.ItemImage
	var err error

	id := c.Param("id")
	if id == "" {
		ErrorMessage(c, http.StatusBadRequest, "item image id not specified", nil)
		return
	}
	image, err = models.GetItemImageByUUID(nil, id, false, claims.Market)
	if image == nil {
		ErrorMessage(c, http.StatusNotFound, "item image not found", nil)
		return
	}
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "error getting item image", err)
		return
	}
	err = image.Delete(nil)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "error deleting item image", err)
		return
	}
	c.Status(http.StatusNoContent)
}

type categoryEntry struct {
	ID       string           `json:"-" db:"category_id"`
	Label    string           `json:"label" db:"label"`
	Path     string           `json:"path" db:"node_path"`
	Parent   zero.String      `json:"-" db:"parent_id"`
	Children []*categoryEntry `json:"children,omitempty"`
}

func itemCategoriesGet(c *gin.Context) {
	var db *sqlx.DB
	entries := []categoryEntry{}
	db, err := models.GetDB()
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	err = db.Select(&entries, "SELECT label, category_id,parent_id, node_path FROM market.item_category ORDER BY node_path")
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	root := categoryEntry{}
	m := make(map[string]*categoryEntry)
	for n, v := range entries {
		if root.ID == "" {
			root = entries[n]
			m[v.ID] = &root
			continue
		}
		m[v.ID] = &entries[n]
		parent := m[v.Parent.String]
		parent.Children = append(parent.Children, &entries[n])
	}
	c.IndentedJSON(http.StatusOK, root)
}
