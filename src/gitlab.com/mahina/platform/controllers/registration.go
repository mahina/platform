package controllers

import (
	"fmt"
	"net/http"

	"strings"

	"github.com/guregu/null"
	"github.com/guregu/null/zero"
	"gitlab.com/mahina/platform/models"
	"gitlab.com/mahina/platform/util"
	"github.com/gin-gonic/gin"
)

func registrationGet(c *gin.Context) {
	var reg *models.Registration
	var err error

	id := c.Param("id")
	if !util.IsUUID(id) {
		ErrorMessage(c, http.StatusBadRequest, "", nil)
		return
	}
	reg = &models.Registration{RegUUID: null.StringFrom(id)}
	err = reg.Get(nil)
	if !reg.Valid() {
		ErrorMessage(c, http.StatusNotFound, "", nil)
		return
	}
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	c.IndentedJSON(http.StatusOK, reg)
}

func registrationCreate(c *gin.Context) {
	if c.Request.ContentLength == 0 {
		ErrorMessage(c, http.StatusBadRequest, "", nil)
		return
	}
	regDoc := &models.RegistrationDoc{}
	err := c.BindJSON(&regDoc)
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "", err)
		return
	}
	err = regDoc.EncryptPassword()
	if err != nil {
		ErrorMessage(c, http.StatusBadRequest, "unable to process password", nil)
		return
	}
	regDoc.IPAddress = c.ClientIP()
	if !regDoc.Valid() {
		ErrorMessage(c, http.StatusBadRequest, "required fields are missing in the registration document posted", nil)
		return
	}
	reg, err := regDoc.Accept()
	if err != nil {
		if strings.Contains(err.Error(), "registration_username_key") {
			ErrorMessage(c, http.StatusBadRequest, "the supplied username has already been processed", nil)
		} else {
			ErrorMessage(c, http.StatusInternalServerError, "", err)
		}
		return
	}
	c.IndentedJSON(http.StatusCreated, reg)
}

func registrationDelete(c *gin.Context) {
	var reg *models.Registration
	var err error

	id := c.Param("id")
	if !util.IsUUID(id) {
		ErrorMessage(c, http.StatusBadRequest, "the requested ID must be a UUID", nil)
		return
	}
	reg = &models.Registration{}
	reg.RegUUID = null.StringFrom(id)
	err = reg.Get(nil)
	if !reg.Valid() {
		ErrorMessage(c, http.StatusInternalServerError, "registration not valid", nil)
		return
	}
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	err = reg.Delete(nil)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	c.Status(http.StatusNoContent)
}

func registrationProcess(c *gin.Context) {
	var reg *models.Registration
	var err error

	id := c.Param("id")
	if !util.IsUUID(id) {
		ErrorMessage(c, http.StatusBadRequest, "the requested ID must be a UUID", nil)
		return
	}
	reg = &models.Registration{}
	reg.RegUUID = null.StringFrom(id)
	err = reg.Get(nil)
	if !reg.Valid() {
		ErrorMessage(c, http.StatusInternalServerError, "invalid registration", nil)
		return
	}
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	regDoc, err := models.NewRegistrationDoc([]byte(reg.Extended.String()))
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	err = regDoc.Process()
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	reg.Status = zero.StringFrom(models.RegStateApproved)
	err = reg.Update(nil)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	c.IndentedJSON(http.StatusOK, reg)
}

func registrationConfirmEmail(c *gin.Context) {
	var reg *models.Registration
	var err error

	id := c.Param("id")
	if !util.IsUUID(id) {
		ErrorMessage(c, http.StatusBadRequest, "the requested ID must be a UUID", nil)
		return
	}
	reg = &models.Registration{}
	reg.RegUUID = null.StringFrom(id)
	err = reg.Get(nil)
	if !reg.Valid() {
		ErrorMessage(c, http.StatusInternalServerError, "invalid registration", nil)
		return
	}
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}

	key, found := c.GetQuery("key")
	if !found {
		ErrorMessage(c, http.StatusBadRequest, "missing confirm email key", err)
		return
	}
	hash := fmt.Sprintf("%x", reg.Hash())
	if hash != key {
		ErrorMessage(c, http.StatusBadRequest, "the requested email confirm key failed", err)
		return
	}
	reg.Status = zero.StringFrom(models.RegStateEmailConfirmed)
	err = reg.Update(nil)
	if err != nil {
		ErrorMessage(c, http.StatusInternalServerError, "", err)
		return
	}
	c.Redirect(http.StatusMovedPermanently, "/email-confirm.html?reg="+reg.RegUUID.String)
}
