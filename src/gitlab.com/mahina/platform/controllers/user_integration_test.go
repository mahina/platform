// +build integration all

package controllers

import (
	"fmt"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"gitlab.com/mahina/platform/models"

	"github.com/gin-gonic/gin"
	"github.com/guregu/null"
	"github.com/stretchr/testify/assert"
)

func TestUserAPIPostContent(t *testing.T) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("POST", version+"/users", strings.NewReader(getUserJSON()))
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusCreated, resp.Code, "Expected to be able to create new user, error: "+resp.Body.String())

	user, err := models.NewUser(resp.Body.Bytes())
	assert.NoError(t, err, "Failed converting response to new User object")
	assert.True(t, user.Valid(), "Expected valid User object")

	// Clean up
	req, _ = http.NewRequest("DELETE", version+"/users/"+user.UserUUID.String, nil)
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusNoContent, resp.Code)
}

func TestUserAPIUpdate(t *testing.T) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)

	req, _ := http.NewRequest("POST", version+"/users", strings.NewReader(getUserJSON()))
	resp := httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusCreated, resp.Code, "Expected to be able to create new user, error: "+resp.Body.String())

	user, err := models.NewUser(resp.Body.Bytes())
	assert.NoError(t, err, "Failed converting response to new User object")
	assert.True(t, user.Valid(), "Expected valid User object")

	user.JobTitle = null.StringFrom("Former President")
	err = user.Update(nil)
	assert.NoError(t, err, "Failed updating user")
	assert.Equal(t, "Former President", user.JobTitle.String, "Failed to update user")

	// Clean up
	req, _ = http.NewRequest("DELETE", version+"/users/"+user.UserUUID.String, nil)
	resp = httptest.NewRecorder()
	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusNoContent, resp.Code)
}

func startTestRouter() (*gin.Engine, string) {
	version := "/v1"
	gin.SetMode(gin.TestMode)
	router := gin.New()
	InitializeAPIRoutes(router)
	rand.Seed(time.Now().UTC().UnixNano())
	port := rand.Intn(0xD8EF) + 10000
	go router.Run(fmt.Sprintf(":%d", port))
	return router, fmt.Sprintf("http://localhost:%d/%s", port, version)
}

func init() {
	models.GetDBParamsFromEnvironment()
}

func getUserJSON() string {
	temporaryName := fmt.Sprintf("gerald.ford%d", time.Now().Nanosecond())
	return fmt.Sprintf(`{
		"status": "active",
		"username": "%s",
		"firstName": "Gerald",
		"lastName": "Ford",
		"jobTitle": "President",
		"email": "potus@whitehouse.gov",
		"birthDate": "1913-07-14"
	}`, temporaryName)

}
