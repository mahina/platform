// +build integration all

package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInsertUserRole(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")
	assert.True(t, user.Exists(nil), "Expected to find user")

	userRole, err := NewUserRole(user.UserID, "supplier")
	err = userRole.Insert(nil)
	assert.NoError(t, err, "Expected clean insert of UserRole")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
}

func TestInsertUserRoles(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")
	assert.True(t, user.Exists(nil), "Expected to find user")

	userRole, err := NewUserRole(user.UserID, "supplier")
	err = userRole.Insert(nil)
	assert.NoError(t, err, "Expected clean insert of UserRole")
	userRole, err = NewUserRole(user.UserID, "buyer")
	err = userRole.Insert(nil)
	assert.NoError(t, err, "Expected clean insert of UserRole")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
}

func TestInsertInvalidUserRole(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")
	assert.True(t, user.Exists(nil), "Expected to find user")

	userRole, err := NewUserRole(user.UserID, "thisisdefinitelynotarole")
	err = userRole.Insert(nil)
	assert.Error(t, err, "Expected error inserting with bad role id")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
}

func TestDeleteUserRole(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")
	assert.True(t, user.Exists(nil), "Expected to find user")

	userRole, err := NewUserRole(user.UserID, "admin")
	err = userRole.Insert(nil)
	assert.NoError(t, err, "Expected clean insert of UserRole")

	err = userRole.Delete(nil)
	assert.NoError(t, err, "Expected clean delete of UserRole")

	userRole, err = NewUserRole(user.UserID, "admin")
	err = userRole.Insert(nil)
	assert.NoError(t, err, "Expected clean insert of UserRole after deleting prior user role association")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
}

func TestGetUserRoles(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")
	assert.True(t, user.Exists(nil), "Expected to find user")

	userRole, err := NewUserRole(user.UserID, "supplier")
	err = userRole.Insert(nil)
	assert.NoError(t, err, "Expected clean insert of UserRole")
	userRole, err = NewUserRole(user.UserID, "buyer")
	err = userRole.Insert(nil)
	assert.NoError(t, err, "Expected clean insert of UserRole")

	roles, err := GetAllRolesForUserUUIDOrName(nil, user.UserUUID.String)
	assert.NoError(t, err, "Expected no error getting roles for user")
	assert.Equal(t, 2, len(roles), "Expected to find two roles")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
}
