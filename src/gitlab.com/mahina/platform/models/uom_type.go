package models

import (
	"database/sql/driver"
	"errors"
)

// UOMType is the 'Unit Of Measure' enum type from schema 'public'.
type UOMType uint16

const (
	UOMTypeEach       = UOMType(1)
	UOMTypeBunch      = UOMType(2)
	UOMTypeOunce      = UOMType(3)
	UOMTypePound      = UOMType(4)
	UOMTypeGram       = UOMType(5)
	UOMTypeKilogram   = UOMType(6)
	UOMTypeBushel     = UOMType(7)
	UOMTypePeck       = UOMType(8)
	UOMTypeFluidOunce = UOMType(9)
	UOMTypeGallon     = UOMType(10)
	UOMTypeLiter      = UOMType(11)
)

func (ct UOMType) String() string {
	var enumVal string

	switch ct {
	case UOMTypeEach:
		enumVal = "each"
	case UOMTypeBunch:
		enumVal = "bunch"
	case UOMTypeOunce:
		enumVal = "ounce"
	case UOMTypePound:
		enumVal = "pound"
	case UOMTypeGram:
		enumVal = "gram"
	case UOMTypeKilogram:
		enumVal = "kilogram"
	case UOMTypeBushel:
		enumVal = "bushel"
	case UOMTypeFluidOunce:
		enumVal = "fluid ounce"
	case UOMTypeGallon:
		enumVal = "gallon"
	case UOMTypeLiter:
		enumVal = "liter"
	}

	return enumVal
}

// MarshalText marshals UOMType into text.
func (ct UOMType) MarshalText() ([]byte, error) {
	return []byte(ct.String()), nil
}

// UnmarshalText unmarshals UOMType from text.
func (ct *UOMType) UnmarshalText(text []byte) error {
	switch string(text) {
	case "each":
		*ct = UOMTypeEach
	case "bunch":
		*ct = UOMTypeBunch
	case "ounce":
		*ct = UOMTypeOunce
	case "pound":
		*ct = UOMTypePound
	case "gram":
		*ct = UOMTypeGram
	case "kilogram":
		*ct = UOMTypeKilogram
	case "bushel":
		*ct = UOMTypeBushel
	case "peck":
		*ct = UOMTypePeck
	case "fluid ounce":
		*ct = UOMTypeFluidOunce
	case "gallon":
		*ct = UOMTypeGallon
	case "liter":
		*ct = UOMTypeLiter
	default:
		return errors.New("invalid UOMType")
	}

	return nil
}

// Value satisfies the sql/driver.Valuer interface for UOMType.
func (ct UOMType) Value() (driver.Value, error) {
	return ct.String(), nil
}

// Scan satisfies the database/sql.Scanner interface for UOMType.
func (ct *UOMType) Scan(src interface{}) error {
	buf, ok := src.([]byte)
	if !ok {
		return errors.New("invalid UOMType")
	}

	return ct.UnmarshalText(buf)
}

func UOMTypeFromString(uom string) (UOMType, error) {
	var uomType UOMType
	err := uomType.Scan([]byte(uom))
	return uomType, err
}
