// +build integration all

package models

import (
	"testing"

	"fmt"

	"io/ioutil"

	"github.com/guregu/null/zero"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

func TestInsertItemImage(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	_, err = insertImage(tx, item)
	if err != nil {
		t.Fatal("Unable to insert item image", err)
	}

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestUpdateItemImage(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	itemImage, err := insertImage(tx, item)
	if err != nil {
		t.Fatal("Unable to insert item image", err)
	}

	name := itemImage.Name
	itemImage.Name = zero.StringFrom("What I cannot create, I do not understand.")
	err = item.Update(tx)
	if err != nil {
		t.Fatal("Unable to update item", err)
	}
	assert.NotEqual(t, name, itemImage.Name)

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestDeleteItemImage(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	itemImage, err := insertImage(tx, item)
	if err != nil {
		t.Fatal("Unable to insert item image", err)
	}

	err = itemImage.Delete(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	assert.False(t, itemImage.Exists(tx), "Expected to not be able to find deleted item image")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindUnknownItemImage(t *testing.T) {
	nonExistentUUID := "fdd93e34-c87d-11e6-804b-06f1521cd849"
	item, _ := GetItemImageByUUID(nil, nonExistentUUID, true, schema)
	assert.Nil(t, item, "Expected to not find a item image")
}

func TestFindItemImageByID(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	itemImage, err := insertImage(tx, item)
	if err != nil {
		t.Fatal("Unable to insert item image", err)
	}

	itemImage, err = GetItemImageByImageID(tx, itemImage.ImageID, true, schema)
	if item == nil {
		t.Fatal("Unexpected null item")
	}
	assert.Nil(t, err, "Error in find by ID")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindItemImageByUUID(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	itemImage, err := insertImage(tx, item)
	if err != nil {
		t.Fatal("Unable to insert item image", err)
	}

	itemImage, err = GetItemImageByUUID(tx, itemImage.ImageUUID.String, true, schema)
	if item == nil {
		t.Fatal("Unexpected null item")
	}
	assert.Nil(t, err, "Error in find by UUID")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindItemImagesForItem(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	for i := 0; i < 3; i++ {
		_, err := insertImage(tx, item)
		if err != nil {
			t.Fatal("Unable to insert item image", err)
		}
	}

	itemImages, err := GetAllItemImageIDsByItem(tx, item.ItemID, schema)
	if err != nil {
		t.Fatal("Unable to find item images", err)
	}
	assert.Len(t, itemImages, 3, "Expected a certain number of item images to be found")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

const itemImageJSON = `{
	"itemUUID": "%s",
	"name": "test image",
	"description": "farm link hawaii logo",
	"width": 200,
	"height": 200,
	"format": "image/png"
}`

// Helper Funcs
func insertItem(tx *sqlx.Tx) (*Item, error) {
	org, err := NewOrganization([]byte(orgJSON))
	err = org.Insert(tx)
	if err != nil {
		return nil, err
	}
	json := fmt.Sprintf(itemJSON, org.OrgUUID.String)
	item, err := NewItem(tx, []byte(json), schema)
	if err != nil {
		return nil, err
	}
	err = item.Insert(tx)
	if err != nil {
		return nil, err
	}
	return item, err
}

func insertImage(tx *sqlx.Tx, item *Item) (*ItemImage, error) {
	imageBytes, err := ioutil.ReadFile("./test_fixtures/flh-logo.png")
	if err != nil {
		return nil, err
	}
	json := fmt.Sprintf(itemImageJSON, item.ItemUUID.String)
	itemImage, err := NewItemImage(tx, []byte(json), schema)
	if err != nil {
		return nil, err
	}
	itemImage.Image = imageBytes
	err = itemImage.Insert(tx)
	if err != nil {
		return nil, err
	}
	return itemImage, err
}
