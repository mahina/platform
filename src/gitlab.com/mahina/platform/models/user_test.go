// +build integration all

package models

import (
	"testing"

	"github.com/guregu/null"
	"github.com/stretchr/testify/assert"
)

func TestInsertUser(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")
	assert.True(t, user.Exists(nil), "Expected to find user")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
}

func TestInsertUserWithoutBirthDate(t *testing.T) {
	user, err := NewUser([]byte(userJSONWithoutBirthDate))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")
	assert.True(t, user.Exists(nil), "Expected to find user")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
}

func TestDuplicateUserNameInsert(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")

	err = user.Insert(nil)
	assert.NotNil(t, err, "Expected error when inserting duplicate key")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
}

func TestUpdateUser(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0)

	firstName := user.FirstName
	user.FirstName = null.StringFrom("Jerry")
	err = user.Update(nil)
	if err != nil {
		t.Fatal("Unable to update person", err)
	}
	assert.NotEqual(t, firstName, user.FirstName)

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
}

func TestFindUnknownUser(t *testing.T) {
	nonExistentUUID := "fdd93e34-c87d-11e6-804b-06f1521cd849"
	user, _ := GetUserByUUID(nil, nonExistentUUID)
	assert.Nil(t, user, "Expected to not find a user")
}

func TestFindUserByUUID(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0)

	user, err = GetUserByUUID(nil, user.UserUUID.String)
	if user == nil {
		t.Fatal("Unexpected null user")
	}
	assert.Nil(t, err, "Error in find by UUID")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
}

func TestGetUsers(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}

	users, err := GetAllUsers(-1, -1)
	assert.NoError(t, err, "Expected to be able to find all users")
	assert.NotEmpty(t, users, "Expected result from find all users to be not empty")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
}

const userJSON = `{
	"status": "active",
	"username": "gerald.ford",
	"firstName": "Gerald",
	"lastName": "Ford",
	"title": "President",
	"email": "potus@whitehouse.gov",
	"birthDate": "1913-07-14"
}`

const userJSONWithoutBirthDate = `{
	"status": "active",
	"username": "gerald.ford",
	"firstName": "Gerald",
	"lastName": "Ford",
	"title": "President",
	"email": "potus@whitehouse.gov"
}`

func init() {
	GetDBParamsFromEnvironment()
}
