package models

import (
	"bytes"
	"database/sql"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/mahina/platform/util"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/mahina/platform/defines"
)

var _db *sqlx.DB

var Infinity = time.Date(2500, time.January, 1, 0, 0, 0, 0, time.UTC)
var NegativeInfinity = time.Date(1500, time.January, 1, 0, 0, 0, 0, time.UTC)
var NilTime time.Time

// GetDB returns a DB handle; designed to be long-lived
func GetDB() (*sqlx.DB, error) {
	var err error
	if _db == nil {
		user := util.ConfigGetString(defines.DBUserFlag)
		if user == "" {
			return nil, errors.New("no dbuser flag configured")
		}
		password := util.ConfigGetString(defines.DBUserPasswordFlag)
		if password == "" {
			return nil, errors.New("no dbpassword flag configured")
		}
		database := util.ConfigGetString(defines.DBDatabaseFlag)
		if database == "" {
			return nil, errors.New("no database flag configured")
		}
		host := util.ConfigGetString(defines.DBHostFlag)
		if host == "" {
			return nil, errors.New("no host flag configured")
		}
		port := util.ConfigGetInt(defines.DBPortFlag)
		if port == 0 {
			return nil, errors.New("no db port flag configured")
		}
		connect := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=require", host, port, user, password, database)
		fmt.Println("About to get db via", connect)
		_db, err = sqlx.Connect("postgres", connect)
		if err != nil {
			fmt.Println("Error getting db", err)
			_db = nil
		}
	}
	return _db, err
}

// BeginTX initiates and returns a DB transaction object
func BeginTX() (*sqlx.Tx, error) {
	db, err := GetDB()
	if err != nil {
		return nil, err
	}
	tx, err := db.Beginx()
	if err != nil {
		return nil, err
	}
	return tx, nil
}

// SetSchemaSearchPath sets the schema search path.
func SetSchemaSearchPath(schema string) (err error) {
	db, err := GetDB()
	if err != nil {
		return
	}
	if schema == "" {
		schema = "market"
	}
	_, err = db.Exec("SET search_path TO $1,public;", schema)
	return
}

// SQLQueryDebugString formats an sql query inlining its arguments
// The purpose is debug only - do not send this to the database!
// Sending this to the DB is unsafe and un-performant.
func SQLQueryDebugString(query string, args ...interface{}) string {
	var buffer bytes.Buffer
	nArgs := len(args)
	// Break the string by question marks, iterate over its parts and for each
	// question mark - append an argument and format the argument according to
	// it's type, taking into consideration NULL values and quoting strings.
	//fmt.Println("QUERY:::::", query)
	//fmt.Println("SPLIT:::::", strings.Split(query, "$"))
	for i, part := range strings.Split(query, "?") {
		buffer.WriteString(part)
		if i < nArgs {
			switch a := args[i].(type) {
			case int64:
				buffer.WriteString(fmt.Sprintf("%d", a))
			case bool:
				buffer.WriteString(fmt.Sprintf("%t", a))
			case sql.NullBool:
				if a.Valid {
					buffer.WriteString(fmt.Sprintf("%t", a.Bool))
				} else {
					buffer.WriteString("NULL")
				}
			case sql.NullInt64:
				if a.Valid {
					buffer.WriteString(fmt.Sprintf("%d", a.Int64))
				} else {
					buffer.WriteString("NULL")
				}
			case sql.NullString:
				if a.Valid {
					buffer.WriteString(fmt.Sprintf("%q", a.String))
				} else {
					buffer.WriteString("NULL")
				}
			case sql.NullFloat64:
				if a.Valid {
					buffer.WriteString(fmt.Sprintf("%f", a.Float64))
				} else {
					buffer.WriteString("NULL")
				}
			default:
				buffer.WriteString(fmt.Sprintf("%q", a))
			}
		}
	}
	return buffer.String()
}

// GetDBParamsFromEnvironment loads database-specific configuration values
// from the environment (useful for tests).
func GetDBParamsFromEnvironment() {
	dbuser := os.Getenv("TEST_DATABASE_USER")
	if dbuser == "" {
		dbuser = "mahina"
	}
	dbpassword := os.Getenv("TEST_DATABASE_PASSWORD")
	if dbpassword == "" {
		dbpassword = "m@hInA"
	}
	database := os.Getenv("TEST_DATABASE")
	if database == "" {
		database = "market"
	}
	dbhost := os.Getenv("TEST_DATABASE_HOST")
	if dbhost == "" {
		dbhost = "localhost"
		//dbhost = "staging.mahina.org"
	}
	dbport := os.Getenv("TEST_DATABASE_PORT")
	if dbport == "" {
		dbport = "5432"
	}
	util.ConfigSet(defines.DBUserFlag, dbuser)
	util.ConfigSet(defines.DBUserPasswordFlag, dbpassword)
	util.ConfigSet(defines.DBDatabaseFlag, database)
	util.ConfigSet(defines.DBHostFlag, dbhost)
	port, _ := strconv.Atoi(dbport)
	util.ConfigSet(defines.DBPortFlag, port)
}
