package models

import (
	"bytes"
	"encoding/json"
	"errors"
	"time"

	validator "gopkg.in/validator.v2"

	"gitlab.com/mahina/platform/util"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"fmt"

	"strings"

	sq "github.com/Masterminds/squirrel"
	"github.com/guregu/null"
	"github.com/guregu/null/zero"
	"github.com/jmoiron/sqlx/types"
)

// Constants for the template entity
const (
	// ItemStatusActive: the item is active in the marketplace
	ItemStatusActive = "active"
	// ItemStatusCreated: the item has been created, but not viewable by the marketplace
	ItemStatusCreated = "created"
	// ItemStatusDeleted: the item has been marked deleted
	ItemStatusDeleted = "deleted"
)

// Item defines a template entity that's schema-based. This module is intended to be duplicated.
type Item struct {
	ItemID       int64          `db:"item_id" json:"-"` // serial ID's are internal and not shared with external systems
	ItemUUID     null.String    `db:"item_uuid" json:"itemUUID"`
	ProductCode  zero.String    `db:"product_code" json:"productCode,omitempty"`
	SupplierID   int64          `db:"supplier_id" json:"-"`
	SupplierUUID null.String    `db:"supplier_uuid" json:"supplierUUID" validate:"nonzero"`
	UOM          UOMType        `db:"uom" json:"uom" validate:"nonzero"`
	Status       zero.String    `db:"status" json:"status,omitempty" validate:"nonzero"`
	Title        null.String    `db:"title" json:"title" validate:"nonzero"`
	Description  zero.String    `db:"description" json:"description,omitempty"`
	Category     string         `db:"category" json:"category" validate:"nonzero"`
	Thumbnail    []byte         `db:"thumbnail" json:"thumbnail,omitempty"`
	Created      zero.Time      `db:"created" json:"created"`
	Modified     zero.Time      `db:"modified" json:"modified"`
	Extended     types.JSONText `db:"extended" json:"extended,omitempty"`

	Schema string `db:"-" json:"-"`
}

// NewItem creates a Item struct from a passed JSON string. No database
// events occur from this call.
func NewItem(tx *sqlx.Tx, jsonStr []byte, schema string) (*Item, error) {
	entity := Item{}
	entity.Schema = schema
	err := json.Unmarshal(jsonStr, &entity)
	if err != nil {
		return nil, err
	}
	if entity.SupplierID == 0 {
		supplier, err := GetOrganizationByUUID(tx, entity.SupplierUUID.String)
		if err != nil {
			return nil, err
		}
		entity.SupplierID = supplier.OrgID
	}
	return &entity, nil
}

func (item *Item) Validate() error {
	return validator.Validate(item)
}

// Get performs a SELECT, using ItemID as the lookup key. The Item object
// is updated with the attributes of the found Item.
func (entity *Item) Get(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	if entity.ItemID <= 0 {
		err = errors.New("invalid item_id")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.item WHERE item_id = $1", entity.Schema)
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(entity, statement, entity.ItemID)
	} else {
		err = tx.Get(entity, statement, entity.ItemID)
	}
	return
}

// Valid returns the validity of the Item object
func (entity *Item) Valid() bool {
	valid := entity.ItemID > 0 &&
		util.IsUUID(entity.ItemUUID.String) &&
		entity.Schema != ""
	return valid
}

// Exists tests for the existence of a Item record corresponding to the ItemID value
func (entity *Item) Exists(tx *sqlx.Tx) bool {
	if entity.ItemID <= 0 {
		return false
	}
	err := entity.Get(tx)
	if err != nil {
		return false
	}
	return entity.ItemID > 0
}

const itemInsertStatement = `INSERT INTO %s.item (` +
	`product_code, supplier_id, supplier_uuid, uom, status, title, description, category, thumbnail, extended` +
	`) VALUES (` +
	`$1, $2, $3, $4, $5, $6, $7, $8, $9, $10` +
	`) RETURNING item_id`

// Insert performs an SQL INSERT statement using the Item struct fields as attributes. The struct
// will contain all DB-generated defaults, IDs, etc.
func (entity *Item) Insert(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(itemInsertStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	row := tx.QueryRowx(statement, entity.ProductCode, entity.SupplierID, entity.SupplierUUID,
		entity.UOM, entity.Status, entity.Title, entity.Description, entity.Category,
		entity.Thumbnail, entity.Extended)
	err = row.Err()
	if err != nil {
		goto end
	}
	err = row.Scan(&entity.ItemID)
	if err != nil {
		goto end
	}
	if entity.ItemID == 0 {
		err = errors.New("Primary key unexpectedly nil")
	}
end:
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

const itemUpdateStatement = `UPDATE %s.item SET (` +
	`product_code, supplier_id, supplier_uuid, uom, status, title, description, category, thumbnail, modified, extended` +
	`) = ( ` +
	`$1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11` +
	`) WHERE item_id = $12`

// Update performs an SQL UPDATE operation using the Item struct fields as attributes.
func (entity *Item) Update(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(itemUpdateStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.ProductCode, entity.SupplierID, entity.SupplierUUID,
		entity.UOM, entity.Status, entity.Title, entity.Description, entity.Category,
		entity.Thumbnail, time.Now(), entity.Extended,
		entity.ItemID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation, using ItemID as the lookup key.
func (entity *Item) Delete(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("DELETE FROM %s.item WHERE item_id = $1", entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.ItemID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		entity.ItemID = 0
	}
	return
}

func (entity *Item) AddAttribute(tx *sqlx.Tx, attribute string) error {
	records, err := GetAllItemAttributesByItemID(tx, entity.ItemID, entity.Schema)
	if err != nil {
		return err
	}
	for _, v := range records {
		if v == attribute {
			return nil
		}
	}
	record := &ItemAttribute{}
	record.ItemID = entity.ItemID
	record.ItemUUID = entity.ItemUUID
	record.Attribute = null.StringFrom(attribute)
	record.Schema = entity.Schema
	err = record.Insert(tx)
	return err
}

func (entity *Item) UpdateAttributes(tx *sqlx.Tx, attributes []string, schema string) error {
	if schema == "" {
		return errors.New("schema not specified")
	}
	originalItemAttrs, err := GetAllItemAttributesByItemID(tx, entity.ItemID, schema)
	if err != nil {
		return err
	}
	for i := range originalItemAttrs {
		attr := &ItemAttribute{Schema: schema, ItemID: entity.ItemID,
			ItemUUID: entity.ItemUUID, Attribute: null.StringFrom(originalItemAttrs[i])}
		err := attr.Delete(tx)
		if err != nil {
			return err
		}
	}
	for i := range attributes {
		attr := &ItemAttribute{Schema: schema, ItemID: entity.ItemID,
			ItemUUID: entity.ItemUUID, Attribute: null.StringFrom(attributes[i])}
		err := attr.Insert(tx)
		if err != nil {
			return err
		}
	}
	return nil
}

func (entity *Item) UpdateItemImages(tx *sqlx.Tx, imageUUIDs []string, schema string) error {
	if schema == "" {
		return errors.New("schema not specified")
	}
	originalItemImages, err := GetAllItemImageUUIDsByItem(tx, entity.ItemID, schema)
	if err != nil {
		return err
	}
	for _, v := range imageUUIDs {
		image, err := GetItemImageByUUID(tx, v, false, schema)
		if err != nil {
			return err
		}
		image.ItemID = entity.ItemID
		image.ItemUUID = entity.ItemUUID
		image.Schema = entity.Schema
		err = image.Update(tx)
		if err != nil {
			return err
		}
	}
	for i := range originalItemImages {
		found := false
		for n := range imageUUIDs {
			if originalItemImages[i] == imageUUIDs[n] {
				found = true
			}
		}
		if !found {
			image, err := GetItemImageByUUID(tx, originalItemImages[i], false, schema)
			if err != nil {
				return err
			}
			err = image.Delete(tx)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (entity *Item) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(entity)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetItemByItemID performs a SELECT, using the passed ItemID as the lookup key
// Returns nil if the entity does not exist--err may/may not be nil in that case.
func GetItemByItemID(tx *sqlx.Tx, entityID int64, schema string) (entity *Item, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.item WHERE item_id = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(Item)
	if tx == nil {
		err = db.Get(entity, statement, entityID)
	} else {
		err = tx.Get(entity, statement, entityID)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	return
}

// GetItemByUUID performs a SELECT, using the passed UUID as the lookup key.
// Returns nil if the entity does not exist—-err may/may not be nil in that case.
func GetItemByUUID(tx *sqlx.Tx, uuid string, schema string) (entity *Item, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.item WHERE item_uuid = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(Item)
	if tx == nil {
		err = db.Get(entity, statement, uuid)
	} else {
		err = tx.Get(entity, statement, uuid)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	return
}

// ItemExpanded is an Expanded Item for Search Results
type ItemExpanded struct {
	Item
	Images     []ItemImage   `json:"images,omitempty"`
	Pricing    []ItemPricing `json:"pricing,omitempty"`
	Attributes []string      `json:"attributes,omitempty"`
}

func (entity *ItemExpanded) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(entity)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetAllItems finds items. Returns a slice of Items up to limit records.
// If a pointer is assigned to totalCount, the total number of records for the
// search criteria is written to that address. The page parameter is one-based (not zero-based)
func GetAllItems(tx *sqlx.Tx, limit uint64, page uint64, schema string, status string,
	organizationUUID string, category string, searchTerm string, attributes []string,
	expanded bool, encodeImage bool, totalCount *uint64) ([]ItemExpanded, error) {

	var err error
	if schema == "" {
		err = errors.New("schema not specified")
		return nil, err
	}
	items := make([]ItemExpanded, 0)
	table := fmt.Sprintf("%s.item i", schema)

	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	q := psql.Select("i.item_id", "i.item_uuid", "i.product_code", "i.supplier_id", "i.supplier_uuid",
		"i.uom", "i.status", "i.title", "i.description", "i.category", "i.thumbnail", "i.created",
		"i.modified", "i.extended")
	q = q.From(table)
	if limit < 1 {
		limit = 20
	} else if limit > 100 {
		limit = 100
	}
	q = q.Limit(limit)
	if page < 1 {
		page = 1
	}
	offset := ((page - 1) * (limit))
	q = q.Offset(offset)
	if status != "" {
		q = q.Where("status = ?", status)
	}
	if organizationUUID != "" {
		q = q.Where("supplier_uuid = ?", organizationUUID)
	}
	if category != "" {
		q = q.Where("category <@ ?", category)
	}
	if searchTerm != "" {
		q = q.Where("to_tsvector('english', title) @@ to_tsquery('english', ?)", searchTerm)
	}
	if len(attributes) > 0 {
		// TODO: Loop through attributes
		q = q.Join("market.item_attribute a USING (item_id)")
		q = q.Where("a.attribute = ?", attributes[0])
	}
	q = q.OrderBy("i.created DESC")
	statement, args, _ := q.ToSql()
	//fmt.Println(SQLQueryDebugString(statement, args))
	var countStatement string
	if totalCount != nil {
		builder := q.PlaceholderFormat(sq.Question)
		builder = builder.Offset(0)
		builder = builder.Limit(0xFFFFFFFF)
		statement, _, _ := builder.ToSql()
		countStatement = SQLQueryDebugString(statement, args...)
		countStatement = strings.Replace(countStatement, "\"", "'", -1)
	}
	if tx == nil {
		db, err := GetDB()
		if err != nil {
			return nil, err
		}
		err = db.Select(&items, statement, args...)
		if err == nil && totalCount != nil {
			err = db.QueryRow("select count_estimate($1)", countStatement).Scan(totalCount)
			if err != nil {
				return nil, err
			}
		}
	} else {
		err = tx.Select(&items, statement, args...)
		if err == nil && totalCount != nil {
			err = tx.QueryRow("select count_estimate($1)", countStatement).Scan(totalCount)
			if err != nil {
				return nil, err
			}
		}
	}
	if err == nil {
		for i, v := range items {
			items[i].Schema = schema
			if expanded {
				items[i].Images, err = GetAllItemImagesByItemUUID(tx, v.ItemUUID.String, encodeImage, schema)
				if err != nil {
					return nil, err
				}
				items[i].Pricing, err = GetAllItemPricingByItemUUID(tx, v.ItemUUID.String, schema)
				if err != nil {
					return nil, err
				}
				items[i].Attributes, err = GetAllItemAttributesByItemUUID(tx, v.ItemUUID.String, schema)
				if err != nil {
					return nil, err
				}
			}
		}
	}
	return items, err
}

// GetItemExpanded loads the Expanded struct with children objects (images, etc).
// Returns nil if the entity does not exist—-err may/may not be nil in that case.
func GetItemExpanded(tx *sqlx.Tx, uuid string, schema string) (entity *ItemExpanded, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.item WHERE %s.item.item_uuid = $1", schema, schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(ItemExpanded)
	if tx == nil {
		err = db.Get(entity, statement, uuid)
	} else {
		err = tx.Get(entity, statement, uuid)
	}
	if err != nil {
		return
	}
	statement = fmt.Sprintf("SELECT * FROM %s.item_image WHERE item_uuid = $1", schema)
	if tx == nil {
		err = db.Select(&entity.Images, statement, uuid)
	} else {
		err = tx.Select(&entity.Images, statement, uuid)
	}
	if err != nil {
		return
	}
	statement = fmt.Sprintf("SELECT * FROM %s.item_pricing WHERE item_uuid = $1 ORDER BY quantity_min", schema)
	if tx == nil {
		err = db.Select(&entity.Pricing, statement, uuid)
	} else {
		err = tx.Select(&entity.Pricing, statement, uuid)
	}
	if err != nil {
		return
	}
	statement = fmt.Sprintf("SELECT attribute FROM %s.item_attribute WHERE item_uuid = $1 ORDER BY attribute", schema)
	if tx == nil {
		err = db.Select(&entity.Attributes, statement, uuid)
	} else {
		err = tx.Select(&entity.Attributes, statement, uuid)
	}
	return
}
