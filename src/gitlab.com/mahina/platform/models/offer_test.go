// +build integration all

package models

import (
	"testing"

	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

func TestInsertOffer(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	offer, err := insertOffer(tx, item)
	if err != nil {
		t.Fatal("Unable to insert offer", err)
	}
	assert.Equal(t, 20.5, offer.Quantity, "Expected certain value following insert")
	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestUpdateOffer(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	offer, err := insertOffer(tx, item)
	if err != nil {
		t.Fatal("Unable to insert offer", err)
	}

	quantity := offer.Quantity
	offer.Quantity = 4
	err = offer.Update(tx)
	if err != nil {
		t.Fatal("Unable to update item", err)
	}
	assert.NotEqual(t, quantity, offer.Quantity)

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestDeleteOffer(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	offer, err := insertOffer(tx, item)
	if err != nil {
		t.Fatal("Unable to insert offer", err)
	}

	err = offer.Delete(tx)
	if err != nil {
		t.Fatal("Unable to delete offer", err)
	}
	assert.False(t, offer.Exists(tx), "Expected to not be able to find deleted offer")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindUnknownOffer(t *testing.T) {
	nonExistentUUID := "fdd93e34-c87d-11e6-804b-06f1521cd849"
	item, _ := GetOfferByUUID(nil, nonExistentUUID, schema)
	assert.Nil(t, item, "Expected to not find a offer")
}

func TestFindOfferByID(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	offer, err := insertOffer(tx, item)
	if err != nil {
		t.Fatal("Unable to insert offer", err)
	}

	offer, err = GetOfferByOfferID(tx, offer.OfferID, schema)
	if offer == nil {
		t.Fatal("Unexpected null item")
	}
	assert.Nil(t, err, "Error in find by ID")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindOfferByUUID(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	offer, err := insertOffer(tx, item)
	if err != nil {
		t.Fatal("Unable to insert offer", err)
	}

	offer, err = GetOfferByUUID(tx, offer.OfferUUID.String, schema)
	if item == nil {
		t.Fatal("Unexpected null item")
	}
	assert.Nil(t, err, "Error in find by UUID")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindOffersByOrgID(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	for i := 0; i < 3; i++ {
		_, err = insertOffer(tx, item)
		if err != nil {
			t.Fatal("Unable to insert offer", err)
		}
	}
	org, err := GetOrganizationByExactLegalName(tx, "Kruger Industrial Smoothing")
	assert.NoError(t, err, "Expected to be able to find organization")
	assert.NotNil(t, org, "Expected to be able to find organization")

	var totalCount uint64
	offers, err := GetAllOffers(tx, 20, 1, "market", "", org.OrgUUID.String, "", "", nil, true, false, "", &totalCount)
	assert.NoError(t, err, "Expected to be able to find offers by org")
	assert.True(t, len(offers) > 0, "Expected to be able to find offers")

	uuid := "857b6a84-9cad-11e7-98a5-10ddb1b87a66" // this uuid doesn't exist
	offers, err = GetAllOffers(tx, 20, 1, "market", "", uuid, "", "", nil, true, false, "", &totalCount)
	assert.NoError(t, err, "Expected to be able to find offers by org")
	assert.True(t, len(offers) == 0, "Expected to be able to find no offers")

	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindOffersForItem(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	for i := 0; i < 3; i++ {
		_, err = insertOffer(tx, item)
		if err != nil {
			t.Fatal("Unable to insert offer", err)
		}
	}

	itemOffers, err := GetOffersForItem(tx, item.ItemUUID.String, schema)
	if err != nil {
		t.Fatal("Unable to find offers", err)
	}
	assert.Len(t, itemOffers, 3, "Expected a certain number of offers to be found")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

const offerJSON = `{
	"supplierUUID": "%s",
	"itemUUID": "%s",
	"status": "ACTIVE",
	"type": "couldsupply",
	"firstDate": "2017-07-14",
	"lastDate": "2500-01-01",
	"quantity": 20.5,
	"deliveryDays": 2
}`

// Helper Funcs
func insertOffer(tx *sqlx.Tx, item *Item) (*SupplierOffer, error) {
	json := fmt.Sprintf(offerJSON, item.SupplierUUID.String, item.ItemUUID.String)
	offer, err := NewOffer(tx, []byte(json), schema)
	if err != nil {
		return nil, err
	}
	err = offer.Insert(tx)
	if err != nil {
		return nil, err
	}
	return offer, err
}
