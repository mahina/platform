package models

import (
	"bytes"
	"crypto/md5"
	"encoding/json"
	"errors"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"github.com/guregu/null"
	"github.com/guregu/null/zero"
	"github.com/jmoiron/sqlx/types"
)

// Registration States
const (
	RegStateAppReceived    = "received"
	RegStateEmailSent      = "email-confirm-sent"
	RegStateEmailConfirmed = "email-confirmed"
	RegStateAwaitApproval  = "awaiting-approval"
	RegStateApproved       = "approved"
	RegStateActive         = "active"
	RegStateDenied         = "denied"
	RegStateDuplicate      = "duplicate"
	RegStateWelcomeSent    = "welcome-sent"
)

// Registration defines a signup application in the system
type Registration struct {
	RegUUID   null.String    `db:"reg_uuid" json:"regUUID"`
	Username  zero.String    `db:"username" json:"username"`
	Status    zero.String    `db:"status" json:"status,omitempty"`
	Created   zero.Time      `db:"created" json:"created"`
	IPAddress zero.String    `db:"ip_address" json:"ipAddress"`
	Extended  types.JSONText `db:"extended" json:"extended,omitempty"`
}

// RegistrationAction defines actions the system takes and observes
// as new users are processed into the system
type RegistrationAction struct {
	RegUUID  zero.String `db:"reg_uuid" json:"regUUID"`
	Username zero.String `db:"username" json:"username"`
	Action   zero.String `db:"action" json:"action"`
	Created  zero.Time   `db:"created" json:"created"`
	Log      null.String `db:"log" json:"log"`
}

// NewRegistration creates a Registration struct from a passed JSON string. No database
// events occur from this call.
func NewRegistration(jsonStr []byte) (*Registration, error) {
	reg := Registration{}
	err := json.Unmarshal(jsonStr, &reg)
	if err != nil {
		return nil, err
	}
	return &reg, nil
}

// Get reads the Registration from the db, using the UUID as the lookup key. The Registration object
// is updated with the attributes of the found Registration.
func (reg *Registration) Get(tx *sqlx.Tx) (err error) {
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(reg, "SELECT * FROM entity.registration WHERE reg_uuid = $1", reg.RegUUID)
	} else {
		err = tx.Get(reg, "SELECT * FROM entity.registration WHERE reg_uuid = $1", reg.RegUUID)
	}
	return
}

// Valid tests the correctness of the Registration
func (reg *Registration) Valid() bool {
	return !reg.Username.IsZero() &&
		!reg.Status.IsZero() &&
		len(reg.Extended.String()) > 0
}

const regInsertStatement = `INSERT INTO entity.registration (` +
	`username, ip_address, extended` +
	`) VALUES (` +
	`$1, $2, $3` +
	`) RETURNING reg_uuid`

// Insert adds a registration. The struct will contain all DB-generated defaults, IDs, etc.
func (reg *Registration) Insert(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	row := tx.QueryRowx(regInsertStatement, reg.Username, reg.IPAddress, reg.Extended)
	err = row.Err()
	if err != nil {
		goto end
	}
	err = row.Scan(&reg.RegUUID)
	if err != nil {
		goto end
	}
	if reg.RegUUID.IsZero() {
		err = errors.New("Primary key unexpectedly nil")
	}
end:
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = tx.Get(reg, "SELECT * FROM entity.registration WHERE reg_uuid = $1", reg.RegUUID)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

const regUpdateStatement = `UPDATE entity.registration SET (` +
	`status, ip_address, extended` +
	`) = ( ` +
	`$1, $2, $3` +
	`) WHERE reg_uuid = $4`

// Update performs an SQL UPDATE operation using the Registration struct fields as attributes.
func (reg *Registration) Update(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(regUpdateStatement, reg.Status, reg.IPAddress, reg.Extended, reg.RegUUID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = tx.Get(reg, "SELECT * FROM entity.registration WHERE reg_uuid = $1", reg.RegUUID)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete expunges the Registration record from the system
func (reg *Registration) Delete(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec("DELETE FROM entity.registration WHERE reg_uuid = $1", reg.RegUUID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		reg.RegUUID = null.StringFrom("")
	}
	return
}

func (reg *Registration) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(reg)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

func (regAction *RegistrationAction) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(regAction)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

const regActionInsertStatement = `INSERT INTO entity.registration_action (` +
	`reg_uuid, username, action, log` +
	`) VALUES (` +
	`$1, $2, $3, $4` +
	`)`

func (reg *Registration) AddAction(tx *sqlx.Tx, action string, log string) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(regActionInsertStatement, reg.RegUUID, reg.Username, action, log)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Hash computes an MD5 hash of the JSON decoding of the registration
func (reg *Registration) Hash() (sum [md5.Size]byte) {
	bytes, _ := reg.Extended.MarshalJSON()
	sum = md5.Sum(bytes)
	return
}

// CompareHash considers the equivalency of the Registration object to the passed hash
func (reg *Registration) CompareHash(hash [md5.Size]byte) bool {
	bytes := reg.Hash()
	return bytes == hash
}

// GetRegistrationForUsername locates a registration record for the username
func GetRegistrationForUsername(tx *sqlx.Tx, username string) (reg *Registration, err error) {
	var db *sqlx.DB
	reg = new(Registration)
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(reg, "SELECT * FROM entity.registration WHERE username = $1", username)
	} else {
		err = tx.Get(reg, "SELECT * FROM entity.registration WHERE username = $1", username)
	}
	if err != nil {
		reg = nil
	}
	return
}

// GetAllRegistrationActionsForUsername returns all registration actions recorded for a particular username.
// The order of the actions is sorted in descending order (of insertion)
func GetAllRegistrationActionsForUsername(tx *sqlx.Tx, username string) ([]RegistrationAction, error) {
	var db *sqlx.DB
	var err error
	actions := []RegistrationAction{}
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return nil, err
		}
		err = db.Select(&actions, "SELECT * FROM entity.registration_action WHERE username = $1 ORDER BY created DESC", username)
	} else {
		err = tx.Select(&actions, "SELECT * FROM entity.registration_action WHERE username = $1 ORDER BY created DESC", username)
	}
	return actions, err
}

// GetRegistrationsByStatus returns all Registration objects that match the status
func GetRegistrationsByStatus(tx *sqlx.Tx, status string) ([]Registration, error) {
	var db *sqlx.DB
	var err error
	regs := []Registration{}
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return nil, err
		}
		err = db.Select(&regs, "SELECT * FROM entity.registration WHERE status = $1", status)
	} else {
		err = tx.Select(&regs, "SELECT * FROM entity.registration WHERE status = $1", status)
	}
	return regs, err
}
