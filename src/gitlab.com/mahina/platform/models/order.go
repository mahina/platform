package models

import (
	"bytes"
	"encoding/json"
	"errors"

	"gitlab.com/mahina/platform/util"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"fmt"

	"strings"

	"github.com/guregu/null"
	"github.com/guregu/null/zero"
	"github.com/jmoiron/sqlx/types"
)

// Constants for the template entity
const (
	OrderTypeSpot     = "spot"
	OrderTypeStanding = "standing"
)

type DeliveryDay uint

const (
	Monday DeliveryDay = iota
	Tuesday
	Wednesday
	Thursday
	Friday
	Saturday
	Sunday
)

var deliveryDays = [...]string{
	"monday",
	"tuesday",
	"wednesday",
	"thursday",
	"friday",
	"saturday",
	"sunday",
}

func (d DeliveryDay) String() string { return deliveryDays[d] }
func (d DeliveryDay) DOW(dow string) DeliveryDay {
	for n, v := range deliveryDays {
		if v == strings.ToLower(dow) {
			return DeliveryDay(n)
		}
	}
	return Monday
}

func DecodeDeliveryDays(daysEncoded int) (days []string) {
	for i := uint(Monday); i <= uint(Sunday); i++ {
		if daysEncoded&(1<<i) > 0 {
			days = append(days, deliveryDays[i])
		}
	}
	return
}

func EncodeDeliveryDays(days []string) (val uint) {
	for _, n := range days {
		day := DeliveryDay(0).DOW(n)
		val |= (1 << uint(day))
	}
	return
}

// Order defines a template entity that's schema-based
type Order struct {
	OrderID          int64          `db:"order_id" json:"-"` // serial ID's are internal and not shared with external systems
	OrderUUID        null.String    `db:"order_uuid" json:"orderUUID"`
	BuyerID          int64          `db:"buyer_id" json:"-"` // serial ID's are internal and not shared with external systems
	BuyerUUID        null.String    `db:"buyer_uuid" json:"buyerUUID"`
	BuyerUserID      int64          `db:"buyer_user_id" json:"-"` // serial ID's are internal and not shared with external systems
	BuyerUserUUID    null.String    `db:"buyer_user_uuid" json:"buyerUserUUID"`
	Type             null.String    `db:"type" json:"type"`
	DeliveryDate     util.ZeroDate  `db:"delivery_date" json:"deliveryDate"`
	LastDeliveryDate util.NullDate  `db:"last_delivery_date" json:"lastDeliveryDate"`
	Notes            zero.String    `db:"notes" json:"notes"`
	Created          zero.Time      `db:"created" json:"created"`
	Modified         zero.Time      `db:"modified" json:"modified"`
	Extended         types.JSONText `db:"extended" json:"extended,omitempty"`

	Schema string `db:"-" json:"-"`
}

// NewOrder creates a Order struct from a passed JSON string. No database
// events occur from this call.
func NewOrder(tx *sqlx.Tx, jsonStr []byte, schema string) (*Order, error) {
	entity := Order{}
	entity.Schema = schema
	err := json.Unmarshal(jsonStr, &entity)
	if err != nil {
		return nil, err
	}
	if entity.BuyerID == 0 {
		buyer, err := GetOrganizationByUUID(tx, entity.BuyerUUID.String)
		if err != nil {
			return nil, err
		}
		entity.BuyerID = buyer.OrgID
	}
	if entity.BuyerUserID == 0 && entity.BuyerUserUUID.String != "" {
		buyer, err := GetUserByUUID(tx, entity.BuyerUserUUID.String)
		if err != nil {
			return nil, err
		}
		entity.BuyerUserID = buyer.UserID
	}
	return &entity, nil
}

// Get performs a SELECT, using OrderID as the lookup key. The Order object
// is updated with the attributes of the found Order.
func (entity *Order) Get(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.order WHERE order_id = $1", entity.Schema)
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(entity, statement, entity.OrderID)
	} else {
		err = tx.Get(entity, statement, entity.OrderID)
	}
	return
}

// Valid returns the validity of the Order object
func (entity *Order) Valid() bool {
	return len(entity.OrderUUID.String) > 0
}

// Exists tests for the existence of a Order record corresponding to the OrderID value
func (entity *Order) Exists(tx *sqlx.Tx) bool {
	if entity.OrderID <= 0 {
		return false
	}
	err := entity.Get(tx)
	if err != nil {
		return false
	}
	return entity.OrderID > 0
}

const orderInsertStatement = `INSERT INTO %s.order (` +
	`buyer_id, buyer_uuid, buyer_user_id, buyer_user_uuid, type, delivery_date, last_delivery_date, notes, extended` +
	`) VALUES (` +
	`$1, $2, $3, $4, $5, $6, $7, $8, $9` +
	`) RETURNING order_id`

// Insert performs an SQL INSERT statement using the Order struct fields as attributes. The struct
// will contain all DB-generated defaults, IDs, etc.
func (entity *Order) Insert(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(orderInsertStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	row := tx.QueryRowx(statement, entity.BuyerID, entity.BuyerUUID, entity.BuyerUserID, entity.BuyerUserUUID,
		entity.Type, entity.DeliveryDate, entity.LastDeliveryDate, entity.Notes, entity.Extended)
	err = row.Err()
	if err != nil {
		goto end
	}
	err = row.Scan(&entity.OrderID)
	if err != nil {
		goto end
	}
	if entity.OrderID == 0 {
		err = errors.New("Primary key unexpectedly nil")
	}
end:
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

const orderUpdateStatement = `UPDATE %s.order SET (` +
	`buyer_id, buyer_uuid, buyer_user_id, buyer_user_uuid, type, delivery_date, last_delivery_date, notes, extended` +
	`) = ( ` +
	`$1, $2, $3, $4, $5, $6, $7, $8, $9` +
	`) WHERE order_id = $10`

// Update performs an SQL UPDATE operation using the Order struct fields as attributes.
func (entity *Order) Update(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(orderUpdateStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.BuyerID, entity.BuyerUUID, entity.BuyerUserID, entity.BuyerUserUUID,
		entity.Type, entity.DeliveryDate, entity.LastDeliveryDate, entity.Notes, entity.Extended,
		entity.OrderID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation, using OrderID as the lookup key.
func (entity *Order) Delete(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("DELETE FROM %s.order WHERE order_id = $1", entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.OrderID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		entity.OrderID = 0
	}
	return
}

func (entity *Order) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(entity)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetOrderByOrderID performs a SELECT, using the passed OrderID as the lookup key
// Returns nil if the entity does not exist--err may/may not be nil in that case.
func GetOrderByOrderID(tx *sqlx.Tx, entityID int64, schema string) (entity *Order, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.order WHERE order_id = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(Order)
	if tx == nil {
		err = db.Get(entity, statement, entityID)
	} else {
		err = tx.Get(entity, statement, entityID)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	return
}

// GetOrderByUUID performs a SELECT, using the passed UUID as the lookup key.
// Returns nil if the entity does not exist—-err may/may not be nil in that case.
func GetOrderByUUID(tx *sqlx.Tx, uuid string, schema string) (entity *Order, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.order WHERE order_uuid = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(Order)
	if tx == nil {
		err = db.Get(entity, statement, uuid)
	} else {
		err = tx.Get(entity, statement, uuid)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	return
}

// GetAllOrders returns all orders, transaction aware. Pass -1 for either limit or offset
// to bypass query limits and/or offset position
// TODO: Add date constraints
func GetAllOrders(tx *sqlx.Tx, limit int, offset int, schema string,
	buyerUUID string, buyerUserUUID string, orderType string) ([]Order, error) {
	if schema == "" {
		err := errors.New("schema not specified")
		return nil, err
	}
	var err error
	orders := []Order{}
	statement := fmt.Sprintf("SELECT * FROM %s.order", schema)
	if limit > 0 {
		statement += fmt.Sprintf(" LIMIT %d", limit)
	}
	if offset > 0 {
		statement += fmt.Sprintf(" OFFSET %d", offset)
	}
	whereActive := false
	if buyerUUID != "" {
		statement += fmt.Sprintf(" WHERE buyer_uuid = '%s'", buyerUUID)
		whereActive = true
	}
	if buyerUserUUID != "" {
		if whereActive {
			statement += fmt.Sprintf(" AND buyer_user_uuid = '%s'", buyerUserUUID)
		} else {
			whereActive = true
			statement += fmt.Sprintf(" WHERE buyer_user_uuid = '%s'", buyerUserUUID)
		}
	}
	if orderType != "" {
		if whereActive {
			statement += fmt.Sprintf(" AND type = '%s'", orderType)
		} else {
			statement += fmt.Sprintf(" WHERE type = '%s'", orderType)
		}
	}
	statement += " ORDER BY delivery_date"
	if tx == nil {
		db, err := GetDB()
		if err != nil {
			return nil, err
		}
		err = db.Select(&orders, statement)
	} else {
		err = tx.Select(&orders, statement)
	}

	if err == nil {
		for i := range orders {
			orders[i].Schema = schema
		}
	}
	return orders, err
}
