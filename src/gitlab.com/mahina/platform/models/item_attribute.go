package models

import (
	"bytes"
	"encoding/json"
	"errors"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"fmt"

	"github.com/guregu/null"
)

// ItemAttribute defines a template entity that's schema-based.
type ItemAttribute struct {
	ItemID    int64       `db:"item_id" json:"-"`
	ItemUUID  null.String `db:"item_uuid" json:"itemUUID"`
	Attribute null.String `db:"attribute" json:"attribute"`

	Schema string `db:"-" json:"-"`
}

// NewItemAttribute creates a ItemAttribute struct from a passed JSON string. No database
// events occur from this call.
func NewItemAttribute(tx *sqlx.Tx, jsonStr []byte, schema string) (*ItemAttribute, error) {
	entity := ItemAttribute{}
	entity.Schema = schema
	err := json.Unmarshal(jsonStr, &entity)
	if err != nil {
		return nil, err
	}
	if entity.ItemID == 0 {
		item, err := GetItemByUUID(tx, entity.ItemUUID.String, schema)
		if err != nil {
			return nil, err
		}
		entity.ItemID = item.ItemID
	}
	return &entity, nil
}

// Get performs a SELECT, using ItemID and Attribute as the lookup key. The ItemAttribute object
// is updated with the attributes of the found ItemAttribute.
func (entity *ItemAttribute) Get(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.item_attribute WHERE item_id = $1 AND attribute = $2", entity.Schema)
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(entity, statement, entity.ItemID, entity.Attribute)
	} else {
		err = tx.Get(entity, statement, entity.ItemID, entity.Attribute)
	}
	return
}

// Valid returns the validity of the ItemAttribute object
func (entity *ItemAttribute) Valid() bool {
	return entity.ItemID > 0 && len(entity.Attribute.String) > 0 && len(entity.Attribute.String) <= 128
}

// Exists tests for the existence of a ItemAttribute record corresponding to the PricingID value
func (entity *ItemAttribute) Exists(tx *sqlx.Tx) bool {
	if entity.ItemID <= 0 {
		return false
	}
	err := entity.Get(tx)
	if err != nil {
		return false
	}
	return entity.ItemID > 0
}

const itemAttributeInsertStatement = `INSERT INTO %s.item_attribute (` +
	`item_id, item_uuid, attribute` +
	`) VALUES (` +
	`$1, $2, $3` +
	`)`

// Insert performs an SQL INSERT statement using the ItemAttribute struct fields as attributes. The struct
// will contain all DB-generated defaults, IDs, etc.
func (entity *ItemAttribute) Insert(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(itemAttributeInsertStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.ItemID, entity.ItemUUID, entity.Attribute)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation, using ItemId and Attribute as the lookup key.
func (entity *ItemAttribute) Delete(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("DELETE FROM %s.item_attribute WHERE item_id = $1 AND attribute = $2", entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.ItemID, entity.Attribute)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		entity.ItemID = 0
	}
	return
}

func (entity *ItemAttribute) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(entity)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetAllItemAttributesByItemID returns all attributes for a given item.
func GetAllItemAttributesByItemID(tx *sqlx.Tx, itemID int64, schema string) ([]string, error) {
	if schema == "" {
		err := errors.New("schema not specified")
		return nil, err
	}
	db, err := GetDB()
	if err != nil {
		return nil, err
	}
	attributes := []string{}
	if tx == nil {
		err = db.Select(&attributes, fmt.Sprintf("SELECT attribute FROM %s.item_attribute WHERE item_id = $1", schema), itemID)
	} else {
		err = tx.Select(&attributes, fmt.Sprintf("SELECT attribute FROM %s.item_attribute WHERE item_id = $1", schema), itemID)
	}
	return attributes, err
}

// GetAllItemAttributesByItemUUID returns all attributes for a given item.
func GetAllItemAttributesByItemUUID(tx *sqlx.Tx, itemUUID string, schema string) ([]string, error) {
	if schema == "" {
		err := errors.New("schema not specified")
		return nil, err
	}
	db, err := GetDB()
	if err != nil {
		return nil, err
	}
	attributes := []string{}
	if tx == nil {
		err = db.Select(&attributes, fmt.Sprintf("SELECT attribute FROM %s.item_attribute WHERE item_uuid = $1", schema), itemUUID)
	} else {
		err = tx.Select(&attributes, fmt.Sprintf("SELECT attribute FROM %s.item_attribute WHERE item_uuid = $1", schema), itemUUID)
	}
	return attributes, err
}
