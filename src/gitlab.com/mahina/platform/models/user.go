package models

import (
	"bytes"
	"encoding/json"
	"errors"

	"gitlab.com/mahina/platform/util"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"github.com/guregu/null"
	"github.com/guregu/null/zero"
	"github.com/jmoiron/sqlx/types"
)

const (
	UserStatusApplied   = "received"
	UserStatusActive    = "active"
	UserStatusDeleted   = "deleted"
	UserStatusSuspended = "suspended"
)

// User defines a member in the system
type User struct {
	UserID      int64          `db:"user_id" json:"-"` // serial ID's are internal and not shared with external systems
	UserUUID    null.String    `db:"user_uuid" json:"userUUID"`
	Status      zero.String    `db:"status" json:"status,omitempty"`
	Username    null.String    `db:"username" json:"username" binding:"exists"`
	FirstName   null.String    `db:"first_name" json:"firstName"`
	LastName    null.String    `db:"last_name" json:"lastName"`
	MiddleName  null.String    `db:"middle_name" json:"middleName,omitempty" `
	Title       null.String    `db:"title" json:"title,omitempty"`
	Suffix      null.String    `db:"suffix" json:"suffix,omitempty" `
	JobTitle    null.String    `db:"job_title" json:"jobTitle,omitempty"`
	BirthDate   util.NullDate  `db:"birth_date" json:"birthDate,omitempty"`
	Gender      zero.String    `db:"gender" json:"gender,omitempty"`
	Timezone    null.String    `db:"timezone" json:"timezone,omitempty"`
	Locale      null.String    `db:"locale" json:"locale,omitempty"`
	Email       zero.String    `db:"email" json:"email,omitempty"`
	WorkPhone   null.String    `db:"work_phone" json:"workPhone,omitempty"`
	HomePhone   null.String    `db:"home_phone" json:"homePhone,omitempty"`
	MobilePhone null.String    `db:"mobile_phone" json:"mobilePhone,omitempty"`
	FaxPhone    null.String    `db:"fax_phone" json:"faxPhone,omitempty"`
	ImageIcon   []byte         `db:"image_icon" json:"imageIcon,omitempty"`
	Created     zero.Time      `db:"created" json:"created"`
	Extended    types.JSONText `db:"extended" json:"extended,omitempty"`
}

// NewUser creates a User struct from a passed JSON string. No database
// events occur from this call.
func NewUser(jsonStr []byte) (*User, error) {
	user := User{}
	err := json.Unmarshal(jsonStr, &user)
	if err != nil {
		return nil, err
	}
	return &user, nil
}

// Get performs a SELECT, using UserID as the lookup key. The User object
// is updated with the attributes of the found User.
func (u *User) Get(tx *sqlx.Tx) (err error) {
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(u, "SELECT * FROM entity.user WHERE user_id = $1", u.UserID)
	} else {
		err = tx.Get(u, "SELECT * FROM entity.user WHERE user_id = $1", u.UserID)
	}
	return
}

// Valid returns the validity of the User object
func (u *User) Valid() bool {
	return len(u.Username.String) > 0 &&
		len(u.FirstName.String) > 0 &&
		len(u.LastName.String) > 0
}

// Exists tests for the existence of a User record corresponding to the UserID value
func (u *User) Exists(tx *sqlx.Tx) bool {
	if u.UserID <= 0 {
		return false
	}
	err := u.Get(tx)
	if err != nil {
		return false
	}
	return u.UserID > 0
}

const userInsertStatement = `INSERT INTO entity.user (` +
	`status, username, first_name, last_name, middle_name, title, suffix, 
    job_title, birth_date, gender, timezone, locale, email, work_phone, home_phone, 
    mobile_phone, fax_phone, image_icon, extended` +
	`) VALUES (` +
	`$1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19` +
	`) RETURNING user_id`

// Insert performs an SQL INSERT statement using the User struct fields as attributes. The struct
// will contain all DB-generated defaults, IDs, etc.
func (u *User) Insert(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	row := tx.QueryRowx(userInsertStatement, u.Status, u.Username, u.FirstName,
		u.LastName, u.MiddleName, u.Title, u.Suffix, u.JobTitle, u.BirthDate,
		u.Gender, u.Timezone, u.Locale, u.Email, u.WorkPhone, u.HomePhone,
		u.MobilePhone, u.FaxPhone, u.ImageIcon, u.Extended)
	err = row.Err()
	if err != nil {
		goto end
	}
	err = row.Scan(&u.UserID)
	if err != nil {
		goto end
	}
	if u.UserID == 0 {
		err = errors.New("Primary key unexpectedly nil")
	}
end:
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = tx.Get(u, "SELECT * FROM entity.user WHERE user_id = $1", u.UserID)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

const userUpdateStatement = `UPDATE entity.user SET (` +
	`status, username, first_name, last_name, middle_name, title, suffix, job_title, 
        birth_date, gender, timezone, locale, email, work_phone, home_phone, mobile_phone, 
        fax_phone, image_icon, extended` +
	`) = ( ` +
	`$1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19` +
	`) WHERE user_id = $20`

// Update performs an SQL UPDATE operation using the User struct fields as attributes.
func (u *User) Update(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(userUpdateStatement, u.Status, u.Username, u.FirstName, u.LastName, u.MiddleName,
		u.Title, u.Suffix, u.JobTitle, u.BirthDate, u.Gender, u.Timezone, u.Locale,
		u.Email, u.WorkPhone, u.HomePhone, u.MobilePhone, u.FaxPhone, u.ImageIcon, u.Extended,
		u.UserID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = tx.Get(u, "SELECT * FROM entity.user WHERE user_id = $1", u.UserID)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation, using UserID as the lookup key.
func (u *User) Delete(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec("DELETE FROM entity.user WHERE user_id = $1", u.UserID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		u.UserID = 0
	}
	return
}

func (u *User) Bytes() []byte {
	b, err := json.Marshal(u)
	if err != nil {
		return []byte("Error encoding")
	}
	return b
}

func (u *User) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(u)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetUserByUUIDOrUsername locates a User record by either a UUID or Username
func GetUserByUUIDOrUsername(tx *sqlx.Tx, userUUIDOrUsername string) (*User, error) {
	if util.IsUUID(userUUIDOrUsername) {
		return GetUserByUUID(tx, userUUIDOrUsername)
	}
	return GetUserByUsername(tx, userUUIDOrUsername)
}

// GetUserByUserID performs a SELECT, using the passed UserID as the lookup key
// Returns nil if the user does not exist--err may/may not be nil in that case.
func GetUserByUserID(tx *sqlx.Tx, userID int64) (user *User, err error) {
	db, err := GetDB()
	if err != nil {
		return
	}
	user = new(User)
	if tx == nil {
		err = db.Get(user, "SELECT * FROM entity.user WHERE user_id = $1", userID)
	} else {
		err = tx.Get(user, "SELECT * FROM entity.user WHERE user_id = $1", userID)
	}
	if err != nil {
		user = nil
	}
	return
}

// GetUserByUUID performs a SELECT, using the passed UUID as the lookup key.
// Returns nil if the user does not exist—-err may/may not be nil in that case.
func GetUserByUUID(tx *sqlx.Tx, uuid string) (user *User, err error) {
	db, err := GetDB()
	if err != nil {
		return
	}
	user = new(User)
	if tx == nil {
		err = db.Get(user, "SELECT * FROM entity.user WHERE user_uuid = $1", uuid)
	} else {
		err = tx.Get(user, "SELECT * FROM entity.user WHERE user_uuid = $1", uuid)
	}
	if err != nil {
		user = nil
	}
	return
}

// GetUserByUsername performs a SELECT, using the passed username as the lookup key
// Returns nil if the user does not exist—-err may/may not be nil in that case.
func GetUserByUsername(tx *sqlx.Tx, username string) (user *User, err error) {
	db, err := GetDB()
	if err != nil {
		return
	}
	user = new(User)
	if tx == nil {
		err = db.Get(user, "SELECT * FROM entity.user WHERE username = $1", username)
	} else {
		err = tx.Get(user, "SELECT * FROM entity.user WHERE username = $1", username)
	}
	if err != nil {
		user = nil
	}
	return
}

// GetAllUsers returns all users. Pass -1 for either limit or offset
// to bypass query limits and/or offset position
func GetAllUsers(limit int, offset int) ([]User, error) {
	db, err := GetDB()
	if err != nil {
		return nil, err
	}
	users := []User{}
	if limit > 0 && offset > 0 {
		err = db.Select(&users, "SELECT * FROM entity.user LIMIT $1 OFFSET $2", limit, offset)
	} else if limit > 0 {
		err = db.Select(&users, "SELECT * FROM entity.user LIMIT $1", limit)
	} else if offset > 0 {
		err = db.Select(&users, "SELECT * FROM entity.user OFFSET $1", offset)
	} else {
		err = db.Select(&users, "SELECT * FROM entity.user")
	}
	return users, err
}

func GetMarketplacesForUser(tx *sqlx.Tx, userUUID string) (marketplaces []string, err error) {
	statement := `SELECT schema_name
    FROM entity.market
    WHERE market_id = (
        SELECT market_id
            FROM entity.organization_market
            WHERE org_id = (SELECT org_id
                            FROM entity.user_organization
                            WHERE user_id = (
                                SELECT user_id 
                                FROM entity.user
                                WHERE user_uuid = $1
                                )
                            )
    )`
	if tx == nil {
		db, err := GetDB()
		if err != nil {
			return nil, err
		}
		err = db.Select(&marketplaces, statement, userUUID)
	} else {
		err = tx.Select(&marketplaces, statement, userUUID)
	}
	return
}
