// +build integration all

package models

import (
	"testing"

	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

func TestInsertItemAttribute(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	itemAttribute, err := insertAttribute(tx, item, "organic")
	if err != nil {
		t.Fatal("Unable to insert item attribute", err)
	}
	assert.Equal(t, "organic", itemAttribute.Attribute.String, "Expected certain value in item attribute insert")
	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestDeleteItemAttribute(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	itemAttribute, err := insertAttribute(tx, item, "organic")
	if err != nil {
		t.Fatal("Unable to insert item attribute", err)
	}

	err = itemAttribute.Delete(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	assert.False(t, itemAttribute.Exists(tx), "Expected to not be able to find deleted item attribute")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindItemAttributesForItem(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	_, err = insertAttribute(tx, item, "organic")
	attribute, err := insertAttribute(tx, item, "shade grown")

	itemAttributes, err := GetAllItemAttributesByItemUUID(tx, item.ItemUUID.String, schema)
	if err != nil {
		t.Fatal("Unable to find item attributes", err)
	}
	assert.Len(t, itemAttributes, 2, "Expected a certain number of item attributes to be found")

	attribute.Delete(tx)
	itemAttributes, err = GetAllItemAttributesByItemUUID(tx, item.ItemUUID.String, schema)
	if err != nil {
		t.Fatal("Unable to find item attributes", err)
	}
	assert.Len(t, itemAttributes, 1, "Expected a certain number of item attributes to be found")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

const itemAttributeJSON = `{
	"itemUUID": "%s",
	"attribute": "%s"
}`

// Helper Funcs
func insertAttribute(tx *sqlx.Tx, item *Item, attribute string) (*ItemAttribute, error) {
	json := fmt.Sprintf(itemAttributeJSON, item.ItemUUID.String, attribute)
	itemAttribute, err := NewItemAttribute(tx, []byte(json), schema)
	if err != nil {
		return nil, err
	}
	err = itemAttribute.Insert(tx)
	if err != nil {
		return nil, err
	}
	return itemAttribute, err
}
