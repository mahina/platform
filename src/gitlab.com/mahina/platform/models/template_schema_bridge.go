package models

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

// LeftRightBridgeSchema defines an Entity to Entity bridge in the system within a schema. This module is intended to be duplicated.
type LeftRightBridgeSchema struct {
	LeftID  int64 `db:"left_id" json:"leftID"`
	RightID int64 `db:"right_id" json:"rightID"`

	Schema string `db:"-" json:"-"`
}

// NewLeftRightSchema creates a LeftRightBridgeSchema struct from a passed values. No database
// events occur from this call.
func NewLeftRightSchema(leftID int64, rightID int64, schema string) (*LeftRightBridgeSchema, error) {
	record := LeftRightBridgeSchema{LeftID: leftID, RightID: rightID, Schema: schema}
	return &record, nil
}

// Get performs a SELECT, using rightID as the lookup key. The Bridge object
// is updated with the attributes of the found Bridge.
func (leftRight *LeftRightBridgeSchema) Get(tx *sqlx.Tx) (err error) {
	if leftRight.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.left_right WHERE left_id = $1 AND right_id = $2", leftRight.Schema)
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(leftRight, statement, leftRight.LeftID, leftRight.RightID)
	} else {
		err = tx.Get(leftRight, statement, leftRight.LeftID, leftRight.RightID)
	}
	return
}

// Valid returns the validity of the Bridge object
func (leftRight *LeftRightBridgeSchema) Valid() bool {
	return leftRight.RightID > 0 && leftRight.LeftID > 0
}

// Exists tests for the existence of a LeftRightBridgeSchema record corresponding to the LeftID and RightID value
func (leftRight *LeftRightBridgeSchema) Exists(tx *sqlx.Tx) bool {
	if leftRight.LeftID == 0 || leftRight.RightID == 0 {
		return false
	}
	if leftRight.Schema == "" {
		return false
	}
	err := leftRight.Get(tx)
	if err != nil {
		return false
	}
	return leftRight.LeftID > 0 && leftRight.RightID > 0
}

const leftRightSchemaInsertStatement = `INSERT INTO %s.left_right (` +
	`left_id, right_id` +
	`) VALUES (` +
	`$1, $2` +
	`)`

// Insert performs an SQL INSERT statement using the Bridge struct fields as attributes.
func (leftRight *LeftRightBridgeSchema) Insert(tx *sqlx.Tx) (err error) {
	if leftRight.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(leftRightSchemaInsertStatement, leftRight.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, leftRight.LeftID, leftRight.RightID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = leftRight.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation
func (leftRight *LeftRightBridgeSchema) Delete(tx *sqlx.Tx) (err error) {
	if leftRight.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(fmt.Sprintf("DELETE FROM %s.left_right WHERE left_id = $1 AND right_id = $2", leftRight.Schema), leftRight.LeftID, leftRight.RightID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		leftRight.LeftID = 0
		leftRight.RightID = 0
	}
	return
}

func (leftRight *LeftRightBridgeSchema) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(leftRight)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetAllLeftsInRight returns all primary keys of the entities associated with the rightID
func GetAllLeftsInRightSchema(tx *sqlx.Tx, rightID int64, schema string) ([]int64, error) {
	if schema == "" {
		err := errors.New("schema not specified")
		return nil, err
	}
	statement := fmt.Sprintf("SELECT left_id FROM %s.left_right WHERE right_id = $1", schema)
	var err error
	leftIDs := []int64{}
	if tx == nil {
		db, err := GetDB()
		if err != nil {
			return nil, err
		}
		err = db.Select(&leftIDs, statement, rightID)
	} else {
		err = tx.Select(&leftIDs, statement, rightID)
	}
	return leftIDs, err
}
