package models

import (
	"errors"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

const credentialFindStatement = `SELECT user_id FROM entity.credential WHERE user_id = $1 AND type = $2`
const credentialInsertStatement = `SELECT entity.insert_credential($1, $2, $3, $4)`
const credentialUpdateStatement = `SELECT entity.update_credential($1, $2, $3, $4)`
const credentialVerifyStatement = `SELECT entity.verify_credential($1, $2, $3, $4)`
const credentialDeleteStatement = `DELETE FROM entity.credential WHERE user_id = $1 AND type = $2`

// InsertCredential inserts a new challenge and response for the credentialType
func InsertCredential(tx *sqlx.Tx, userNameOrUUID string, credentialType CredentialType, challenge []byte, response []byte) error {
	if len(challenge) == 0 {
		return errors.New("Unexpected empty challenge")
	}
	user, err := GetUserByUUIDOrUsername(tx, userNameOrUUID)
	if err != nil {
		return err
	} else if user == nil {
		return errors.New("User not found")
	}
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return err
		}
	}
	_, err = tx.Exec(credentialInsertStatement, user.UserID, credentialType, challenge, response)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
		return err
	}
	if isolated {
		err = tx.Commit()
		if err != nil {
			return err
		}
	}
	return nil
}

// ExistsCredential tests for the existence of a credential of type credentialType
// for a given userNameOrUUID
func ExistsCredential(tx *sqlx.Tx, userNameOrUUID string, credentialType CredentialType) (bool, error) {
	var db *sqlx.DB
	user, err := GetUserByUUIDOrUsername(tx, userNameOrUUID)
	if err != nil || user == nil {
		return false, err
	}
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return false, err
		}
		err = db.Get(user, credentialFindStatement, user.UserID, credentialType)
	} else {
		err = tx.Get(user, credentialFindStatement, user.UserID, credentialType)
	}
	return err == nil, err
}

// UpdateCredential updates the credentialType with a new response
func UpdateCredential(tx *sqlx.Tx, userNameOrUUID string, credentialType CredentialType, priorResponse []byte, response []byte) error {
	user, err := GetUserByUUIDOrUsername(tx, userNameOrUUID)
	if err != nil {
		return err
	} else if user == nil {
		return errors.New("User not found")
	}
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return err
		}
	}
	_, err = tx.Exec(credentialUpdateStatement, user.UserID, credentialType, priorResponse, response)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
		return err
	}
	if isolated {
		err = tx.Commit()
		if err != nil {
			return err
		}
	}
	return nil
}

// VerifyCredential tests the passed challenge and response for the credentialType
func VerifyCredential(tx *sqlx.Tx, userNameOrUUID string, credentialType CredentialType, challenge []byte, response []byte) (*User, error) {
	var db *sqlx.DB
	user, err := GetUserByUUIDOrUsername(tx, userNameOrUUID)
	if err != nil {
		return nil, err
	} else if user == nil {
		return nil, errors.New("user not found")
	}
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return nil, err
		}
		_, err = db.Exec(credentialVerifyStatement, user.UserID, credentialType, challenge, response)
	} else {
		_, err = tx.Exec(credentialVerifyStatement, user.UserID, credentialType, challenge, response)
	}
	return user, err
}

// DeleteCredential removes the credentialType associated with userNameOrUUID
func DeleteCredential(tx *sqlx.Tx, userNameOrUUID string, credentialType CredentialType) (err error) {
	user, err := GetUserByUUIDOrUsername(tx, userNameOrUUID)
	if err != nil {
		return
	} else if user == nil {
		err = errors.New("User not found")
		return
	}
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(credentialDeleteStatement, user.UserID, credentialType)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
		return
	}
	if isolated {
		err = tx.Commit()
	}
	return
}
