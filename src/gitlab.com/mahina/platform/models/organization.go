package models

import (
	"bytes"
	"encoding/json"
	"errors"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"github.com/guregu/null"
	"github.com/guregu/null/zero"
	"github.com/jmoiron/sqlx/types"
)

// Organization defines a company entity in the system
type Organization struct {
	OrgID         int64            `db:"org_id" json:"-"`                               // serialized ID's are internal
	OrgUUID       null.String      `db:"org_uuid" json:"orgUUID"`                       // uuid
	Status        null.String      `db:"status" json:"status,omitempty"`                // status
	LegalName     null.String      `db:"legal_name" json:"legalName"`                   // legal_name
	Dba           zero.String      `db:"dba" json:"dba,omitempty"`                      // dba
	ShortName     zero.String      `db:"short_name" json:"shortName,omitempty"`         // short_name
	OrgType       OrganizationType `db:"org_type" json:"orgType"`                       // org_type
	Duns          zero.String      `db:"duns" json:"duns,omitempty"`                    // duns
	EmployeeCount null.Int         `db:"employee_count" json:"employeeCount,omitempty"` // employee_count
	ImageLogo     []byte           `db:"image_logo" json:"imageLogo,omitempty"`         // image_logo
	Created       zero.Time        `db:"created" json:"created"`                        // created
	Extended      types.JSONText   `db:"extended" json:"extended,omitempty"`            // extended
}

// NewOrganization creates a Organization struct from a passed JSON string. No database
// events occur from this call.
func NewOrganization(jsonStr []byte) (*Organization, error) {
	org := Organization{}
	err := json.Unmarshal(jsonStr, &org)
	if err != nil {
		return nil, err
	}
	return &org, nil
}

// Get performs a SELECT, using OrgID as the lookup key. The Organization object
// is updated with the attributes of the found Org.
func (org *Organization) Get(tx *sqlx.Tx) (err error) {
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(org, "SELECT * FROM entity.organization WHERE org_id = $1", org.OrgID)
	} else {
		err = tx.Get(org, "SELECT * FROM entity.organization WHERE org_id = $1", org.OrgID)
	}
	return
}

// Valid returns the validity of the Organization object
func (org *Organization) Valid() bool {
	return len(org.LegalName.String) > 0 && org.OrgID > 0 && len(org.OrgUUID.String) > 0
}

// Exists tests for the existence of an Organization record corresponding to the OrgID value
func (org *Organization) Exists(tx *sqlx.Tx) bool {
	if org.OrgID <= 0 {
		return false
	}
	err := org.Get(tx)
	if err != nil {
		return false
	}
	return org.OrgID > 0
}

const orgInsertStatement = `INSERT INTO entity.organization (` +
	`status, legal_name, dba, short_name, org_type, duns, employee_count, image_logo, extended` +
	`) VALUES (` +
	`$1, $2, $3, $4, $5, $6, $7, $8, $9` +
	`) RETURNING org_id`

// Insert performs an SQL INSERT statement using the Organization struct fields as attributes. The struct
// will contain all DB-generated defaults, IDs, etc.
func (org *Organization) Insert(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	row := tx.QueryRowx(orgInsertStatement, org.Status, org.LegalName, org.Dba, org.ShortName,
		org.OrgType, org.Duns, org.EmployeeCount, org.ImageLogo, org.Extended)
	err = row.Err()
	if err != nil {
		goto end
	}
	err = row.Scan(&org.OrgID)
	if err != nil {
		goto end
	}
	if org.OrgID == 0 {
		err = errors.New("Primary key unexpectedly nil")
	}
end:
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = tx.Get(org, "SELECT * FROM entity.organization WHERE org_id = $1", org.OrgID)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

const orgUpdateStatement = `UPDATE entity.organization SET (` +
	`status, legal_name, dba, short_name, org_type, duns, employee_count, image_logo, extended` +
	`) = ( ` +
	`$1, $2, $3, $4, $5, $6, $7, $8, $9` +
	`) WHERE org_id = $10`

// Update performs an SQL UPDATE operation using the Organization struct fields as attributes.
func (org *Organization) Update(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(orgUpdateStatement, org.Status, org.LegalName, org.Dba, org.ShortName, org.OrgType,
		org.Duns, org.EmployeeCount, org.ImageLogo, org.Extended, org.OrgID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = tx.Get(org, "SELECT * FROM entity.organization WHERE org_id = $1", org.OrgID)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation, using OrgID as the lookup key.
func (org *Organization) Delete(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec("DELETE FROM entity.organization WHERE org_id = $1", org.OrgID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		org.OrgID = 0
	}
	return
}

func (org *Organization) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(org)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetOrganizationByUUID locates a Organization record by UUID
func GetOrganizationByUUID(tx *sqlx.Tx, uuid string) (org *Organization, err error) {
	var db *sqlx.DB
	org = new(Organization)
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(org, "SELECT * FROM entity.organization WHERE org_uuid = $1", uuid)
	} else {
		err = tx.Get(org, "SELECT * FROM entity.organization WHERE org_uuid = $1", uuid)
	}
	if err != nil {
		org = nil
	}
	return
}

// GetOrganizationByOrgID performs a SELECT, using the passed OrgID as the lookup key
// Returns nil if the org does not exist--err may/may not be nil in that case.
func GetOrganizationByOrgID(tx *sqlx.Tx, orgID int64) (org *Organization, err error) {
	var db *sqlx.DB
	org = new(Organization)
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(org, "SELECT * FROM entity.organization WHERE org_id = $1", orgID)
	} else {
		err = tx.Get(org, "SELECT * FROM entity.organization WHERE org_id = $1", orgID)
	}
	if err != nil {
		org = nil
	}
	return
}

// GetOrganizationByExactLegalName returns an organization that exactly matches a given name
func GetOrganizationByExactLegalName(tx *sqlx.Tx, name string) (org *Organization, err error) {
	var db *sqlx.DB
	org = new(Organization)
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(org, "SELECT * FROM entity.organization WHERE legal_name = $1", name)
	} else {
		err = tx.Get(org, "SELECT * FROM entity.organization WHERE legal_name = $1", name)
	}
	if err != nil {
		org = nil
	}
	return
}

// GetOrganizationsBySimilarLegalName returns a number of organizations that are similar
// to the supplied name
func GetOrganizationsBySimilarLegalName(tx *sqlx.Tx, name string) (org []Organization, err error) {
	var db *sqlx.DB
	orgs := []Organization{}
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return nil, err
		}
		err = db.Select(&orgs, "SELECT * FROM entity.organization WHERE SOUNDEX(legal_name) = SOUNDEX($1)", name)
	} else {
		err = tx.Select(&orgs, "SELECT * FROM entity.organization WHERE SOUNDEX(legal_name) = SOUNDEX($1)", name)
	}
	return orgs, err
}

// GetAllOrganizations returns all organizations. Pass -1 for either limit or offset
// to bypass query limits and/or offset position
func GetAllOrganizations(limit int, offset int) ([]Organization, error) {
	db, err := GetDB()
	if err != nil {
		return nil, err
	}
	orgs := []Organization{}
	if limit > 0 && offset > 0 {
		err = db.Select(&orgs, "SELECT * FROM entity.organization LIMIT $1 OFFSET $2", limit, offset)
	} else if limit > 0 {
		err = db.Select(&orgs, "SELECT * FROM entity.organization LIMIT $1", limit)
	} else if offset > 0 {
		err = db.Select(&orgs, "SELECT * FROM entity.organization OFFSET $1", offset)
	} else {
		err = db.Select(&orgs, "SELECT * FROM entity.organization")
	}
	return orgs, err
}
