package models

import (
	"bytes"
	"encoding/json"
	"errors"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"github.com/guregu/null"
	"github.com/guregu/null/zero"
)

// Role defines a role in the system
type Role struct {
	RoleID      null.String `db:"role_id" json:"roleID"`
	Description zero.String `db:"description" json:"description,omitempty"`
}

// NewRole creates a Role struct from a passed JSON string. No database
// events occur from this call.
func NewRole(jsonStr []byte) (*Role, error) {
	role := Role{}
	err := json.Unmarshal(jsonStr, &role)
	if err != nil {
		return nil, err
	}
	return &role, nil
}

// Get performs a SELECT, using RoleID as the lookup key. The Role object
// is updated with the attributes of the found Role.
func (r *Role) Get(tx *sqlx.Tx) (err error) {
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(r, "SELECT * FROM entity.role WHERE role_id = $1", r.RoleID)
	} else {
		err = tx.Get(r, "SELECT * FROM entity.role WHERE role_id = $1", r.RoleID)
	}
	return
}

// Exists tests for the existence of a Role record corresponding to the RoleID value
func (r *Role) Exists(tx *sqlx.Tx) bool {
	if r.RoleID.IsZero() {
		return false
	}
	err := r.Get(tx)
	if err != nil {
		return false
	}
	return !r.RoleID.IsZero()
}

const roleInsertStatement = `INSERT INTO entity.role (` +
	`role_id, description` +
	`) VALUES (` +
	`$1, $2` +
	`) RETURNING role_id`

// Insert performs an SQL INSERT statement using the Role struct fields as attributes.
func (r *Role) Insert(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	row := tx.QueryRowx(roleInsertStatement, r.RoleID, r.Description)
	err = row.Err()
	if err != nil {
		goto end
	}
	err = row.Scan(&r.RoleID)
	if err != nil {
		goto end
	}
	if r.RoleID.IsZero() {
		err = errors.New("RoleID unexpectedly nil")
	}
end:
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = tx.Get(r, "SELECT * FROM entity.role WHERE role_id = $1", r.RoleID)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

const roleUpdateStatement = `UPDATE entity.role SET (` +
	`role_id, description` +
	`) = ( ` +
	`$1, $2` +
	`) WHERE role_id = $3`

// Update performs an SQL UPDATE operation using the Role struct fields as attributes.
func (r *Role) Update(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(roleUpdateStatement, r.RoleID, r.Description, r.RoleID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = tx.Get(r, "SELECT * FROM entity.role WHERE role_id = $1", r.RoleID)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation, using RoleID as the lookup key.
func (r *Role) Delete(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec("DELETE FROM entity.role WHERE role_id = $1", r.RoleID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		r.RoleID.String = ""
	}
	return
}

func (r *Role) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(r)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// Valid returns the correctness of the Role object
func (r *Role) Valid() bool {
	return len(r.RoleID.String) > 0 &&
		len(r.Description.String) > 0
}

// GetAllRoles returns all roles. Pass -1 for either limit or offset
// to bypass query limits and/or offset position
func GetAllRoles(limit int, offset int) ([]Role, error) {
	db, err := GetDB()
	if err != nil {
		return nil, err
	}
	roles := []Role{}
	if limit > 0 && offset > 0 {
		err = db.Select(&roles, "SELECT * FROM entity.role LIMIT $1 OFFSET $2", limit, offset)
	} else if limit > 0 {
		err = db.Select(&roles, "SELECT * FROM entity.role LIMIT $1", limit)
	} else if offset > 0 {
		err = db.Select(&roles, "SELECT * FROM entity.role OFFSET $1", offset)
	} else {
		err = db.Select(&roles, "SELECT * FROM entity.role")
	}
	return roles, err
}
