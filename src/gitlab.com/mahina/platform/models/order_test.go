// +build integration all

package models

import (
	"testing"

	"fmt"

	"github.com/guregu/null/zero"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

func TestInsertOrder(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create user", err)
	}
	err = user.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert user", err)
	}

	order, err := insertOrder(tx, org.OrgUUID.String, user.UserUUID.String, "", schema)
	if err != nil {
		t.Fatal("Unable to insert order", err)
	}
	assert.Equal(t, true, order.OrderID > 0, "Expected OrderID to be non-zero")
	assert.True(t, order.Exists(tx), "Expected to find order")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestUpdateOrder(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create user", err)
	}
	err = user.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert user", err)
	}
	order, err := insertOrder(tx, org.OrgUUID.String, user.UserUUID.String, "", schema)
	if err != nil {
		t.Fatal("Unable to insert order", err)
	}

	notes := order.Notes
	order.Notes = zero.StringFrom("Here are some new notes")
	err = order.Update(tx)
	if err != nil {
		t.Fatal("Unable to update order", err)
	}
	assert.NotEqual(t, notes, order.Notes)

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestDeleteOrder(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create user", err)
	}
	err = user.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert user", err)
	}
	order, err := insertOrder(tx, org.OrgUUID.String, user.UserUUID.String, "", schema)
	if err != nil {
		t.Fatal("Unable to insert order", err)
	}
	err = order.Delete(tx)
	if err != nil {
		t.Fatal("Unable to delete order", err)
	}
	assert.False(t, order.Exists(tx), "Expected to not be able to find delete order")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindUnknownOrder(t *testing.T) {
	nonExistentUUID := "fdd93e34-c87d-11e6-804b-06f1521cd849"
	order, _ := GetOrderByUUID(nil, nonExistentUUID, schema)
	assert.Nil(t, order, "Expected to not find a order")
}

func TestFindOrderByID(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create user", err)
	}
	err = user.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert user", err)
	}
	order, err := insertOrder(tx, org.OrgUUID.String, user.UserUUID.String, "", schema)
	if err != nil {
		t.Fatal("Unable to insert order", err)
	}

	order, err = GetOrderByOrderID(tx, order.OrderID, schema)
	if order == nil {
		t.Fatal("Unexpected null order")
	}
	assert.Nil(t, err, "Error in find by ID")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindOrderByUUID(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create user", err)
	}
	err = user.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert user", err)
	}
	order, err := insertOrder(tx, org.OrgUUID.String, user.UserUUID.String, "", schema)
	if err != nil {
		t.Fatal("Unable to insert order", err)
	}

	order, err = GetOrderByUUID(tx, order.OrderUUID.String, schema)
	if order == nil {
		t.Fatal("Unexpected null order")
	}
	assert.Nil(t, err, "Error in find by UUID")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestGetOrders(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create user", err)
	}
	err = user.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert user", err)
	}
	_, err = insertOrder(tx, org.OrgUUID.String, user.UserUUID.String, "", schema)
	if err != nil {
		t.Fatal("Unable to insert order", err)
	}

	orders, err := GetAllOrders(tx, -1, -1, schema, "", "", "")
	assert.NoError(t, err, "Expected to be able to find all orders")
	assert.NotEmpty(t, orders, "Expected result from find all orders to be not empty")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestGetOrdersByBuyerUUIDTerm(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create user", err)
	}
	err = user.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert user", err)
	}
	_, err = insertOrder(tx, org.OrgUUID.String, user.UserUUID.String, "", schema)
	if err != nil {
		t.Fatal("Unable to insert order", err)
	}

	orders, err := GetAllOrders(tx, -1, -1, schema, org.OrgUUID.String, "", "")
	assert.NoError(t, err, "Expected to be able to find all orders")
	assert.NotEmpty(t, orders, "Expected result from find all orders to be not empty")
	assert.Len(t, orders, 1, "Expected to find one order")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

/*func TestFindOrderExpanded(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	order, err := insertOrder(tx)
	if err != nil {
		t.Fatal("Unable to insert order", err)
	}
	for i := 0; i < 3; i++ {
		_, err := insertImage(tx, order)
		if err != nil {
			t.Fatal("Unable to insert order image", err)
		}
	}
	_, err = insertPricing(tx, order, 0, 10.9, 1.10)
	_, err = insertPricing(tx, order, 11, 20.9, 0.95)
	_, err = insertPricing(tx, order, 21, 30, 0.75)

	_, err = insertAttribute(tx, order, "organic")
	_, err = insertAttribute(tx, order, "pastured")

	expanded, err := GetOrderExpanded(tx, order.UUID.String, schema)
	if err != nil {
		t.Fatal("Unable to find order expanded:", err)
	}
	assert.Len(t, expanded.Images, 3, "Expected expanded to find a certain number of images")
	assert.Len(t, expanded.Pricing, 3, "Expected expanded to find a certain number of pricing entries")
	assert.Len(t, expanded.Attributes, 2, "Expected expanded to find a certain number of attribute entries")

	//fmt.Println(expanded)

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}*/

/*func insertMultipleOrders(tx *sqlx.Tx, supplierUUID string) error {
	var orders = []string{
		`{
			"productCode": "1-12222112-2",
			"supplierUUID": "%s",
			"uom": "pound",
			"status": "active",
			"title": "Green Beans",
			"description": "These are great beans",
			"category": "001.006"
		}`,
		`{
			"productCode": "1-12222112-3",
			"supplierUUID": "%s",
			"uom": "pound",
			"status": "active",
			"title": "Basil",
			"description": "Genovese",
			"category": "001.006.010"
		}`,
		`{
			"productCode": "1-12222112-4",
			"supplierUUID": "%s",
			"uom": "bunch",
			"status": "active",
			"title": "Rodent Hair",
			"description": "Braided Rat Fur",
			"category": "001.016"
		}`,
		`{
			"productCode": "1-12222112-5",
			"supplierUUID": "%s",
			"uom": "pound",
			"status": "active",
			"title": "Meyer Lemons",
			"description": "Really yellow",
			"category": "001.002.003"
		}`,
		`{
			"productCode": "1-12222112-6",
			"supplierUUID": "%s",
			"uom": "pound",
			"status": "active",
			"title": "Red Raspberries",
			"description": "Lorem Ipsum",
			"category": "001.002.004"
		}`,
		`{
			"productCode": "1-12222112-7",
			"supplierUUID": "%s",
			"uom": "pound",
			"status": "active",
			"title": "Pricky Pear beans",
			"description": "Lorem Ipsum",
			"category": "001.002.005"
		}`,
	}

	for _, v := range orders {
		json := fmt.Sprintf(v, supplierUUID)
		order, err := NewOrder(tx, []byte(json), schema)
		if err != nil {
			return err
		}
		err = order.Insert(tx)
		if err != nil {
			return err
		}
	}
	return nil
}*/

const orderJSON = `{
	"buyerUUID": "%s",
	"buyerUserUUID": "%s",
	"type": "spot",
	"deliveryDate": "2017-07-01",
	"notes": "leave on loading dock. -H"
}`

func insertOrder(tx *sqlx.Tx, buyerUUID string, buyerUserUUID string, json string, schema string) (*Order, error) {
	if json == "" {
		json = fmt.Sprintf(orderJSON, buyerUUID, buyerUserUUID)
	}
	order, err := NewOrder(tx, []byte(json), schema)
	if err != nil {
		return nil, err
	}
	err = order.Insert(tx)
	if err != nil {
		return nil, err
	}
	return order, err
}

const standingOrderJSON = `{
	"buyerUUID": "%s",
	"buyerUserUUID": "%s",
	"type": "standing",
	"deliveryDate": "2017-07-01",
	"lastDeliveryDate": "2017-11-01",
	"notes": "leave on loading dock. -H"	
}`

func insertStandingOrder(tx *sqlx.Tx, buyerUUID string, buyerUserUUID string, json string, schema string) (*Order, error) {
	if json == "" {
		json = fmt.Sprintf(standingOrderJSON, buyerUUID, buyerUserUUID)
	}
	order, err := NewOrder(tx, []byte(json), schema)
	if err != nil {
		return nil, err
	}
	err = order.Insert(tx)
	if err != nil {
		return nil, err
	}
	return order, err
}
