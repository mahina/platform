package models

import (
	"bytes"
	"encoding/json"
	"errors"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

// UserRole defines a User to Role association in the system
type UserRole struct {
	UserID int64  `db:"user_id" json:"userID"`
	RoleID string `db:"role_id" json:"roleID"`
}

// NewRole creates a Role struct from a passed JSON string. No database
// events occur from this call.
func NewUserRole(userID int64, roleID string) (*UserRole, error) {
	record := UserRole{UserID: userID, RoleID: roleID}
	return &record, nil
}

// Get performs a SELECT, using RoleID as the lookup key. The Role object
// is updated with the attributes of the found Role.
func (ur *UserRole) Get(tx *sqlx.Tx) (err error) {
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(ur, "SELECT * FROM entity.user_role WHERE user_id = $1 AND role_id = $2", ur.UserID, ur.RoleID)
	} else {
		err = tx.Get(ur, "SELECT * FROM entity.user_role WHERE user_id = $1 AND role_id = $2", ur.UserID, ur.RoleID)
	}
	return
}

// Valid returns the validity of the Role object
func (ur *UserRole) Valid() bool {
	return len(ur.RoleID) > 0 && ur.UserID > 0
}

// Exists tests for the existence of a Role record corresponding to the RoleID value
func (ur *UserRole) Exists(tx *sqlx.Tx) bool {
	if ur.UserID == 0 || len(ur.RoleID) == 0 {
		return false
	}
	err := ur.Get(tx)
	if err != nil {
		return false
	}
	return ur.UserID > 0 && len(ur.RoleID) > 0
}

const userRoleInsertStatement = `INSERT INTO entity.user_role (` +
	`user_id, role_id` +
	`) VALUES (` +
	`$1, $2` +
	`)`

// Insert performs an SQL INSERT statement using the Role struct fields as attributes.
func (ur *UserRole) Insert(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(userRoleInsertStatement, ur.UserID, ur.RoleID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = tx.Get(ur, "SELECT * FROM entity.user_role WHERE user_id = $1 AND role_id = $2", ur.UserID, ur.RoleID)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation, using RoleID as the lookup key.
func (ur *UserRole) Delete(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec("DELETE FROM entity.user_role WHERE user_id = $1 AND role_id = $2", ur.UserID, ur.RoleID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		ur.UserID = 0
		ur.RoleID = ""
	}
	return
}

func (ur *UserRole) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(ur)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetAllRolesForUserUUIDOrName returns the roles that the userUUIDOrName is assigned
func GetAllRolesForUserUUIDOrName(tx *sqlx.Tx, userUUIDOrName string) ([]UserRole, error) {
	user, err := GetUserByUUIDOrUsername(tx, userUUIDOrName)
	if !user.Valid() {
		return nil, errors.New("Cannot find user")
	}
	if err != nil {
		return nil, err
	}
	roles := []UserRole{}
	if tx == nil {
		db, err := GetDB()
		if err != nil {
			return nil, err
		}
		err = db.Select(&roles, "SELECT * FROM entity.user_role WHERE user_id = $1", user.UserID)
	} else {
		err = tx.Select(&roles, "SELECT * FROM entity.user_role WHERE user_id = $1", user.UserID)
	}
	return roles, err
}
