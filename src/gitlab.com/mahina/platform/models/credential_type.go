package models

import (
	"database/sql/driver"
	"errors"
)

// CredentialType is the 'credential_type' enum type from schema 'public'.
type CredentialType uint16

const (
	// CredentialTypePassword is the 'password' CredentialType.
	CredentialTypePassword = CredentialType(1)
)

func (ct CredentialType) String() string {
	var enumVal string

	switch ct {
	case CredentialTypePassword:
		enumVal = "password"
	}

	return enumVal
}

// MarshalText marshals CredentialType into text.
func (ct CredentialType) MarshalText() ([]byte, error) {
	return []byte(ct.String()), nil
}

// UnmarshalText unmarshals CredentialType from text.
func (ct *CredentialType) UnmarshalText(text []byte) error {
	switch string(text) {
	case "password":
		*ct = CredentialTypePassword
	default:
		return errors.New("invalid CredentialType")
	}

	return nil
}

// Value satisfies the sql/driver.Valuer interface for CredentialType.
func (ct CredentialType) Value() (driver.Value, error) {
	return ct.String(), nil
}

// Scan satisfies the database/sql.Scanner interface for CredentialType.
func (ct *CredentialType) Scan(src interface{}) error {
	buf, ok := src.([]byte)
	if !ok {
		return errors.New("invalid CredentialType")
	}

	return ct.UnmarshalText(buf)
}
