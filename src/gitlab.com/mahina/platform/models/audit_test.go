// +build integration all

package models

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestInsertAudit(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}

	audit, err := newAuditRecord("", "", "insert")
	assert.NoError(t, err, "failed to get new audit record")
	err = audit.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert audit", err)
	}

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestGetAuditRecords(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}

	for i := 0; i < 20; i++ {
		audit, err := newAuditRecord("", "", "insert")
		assert.NoError(t, err, "failed to get new audit record")
		err = audit.Insert(tx)
		if err != nil {
			t.Fatal("Unable to insert audit", err)
		}
	}
	var nilTime time.Time
	records, err := GetAuditRecords(tx, 0, 0, schema, "", "", "insert", nilTime, nilTime)
	assert.NoError(t, err, "failed to find records")
	assert.True(t, len(records) >= 20, "expected to find a certain number of records")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

const AuditJSON = `{
	"entityUUID": "%s",
	"entityType": "%s",
	"action": "%s"
}`

func newAuditRecord(entityUUID string, entityType string, action string) (*Audit, error) {
	json := fmt.Sprintf(AuditJSON, entityUUID, entityType, action)
	audit, err := NewAudit([]byte(json), schema)
	return audit, err
}
