// +build integration all

package models

import (
	"testing"

	"github.com/guregu/null/zero"
	"github.com/stretchr/testify/assert"
)

func TestInsertRole(t *testing.T) {
	role, err := NewRole([]byte(roleJSON))
	if err != nil {
		t.Fatal("Unable to create role", err)
	}
	err = role.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert role", err)
	}
	assert.Equal(t, true, role.Valid(), "Expected Role to be valid")
	assert.True(t, role.Exists(nil), "Expected to find role")

	// clean up
	err = role.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete role (clean up)", err)
	}
}

func TestDuplicateRoleInsert(t *testing.T) {
	role, err := NewRole([]byte(roleJSON))
	if err != nil {
		t.Fatal("Unable to create role", err)
	}
	err = role.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert role", err)
	}
	assert.Equal(t, true, role.Valid(), "Expected Role to be valid")
	assert.True(t, role.Exists(nil), "Expected to find role")

	err = role.Insert(nil)
	assert.NotNil(t, err, "Expected error when inserting duplicate key")

	// clean up
	err = role.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete role (clean up)", err)
	}
}

func TestUpdateRole(t *testing.T) {
	role, err := NewRole([]byte(roleJSON))
	if err != nil {
		t.Fatal("Unable to create role", err)
	}
	err = role.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert role", err)
	}
	assert.Equal(t, true, role.Valid(), "Expected Role to be valid")

	description := role.Description
	role.Description = zero.StringFrom("Here's a new description")
	err = role.Update(nil)
	if err != nil {
		t.Fatal("Unable to update role", err)
	}
	assert.NotEqual(t, description, role.Description)

	// clean up
	err = role.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete role (clean up)", err)
	}
}

func TestGetRoles(t *testing.T) {
	roles, err := GetAllRoles(-1, -1)
	assert.NoError(t, err, "Expected to be able to find all roles")
	assert.NotEmpty(t, roles, "Expected result from find all roles to be not empty")
}

const roleJSON = `{
	"roleID": "newRole",
	"description": "A Shiny New Role"
}`
