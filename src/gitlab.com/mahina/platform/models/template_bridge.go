package models

import (
	"bytes"
	"encoding/json"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

// LeftRightBridge defines an Entity to Entity bridge in the system. This module is intended to be duplicated.
type LeftRightBridge struct {
	LeftID  int64 `db:"left_id" json:"leftID"`
	RightID int64 `db:"right_id" json:"rightID"`
}

// NewLeftRight creates a LeftRightBridge struct from a passed values. No database
// events occur from this call.
func NewLeftRight(leftID int64, rightID int64) (*LeftRightBridge, error) {
	record := LeftRightBridge{LeftID: leftID, RightID: rightID}
	return &record, nil
}

// Get performs a SELECT, using rightID as the lookup key. The Bridge object
// is updated with the attributes of the found Bridge.
func (leftRight *LeftRightBridge) Get(tx *sqlx.Tx) (err error) {
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(leftRight, "SELECT * FROM entity.left_right WHERE left_id = $1 AND right_id = $2", leftRight.LeftID, leftRight.RightID)
	} else {
		err = tx.Get(leftRight, "SELECT * FROM entity.left_right WHERE left_id = $1 AND right_id = $2", leftRight.LeftID, leftRight.RightID)
	}
	return
}

// Valid returns the validity of the Bridge object
func (leftRight *LeftRightBridge) Valid() bool {
	return leftRight.RightID > 0 && leftRight.LeftID > 0
}

// Exists tests for the existence of a LeftRightBridge record corresponding to the LeftID and RightID value
func (leftRight *LeftRightBridge) Exists(tx *sqlx.Tx) bool {
	if leftRight.LeftID == 0 || leftRight.RightID == 0 {
		return false
	}
	err := leftRight.Get(tx)
	if err != nil {
		return false
	}
	return leftRight.LeftID > 0 && leftRight.RightID > 0
}

const leftRightInsertStatement = `INSERT INTO entity.left_right (` +
	`left_id, right_id` +
	`) VALUES (` +
	`$1, $2` +
	`)`

// Insert performs an SQL INSERT statement using the Bridge struct fields as attributes.
func (leftRight *LeftRightBridge) Insert(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(leftRightInsertStatement, leftRight.LeftID, leftRight.RightID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = tx.Get(leftRight, "SELECT * FROM entity.left_right WHERE left_id = $1 AND right_id = $2", leftRight.LeftID, leftRight.RightID)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation
func (leftRight *LeftRightBridge) Delete(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec("DELETE FROM entity.left_right WHERE left_id = $1 AND right_id = $2", leftRight.LeftID, leftRight.RightID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		leftRight.LeftID = 0
		leftRight.RightID = 0
	}
	return
}

func (leftRight *LeftRightBridge) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(leftRight)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetAllRightsForLeftUUIDOrName returns all organization associations for the supplied user. This function
// does not support pagination.
/*
func GetAllRightsForLeftUUIDOrName(tx *sqlx.Tx, userUUIDOrName string) ([]LeftRightBridge, error) {
	user, err := GetLeftByUUIDOrLeftname(tx, userUUIDOrName)
	if !user.Valid() {
		return nil, errors.New("Cannot find user")
	}
	if err != nil {
		return nil, err
	}
	orgs := []LeftRightBridge{}
	if tx == nil {
		db, err := GetDB()
		if err != nil {
			return nil, err
		}
		err = db.Select(&orgs, "SELECT * FROM entity.left_right WHERE left_id = $1", user.LeftID)
	} else {
		err = tx.Select(&orgs, "SELECT * FROM entity.left_right WHERE left_id = $1", user.LeftID)
	}
	return orgs, err
}
*/

// GetAllLeftsInRight returns all primary keys of the entities associated with the rightID
func GetAllLeftsInRight(tx *sqlx.Tx, rightID int64) ([]int64, error) {
	var err error
	leftIDs := []int64{}
	if tx == nil {
		db, err := GetDB()
		if err != nil {
			return nil, err
		}
		err = db.Select(&leftIDs, "SELECT left_id FROM entity.left_right WHERE right_id = $1", rightID)
	} else {
		err = tx.Select(&leftIDs, "SELECT left_id FROM entity.left_right WHERE right_id = $1", rightID)
	}
	return leftIDs, err
}
