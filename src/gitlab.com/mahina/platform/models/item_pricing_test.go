// +build integration all

package models

import (
	"testing"

	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

func TestInsertItemPricing(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	itemPricing, err := insertPricing(tx, item, 0.5, 10.0, 1.25)
	if err != nil {
		t.Fatal("Unable to insert item pricing", err)
	}
	assert.Equal(t, 0.5, itemPricing.QuantityMin, "Expected certain value in item pricing insert")
	assert.Equal(t, 10.0, itemPricing.QuantityMax, "Expected certain value in item pricing insert")
	assert.Equal(t, 1.25, itemPricing.Price, "Expected certain value in item pricing insert")
	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestUpdateItemPricing(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	itemPricing, err := insertPricing(tx, item, 0, 10.0, 1.25)
	if err != nil {
		t.Fatal("Unable to insert item pricing", err)
	}

	price := itemPricing.Price
	itemPricing.Price = 1.30
	err = itemPricing.Update(tx)
	if err != nil {
		t.Fatal("Unable to update item", err)
	}
	assert.NotEqual(t, price, itemPricing.Price)

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestDeleteItemPricing(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	itemPricing, err := insertPricing(tx, item, 0, 10.0, 1.25)
	if err != nil {
		t.Fatal("Unable to insert item pricing", err)
	}

	err = itemPricing.Delete(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	assert.False(t, itemPricing.Exists(tx), "Expected to not be able to find deleted item pricing")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindUnknownItemPricing(t *testing.T) {
	nonExistentUUID := "fdd93e34-c87d-11e6-804b-06f1521cd849"
	item, _ := GetItemPricingByUUID(nil, nonExistentUUID, schema)
	assert.Nil(t, item, "Expected to not find a item pricing")
}

func TestFindItemPricingByID(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	itemPricing, err := insertPricing(tx, item, 0, 10.0, 1.25)
	if err != nil {
		t.Fatal("Unable to insert item pricing", err)
	}

	itemPricing, err = GetItemPricingByPricingID(tx, itemPricing.PricingID, schema)
	if itemPricing == nil {
		t.Fatal("Unexpected null item")
	}
	assert.Nil(t, err, "Error in find by ID")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindItemPricingByUUID(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	itemPricing, err := insertPricing(tx, item, 0, 10.0, 1.25)
	if err != nil {
		t.Fatal("Unable to insert item pricing", err)
	}

	itemPricing, err = GetItemPricingByUUID(tx, itemPricing.PricingUUID.String, schema)
	if item == nil {
		t.Fatal("Unexpected null item")
	}
	assert.Nil(t, err, "Error in find by UUID")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindItemPricingsForItem(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	for i := 0; i < 3; i++ {
		_, err = insertPricing(tx, item, 0, 10.0, 1.25)
		if err != nil {
			t.Fatal("Unable to insert item pricing", err)
		}
	}

	itemPricings, err := GetAllItemPricingIDsByItem(tx, item.ItemID, schema)
	if err != nil {
		t.Fatal("Unable to find item pricings", err)
	}
	assert.Len(t, itemPricings, 3, "Expected a certain number of item pricings to be found")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

const itemPricingJSON = `{
	"itemUUID": "%s",
	"quantityMin": %f,
	"quantityMax": %f,
	"price": %f
}`

// Helper Funcs
func insertPricing(tx *sqlx.Tx, item *Item, min float64, max float64, pricing float64) (*ItemPricing, error) {
	json := fmt.Sprintf(itemPricingJSON, item.ItemUUID.String, min, max, pricing)
	itemPricing, err := NewItemPricing(tx, []byte(json), schema)
	if err != nil {
		return nil, err
	}
	err = itemPricing.Insert(tx)
	if err != nil {
		return nil, err
	}
	return itemPricing, err
}
