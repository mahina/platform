// +build integration all

package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInsertLocation(t *testing.T) {
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	assert.Equal(t, true, org.OrgID > 0, "Expected OrgID to be non-zero")
	assert.True(t, org.Exists(nil), "Expected to find organization")

	loc, err := NewLocation([]byte(locationJSON))
	if err != nil {
		t.Fatal("Unable to create location", err)
	}
	loc.OrgID = org.OrgID
	err = loc.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert location", err)
	}
	assert.True(t, loc.Valid(), "Expected inserted location to be valid")
	assert.True(t, loc.Exists(nil), "Expected to find location")

	// clean up
	err = org.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete organization (clean up)", err)
	}
}

func TestInsertDuplicateLocation(t *testing.T) {
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	assert.Equal(t, true, org.OrgID > 0, "Expected OrgID to be non-zero")
	assert.True(t, org.Exists(nil), "Expected to find organization")

	loc, err := NewLocation([]byte(locationJSON))
	if err != nil {
		t.Fatal("Unable to create location", err)
	}
	loc.OrgID = org.OrgID
	err = loc.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert location", err)
	}
	assert.True(t, loc.Valid(), "Expected inserted location to be valid")
	assert.True(t, loc.Exists(nil), "Expected to find location")

	// try to reinsert again
	err = loc.Insert(nil)
	assert.Error(t, err, "Expected to fail insert duplicate location")

	// clean up
	err = org.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete organization (clean up)", err)
	}
}

func TestInsertLocationWithInvalidOrgID(t *testing.T) {
	loc, err := NewLocation([]byte(locationJSON))
	if err != nil {
		t.Fatal("Unable to create location", err)
	}
	loc.OrgID = -1
	err = loc.Insert(nil)
	assert.Error(t, err, "Expected to fail location insert with invalid org ID")
}

func TestInsertWithIncompleteObject(t *testing.T) {
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	assert.Equal(t, true, org.OrgID > 0, "Expected OrgID to be non-zero")
	assert.True(t, org.Exists(nil), "Expected to find organization")

	loc, err := NewLocation([]byte(locationMissingAddressLine1JSON))
	if err != nil {
		t.Fatal("Unable to create location", err)
	}
	loc.OrgID = org.OrgID
	err = loc.Insert(nil)
	assert.Error(t, err, "Expected to fail location insert with incomplete info")

	// clean up
	err = org.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete organization (clean up)", err)
	}

}

const locationJSON = `{
	"name": "uptown",
	"addressLine1": "123 Main Street",
	"addressLine2": "Unit 21",
	"locality": "Fairmount",
	"region": "New Hampshire",
	"postcode": "03042",
	"country": "USA",
	"phone": "603-555-1212"
}`

const locationMissingAddressLine1JSON = `{
	"name": "uptown",
	"addressLine2": "Unit 21",
	"locality": "Fairmount",
	"region": "New Hampshire",
	"postcode": "03042",
	"country": "USA",
	"phone": "603-555-1212"
}`
