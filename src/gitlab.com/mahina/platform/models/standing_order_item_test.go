// +build integration all

package models

import (
	"testing"

	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

func TestInsertStandingOrderItem(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	buyer, err := NewOrganization([]byte(buyerJSON))
	if err != nil {
		t.Fatal("Unable to create buyer", err)
	}
	err = buyer.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert buyer", err)
	}
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create user", err)
	}
	err = user.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert user", err)
	}
	order, err := insertOrder(tx, buyer.OrgUUID.String, user.UserUUID.String, "", schema)
	if err != nil {
		t.Fatal("Unable to insert order", err)
	}
	orderItem, err := insertStandingOrderItem(tx, order, item, nil, 1, 4.5, 1.25, "some notes")
	if err != nil {
		t.Fatal("Unable to insert order item", err)
	}
	assert.NotEmpty(t, orderItem.StandingOrderItemUUID.String, "Expected item UUID to be set")
	assert.Equal(t, 1.25, orderItem.Price, "Expected certain value in order item insert")
	assert.Len(t, orderItem.QuantityByDOW, 7, "Expected seven quantity dow values in order item insert")
	assert.Equal(t, 0.0, orderItem.QuantityByDOW[0], "Expected certain value of quantity dow values in order item insert")
	assert.Equal(t, 4.5, orderItem.QuantityByDOW[1], "Expected certain value of quantity dow values in order item insert")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestInsertStandingOrder(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	offer, err := insertOffer(tx, item)
	if err != nil {
		t.Fatal("Unable to insert offer", err)
	}
	buyer, err := NewOrganization([]byte(buyerJSON))
	if err != nil {
		t.Fatal("Unable to create buyer", err)
	}
	err = buyer.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert buyer", err)
	}
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create user", err)
	}
	err = user.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert user", err)
	}
	order, err := insertStandingOrder(tx, buyer.OrgUUID.String, user.UserUUID.String, "", schema)
	if err != nil {
		t.Fatal("Unable to insert order", err)
	}

	orderItem, err := insertStandingOrderItem(tx, order, item, offer, 1, 4.0, 1.25, "some notes")
	if err != nil {
		t.Fatal("Unable to insert order item", err)
	}
	assert.Equal(t, 1.25, orderItem.Price, "Expected certain value in order item insert")

	// second order
	order, err = insertStandingOrder(tx, buyer.OrgUUID.String, user.UserUUID.String, "", schema)
	if err != nil {
		t.Fatal("Unable to insert order", err)
	}

	orderItem, err = insertStandingOrderItem(tx, order, item, offer, 1, 4.0, 1.25, "some notes")
	if err != nil {
		t.Fatal("Unable to insert order item", err)
	}
	assert.Equal(t, 1.25, orderItem.Price, "Expected certain value in order item insert")

	// third order
	order, err = insertStandingOrder(tx, buyer.OrgUUID.String, user.UserUUID.String, "", schema)
	if err != nil {
		t.Fatal("Unable to insert order", err)
	}

	orderItem, err = insertStandingOrderItem(tx, order, item, offer, 1, 4.0, 1.25, "some notes")
	if err != nil {
		t.Fatal("Unable to insert order item", err)
	}
	assert.Equal(t, 1.25, orderItem.Price, "Expected certain value in order item insert")

	orders, err := offer.GetAllStandingOrderUUIDs(tx, schema)
	if err != nil {
		t.Fatal("Unable to get order uuids", err)
	}
	assert.Len(t, orders, 3, "Expected to find a certain number of orders")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestInsertStandingOrderItemDeletedOffer(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	offer, err := insertOffer(tx, item)
	if err != nil {
		t.Fatal("Unable to insert offer", err)
	}
	buyer, err := NewOrganization([]byte(buyerJSON))
	if err != nil {
		t.Fatal("Unable to create buyer", err)
	}
	err = buyer.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert buyer", err)
	}
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create user", err)
	}
	err = user.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert user", err)
	}
	order, err := insertOrder(tx, buyer.OrgUUID.String, user.UserUUID.String, "", schema)
	if err != nil {
		t.Fatal("Unable to insert order", err)
	}

	orderItem, err := insertStandingOrderItem(tx, order, item, offer, 1, 4.0, 1.25, "some notes")
	if err != nil {
		t.Fatal("Unable to insert order item", err)
	}
	assert.Equal(t, 1.25, orderItem.Price, "Expected certain value in order item insert")

	// delete the offer
	err = offer.Delete(tx)
	if err != nil {
		t.Fatal("Unable to delete offer", err)
	}

	err = orderItem.Get(tx)
	if err != nil {
		t.Fatal("Unable to get order item", err)
	}
	assert.True(t, orderItem.OfferID.IsZero(), "Expected offer ID to be nil following offer deletion")
	assert.Equal(t, "", orderItem.OfferUUID.String, "Expected offer UUID to be nil following offer deletion")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestUpdateStandingOrderItem(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	buyer, err := NewOrganization([]byte(buyerJSON))
	if err != nil {
		t.Fatal("Unable to create buyer", err)
	}
	err = buyer.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert buyer", err)
	}
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create user", err)
	}
	err = user.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert user", err)
	}
	order, err := insertOrder(tx, buyer.OrgUUID.String, user.UserUUID.String, "", schema)
	if err != nil {
		t.Fatal("Unable to insert order", err)
	}
	orderItem, err := insertStandingOrderItem(tx, order, item, nil, 1, 4.0, 1.25, "some notes")
	if err != nil {
		t.Fatal("Unable to insert order item", err)
	}
	assert.Equal(t, 1.25, orderItem.Price, "Expected certain value in order item insert")

	price := orderItem.Price
	orderItem.Price = 1.30
	err = orderItem.Update(tx)
	if err != nil {
		t.Fatal("Unable to update order item", err)
	}
	assert.NotEqual(t, price, orderItem.Price)

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestDeleteStandingOrderItem(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	buyer, err := NewOrganization([]byte(buyerJSON))
	if err != nil {
		t.Fatal("Unable to create buyer", err)
	}
	err = buyer.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert buyer", err)
	}
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create user", err)
	}
	err = user.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert user", err)
	}
	order, err := insertOrder(tx, buyer.OrgUUID.String, user.UserUUID.String, "", schema)
	if err != nil {
		t.Fatal("Unable to insert order", err)
	}
	orderItem, err := insertStandingOrderItem(tx, order, item, nil, 1, 4.0, 1.25, "some notes")
	if err != nil {
		t.Fatal("Unable to insert order item", err)
	}
	assert.Equal(t, 1.25, orderItem.Price, "Expected certain value in order item insert")

	err = orderItem.Delete(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	assert.False(t, orderItem.Exists(tx), "Expected to not be able to find deleted order item")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindUnknownStandingOrderItem(t *testing.T) {
	nonExistentUUID := "fdd93e34-c87d-11e6-804b-06f1521cd849"
	orderItem, _ := GetStandingOrderItemByUUID(nil, nonExistentUUID, schema)
	assert.Nil(t, orderItem, "Expected to not find a order item")
}

func TestFindStandingOrderItemByID(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	buyer, err := NewOrganization([]byte(buyerJSON))
	if err != nil {
		t.Fatal("Unable to create buyer", err)
	}
	err = buyer.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert buyer", err)
	}
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create user", err)
	}
	err = user.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert user", err)
	}
	order, err := insertOrder(tx, buyer.OrgUUID.String, user.UserUUID.String, "", schema)
	if err != nil {
		t.Fatal("Unable to insert order", err)
	}
	orderItem, err := insertStandingOrderItem(tx, order, item, nil, 1, 4.0, 1.25, "some notes")
	if err != nil {
		t.Fatal("Unable to insert order item", err)
	}
	assert.Equal(t, 1.25, orderItem.Price, "Expected certain value in order item insert")

	orderItem, err = GetStandingOrderItemByStandingOrderItemID(tx, orderItem.StandingOrderItemID, schema)
	if orderItem == nil {
		t.Fatal("Unexpected null item")
	}
	assert.Nil(t, err, "Error in find by ID")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindStandingOrderItemByUUID(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	buyer, err := NewOrganization([]byte(buyerJSON))
	if err != nil {
		t.Fatal("Unable to create buyer", err)
	}
	err = buyer.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert buyer", err)
	}
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create user", err)
	}
	err = user.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert user", err)
	}
	order, err := insertOrder(tx, buyer.OrgUUID.String, user.UserUUID.String, "", schema)
	if err != nil {
		t.Fatal("Unable to insert order", err)
	}
	orderItem, err := insertStandingOrderItem(tx, order, item, nil, 1, 4.0, 1.25, "some notes")
	if err != nil {
		t.Fatal("Unable to insert order item", err)
	}
	assert.Equal(t, 1.25, orderItem.Price, "Expected certain value in order item insert")

	orderItem, err = GetStandingOrderItemByUUID(tx, orderItem.StandingOrderItemUUID.String, schema)
	if item == nil {
		t.Fatal("Unexpected null item")
	}
	assert.Nil(t, err, "Error in find by UUID")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

const standingOrderItemJSON = `{
	"orderUUID": "%s",
	"supplierUUID": "%s",
	"itemUUID": "%s",
	"offerUUID": "%s",
	"lineItem": %d,
	"quantityByDOW": [0, %f, 0, 0, %f, 0, 0],
	"frequency": "weekly",
	"price": %f,
	"notes": "%s"
}`

// Helper Func
func insertStandingOrderItem(tx *sqlx.Tx, order *Order, item *Item, offer *SupplierOffer, lineItem int64, quantity float64, price float64, notes string) (*StandingOrderItem, error) {
	offerUUID := ""
	if offer != nil {
		offerUUID = offer.OfferUUID.String
	}
	json := fmt.Sprintf(standingOrderItemJSON, order.OrderUUID.String, item.SupplierUUID.String, item.ItemUUID.String,
		offerUUID, lineItem, quantity, quantity, price, notes)
	orderItem, err := NewStandingOrderItem(tx, []byte(json), schema)

	if err != nil {
		return nil, err
	}
	err = orderItem.Insert(tx)
	if err != nil {
		return nil, err
	}
	return orderItem, err
}
