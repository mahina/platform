package models

import (
	"bytes"
	"encoding/json"
	"errors"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

// UserOrganization defines a User to Organization association in the system
type UserOrganization struct {
	UserID int64 `db:"user_id" json:"userID"`
	OrgID  int64 `db:"org_id" json:"orgID"`
}

// NewUserOrganization creates a UserOrganization struct from a passed value. No database
// events occur from this call.
func NewUserOrganization(userID int64, orgID int64) (*UserOrganization, error) {
	record := UserOrganization{UserID: userID, OrgID: orgID}
	return &record, nil
}

// Get performs a SELECT, using orgID as the lookup key. The Role object
// is updated with the attributes of the found Role.
func (uo *UserOrganization) Get(tx *sqlx.Tx) (err error) {
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(uo, "SELECT * FROM entity.user_organization WHERE user_id = $1 AND org_id = $2", uo.UserID, uo.OrgID)
	} else {
		err = tx.Get(uo, "SELECT * FROM entity.user_organization WHERE user_id = $1 AND org_id = $2", uo.UserID, uo.OrgID)
	}
	return
}

// Valid returns the validity of the Role object
func (uo *UserOrganization) Valid() bool {
	return uo.OrgID > 0 && uo.UserID > 0
}

// Exists tests for the existence of a UserOrganization record corresponding to the UserID and OrgID value
func (uo *UserOrganization) Exists(tx *sqlx.Tx) bool {
	if uo.UserID == 0 || uo.OrgID == 0 {
		return false
	}
	err := uo.Get(tx)
	if err != nil {
		return false
	}
	return uo.UserID > 0 && uo.OrgID > 0
}

const userOrganizationInsertStatement = `INSERT INTO entity.user_organization (` +
	`user_id, org_id` +
	`) VALUES (` +
	`$1, $2` +
	`)`

// Insert performs an SQL INSERT statement using the Role struct fields as attributes.
func (uo *UserOrganization) Insert(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(userOrganizationInsertStatement, uo.UserID, uo.OrgID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = tx.Get(uo, "SELECT * FROM entity.user_organization WHERE user_id = $1 AND org_id = $2", uo.UserID, uo.OrgID)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation, using orgID as the lookup key.
func (uo *UserOrganization) Delete(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec("DELETE FROM entity.user_organization WHERE user_id = $1 AND org_id = $2", uo.UserID, uo.OrgID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		uo.UserID = 0
		uo.OrgID = 0
	}
	return
}

func (uo *UserOrganization) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(uo)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetAllOrganizationsForUserUUIDOrName returns all organization associations for the supplied user. This function
// does not support pagination.
func GetAllOrganizationsForUserUUIDOrName(tx *sqlx.Tx, userUUIDOrName string) ([]UserOrganization, error) {
	user, err := GetUserByUUIDOrUsername(tx, userUUIDOrName)
	if user == nil || !user.Valid() {
		return nil, errors.New("Cannot find user " + userUUIDOrName)
	}
	if err != nil {
		return nil, err
	}
	orgs := []UserOrganization{}
	if tx == nil {
		db, err := GetDB()
		if err != nil {
			return nil, err
		}
		err = db.Select(&orgs, "SELECT * FROM entity.user_organization WHERE user_id = $1", user.UserID)
	} else {
		err = tx.Select(&orgs, "SELECT * FROM entity.user_organization WHERE user_id = $1", user.UserID)
	}
	return orgs, err
}

// GetAllUsersInOrganization returns all the users associated with the orgID
func GetAllUsersInOrganization(tx *sqlx.Tx, orgID int64) ([]int64, error) {
	var err error
	userIDs := []int64{}
	if tx == nil {
		db, err := GetDB()
		if err != nil {
			return nil, err
		}
		err = db.Select(&userIDs, "SELECT user_id FROM entity.user_organization WHERE org_id = $1", orgID)
	} else {
		err = tx.Select(&userIDs, "SELECT user_id FROM entity.user_organization WHERE org_id = $1", orgID)
	}
	return userIDs, err
}

// UserInOrganization tests the association of a person (userUUID) with an organization (orgUUID)
func UserInOrganization(tx *sqlx.Tx, userUUID string, orgUUID string) (bool, error) {
	if userUUID == "" || orgUUID == "" {
		return false, nil
	}
	user, err := GetUserByUUID(tx, userUUID)
	if err != nil {
		return false, err
	}
	org, err := GetOrganizationByUUID(tx, orgUUID)
	if err != nil {
		return false, err
	}
	uo, _ := NewUserOrganization(user.UserID, org.OrgID)
	err = uo.Get(tx)
	return err == nil, nil
}
