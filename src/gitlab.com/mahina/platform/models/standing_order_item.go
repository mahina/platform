package models

import (
	"bytes"
	"encoding/json"
	"errors"

	"github.com/jmoiron/sqlx"
	"github.com/jmoiron/sqlx/types"
	pq "github.com/lib/pq"

	"fmt"

	"github.com/guregu/null"
	"github.com/guregu/null/zero"
)

// StandingOrderItem defines an line item in an order.
type StandingOrderItem struct {
	StandingOrderItemID   int64           `db:"order_item_id" json:"-"`
	StandingOrderItemUUID null.String     `db:"order_item_uuid" json:"orderItemUUID"`
	OrderID               int64           `db:"order_id" json:"-"`
	OrderUUID             null.String     `db:"order_uuid" json:"orderUUID"`
	SupplierID            int64           `db:"supplier_id" json:"-"`
	SupplierUUID          null.String     `db:"supplier_uuid" json:"supplierUUID"`
	ItemID                int64           `db:"item_id" json:"-"`
	ItemUUID              null.String     `db:"item_uuid" json:"itemUUID"`
	OfferID               null.Int        `db:"offer_id" json:"-"`
	OfferUUID             zero.String     `db:"offer_uuid" json:"offerUUID"`
	LineItem              int64           `db:"line_item" json:"lineItem"`
	QuantityByDOW         pq.Float64Array `db:"quantity_by_dow" json:"quantityByDOW"`
	Frequency             null.String     `db:"frequency" json:"frequency"`
	Price                 float64         `db:"price" json:"price"`
	Notes                 zero.String     `db:"notes" json:"notes"`
	Created               zero.Time       `db:"created" json:"created"`
	Extended              types.JSONText  `db:"extended" json:"extended,omitempty"`

	Schema string `db:"-" json:"-"`
}

// NewStandingOrderItem creates a StandingOrderItem struct from a passed JSON string. No database
// update events occur from this call.
func NewStandingOrderItem(tx *sqlx.Tx, jsonStr []byte, schema string) (*StandingOrderItem, error) {
	entity := StandingOrderItem{}
	entity.Schema = schema
	err := json.Unmarshal(jsonStr, &entity)
	if err != nil {
		return nil, err
	}
	if entity.OrderID == 0 {
		order, err := GetOrderByUUID(tx, entity.OrderUUID.String, schema)
		if err != nil {
			return nil, err
		}
		entity.OrderID = order.OrderID
	}
	if entity.SupplierID == 0 {
		org, err := GetOrganizationByUUID(tx, entity.SupplierUUID.String)
		if err != nil {
			return nil, err
		}
		entity.SupplierID = org.OrgID
	}
	if entity.ItemID == 0 {
		item, err := GetItemByUUID(tx, entity.ItemUUID.String, schema)
		if err != nil {
			return nil, err
		}
		entity.ItemID = item.ItemID
	}
	if entity.OfferID.Int64 == 0 && entity.OfferUUID.String != "" {
		offer, err := GetOfferByUUID(tx, entity.OfferUUID.String, schema)
		if err != nil {
			return nil, err
		}
		entity.OfferID = null.IntFrom(offer.OfferID)
	}
	return &entity, nil
}

// Get performs a SELECT, using StandingOrderItemID as the lookup key. The StandingOrderItem object
// is updated with the attributes of the found StandingOrderItem.
func (entity *StandingOrderItem) Get(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.standing_order_item WHERE order_item_id = $1", entity.Schema)
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(entity, statement, entity.StandingOrderItemID)
	} else {
		err = tx.Get(entity, statement, entity.StandingOrderItemID)
	}
	if err == nil {
		if entity.OfferID.IsZero() {
			entity.OfferUUID = zero.StringFrom("")
		}
	}
	return
}

// Valid returns the validity of the StandingOrderItem object
func (entity *StandingOrderItem) Valid() bool {
	return len(entity.StandingOrderItemUUID.String) > 0
}

// Exists tests for the existence of a StandingOrderItem record corresponding to the StandingOrderItemID value
func (entity *StandingOrderItem) Exists(tx *sqlx.Tx) bool {
	if entity.StandingOrderItemID <= 0 {
		return false
	}
	err := entity.Get(tx)
	if err != nil {
		return false
	}
	return entity.StandingOrderItemID > 0
}

const standingOrderItemInsertStatement = `INSERT INTO %s.standing_order_item (` +
	`order_id, order_uuid, supplier_id, supplier_uuid, item_id, item_uuid, offer_id, offer_uuid, line_item, quantity_by_dow, frequency, price, notes, extended` +
	`) VALUES (` +
	`$1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14` +
	`) RETURNING order_item_id`

// Insert performs an SQL INSERT statement using the StandingOrderItem struct fields as attributes. The struct
// will contain all DB-generated defaults, IDs, etc.
func (entity *StandingOrderItem) Insert(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(standingOrderItemInsertStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	row := tx.QueryRowx(statement, entity.OrderID, entity.OrderUUID, entity.SupplierID, entity.SupplierUUID,
		entity.ItemID, entity.ItemUUID, entity.OfferID, entity.OfferUUID, entity.LineItem, entity.QuantityByDOW, entity.Frequency, entity.Price, entity.Notes, entity.Extended)
	err = row.Err()
	if err != nil {
		goto end
	}
	err = row.Scan(&entity.StandingOrderItemID)
	if err != nil {
		goto end
	}
	if entity.StandingOrderItemID == 0 {
		err = errors.New("Primary key unexpectedly nil")
	}
end:
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

const standingOrderItemUpdateStatement = `UPDATE %s.standing_order_item SET (` +
	`order_id, order_uuid, supplier_id, supplier_uuid, item_id, item_uuid, offer_id, offer_uuid, line_item, quantity_by_dow, frequency, price, notes, extended` +
	`) = ( ` +
	`$1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14` +
	`) WHERE order_item_id = $15`

// Update performs an SQL UPDATE operation using the StandingOrderItem struct fields as attributes.
func (entity *StandingOrderItem) Update(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(standingOrderItemUpdateStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.OrderID, entity.OrderUUID, entity.SupplierID, entity.SupplierUUID,
		entity.ItemID, entity.ItemUUID, entity.OfferID, entity.OfferUUID, entity.LineItem, entity.QuantityByDOW,
		entity.Frequency, entity.Price, entity.Notes, entity.Extended,
		entity.StandingOrderItemID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation, using StandingOrderItemID as the lookup key.
func (entity *StandingOrderItem) Delete(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("DELETE FROM %s.standing_order_item WHERE order_item_id = $1", entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.StandingOrderItemID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		entity.StandingOrderItemID = 0
	}
	return
}

func (entity *StandingOrderItem) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(entity)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetStandingOrderItemByStandingOrderItemID performs a SELECT, using the passed StandingOrderItemID as the lookup key
// Returns nil if the entity does not exist--err may/may not be nil in that case.
func GetStandingOrderItemByStandingOrderItemID(tx *sqlx.Tx, entityID int64, schema string) (entity *StandingOrderItem, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.standing_order_item WHERE order_item_id = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(StandingOrderItem)
	if tx == nil {
		err = db.Get(entity, statement, entityID)
	} else {
		err = tx.Get(entity, statement, entityID)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	return
}

// GetStandingOrderItemByUUID performs a SELECT, using the passed UUID as the lookup key.
// Returns nil if the entity does not exist—-err may/may not be nil in that case.
func GetStandingOrderItemByUUID(tx *sqlx.Tx, uuid string, schema string) (entity *StandingOrderItem, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.standing_order_item WHERE order_item_uuid = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(StandingOrderItem)
	if tx == nil {
		err = db.Get(entity, statement, uuid)
	} else {
		err = tx.Get(entity, statement, uuid)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	return
}

// GetAllStandingOrderItemsForOrder returns all items from a standing order
func GetAllStandingOrderItemsForOrder(tx *sqlx.Tx, orderUUID string, schema string) ([]StandingOrderItem, error) {
	if schema == "" {
		err := errors.New("schema not specified")
		return nil, err
	}
	db, err := GetDB()
	if err != nil {
		return nil, err
	}
	items := []StandingOrderItem{}
	if tx == nil {
		err = db.Select(&items, fmt.Sprintf("SELECT * FROM %s.standing_order_item WHERE order_uuid = $1 ORDER BY line_item", schema), orderUUID)
	} else {
		err = tx.Select(&items, fmt.Sprintf("SELECT * FROM %s.standing_order_item WHERE order_uuid = $1 ORDER BY line_item", schema), orderUUID)
	}
	return items, err
}
