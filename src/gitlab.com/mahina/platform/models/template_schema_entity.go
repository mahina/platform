package models

import (
	"bytes"
	"encoding/json"
	"errors"

	"gitlab.com/mahina/platform/util"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"fmt"

	"github.com/guregu/null"
	"github.com/guregu/null/zero"
	"github.com/jmoiron/sqlx/types"
)

// Constants for the template entity
const (
	SchemaEntityStatusActive  = "active"
	SchemaEntityStatusDeleted = "deleted"
)

// SchemaEntity defines a template entity that's schema-based. This module is intended to be duplicated.
type SchemaEntity struct {
	EntityID   int64          `db:"entity_id" json:"-"` // serial ID's are internal and not shared with external systems
	EntityUUID null.String    `db:"entity_uuid" json:"entityUUID"`
	Status     zero.String    `db:"status" json:"status,omitempty"`
	Entityname null.String    `db:"entityname" json:"entityname" binding:"exists"`
	Date       null.Time      `db:"birth_date" json:"birthDate,omitempty"`
	ImageIcon  []byte         `db:"image_icon" json:"imageIcon,omitempty"`
	Created    zero.Time      `db:"created" json:"created"`
	Extended   types.JSONText `db:"extended" json:"extended,omitempty"`

	Schema string `db:"-" json:"-"`
}

// NewEntity creates a SchemaEntity struct from a passed JSON string. No database
// events occur from this call.
func NewSchemaEntity(jsonStr []byte, schema string) (*SchemaEntity, error) {
	entity := SchemaEntity{}
	entity.Schema = schema
	err := json.Unmarshal(jsonStr, &entity)
	if err != nil {
		return nil, err
	}
	return &entity, nil
}

// Get performs a SELECT, using EntityID as the lookup key. The SchemaEntity object
// is updated with the attributes of the found SchemaEntity.
func (entity *SchemaEntity) Get(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.entity WHERE entity_id = $1", entity.Schema)
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(entity, statement, entity.EntityID)
	} else {
		err = tx.Get(entity, statement, entity.EntityID)
	}
	return
}

// Valid returns the validity of the SchemaEntity object
func (entity *SchemaEntity) Valid() bool {
	return len(entity.Entityname.String) > 0
}

// Exists tests for the existence of a SchemaEntity record corresponding to the EntityID value
func (entity *SchemaEntity) Exists(tx *sqlx.Tx) bool {
	if entity.EntityID <= 0 {
		return false
	}
	err := entity.Get(tx)
	if err != nil {
		return false
	}
	return entity.EntityID > 0
}

const schemaEntityInsertStatement = `INSERT INTO %s.entity (` +
	`status, entityname, birth_date, image_icon, extended` +
	`) VALUES (` +
	`$1, $2, $3, $4, $5` +
	`) RETURNING entity_id`

// Insert performs an SQL INSERT statement using the SchemaEntity struct fields as attributes. The struct
// will contain all DB-generated defaults, IDs, etc.
func (entity *SchemaEntity) Insert(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(schemaEntityInsertStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	row := tx.QueryRowx(statement, entity.Status, entity.Entityname, entity.Date,
		entity.ImageIcon, entity.Extended)
	err = row.Err()
	if err != nil {
		goto end
	}
	err = row.Scan(&entity.EntityID)
	if err != nil {
		goto end
	}
	if entity.EntityID == 0 {
		err = errors.New("Primary key unexpectedly nil")
	}
end:
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

const schemaEntityUpdateStatement = `UPDATE %s.entity SET (` +
	`status, entityname, birth_date, image_icon, extended` +
	`) = ( ` +
	`$1, $2, $3, $4, $5` +
	`) WHERE entity_id = $6`

// Update performs an SQL UPDATE operation using the SchemaEntity struct fields as attributes.
func (entity *SchemaEntity) Update(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(schemaEntityUpdateStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.Status, entity.Entityname, entity.Date, entity.ImageIcon, entity.Extended,
		entity.EntityID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation, using EntityID as the lookup key.
func (entity *SchemaEntity) Delete(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("DELETE FROM %s.entity WHERE entity_id = $1", entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.EntityID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		entity.EntityID = 0
	}
	return
}

func (entity *SchemaEntity) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(entity)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetSchemaEntityByUUIDOrEntityname locates a SchemaEntity record by either a UUID or Entityname
func GetSchemaEntityByUUIDOrEntityname(tx *sqlx.Tx, entityUUIDOrEntityname string, schema string) (*SchemaEntity, error) {
	if util.IsUUID(entityUUIDOrEntityname) {
		return GetSchemaEntityByUUID(tx, entityUUIDOrEntityname, schema)
	}
	return GetSchemaEntityByEntityname(tx, entityUUIDOrEntityname, schema)
}

// GetSchemaEntityByEntityID performs a SELECT, using the passed EntityID as the lookup key
// Returns nil if the entity does not exist--err may/may not be nil in that case.
func GetSchemaEntityByEntityID(tx *sqlx.Tx, entityID int64, schema string) (entity *SchemaEntity, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.entity WHERE entity_id = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(SchemaEntity)
	if tx == nil {
		err = db.Get(entity, statement, entityID)
	} else {
		err = tx.Get(entity, statement, entityID)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	return
}

// GetSchemaEntityByUUID performs a SELECT, using the passed UUID as the lookup key.
// Returns nil if the entity does not exist—-err may/may not be nil in that case.
func GetSchemaEntityByUUID(tx *sqlx.Tx, uuid string, schema string) (entity *SchemaEntity, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.entity WHERE entity_uuid = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(SchemaEntity)
	if tx == nil {
		err = db.Get(entity, statement, uuid)
	} else {
		err = tx.Get(entity, statement, uuid)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	return
}

// GetSchemaEntityByEntityname performs a SELECT, using the passed entityname as the lookup key
// Returns nil if the entity does not exist—-err may/may not be nil in that case.
func GetSchemaEntityByEntityname(tx *sqlx.Tx, entityname string, schema string) (entity *SchemaEntity, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.entity WHERE entityname = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(SchemaEntity)
	if tx == nil {
		err = db.Get(entity, statement, entityname)
	} else {
		err = tx.Get(entity, statement, entityname)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	return
}

// GetAllSchemaEntitys returns all entitys. Pass -1 for either limit or offset
// to bypass query limits and/or offset position
func GetAllSchemaEntitys(limit int, offset int, schema string) ([]SchemaEntity, error) {
	if schema == "" {
		err := errors.New("schema not specified")
		return nil, err
	}
	db, err := GetDB()
	if err != nil {
		return nil, err
	}
	entitys := []SchemaEntity{}
	if limit > 0 && offset > 0 {
		err = db.Select(&entitys, fmt.Sprintf("SELECT * FROM %s.entity LIMIT $1 OFFSET $2", schema), limit, offset)
	} else if limit > 0 {
		err = db.Select(&entitys, fmt.Sprintf("SELECT * FROM %s.entity LIMIT $1", schema), limit)
	} else if offset > 0 {
		err = db.Select(&entitys, fmt.Sprintf("SELECT * FROM %s.entity OFFSET $1", schema), offset)
	} else {
		err = db.Select(&entitys, fmt.Sprintf("SELECT * FROM %s.entity", schema))
	}
	if err != nil {
		for _, v := range entitys {
			v.Schema = schema
		}
	}
	return entitys, err
}
