// +build integration all

package models

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestConvertUOM(t *testing.T) {

	uom, err := UOMTypeFromString("pound")
	require.NoError(t, err, "Expected to convert UOM")
	require.Equal(t, uom, UOMTypePound, "Expected UOM equality")
}
