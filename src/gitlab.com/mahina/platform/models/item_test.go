// +build integration all

package models

import (
	"testing"
	"time"

	"fmt"

	"github.com/guregu/null"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

const schema = "market"

func TestInsertItem(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	json := fmt.Sprintf(itemJSON, org.OrgUUID.String)
	item, err := NewItem(tx, []byte(json), schema)
	if err != nil {
		t.Fatal("Unable to create item", err)
	}
	err = item.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	assert.Equal(t, true, item.ItemID > 0, "Expected ItemID to be non-zero")
	assert.True(t, item.Exists(tx), "Expected to find item")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestUpdateItem(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	json := fmt.Sprintf(itemJSON, org.OrgUUID.String)
	item, err := NewItem(tx, []byte(json), schema)
	if err != nil {
		t.Fatal("Unable to create item", err)
	}
	err = item.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	assert.Equal(t, true, item.ItemID > 0, "Expected ItemID to be non-zero")
	assert.True(t, item.Exists(tx), "Expected to find item")

	title := item.Title
	item.Title = null.StringFrom("Fresh Green Beans")
	err = item.Update(tx)
	if err != nil {
		t.Fatal("Unable to update item", err)
	}
	assert.NotEqual(t, title, item.Title)

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestDeleteItem(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	json := fmt.Sprintf(itemJSON, org.OrgUUID.String)
	item, err := NewItem(tx, []byte(json), schema)
	if err != nil {
		t.Fatal("Unable to create item", err)
	}
	err = item.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	assert.Equal(t, true, item.ItemID > 0, "Expected ItemID to be non-zero")
	assert.True(t, item.Exists(tx), "Expected to find item")

	err = item.Delete(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	assert.False(t, item.Exists(tx), "Expected to not be able to find delete item")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindUnknownItem(t *testing.T) {
	nonExistentUUID := "fdd93e34-c87d-11e6-804b-06f1521cd849"
	item, _ := GetItemByUUID(nil, nonExistentUUID, schema)
	assert.Nil(t, item, "Expected to not find a item")
}

func TestFindItemByID(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	json := fmt.Sprintf(itemJSON, org.OrgUUID.String)
	item, err := NewItem(tx, []byte(json), schema)
	if err != nil {
		t.Fatal("Unable to create item", err)
	}
	err = item.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	assert.Equal(t, true, item.ItemID > 0, "Expected ItemID to be non-zero")

	item, err = GetItemByItemID(tx, item.ItemID, schema)
	if item == nil {
		t.Fatal("Unexpected null item")
	}
	assert.Nil(t, err, "Error in find by ID")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindItemByUUID(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	json := fmt.Sprintf(itemJSON, org.OrgUUID.String)
	item, err := NewItem(tx, []byte(json), schema)
	if err != nil {
		t.Fatal("Unable to create item", err)
	}
	err = item.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	assert.Equal(t, true, item.ItemID > 0, "Expected ItemID to be non-zero")

	item, err = GetItemByUUID(tx, item.ItemUUID.String, schema)
	if item == nil {
		t.Fatal("Unexpected null item")
	}
	assert.Nil(t, err, "Error in find by UUID")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestGetItems(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	json := fmt.Sprintf(itemJSON, org.OrgUUID.String)
	item, err := NewItem(tx, []byte(json), schema)
	if err != nil {
		t.Fatal("Unable to create item", err)
	}
	err = item.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	assert.Equal(t, true, item.ItemID > 0, "Expected ItemID to be non-zero")

	items, err := GetAllItems(tx, 0, 0, schema, "", "", "", "", nil, false, true, nil)
	assert.NoError(t, err, "Expected to be able to find all items")
	assert.NotEmpty(t, items, "Expected result from find all items to be not empty")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestGetItemsBySupplierUUIDTerm(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	json := fmt.Sprintf(itemJSON, org.OrgUUID.String)
	item, err := NewItem(tx, []byte(json), schema)
	if err != nil {
		t.Fatal("Unable to create item", err)
	}
	err = item.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	assert.Equal(t, true, item.ItemID > 0, "Expected ItemID to be non-zero")

	items, err := GetAllItems(tx, 0, 0, schema, "", org.OrgUUID.String, "", "", nil, false, true, nil)
	assert.NoError(t, err, "Expected to be able to find all items")
	assert.NotEmpty(t, items, "Expected result from find all items to be not empty")
	assert.Len(t, items, 1, "Expected to find one item")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindManyItemsByUUID(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	err = insertMultipleItems(tx, org.OrgUUID.String)
	if err != nil {
		t.Fatal("Unable to insert multiple items", err)
	}
	items, err := GetAllItems(tx, 0, 0, schema, "", org.OrgUUID.String, "", "", nil, false, true, nil)
	assert.NoError(t, err, "Expected to be able to find all items")
	assert.NotEmpty(t, items, "Expected result from find all items to be not empty")
	assert.Len(t, items, 6, "Expected to find all items")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindManyByCategory(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	err = insertMultipleItems(tx, org.OrgUUID.String)
	if err != nil {
		t.Fatal("Unable to insert multiple items", err)
	}
	items, err := GetAllItems(tx, 0, 0, schema, "", org.OrgUUID.String, "001.006", "", nil, false, true, nil)
	assert.NoError(t, err, "Expected to be able to find all items")
	assert.NotEmpty(t, items, "Expected result from find all items to be not empty")
	assert.Len(t, items, 2, "Expected to find all items")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindManyByTerm(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	err = insertMultipleItems(tx, org.OrgUUID.String)
	if err != nil {
		t.Fatal("Unable to insert multiple items", err)
	}
	items, err := GetAllItems(tx, 0, 0, schema, "", org.OrgUUID.String, "", "beans", nil, false, true, nil)
	assert.NoError(t, err, "Expected to be able to find all items")
	assert.NotEmpty(t, items, "Expected result from find all items to be not empty")
	assert.Len(t, items, 2, "Expected to find all items")

	items, err = GetAllItems(tx, 0, 0, schema, "", org.OrgUUID.String, "001.002", "beans", nil, false, true, nil)
	assert.NoError(t, err, "Expected to be able to find all items")
	assert.NotEmpty(t, items, "Expected result from find all items to be not empty")
	assert.Len(t, items, 1, "Expected to find all items")

	items, err = GetAllItems(tx, 0, 0, schema, "", org.OrgUUID.String, "001.002", "red | lemon", nil, false, true, nil)
	assert.NoError(t, err, "Expected to be able to find all items")
	assert.NotEmpty(t, items, "Expected result from find all items to be not empty")
	assert.Len(t, items, 2, "Expected to find all items")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindItemExpanded(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	for i := 0; i < 3; i++ {
		_, err := insertImage(tx, item)
		if err != nil {
			t.Fatal("Unable to insert item image", err)
		}
	}
	_, err = insertPricing(tx, item, 0, 10.9, 1.10)
	_, err = insertPricing(tx, item, 11, 20.9, 0.95)
	_, err = insertPricing(tx, item, 21, 30, 0.75)

	_, err = insertAttribute(tx, item, "organic")
	_, err = insertAttribute(tx, item, "pastured")

	expanded, err := GetItemExpanded(tx, item.ItemUUID.String, schema)
	if err != nil {
		t.Fatal("Unable to find item expanded:", err)
	}
	assert.Len(t, expanded.Images, 3, "Expected expanded to find a certain number of images")
	assert.Len(t, expanded.Pricing, 3, "Expected expanded to find a certain number of pricing entries")
	assert.Len(t, expanded.Attributes, 2, "Expected expanded to find a certain number of attribute entries")

	//fmt.Println(expanded)

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindManyItemsExpandedByUUID(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	err = insertMultipleItems(tx, org.OrgUUID.String)
	if err != nil {
		t.Fatal("Unable to insert multiple items", err)
	}
	items, err := GetAllItems(tx, 0, 0, schema, "", org.OrgUUID.String, "", "", nil, true, true, nil)
	assert.NoError(t, err, "Expected to be able to find all items")
	assert.Len(t, items, 6, "Expected to find all items")
	assert.Len(t, items[0].Pricing, 3, "Expected to find a certain number of extended pricing entries")

	/*
		var prettyJSON bytes.Buffer
		b, err := json.Marshal(items)
		err = json.Indent(&prettyJSON, b, "", "    ")
		fmt.Println(string(prettyJSON.Bytes()))
	*/

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestGetItemsAdvanced(t *testing.T) {
	limit := uint64(20)
	page := uint64(1)
	schema := "market"
	status := "active"
	orgID := ""
	category := "001"
	searchTerm := ""
	attributes := []string{"organic"}
	//attributes = nil
	expanded := false
	totalCount := uint64(0)

	timer := time.Now()
	results, err := GetAllItems(nil, limit, page, schema, status, orgID, category, searchTerm, attributes, expanded, true, &totalCount)
	fmt.Println("query took:", time.Now().Sub(timer))
	assert.NoError(t, err, "Expected to be able to find items")

	fmt.Println("result count: ", len(results))
	//for i := uint64(0); i < limit; i++ {
	//	fmt.Printf("result[%d]: %d, %s\n", i, results[i].ItemID, results[i].Created)
	//}
	fmt.Println("totalcount: ", totalCount)
}

func insertMultipleItems(tx *sqlx.Tx, supplierUUID string) error {
	var items = []string{
		`{
			"productCode": "1-12222112-2",
			"supplierUUID": "%s",
			"uom": "pound",
			"status": "active",
			"title": "Green Beans",
			"description": "These are great beans",
			"category": "001.006"
		}`,
		`{
			"productCode": "1-12222112-3",
			"supplierUUID": "%s",
			"uom": "pound",
			"status": "active",
			"title": "Basil",
			"description": "Genovese",
			"category": "001.006.010"
		}`,
		`{
			"productCode": "1-12222112-4",
			"supplierUUID": "%s",
			"uom": "bunch",
			"status": "active",
			"title": "Rodent Hair",
			"description": "Braided Rat Fur",
			"category": "001.016"
		}`,
		`{
			"productCode": "1-12222112-5",
			"supplierUUID": "%s",
			"uom": "pound",
			"status": "active",
			"title": "Meyer Lemons",
			"description": "Really yellow",
			"category": "001.002.003"
		}`,
		`{
			"productCode": "1-12222112-6",
			"supplierUUID": "%s",
			"uom": "pound",
			"status": "active",
			"title": "Red Raspberries",
			"description": "Lorem Ipsum",
			"category": "001.002.004"
		}`,
		`{
			"productCode": "1-12222112-7",
			"supplierUUID": "%s",
			"uom": "pound",
			"status": "active",
			"title": "Pricky Pear beans",
			"description": "Lorem Ipsum",
			"category": "001.002.005"
		}`,
	}

	for _, v := range items {
		json := fmt.Sprintf(v, supplierUUID)
		item, err := NewItem(tx, []byte(json), schema)
		if err != nil {
			return err
		}
		err = item.Insert(tx)
		if err != nil {
			return err
		}
		_, err = insertPricing(tx, item, 0, 10.9, 1.10)
		_, err = insertPricing(tx, item, 11, 20.9, 0.95)
		_, err = insertPricing(tx, item, 21, 30, 0.75)
		if err != nil {
			return err
		}

		_, err = insertAttribute(tx, item, "organic")
		_, err = insertAttribute(tx, item, "pastured")
		if err != nil {
			return err
		}
	}
	return nil
}

const itemJSON = `{
	"productCode": "1-12222112-2",
	"supplierUUID": "%s",
	"uom": "pound",
	"status": "active",
	"title": "Green Beans",
	"description": "These are great beans",
	"category": "001.006"
}`
