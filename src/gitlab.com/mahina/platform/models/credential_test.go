// +build integration all

package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCredentialInsert(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")
	assert.True(t, user.Exists(nil), "Expected to find user")

	password := "我的狗很好"
	err = InsertCredential(nil, user.UserUUID.String, CredentialTypePassword, []byte(user.Email.String), []byte(password))
	assert.NoError(t, err, "Expected to be able to insert credential")

	err = InsertCredential(nil, user.UserUUID.String, CredentialTypePassword, []byte(user.Email.String), []byte(password))
	assert.Error(t, err, "Expected an error attempted to insert duplicate credential")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
}

func TestCredentialExists(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")

	password := "내 개가 멋지다."
	err = InsertCredential(nil, user.UserUUID.String, CredentialTypePassword, []byte(user.Email.String), []byte(password))
	assert.NoError(t, err, "Expected to be able to insert credential")

	exists, err := ExistsCredential(nil, user.UserUUID.String, CredentialTypePassword)
	assert.True(t, exists, "Expected to find new credential")
	assert.NoError(t, err, "Error in testing existence of Credential")

	exists, err = ExistsCredential(nil, "this-is-not-a-valid-uuid", CredentialTypePassword)
	assert.False(t, exists, "Expected to not find bogus credential")
	assert.Error(t, err, "Expected an error in testing bogus existence of Credential")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
}

func TestCredentialUpdate(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")

	password := "내 개가 멋지다."
	err = InsertCredential(nil, user.UserUUID.String, CredentialTypePassword, []byte(user.Email.String), []byte(password))
	assert.NoError(t, err, "Expected to be able to insert credential")

	newPassword := "mynewshinypassword"
	err = UpdateCredential(nil, user.UserUUID.String, CredentialTypePassword, []byte(password), []byte(newPassword))
	assert.NoError(t, err, "Expected to be able to update credential")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
}

func TestCredentialVerify(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")

	password := "내 개가 멋지다."
	err = InsertCredential(nil, user.UserUUID.String, CredentialTypePassword, []byte(user.Email.String), []byte(password))
	assert.NoError(t, err, "Expected to be able to insert credential")

	_, err = VerifyCredential(nil, user.UserUUID.String, CredentialTypePassword, []byte(user.Email.String), []byte(password))
	assert.NoError(t, err, "Expected to be able to verify credential")

	newPassword := "mynewshinypassword™"
	err = UpdateCredential(nil, user.UserUUID.String, CredentialTypePassword, []byte(password), []byte(newPassword))
	assert.NoError(t, err, "Expected to be able to update credential")

	_, err = VerifyCredential(nil, user.UserUUID.String, CredentialTypePassword, []byte(user.Email.String), []byte(newPassword))
	assert.NoError(t, err, "Expected to be able to verify credential")

	_, err = VerifyCredential(nil, user.UserUUID.String, CredentialTypePassword, []byte(user.Email.String), []byte(password))
	assert.Error(t, err, "Expected invalid credential to fail")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
}

func TestCredentialDelete(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")

	password := "내 개가 멋지다."
	err = InsertCredential(nil, user.UserUUID.String, CredentialTypePassword, []byte(user.Email.String), []byte(password))
	assert.NoError(t, err, "Expected to be able to insert credential")

	exists, err := ExistsCredential(nil, user.UserUUID.String, CredentialTypePassword)
	assert.True(t, exists, "Expected to find inserted credential")

	err = DeleteCredential(nil, user.UserUUID.String, CredentialTypePassword)
	assert.NoError(t, err, "Expected to be able to delete credential")

	exists, err = ExistsCredential(nil, user.UserUUID.String, CredentialTypePassword)
	assert.False(t, exists, "Expected to NOT find deleted credential")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}

}
