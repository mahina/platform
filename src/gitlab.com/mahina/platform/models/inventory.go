package models

import (
	"bytes"
	"encoding/json"
	"errors"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"fmt"

	"time"

	"github.com/guregu/null"
	"github.com/guregu/null/zero"
	"github.com/jmoiron/sqlx/types"
)

// Inventory defines a Inventory entity that's schema-based.
type Inventory struct {
	ItemID     int64          `db:"item_id" json:"-"` // serial ID's are internal and not shared with external systems
	ItemUUID   null.String    `db:"item_uuid" json:"itemUUID"`
	Quantity   float64        `db:"quantity" json:"quantity"`
	RecordDate zero.Time      `db:"record_date" json:"recordDate"`
	Notes      zero.String    `db:"notes" json:"notes,omitempty"`
	Extended   types.JSONText `db:"extended" json:"extended,omitempty"`

	Schema string `db:"-" json:"-"`
}

// NewInventory creates a Inventory struct from a passed JSON string. No database
// update events occur from this call.
func NewInventory(tx *sqlx.Tx, jsonStr []byte, schema string) (*Inventory, error) {
	entity := Inventory{}
	entity.Schema = schema
	err := json.Unmarshal(jsonStr, &entity)
	if err != nil {
		return nil, err
	}
	if entity.ItemID == 0 {
		item, err := GetItemByUUID(tx, entity.ItemUUID.String, schema)
		if err != nil {
			return nil, err
		}
		entity.ItemID = item.ItemID
	}
	return &entity, nil
}

// Get performs a SELECT, accessing the latest record_date. The Inventory object
// is updated with the attributes of the found Inventory record.
func (entity *Inventory) Get(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.inventory ORDER BY record_date DESC LIMIT 1", entity.Schema)
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(entity, statement)
	} else {
		err = tx.Get(entity, statement)
	}
	return
}

const inventoryInsertStatement = `INSERT INTO %s.inventory (` +
	`item_id, item_uuid, quantity, record_date, notes, extended` +
	`) VALUES (` +
	`$1, $2, $3, $4, $5, $6` +
	`)`

// Insert performs an SQL INSERT statement using the Inventory struct fields as attributes. The struct
// will contain all DB-generated defaults, IDs, etc.
func (entity *Inventory) Insert(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(inventoryInsertStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	if !entity.RecordDate.Valid {
		entity.RecordDate = zero.NewTime(time.Now(), true)
	}
	_, err = tx.Exec(statement, entity.ItemID, entity.ItemUUID, entity.Quantity, entity.RecordDate, entity.Notes, entity.Extended)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

func (entity *Inventory) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(entity)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetAllInventoryRecordsByItemUUID returns all inventory records for a given item.
func GetAllInventoryRecordsByItemUUID(tx *sqlx.Tx, itemUUID string, schema string) ([]Inventory, error) {
	if schema == "" {
		err := errors.New("schema not specified")
		return nil, err
	}
	db, err := GetDB()
	if err != nil {
		return nil, err
	}
	inventories := []Inventory{}
	if tx == nil {
		err = db.Select(&inventories, fmt.Sprintf("SELECT * FROM %s.inventory WHERE item_uuid = $1 ORDER BY record_date DESC", schema), itemUUID)
	} else {
		err = tx.Select(&inventories, fmt.Sprintf("SELECT * FROM %s.inventory WHERE item_uuid = $1 ORDER BY record_date DESC", schema), itemUUID)
	}
	if err == nil {
		for i := range inventories {
			inventories[i].Schema = schema
		}
	}
	return inventories, err
}
