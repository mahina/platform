package models

import (
	"bytes"
	"encoding/json"

	"github.com/jmoiron/sqlx"
	"github.com/jmoiron/sqlx/types"
	_ "github.com/lib/pq"

	"github.com/guregu/null"
	"github.com/guregu/null/zero"
)

// Location defines one company location
type Location struct {
	OrgID        int64          `db:"org_id" json:"-"`                             // serialized ID's are internal
	Name         null.String    `db:"name" json:"name"`                            // name
	AddressLine1 null.String    `db:"address_line1" json:"addressLine1"`           // address_line1
	AddressLine2 zero.String    `db:"address_line2" json:"addressLine2,omitempty"` // address_line2
	AddressLine3 zero.String    `db:"address_line3" json:"addressLine3,omitempty"` // address_line3
	AddressLine4 zero.String    `db:"address_line4" json:"addressLine4,omitempty"` // address_line4
	Locality     null.String    `db:"locality" json:"locality"`                    // locality
	Region       null.String    `db:"region" json:"region"`                        // region
	Postcode     null.String    `db:"postcode" json:"postcode"`                    // postcode
	Country      null.String    `db:"country" json:"country"`                      // country
	Phone        zero.String    `db:"phone" json:"phone"`                          // phone
	FaxPhone     zero.String    `db:"fax_phone" json:"faxPhone"`                   // fax_phone
	Created      zero.Time      `db:"created" json:"created"`                      // created
	Extended     types.JSONText `db:"extended" json:"extended,omitempty"`          // extended
}

// NewLocation creates a Location struct from a passed JSON string. No database
// events occur from this call.
func NewLocation(jsonStr []byte) (*Location, error) {
	loc := Location{}
	err := json.Unmarshal(jsonStr, &loc)
	if err != nil {
		return nil, err
	}
	return &loc, nil
}

// Get performs a SELECT, using OrgID as the lookup key. The Location object
// is updated with the attributes of the found loc.
func (loc *Location) Get(tx *sqlx.Tx) (err error) {
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(loc, "SELECT * FROM entity.location WHERE org_id = $1 AND name = $2", loc.OrgID, loc.Name)
	} else {
		err = tx.Get(loc, "SELECT * FROM entity.location WHERE org_id = $1 AND name = $2", loc.OrgID, loc.Name)
	}
	return
}

// Valid returns the validity of the Location object
func (loc *Location) Valid() bool {
	return loc.OrgID > 0 && len(loc.Name.String) > 0
}

// Exists tests for the existence of an Location record corresponding to the OrgID value
func (loc *Location) Exists(tx *sqlx.Tx) bool {
	if loc.OrgID <= 0 {
		return false
	}
	err := loc.Get(tx)
	if err != nil {
		return false
	}
	return loc.OrgID > 0
}

const locInsertStatement = `INSERT INTO entity.location (` +
	`org_id, name, address_line1, address_line2, address_line3, address_line4, locality, region, postcode, country, phone, fax_phone, extended` +
	`) VALUES (` +
	`$1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13` +
	`)`

// Insert performs an SQL INSERT statement using the Location struct fields as attributes. The struct
// will contain all DB-generated defaults, IDs, etc.
func (loc *Location) Insert(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	if loc.Country.IsZero() {
		loc.Country = null.StringFrom("USA")
	}
	_, err = tx.Exec(locInsertStatement, loc.OrgID, loc.Name, loc.AddressLine1, loc.AddressLine2, loc.AddressLine3, loc.AddressLine4, loc.Locality, loc.Region, loc.Postcode, loc.Country, loc.Phone, loc.FaxPhone, loc.Extended)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = tx.Get(loc, "SELECT * FROM entity.Location WHERE org_id = $1 AND name = $2", loc.OrgID, loc.Name)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

const locUpdateStatement = `UPDATE entity.Location SET (` +
	`name, address_line1, address_line2, address_line3, address_line4, locality, region, postcode, country, phone, fax_phone, extended` +
	`) = ( ` +
	`$1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12` +
	`) WHERE org_id = $13 AND name = $14`

// Update performs an SQL UPDATE operation using the Location struct fields as attributes.
func (loc *Location) Update(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(locUpdateStatement, loc.Name, loc.AddressLine1, loc.AddressLine2, loc.AddressLine3, loc.AddressLine4,
		loc.Locality, loc.Region, loc.Postcode, loc.Country, loc.Phone, loc.FaxPhone, loc.Extended)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = tx.Get(loc, "SELECT * FROM entity.location WHERE org_id = $1 AND name = $2", loc.OrgID, loc.Name)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation, using OrgID as the lookup key.
func (loc *Location) Delete(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec("DELETE FROM entity.Location WHERE org_id = $1 AND name = $2", loc.OrgID, loc.Name)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		loc.OrgID = 0
	}
	return
}

func (loc *Location) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(loc)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetLocationByUUID locates a Location record by UUID
func GetLocationByOrgIDAndName(tx *sqlx.Tx, id int64, name string) (loc *Location, err error) {
	var db *sqlx.DB
	loc = new(Location)
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(loc, "SELECT * FROM entity.location WHERE org_id = $1 and name = $2", id, name)
		if err != nil {
			loc = nil
		}
	} else {
		err = tx.Get(loc, "SELECT * FROM entity.location WHERE org_id = $1 and name = $2", id, name)
		if err != nil {
			loc = nil
		}
	}
	return
}

// GetLocationsByOrgID performs a SELECT, using the passed OrgID as the lookup key
// Returns nil if the org does not exist--err may/may not be nil in that case.
func GetLocationsByOrgID(tx *sqlx.Tx, orgID int64) ([]Location, error) {
	var err error
	locs := []Location{}
	if tx == nil {
		db, err := GetDB()
		if err != nil {
			return nil, err
		}
		err = db.Select(&locs, "SELECT * FROM entity.location WHERE org_id = $1", orgID)
	} else {
		err = tx.Select(&locs, "SELECT * FROM entity.location WHERE org_id = $1", orgID)
	}
	return locs, err
}
