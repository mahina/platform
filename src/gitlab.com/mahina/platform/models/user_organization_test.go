// +build integration all

package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInsertUserOrganization(t *testing.T) {
	// set up
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")
	assert.True(t, user.Exists(nil), "Expected to find user")

	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}

	// test
	userOrg, err := NewUserOrganization(user.UserID, org.OrgID)
	err = userOrg.Insert(nil)
	assert.NoError(t, err, "Expected clean insert of UserOrganization")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
	err = org.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete organization (clean up)", err)
	}
}

func TestInsertInvalidUserOrg(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")
	assert.True(t, user.Exists(nil), "Expected to find user")

	// test
	userOrg, err := NewUserOrganization(user.UserID, 0)
	err = userOrg.Insert(nil)
	assert.Error(t, err, "Expected error inserting non-existent orgID")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
}

func TestDeleteUserOrg(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")
	assert.True(t, user.Exists(nil), "Expected to find user")

	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}

	// test
	userOrg, err := NewUserOrganization(user.UserID, org.OrgID)
	err = userOrg.Insert(nil)
	assert.NoError(t, err, "Expected clean insert of UserOrganization")

	err = userOrg.Delete(nil)
	assert.NoError(t, err, "Expected clean delete of UserOrganization")

	userOrg, err = NewUserOrganization(user.UserID, org.OrgID)
	err = userOrg.Insert(nil)
	assert.NoError(t, err, "Expected clean insert of UserOrg after deleting prior user org association")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
	err = org.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete organization (clean up)", err)
	}
}

func TestGetUserOrgs(t *testing.T) {
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")
	assert.True(t, user.Exists(nil), "Expected to find user")

	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	userOrg, err := NewUserOrganization(user.UserID, org.OrgID)
	err = userOrg.Insert(nil)
	assert.NoError(t, err, "Expected clean insert of UserOrganization")

	roles, err := GetAllOrganizationsForUserUUIDOrName(nil, user.UserUUID.String)
	assert.NoError(t, err, "Expected no error getting orgs for user")
	assert.Equal(t, 1, len(roles), "Expected to find one org")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
	err = org.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete organization (clean up)", err)
	}
}

func TestUserInOrganization(t *testing.T) {
	// set up
	user, err := NewUser([]byte(userJSON))
	if err != nil {
		t.Fatal("Unable to create person", err)
	}
	err = user.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert person", err)
	}
	assert.Equal(t, true, user.UserID > 0, "Expected UserID to be non-zero")
	assert.True(t, user.Exists(nil), "Expected to find user")

	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}

	org2, err := NewOrganization([]byte(org2JSON))
	if err != nil {
		t.Fatal("Unable to create organization (2)", err)
	}
	err = org2.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert organization (2)", err)
	}

	// test
	userOrg, err := NewUserOrganization(user.UserID, org.OrgID)
	err = userOrg.Insert(nil)
	assert.NoError(t, err, "Expected clean insert of UserOrganization")

	found, err := UserInOrganization(nil, user.UserUUID.String, org.OrgUUID.String)
	assert.NoError(t, err, "Expected successful call to UserInOrg")
	assert.True(t, found, "Expected to find user in organization")

	found, err = UserInOrganization(nil, user.UserUUID.String, org2.OrgUUID.String)
	assert.NoError(t, err, "Expected successful call to UserInOrg")
	assert.False(t, found, "Expected to not find user in organization")

	// clean up
	err = user.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete person (clean up)", err)
	}
	err = org.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete organization (clean up)", err)
	}
	err = org2.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete organization (2) (clean up)", err)
	}
}
