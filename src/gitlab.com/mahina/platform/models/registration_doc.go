package models

import (
	"bytes"
	"encoding/json"
	"errors"

	_ "github.com/lib/pq"

	"gitlab.com/mahina/platform/util"

	"github.com/guregu/null"
	"github.com/guregu/null/zero"
	"github.com/jmoiron/sqlx/types"
)

// RegistrationDoc defines a signup object in the system
type RegistrationDoc struct {
	User            User         `json:"user"`
	Roles           []string     `json:"roles"`
	Password        string       `json:"password"`
	PasswordConfirm string       `json:"passwordConfirm"`
	Organization    Organization `json:"organization"`
	Location        Location     `json:"location"`
	IPAddress       string       `json:"ipAddress"`
}

// NewRegistrationDoc creates a RegistrationDoc struct from a passed JSON string. No database
// events occur from this call.
func NewRegistrationDoc(jsonStr []byte) (*RegistrationDoc, error) {
	reg := RegistrationDoc{}
	err := json.Unmarshal(jsonStr, &reg)
	if err != nil {
		return nil, err
	}
	if reg.User.Status.IsZero() {
		reg.User.Status = zero.StringFrom(RegStateAppReceived)
	}
	if reg.Organization.Status.IsZero() {
		reg.Organization.Status = null.StringFrom(RegStateAppReceived)
	}
	return &reg, nil
}

// Valid tests the correctness of a RegistrationDoc
func (reg *RegistrationDoc) Valid() bool {
	return !reg.User.Username.IsZero() &&
		len(reg.Password) > 0 &&
		len(reg.PasswordConfirm) > 0 &&
		!reg.User.LastName.IsZero() &&
		len(reg.Roles) > 0 &&
		!reg.Organization.LegalName.IsZero() &&
		!reg.Location.AddressLine1.IsZero() &&
		!reg.Location.Region.IsZero() &&
		!reg.Location.Locality.IsZero() &&
		!reg.Location.Postcode.IsZero()
}

// EncryptPassword enciphers the two RegistrationDoc passwords, the password
// and the confirmed password. Do not be fazed by the two encoded passwords
// being inequal.
func (reg *RegistrationDoc) EncryptPassword() error {
	privateKey, err := util.GetPrivateRSAKey()
	if err != nil {
		return err
	}
	reg.Password, err = util.Encrypt(privateKey, []byte(reg.Password))
	if err != nil {
		return err
	}
	reg.PasswordConfirm, err = util.Encrypt(privateKey, []byte(reg.PasswordConfirm))
	return err
}

func (reg *RegistrationDoc) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(reg)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// Accept inserts the registration document into the db
func (reg *RegistrationDoc) Accept() (registration *Registration, err error) {
	privateKey, err := util.GetPrivateRSAKey()
	if err != nil {
		return nil, err
	}
	if len(privateKey) < 16 {
		return nil, errors.New("Private key length invalid")
	}
	password, err := util.Decrypt(privateKey, reg.Password)
	if err != nil {
		return nil, err
	}
	passwordConfirm, err := util.Decrypt(privateKey, reg.PasswordConfirm)
	if err != nil {
		return nil, err
	}
	if password != passwordConfirm {
		return nil, errors.New("Password confirmation failed")
	}
	user, err := GetUserByUsername(nil, reg.User.Username.String)
	if user != nil {
		return nil, errors.New("Username already registered")
	}
	registration = &Registration{
		Username:  zero.StringFrom(reg.User.Username.String),
		IPAddress: zero.StringFrom(reg.IPAddress),
		Extended:  types.JSONText(reg.String()),
	}
	err = registration.Insert(nil)
	return
}

// Process adds needed table records and connection relationships
func (reg *RegistrationDoc) Process() error {
	var org *Organization
	var userUUID string
	var orgID, userID int64
	var userOrg *UserOrganization
	var password string
	var privateKey []byte

	tx, err := BeginTX()
	if err != nil {
		return err
	}
	reg.User.Status = zero.StringFrom(UserStatusActive)
	err = reg.User.Insert(tx)
	userUUID = reg.User.UserUUID.String
	userID = reg.User.UserID
	if err != nil {
		goto end
	}
	if !util.IsUUID(userUUID) {
		err = errors.New("Invalid User UUID processing Registration")
		goto end
	}

	privateKey, err = util.GetPrivateRSAKey()
	if err != nil {
		err = errors.New("Error getting private rsa key: " + err.Error())
		goto end
	}
	password, err = util.Decrypt(privateKey, reg.Password)
	if err != nil {
		err = errors.New("Error decrypting password: " + err.Error())
		goto end
	}
	err = InsertCredential(tx, userUUID, CredentialTypePassword, []byte(reg.User.Email.String), []byte(password))
	if err != nil {
		err = errors.New("Error inserting credentials: " + err.Error())
		goto end
	}

	for _, v := range reg.Roles {
		userRole, err := NewUserRole(userID, v)
		if err != nil {
			err = errors.New("Error instantiating user-role: " + err.Error())
			goto end
		}
		err = userRole.Insert(tx)
		if err != nil {
			err = errors.New("Error inserting user-role: " + err.Error())
			goto end
		}
	}

	org, _ = GetOrganizationByExactLegalName(tx, reg.Organization.LegalName.String)
	if org == nil || !org.Valid() {
		err = reg.Organization.Insert(tx)
		if err != nil {
			err = errors.New("Error inserting organization: " + err.Error())
			goto end
		}
		orgID = reg.Organization.OrgID

		// assuming no new locations in v1
		reg.Location.OrgID = orgID
		err = reg.Location.Insert(tx)
		if err != nil {
			err = errors.New("Error inserting location: " + err.Error())
			goto end
		}
	} else {
		orgID = org.OrgID
	}
	userOrg, err = NewUserOrganization(userID, orgID)
	err = userOrg.Insert(tx)
	if err != nil {
		err = errors.New("Error inserting user-org mapping: " + err.Error())
	}

	// TODO: associate the organization with a marketplace

end:
	if err != nil {
		tx.Rollback()
	} else {
		err = tx.Commit()
	}
	return err
}
