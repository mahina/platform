package models

import (
	"database/sql/driver"
	"errors"
)

// OrganizationType is the 'organization_type' enum type from schema 'public'.
type OrganizationType uint16

const (
	// OrganizationTypeUnknown is the 'unknown' OrganizationType.
	OrganizationTypeUnknown = OrganizationType(1)

	// OrganizationTypeCharity is the 'charity' OrganizationType.
	OrganizationTypeCharity = OrganizationType(2)

	// OrganizationTypeNonprofit is the 'nonprofit' OrganizationType.
	OrganizationTypeNonprofit = OrganizationType(3)

	// OrganizationTypeGovt is the 'govt' OrganizationType.
	OrganizationTypeGovt = OrganizationType(4)

	// OrganizationTypeNgo is the 'ngo' OrganizationType.
	OrganizationTypeNgo = OrganizationType(5)

	// OrganizationTypeSole is the 'sole' OrganizationType.
	OrganizationTypeSole = OrganizationType(6)

	// OrganizationTypeLlc is the 'llc' OrganizationType.
	OrganizationTypeLlc = OrganizationType(7)

	// OrganizationTypeCorp is the 'corp' OrganizationType.
	OrganizationTypeCorp = OrganizationType(8)
)

// String returns the string value of the OrganizationType.
func (ot OrganizationType) String() string {
	var enumVal string

	switch ot {
	case OrganizationTypeUnknown:
		enumVal = "unknown"

	case OrganizationTypeCharity:
		enumVal = "charity"

	case OrganizationTypeNonprofit:
		enumVal = "nonprofit"

	case OrganizationTypeGovt:
		enumVal = "govt"

	case OrganizationTypeNgo:
		enumVal = "ngo"

	case OrganizationTypeSole:
		enumVal = "sole"

	case OrganizationTypeLlc:
		enumVal = "llc"

	case OrganizationTypeCorp:
		enumVal = "corp"
	}

	return enumVal
}

// MarshalText marshals OrganizationType into text.
func (ot OrganizationType) MarshalText() ([]byte, error) {
	return []byte(ot.String()), nil
}

// UnmarshalText unmarshals OrganizationType from text.
func (ot *OrganizationType) UnmarshalText(text []byte) error {
	switch string(text) {
	case "unknown":
		*ot = OrganizationTypeUnknown

	case "charity":
		*ot = OrganizationTypeCharity

	case "nonprofit":
		*ot = OrganizationTypeNonprofit

	case "govt":
		*ot = OrganizationTypeGovt

	case "ngo":
		*ot = OrganizationTypeNgo

	case "sole":
		*ot = OrganizationTypeSole

	case "llc":
		*ot = OrganizationTypeLlc

	case "corp":
		*ot = OrganizationTypeCorp

	default:
		return errors.New("invalid OrganizationType")
	}

	return nil
}

// Value satisfies the sql/driver.Valuer interface for OrganizationType.
func (ot OrganizationType) Value() (driver.Value, error) {
	return ot.String(), nil
}

// Scan satisfies the database/sql.Scanner interface for OrganizationType.
func (ot *OrganizationType) Scan(src interface{}) error {
	buf, ok := src.([]byte)
	if !ok {
		return errors.New("invalid OrganizationType")
	}

	return ot.UnmarshalText(buf)
}
