package models

import (
	"bytes"
	"encoding/json"
	"errors"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"fmt"

	"github.com/guregu/null"
)

// OfferPricing defines pricing information for an item that's schema-based.
type OfferPricing struct {
	PricingID   int64       `db:"pricing_id" json:"-"` // serial ID's are internal and not shared with external systems
	PricingUUID null.String `db:"pricing_uuid" json:"pricingUUID"`
	OfferID     int64       `db:"offer_id" json:"-"`
	OfferUUID   null.String `db:"offer_uuid" json:"offerUUID"`
	QuantityMin float64     `db:"quantity_min" json:"quantityMin"`
	QuantityMax float64     `db:"quantity_max" json:"quantityMax"`
	Price       float64     `db:"price" json:"price"`

	Schema string `db:"-" json:"-"`
}

// NewOfferPricing creates a OfferPricing struct from a passed JSON string. No database
// events occur from this call.
func NewOfferPricing(tx *sqlx.Tx, jsonStr []byte, schema string) (*OfferPricing, error) {
	entity := OfferPricing{}
	entity.Schema = schema
	err := json.Unmarshal(jsonStr, &entity)
	if err != nil {
		return nil, err
	}
	if entity.OfferID == 0 {
		offer, err := GetOfferByUUID(tx, entity.OfferUUID.String, schema)
		if err != nil {
			return nil, err
		}
		entity.OfferID = offer.OfferID
	}
	return &entity, nil
}

// Get performs a SELECT, using PricingID as the lookup key. The OfferPricing object
// is updated with the attributes of the found OfferPricing.
func (entity *OfferPricing) Get(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.offer_pricing WHERE pricing_id = $1", entity.Schema)
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(entity, statement, entity.PricingID)
	} else {
		err = tx.Get(entity, statement, entity.PricingID)
	}
	return
}

// Valid returns the validity of the OfferPricing object
func (entity *OfferPricing) Valid() bool {
	return len(entity.PricingUUID.String) > 0
}

// Exists tests for the existence of a OfferPricing record corresponding to the PricingID value
func (entity *OfferPricing) Exists(tx *sqlx.Tx) bool {
	if entity.PricingID <= 0 {
		return false
	}
	err := entity.Get(tx)
	if err != nil {
		return false
	}
	return entity.PricingID > 0
}

const offerPricingInsertStatement = `INSERT INTO %s.offer_pricing (` +
	`offer_id, offer_uuid, quantity_min, quantity_max, price` +
	`) VALUES (` +
	`$1, $2, $3, $4, $5` +
	`) RETURNING pricing_id`

// Insert performs an SQL INSERT statement using the OfferPricing struct fields as attributes. The struct
// will contain all DB-generated defaults, IDs, etc.
func (entity *OfferPricing) Insert(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(offerPricingInsertStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	row := tx.QueryRowx(statement, entity.OfferID, entity.OfferUUID, entity.QuantityMin, entity.QuantityMax, entity.Price)
	err = row.Err()
	if err != nil {
		goto end
	}
	err = row.Scan(&entity.PricingID)
	if err != nil {
		goto end
	}
	if entity.PricingID == 0 {
		err = errors.New("Primary key unexpectedly nil")
	}
end:
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

const offerPricingUpdateStatement = `UPDATE %s.offer_pricing SET (` +
	`offer_id, offer_uuid, quantity_min, quantity_max, price` +
	`) = ( ` +
	`$1, $2, $3, $4, $5` +
	`) WHERE pricing_id = $6`

// Update performs an SQL UPDATE operation using the OfferPricing struct fields as attributes.
func (entity *OfferPricing) Update(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(offerPricingUpdateStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.OfferID, entity.OfferUUID, entity.QuantityMin, entity.QuantityMax, entity.Price,
		entity.PricingID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation, using PricingID as the lookup key.
func (entity *OfferPricing) Delete(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("DELETE FROM %s.offer_pricing WHERE pricing_id = $1", entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.PricingID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		entity.PricingID = 0
	}
	return
}

func (entity *OfferPricing) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(entity)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetOfferPricingByPricingID performs a SELECT, using the passed PricingID as the lookup key
// Returns nil if the entity does not exist--err may/may not be nil in that case.
func GetOfferPricingByPricingID(tx *sqlx.Tx, entityID int64, schema string) (entity *OfferPricing, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.offer_pricing WHERE pricing_id = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(OfferPricing)
	if tx == nil {
		err = db.Get(entity, statement, entityID)
	} else {
		err = tx.Get(entity, statement, entityID)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	return
}

// GetOfferPricingByUUID performs a SELECT, using the passed UUID as the lookup key.
// Returns nil if the entity does not exist—-err may/may not be nil in that case.
func GetOfferPricingByUUID(tx *sqlx.Tx, uuid string, schema string) (entity *OfferPricing, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.offer_pricing WHERE pricing_uuid = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(OfferPricing)
	if tx == nil {
		err = db.Get(entity, statement, uuid)
	} else {
		err = tx.Get(entity, statement, uuid)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	return
}

// GetAllOfferPricingForOffer returns all offer pricing associated for a specified offer
func GetAllOfferPricingForOffer(tx *sqlx.Tx, offerID int64, schema string) (pricing []OfferPricing, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.offer_pricing WHERE offer_id = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	if tx == nil {
		err = db.Select(&pricing, statement, offerID)
	} else {
		err = tx.Select(&pricing, statement, offerID)
	}
	if err == nil {
		for i := range pricing {
			pricing[i].Schema = schema
		}
	}
	if len(pricing) == 0 {
		offer, err := GetOfferByOfferID(tx, offerID, schema)
		if err != nil {
			return pricing, err
		}
		if offer == nil {
			return pricing, errors.New("Non-existent offer")
		}
		itemPricings, err := GetAllItemPricingByItemUUID(tx, offer.ItemUUID.String, schema)
		for i := range itemPricings {
			p := OfferPricing{}
			p.OfferID = offer.OfferID
			p.OfferUUID = offer.ItemUUID
			p.Price = itemPricings[i].Price
			p.QuantityMin = itemPricings[i].QuantityMin
			p.QuantityMax = itemPricings[i].QuantityMax
			pricing = append(pricing, p)
		}
	}
	return
}
