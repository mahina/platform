package models

import (
	"bytes"
	"encoding/json"
	"errors"

	"gitlab.com/mahina/platform/util"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"fmt"

	"strings"

	"github.com/guregu/null"
	"github.com/guregu/null/zero"
	"github.com/jmoiron/sqlx/types"
)

// Constants for the offer entity
const (
	OfferStatusActive   = "active"
	OfferStatusInactive = "inactive"
	OfferStatusDeleted  = "deleted"
)

const (
	OfferTypeWillSupply  = "willsupply"
	OfferTypeCouldSupply = "couldsupply"
	OfferTypeWillBuy     = "willbuy"
	OfferTypeCouldBuy    = "couldbuy"
)

// SupplierOffer defines a schema-based offer entity
type SupplierOffer struct {
	OfferID      int64          `db:"offer_id" json:"-"` // serial ID's are internal and not shared with external systems
	OfferUUID    null.String    `db:"offer_uuid" json:"offerUUID"`
	SupplierID   int64          `db:"supplier_id" json:"-"`
	SupplierUUID null.String    `db:"supplier_uuid" json:"supplierUUID"`
	ItemID       int64          `db:"item_id" json:"-"` // serial ID's are internal and not shared with external systems
	ItemUUID     null.String    `db:"item_uuid" json:"itemUUID"`
	Status       zero.String    `db:"status" json:"status,omitempty"`
	Type         null.String    `db:"type" json:"type"`
	FirstDate    *util.NullDate `db:"first_date" json:"firstDate"`
	LastDate     *util.NullDate `db:"last_date" json:"lastDate"`
	Quantity     float64        `db:"quantity" json:"quantity"`
	DeliveryDays int64          `db:"delivery_days" json:"deliveryDays"` // bitmask, position 1 = monday, 2 = tuesday...
	Created      zero.Time      `db:"created" json:"created"`
	Modified     zero.Time      `db:"modified" json:"modified"`
	Notes        zero.String    `db:"notes" json:"notes"`
	Extended     types.JSONText `db:"extended" json:"extended,omitempty"`

	Title    string  `db:"title" json:"-"`
	Price    float64 `db:"price" json:"-"`
	Category string  `db:"category" json:"-"`
	Schema   string  `db:"-" json:"-"`
}

// NewOffer creates a Offer struct from a passed JSON string. No database
// events occur from this call.
func NewOffer(tx *sqlx.Tx, jsonStr []byte, schema string) (*SupplierOffer, error) {
	entity := SupplierOffer{}
	entity.Schema = schema
	err := json.Unmarshal(jsonStr, &entity)
	if err != nil {
		return nil, err
	}
	if entity.ItemID == 0 {
		item, err := GetItemByUUID(tx, entity.ItemUUID.String, schema)
		if err != nil {
			return nil, err
		}
		entity.ItemID = item.ItemID
	}
	if entity.SupplierID == 0 {
		supplier, err := GetOrganizationByUUID(tx, entity.SupplierUUID.String)
		if err != nil {
			return nil, err
		}
		entity.SupplierID = supplier.OrgID
	}
	return &entity, nil
}

// Get performs a SELECT, using OfferID as the lookup key. The Offer object
// is updated with the attributes of the found Offer.
func (entity *SupplierOffer) Get(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.offer WHERE offer_id = $1", entity.Schema)
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(entity, statement, entity.OfferID)
	} else {
		err = tx.Get(entity, statement, entity.OfferID)
	}
	return
}

// Valid returns the validity of the Offer object
func (entity *SupplierOffer) Valid() bool {
	return entity.SupplierID > 0 && entity.ItemID > 0
}

// Exists tests for the existence of a Offer record corresponding to the OfferID value
func (entity *SupplierOffer) Exists(tx *sqlx.Tx) bool {
	if entity.OfferID <= 0 {
		return false
	}
	err := entity.Get(tx)
	if err != nil {
		return false
	}
	return entity.OfferID > 0
}

const offerInsertStatement = `INSERT INTO %s.offer (` +
	`supplier_id, supplier_uuid, item_id, item_uuid, status, type, first_date, last_date, quantity, delivery_days, notes, extended` +
	`) VALUES (` +
	`$1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12` +
	`) RETURNING offer_id`

// Insert performs an SQL INSERT statement using the Offer struct fields as attributes. The struct
// will contain all DB-generated defaults, IDs, etc.
func (entity *SupplierOffer) Insert(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(offerInsertStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	row := tx.QueryRowx(statement, entity.SupplierID, entity.SupplierUUID, entity.ItemID, entity.ItemUUID,
		entity.Status, entity.Type, entity.FirstDate, entity.LastDate, entity.Quantity, entity.DeliveryDays, entity.Notes, entity.Extended)
	err = row.Err()
	if err != nil {
		goto end
	}
	err = row.Scan(&entity.OfferID)
	if err != nil {
		goto end
	}
	if entity.OfferID == 0 {
		err = errors.New("Primary key unexpectedly nil")
	}
end:
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

const offerUpdateStatement = `UPDATE %s.offer SET (` +
	`supplier_id, supplier_uuid, item_id, item_uuid, status, type, first_date, last_date, quantity, delivery_days, notes, extended` +
	`) = ( ` +
	`$1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12` +
	`) WHERE offer_id = $13`

// Update performs an SQL UPDATE operation using the Offer struct fields as attributes.
func (entity *SupplierOffer) Update(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(offerUpdateStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.SupplierID, entity.SupplierUUID, entity.ItemID, entity.ItemUUID,
		entity.Status, entity.Type, entity.FirstDate, entity.LastDate, entity.Quantity, entity.DeliveryDays, entity.Notes, entity.Extended,
		entity.OfferID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation, using OfferID as the lookup key.
func (entity *SupplierOffer) Delete(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("DELETE FROM %s.offer WHERE offer_id = $1", entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.OfferID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		entity.OfferID = 0
	}
	return
}

// bit position 0 is monday
func (entity *SupplierOffer) GetDeliveryDays() []string {
	return DecodeDeliveryDays(int(entity.DeliveryDays))
}

func (entity *SupplierOffer) SetDeliveryDays(days []string) {
	entity.DeliveryDays = int64(EncodeDeliveryDays(days))
}

// GetAllStandingOrderUUIDs returns the UUIDs of all active orders for this Offer
func (entity *SupplierOffer) GetAllStandingOrderUUIDs(tx *sqlx.Tx, schema string) (ids []string, err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(`select DISTINCT order_uuid FROM %s.standing_order_item WHERE order_uuid IN (select order_uuid FROM 
		%s.order where type = 'standing' AND last_delivery_date >= NOW()) AND offer_id = $1`, schema, schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	if tx == nil {
		err = db.Select(&ids, statement, entity.OfferID)
	} else {
		err = tx.Select(&ids, statement, entity.OfferID)
	}
	return
}

func (entity *SupplierOffer) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(entity)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

func (entity *SupplierOffer) Bytes() []byte {
	b, err := json.Marshal(entity)
	if err != nil {
		return []byte("Error encoding")
	}
	return b
}

// GetOfferByOfferID performs a SELECT, using the passed OfferID as the lookup key
// Returns nil if the entity does not exist--err may/may not be nil in that case.
func GetOfferByOfferID(tx *sqlx.Tx, entityID int64, schema string) (entity *SupplierOffer, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.offer WHERE offer_id = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(SupplierOffer)
	if tx == nil {
		err = db.Get(entity, statement, entityID)
	} else {
		err = tx.Get(entity, statement, entityID)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	return
}

// GetOfferByUUID performs a SELECT, using the passed UUID as the lookup key.
// Returns nil if the entity does not exist—-err may/may not be nil in that case.
func GetOfferByUUID(tx *sqlx.Tx, uuid string, schema string) (entity *SupplierOffer, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.offer WHERE offer_uuid = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(SupplierOffer)
	if tx == nil {
		err = db.Get(entity, statement, uuid)
	} else {
		err = tx.Get(entity, statement, uuid)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	return
}

// GetOffersForItem returns all offers associated for a specified item
func GetOffersForItem(tx *sqlx.Tx, itemUUID string, schema string) (offers []SupplierOffer, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.offer WHERE item_uuid = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	if tx == nil {
		err = db.Select(&offers, statement, itemUUID)
	} else {
		err = tx.Select(&offers, statement, itemUUID)
	}
	if err == nil {
		for i := range offers {
			offers[i].Schema = schema
		}
	}
	return
}

// GetAllOffers finds offers. Returns a slice of Offers up to limit records.
// If a pointer is assigned to totalCount, the total number of records for the
// search criteria is written to that address. The page parameter is one-based (not zero-based)
func GetAllOffers(tx *sqlx.Tx, limit uint64, page uint64, schema string, status string,
	organizationUUID string, category string, searchTerm string, attributes []string,
	expanded bool, encodeImages bool, orderBy string, totalCount *uint64) ([]SupplierOffer, error) {

	var err error
	if schema == "" {
		err = errors.New("schema not specified")
		return nil, err
	}
	offers := make([]SupplierOffer, 0)
	table := fmt.Sprintf("%s.offer o", schema)

	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	q := psql.Select("o.offer_id", "o.offer_uuid", "o.item_id", "o.item_uuid", "o.supplier_uuid", "o.type", "o.status",
		"o.first_date", "o.last_date", "o.quantity", "o.delivery_days", "o.created", "o.modified", "o.notes", "o.extended",
		"i.title", "i.category")
	cq := psql.Select("COUNT(o.offer_id)")
	q = q.From(table)
	cq = cq.From(table)
	q = q.Join(fmt.Sprintf("%s.item i USING (item_id)", schema))
	cq = cq.Join(fmt.Sprintf("%s.item i USING (item_id)", schema))
	q = q.Join("entity.organization e ON o.supplier_uuid = e.org_uuid")
	cq = cq.Join("entity.organization e ON o.supplier_uuid = e.org_uuid")
	if limit < 1 {
		limit = 20
	} else if limit > 100 {
		limit = 100
	}
	q = q.Limit(limit)
	if page < 1 {
		page = 1
	}
	offset := ((page - 1) * (limit))
	q = q.Offset(offset)
	if status != "" {
		q = q.Where("o.status = ?", status)
		cq = cq.Where("o.status = ?", status)
	}
	if organizationUUID != "" {
		q = q.Where("o.supplier_uuid = ?", organizationUUID)
		cq = cq.Where("o.supplier_uuid = ?", organizationUUID)
	}
	if category != "" {
		q = q.Where("i.category <@ ?", category)
		cq = cq.Where("i.category <@ ?", category)
	}
	if searchTerm != "" {
		terms := strings.Replace(searchTerm, " ", " & ", -1)
		searchStatement := "(to_tsvector('english', i.title) || to_tsvector('english', i.description) || to_tsvector('english', e.legal_name)) @@ to_tsquery('english', ?)"
		q = q.Where(searchStatement, terms)
		cq = cq.Where(searchStatement, terms)
	}
	if len(attributes) > 0 {
		attributeJoin := `JOIN (
            SELECT item_id FROM %s.item_attribute
            WHERE attribute = '%s'
        ) atts%d ON (atts%d.item_id = o.item_id)`
		for i := range attributes {
			val := fmt.Sprintf(attributeJoin, schema, attributes[i], i, i)
			q = q.JoinClause(val)
			cq = cq.JoinClause(val)
		}
	}

	if orderBy != "" {
		switch orderBy {
		case "category":
			fallthrough
		case "category:asc":
			q = q.OrderBy("i.category")
		case "category:desc":
			q = q.OrderBy("i.category DESC")
		case "startDate":
			fallthrough
		case "startDate:asc":
			q = q.OrderBy("o.first_date")
		case "startDate:desc":
			q = q.OrderBy("o.first_date DESC")
		case "title":
			fallthrough
		case "title:asc":
			q = q.OrderBy("i.title")
		case "title:desc":
			q = q.OrderBy("i.title DESC")
		case "quantity":
			fallthrough
		case "quantity:asc":
			q = q.OrderBy("o.quantity")
		case "quantity:desc":
			q = q.OrderBy("o.quantity DESC")
		case "price":
			fallthrough
		case "price:asc":
			q = q.Column("p.price")
			q = q.Join(`(
				select offer_id, MIN(price) as price 
				from market.offer_pricing 
				group by offer_id) p USING (offer_id)`)
			q = q.OrderBy("p.price")
		case "price:desc":
			q = q.Column("p.price")
			q = q.Join(`(
				select offer_id, MIN(price) as price 
				from market.offer_pricing 
				group by offer_id) p USING (offer_id)`)
			q = q.OrderBy("p.price DESC")
		}
	} else {
		q = q.OrderBy("o.created DESC")
	}
	statement, args, _ := q.ToSql()
	//if orderBy != "" {
	//	fmt.Println(SQLQueryDebugString(statement, args))
	//}
	if tx == nil {
		db, errDB := GetDB()
		if errDB != nil {
			return nil, errDB
		}
		err = db.Select(&offers, statement, args...)
		if err == nil && totalCount != nil {
			statement, args, _ := cq.ToSql()
			err = db.QueryRow(statement, args...).Scan(totalCount)
			if err != nil {
				return nil, err
			}
		}
	} else {
		err = tx.Select(&offers, statement, args...)
		if err == nil && totalCount != nil {
			statement, args, _ := cq.ToSql()
			err = tx.QueryRow(statement, args...).Scan(totalCount)
			if err != nil {
				return nil, err
			}
		}
	}
	if err == nil {
		for i := range offers {
			offers[i].Schema = schema
		}
	}
	return offers, err
}
