package models

import (
	"bytes"
	"encoding/json"
	"errors"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"fmt"

	"github.com/guregu/null"
	"github.com/guregu/null/zero"
)

// ItemImage defines a template entity that's schema-based. This module is intended to be duplicated.
type ItemImage struct {
	ImageID     int64       `db:"image_id" json:"-"` // serial ID's are internal and not shared with external systems
	ImageUUID   null.String `db:"image_uuid" json:"imageUUID"`
	ItemID      int64       `db:"item_id" json:"-"`
	ItemUUID    null.String `db:"item_uuid" json:"itemUUID"`
	Name        zero.String `db:"name" json:"name"`
	Format      null.String `db:"format" json:"format"`
	Description zero.String `db:"description" json:"description,omitempty"`
	Width       int64       `db:"width" json:"width"`
	Height      int64       `db:"height" json:"height"`
	Image       []byte      `db:"image" json:"image,omitempty"`

	URL    string `db:"-" json:"imageURL,omitempty"`
	Schema string `db:"-" json:"-"`
}

// NewItemImage creates a ItemImage struct from a passed JSON string. No database
// events occur from this call.
func NewItemImage(tx *sqlx.Tx, jsonStr []byte, schema string) (*ItemImage, error) {
	entity := ItemImage{}
	entity.Schema = schema
	err := json.Unmarshal(jsonStr, &entity)
	if err != nil {
		return nil, err
	}
	if entity.ItemID == 0 {
		item, err := GetItemByUUID(tx, entity.ItemUUID.String, schema)
		if err != nil {
			return nil, err
		}
		entity.ItemID = item.ItemID
	}
	return &entity, nil
}

// Get performs a SELECT, using ImageID as the lookup key. The ItemImage object
// is updated with the attributes of the found ItemImage.
func (entity *ItemImage) Get(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.item_image WHERE image_id = $1", entity.Schema)
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(entity, statement, entity.ImageID)
	} else {
		err = tx.Get(entity, statement, entity.ImageID)
	}
	return
}

// Valid returns the validity of the ItemImage object
func (entity *ItemImage) Valid() bool {
	return len(entity.ImageUUID.String) > 0
}

// Exists tests for the existence of a ItemImage record corresponding to the ImageID value
func (entity *ItemImage) Exists(tx *sqlx.Tx) bool {
	if entity.ImageID <= 0 {
		return false
	}
	err := entity.Get(tx)
	if err != nil {
		return false
	}
	return entity.ImageID > 0
}

const itemImageInsertStatement = `INSERT INTO %s.item_image (` +
	`item_id, item_uuid, name, description, width, height, image, format` +
	`) VALUES (` +
	`$1, $2, $3, $4, $5, $6, $7, $8` +
	`) RETURNING image_id`

// Insert performs an SQL INSERT statement using the ItemImage struct fields as attributes. The struct
// will contain all DB-generated defaults, IDs, etc.
func (entity *ItemImage) Insert(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(itemImageInsertStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	row := tx.QueryRowx(statement, entity.ItemID, entity.ItemUUID, entity.Name, entity.Description,
		entity.Width, entity.Height, entity.Image, entity.Format)
	err = row.Err()
	if err != nil {
		goto end
	}
	err = row.Scan(&entity.ImageID)
	if err != nil {
		goto end
	}
	if entity.ImageID == 0 {
		err = errors.New("Primary key unexpectedly nil")
	}
end:
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

const itemImageUpdateStatement = `UPDATE %s.item_image SET (` +
	`item_id, item_uuid, name, description, width, height, image, format` +
	`) = ( ` +
	`$1, $2, $3, $4, $5, $6, $7, $8` +
	`) WHERE image_id = $9`

// Update performs an SQL UPDATE operation using the ItemImage struct fields as attributes.
// TODO: if the itemUUID is updated, verify the item existence and set the itemID
func (entity *ItemImage) Update(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(itemImageUpdateStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.ItemID, entity.ItemUUID, entity.Name, entity.Description,
		entity.Width, entity.Height, entity.Image, entity.Format, entity.ImageID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation, using ImageID as the lookup key.
func (entity *ItemImage) Delete(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("DELETE FROM %s.item_image WHERE image_id = $1", entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.ImageID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		entity.ImageID = 0
	}
	return
}

func (entity *ItemImage) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(entity)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

func (entity *ItemImage) NormalizeImageURL() {
	entity.URL = fmt.Sprintf("/v1/items/%s/images/%s", entity.ItemUUID.String, entity.ImageUUID.String)
}

// GetItemImageByImageID performs a SELECT, using the passed ImageID as the lookup key
// Returns nil if the entity does not exist--err may/may not be nil in that case.
func GetItemImageByImageID(tx *sqlx.Tx, imageID int64, raw bool, schema string) (entity *ItemImage, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	var statement string
	if raw {
		statement = fmt.Sprintf("SELECT * FROM %s.item_image WHERE image_id = $1", schema)
	} else {
		statement = fmt.Sprintf("SELECT image_id, image_uuid, item_id, item_uuid, name, format, description, width, height FROM %s.item_image WHERE image_id = $1", schema)
	}
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(ItemImage)
	if tx == nil {
		err = db.Get(entity, statement, imageID)
	} else {
		err = tx.Get(entity, statement, imageID)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	entity.NormalizeImageURL()
	return
}

// GetItemImageByUUID performs a SELECT, using the passed UUID as the lookup key.
// Returns nil if the entity does not exist—-err may/may not be nil in that case.
func GetItemImageByUUID(tx *sqlx.Tx, uuid string, raw bool, schema string) (entity *ItemImage, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	var statement string
	if raw {
		statement = fmt.Sprintf("SELECT * FROM %s.item_image WHERE image_uuid = $1", schema)
	} else {
		statement = fmt.Sprintf("SELECT image_id, image_uuid, item_id, item_uuid, name, format, description, width, height FROM %s.item_image WHERE image_uuid = $1", schema)
	}
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(ItemImage)
	if tx == nil {
		err = db.Get(entity, statement, uuid)
	} else {
		err = tx.Get(entity, statement, uuid)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
		entity.NormalizeImageURL()
	}
	return
}

// GetAllItemImageIDsByItem returns all imageID for a given item.
func GetAllItemImageIDsByItem(tx *sqlx.Tx, itemID int64, schema string) ([]int64, error) {
	if schema == "" {
		err := errors.New("schema not specified")
		return nil, err
	}
	db, err := GetDB()
	if err != nil {
		return nil, err
	}
	ids := []int64{}
	if tx == nil {
		err = db.Select(&ids, fmt.Sprintf("SELECT image_id FROM %s.item_image WHERE item_id = $1", schema), itemID)
	} else {
		err = tx.Select(&ids, fmt.Sprintf("SELECT image_id FROM %s.item_image WHERE item_id = $1", schema), itemID)
	}
	return ids, err
}

// GetAllItemImageUUIDsByItem returns all imageID for a given item.
func GetAllItemImageUUIDsByItem(tx *sqlx.Tx, itemID int64, schema string) ([]string, error) {
	if schema == "" {
		err := errors.New("schema not specified")
		return nil, err
	}
	db, err := GetDB()
	if err != nil {
		return nil, err
	}
	ids := []string{}
	if tx == nil {
		err = db.Select(&ids, fmt.Sprintf("SELECT image_uuid FROM %s.item_image WHERE item_id = $1", schema), itemID)
	} else {
		err = tx.Select(&ids, fmt.Sprintf("SELECT image_uuid FROM %s.item_image WHERE item_id = $1", schema), itemID)
	}
	return ids, err
}

// GetAllItemImagesByItemUUID returns all images for a given item UUID.
func GetAllItemImagesByItemUUID(tx *sqlx.Tx, itemUUID string, raw bool, schema string) ([]ItemImage, error) {
	if schema == "" {
		err := errors.New("schema not specified")
		return nil, err
	}
	db, err := GetDB()
	if err != nil {
		return nil, err
	}
	var statement string
	if raw {
		statement = fmt.Sprintf("SELECT * FROM %s.item_image WHERE item_uuid = $1", schema)
	} else {
		statement = fmt.Sprintf("SELECT image_id, image_uuid, item_id, item_uuid, name, format, description, width, height FROM %s.item_image WHERE item_uuid = $1", schema)
	}
	images := []ItemImage{}
	if tx == nil {
		err = db.Select(&images, statement, itemUUID)
	} else {
		err = tx.Select(&images, statement, itemUUID)
	}
	if err == nil {
		for i := range images {
			images[i].Schema = schema
			images[i].NormalizeImageURL()
		}
	}
	return images, err
}
