package models

import (
	"bytes"
	"encoding/json"
	"errors"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"fmt"

	sq "github.com/Masterminds/squirrel"
	"github.com/guregu/null/zero"
)

// Constants for the template entity
const (
	AuditActionInsert = "insert"
	AuditActionDelete = "delete"
	AuditActionUpdate = "update"
	AuditActionAccess = "access"
	AuditActionSearch = "search"
)

// Audit defines a template entity that's schema-based. This module is intended to be duplicated.
type Audit struct {
	EntityUUID string      `db:"entity_uuid" json:"entityUUID,omitempty"`
	EntityType string      `db:"entity_type" json:"entityType,omitempty"`
	UserUUID   string      `db:"user_uuid" json:"userUUID,omitempty"`
	AuthToken  string      `db:"auth_token" json:"authToken,omitempty"`
	IPAddress  string      `db:"ip_address" json:"ipAddress,omitempty"`
	Action     zero.String `db:"action" json:"action"`
	Difference string      `db:"difference" json:"difference,omitempty"`
	Timestamp  zero.Time   `db:"timestamp" json:"timestamp"`

	Schema string `db:"-" json:"-"`
}

// NewAudit creates a Audit struct from a passed JSON string. No database
// events occur from this call.
func NewAudit(jsonStr []byte, schema string) (*Audit, error) {
	entity := Audit{}
	entity.Schema = schema
	err := json.Unmarshal(jsonStr, &entity)
	if err != nil {
		return nil, err
	}
	return &entity, nil
}

const auditInsertStatement = `INSERT INTO %s.audit (` +
	`entity_uuid, entity_type, user_uuid, auth_token, ip_address, action, difference` +
	`) VALUES (` +
	`$1, $2, $3, $4, $5, $6, $7` +
	`)`

// Insert performs an SQL INSERT statement using the Audit struct fields as attributes. The struct
// will contain all DB-generated defaults, IDs, etc.
func (entity *Audit) Insert(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(auditInsertStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.EntityUUID, entity.EntityType, entity.UserUUID, entity.AuthToken,
		entity.IPAddress, entity.Action, entity.Difference)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

func (entity *Audit) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(entity)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetAuditRecords returns all audit records, transaction aware. Pass 0 for either limit or offset
// to bypass query limits and/or offset position
func GetAuditRecords(tx *sqlx.Tx, limit uint64, offset uint64, schema string,
	entityUUID string, userUUID string, action string,
	beginTimestamp time.Time, endTimestamp time.Time) ([]Audit, error) {

	if schema == "" {
		err := errors.New("schema not specified")
		return nil, err
	}
	var err error
	records := []Audit{}
	table := fmt.Sprintf("%s.audit", schema)

	psql := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	q := psql.Select("*").From(table)
	if limit > 0 {
		q = q.Limit(limit)
	}
	if offset > 0 {
		q = q.Offset(offset)
	}
	if entityUUID != "" {
		q = q.Where("entity_uuid = ?", entityUUID)
	}
	if userUUID != "" {
		q = q.Where("user_uuid = ?", userUUID)
	}
	if action != "" {
		q = q.Where("action = ?", action)
	}
	if !beginTimestamp.Equal(NilTime) {
		q = q.Where("timestamp >= ?", beginTimestamp)
	}
	if !endTimestamp.Equal(NilTime) {
		q = q.Where("timestamp < ?", endTimestamp)
	}
	statement, args, _ := q.ToSql()
	if tx == nil {
		db, err := GetDB()
		if err != nil {
			return nil, err
		}
		err = db.Select(&records, statement, args...)
	} else {
		err = tx.Select(&records, statement, args...)
	}

	if err == nil {
		for i := range records {
			records[i].Schema = schema
		}
	}
	return records, err
}
