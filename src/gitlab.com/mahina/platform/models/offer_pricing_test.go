// +build integration all

package models

import (
	"testing"

	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

func TestInsertOfferPricing(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	offer, err := insertOffer(tx, item)
	if err != nil {
		t.Fatal("Unable to insert offer", err)
	}
	pricing, err := insertOfferPricing(tx, offer, 0, 11.5, 0.95)
	if err != nil {
		t.Fatal("Unable to insert offer pricing", err)
	}
	assert.Equal(t, 0.0, pricing.QuantityMin, "Expected certain value in offer pricing insert")
	assert.Equal(t, 11.5, pricing.QuantityMax, "Expected certain value in offer pricing insert")
	assert.Equal(t, 0.95, pricing.Price, "Expected certain value in offer pricing insert")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestUpdateOfferPricing(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	offer, err := insertOffer(tx, item)
	if err != nil {
		t.Fatal("Unable to insert offer", err)
	}
	pricing, err := insertOfferPricing(tx, offer, 0, 11.5, 0.95)

	price := pricing.Price
	pricing.Price = 1.30
	err = pricing.Update(tx)
	if err != nil {
		t.Fatal("Unable to update offer pricing", err)
	}
	assert.NotEqual(t, price, pricing.Price)

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestDeleteOfferPricing(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	offer, err := insertOffer(tx, item)
	if err != nil {
		t.Fatal("Unable to insert offer", err)
	}
	pricing, err := insertOfferPricing(tx, offer, 0, 11.5, 0.95)

	err = pricing.Delete(tx)
	if err != nil {
		t.Fatal("Unable to delete offer pricing", err)
	}
	assert.False(t, pricing.Exists(tx), "Expected to not be able to find deleted item pricing")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindUnknownOfferPricing(t *testing.T) {
	nonExistentUUID := "fdd93e34-c87d-11e6-804b-06f1521cd849"
	offerPricing, _ := GetOfferPricingByUUID(nil, nonExistentUUID, schema)
	assert.Nil(t, offerPricing, "Expected to not find an offer pricing")
}

func TestFindOfferPricingByID(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	offer, err := insertOffer(tx, item)
	if err != nil {
		t.Fatal("Unable to insert offer", err)
	}
	pricing, err := insertOfferPricing(tx, offer, 0, 11.5, 0.95)

	pricing, err = GetOfferPricingByPricingID(tx, pricing.PricingID, schema)
	if pricing == nil {
		t.Fatal("Unexpected null item")
	}
	assert.Nil(t, err, "Error in find by ID")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindOfferPricingByUUID(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	offer, err := insertOffer(tx, item)
	if err != nil {
		t.Fatal("Unable to insert offer", err)
	}
	pricing, err := insertOfferPricing(tx, offer, 0, 11.5, 0.95)

	pricing, err = GetOfferPricingByUUID(tx, pricing.PricingUUID.String, schema)
	if item == nil {
		t.Fatal("Unexpected null item")
	}
	assert.Nil(t, err, "Error in find by UUID")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

func TestFindOfferPricingsForItem(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	offer, err := insertOffer(tx, item)
	if err != nil {
		t.Fatal("Unable to insert offer", err)
	}
	for i := 0; i < 3; i++ {
		_, err := insertOfferPricing(tx, offer, 0, 11.5, 0.95)
		if err != nil {
			t.Fatal("Unable to insert item pricing", err)
		}
	}

	offerPricings, err := GetAllOfferPricingForOffer(tx, offer.OfferID, schema)
	if err != nil {
		t.Fatal("Unable to find item pricings", err)
	}
	assert.Len(t, offerPricings, 3, "Expected a certain number of item pricings to be found")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

const offerPricingJSON = `{
	"offerUUID": "%s",
	"quantityMin": %f,
	"quantityMax": %f,
	"price": %f
}`

// Helper Funcs
func insertOfferPricing(tx *sqlx.Tx, offer *SupplierOffer, min float64, max float64, pricing float64) (*OfferPricing, error) {
	json := fmt.Sprintf(offerPricingJSON, offer.OfferUUID.String, min, max, pricing)
	offerPricing, err := NewOfferPricing(tx, []byte(json), schema)
	if err != nil {
		return nil, err
	}
	err = offerPricing.Insert(tx)
	if err != nil {
		return nil, err
	}
	return offerPricing, err
}
