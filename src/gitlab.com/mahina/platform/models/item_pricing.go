package models

import (
	"bytes"
	"encoding/json"
	"errors"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"fmt"

	"github.com/guregu/null"
)

// ItemPricing defines a template entity that's schema-based.
type ItemPricing struct {
	PricingID   int64       `db:"pricing_id" json:"-"` // serial ID's are internal and not shared with external systems
	PricingUUID null.String `db:"pricing_uuid" json:"pricingUUID"`
	ItemID      int64       `db:"item_id" json:"-"`
	ItemUUID    null.String `db:"item_uuid" json:"itemUUID"`
	QuantityMin float64     `db:"quantity_min" json:"quantityMin"`
	QuantityMax float64     `db:"quantity_max" json:"quantityMax"`
	Price       float64     `db:"price" json:"price"`

	Schema string `db:"-" json:"-"`
}

// NewItemPricing creates a ItemPricing struct from a passed JSON string. No database
// events occur from this call.
func NewItemPricing(tx *sqlx.Tx, jsonStr []byte, schema string) (*ItemPricing, error) {
	entity := ItemPricing{}
	entity.Schema = schema
	err := json.Unmarshal(jsonStr, &entity)
	if err != nil {
		return nil, err
	}
	if entity.ItemID == 0 {
		item, err := GetItemByUUID(tx, entity.ItemUUID.String, schema)
		if err != nil {
			return nil, err
		}
		entity.ItemID = item.ItemID
	}
	return &entity, nil
}

// Get performs a SELECT, using PricingID as the lookup key. The ItemPricing object
// is updated with the attributes of the found ItemPricing.
func (entity *ItemPricing) Get(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.item_pricing WHERE pricing_id = $1", entity.Schema)
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(entity, statement, entity.PricingID)
	} else {
		err = tx.Get(entity, statement, entity.PricingID)
	}
	return
}

// Valid returns the validity of the ItemPricing object
func (entity *ItemPricing) Valid() bool {
	return len(entity.PricingUUID.String) > 0
}

// Exists tests for the existence of a ItemPricing record corresponding to the PricingID value
func (entity *ItemPricing) Exists(tx *sqlx.Tx) bool {
	if entity.PricingID <= 0 {
		return false
	}
	err := entity.Get(tx)
	if err != nil {
		return false
	}
	return entity.PricingID > 0
}

const itemPricingInsertStatement = `INSERT INTO %s.item_pricing (` +
	`item_id, item_uuid, quantity_min, quantity_max, price` +
	`) VALUES (` +
	`$1, $2, $3, $4, $5` +
	`) RETURNING pricing_id`

// Insert performs an SQL INSERT statement using the ItemPricing struct fields as attributes. The struct
// will contain all DB-generated defaults, IDs, etc.
func (entity *ItemPricing) Insert(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(itemPricingInsertStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	row := tx.QueryRowx(statement, entity.ItemID, entity.ItemUUID, entity.QuantityMin, entity.QuantityMax, entity.Price)
	err = row.Err()
	if err != nil {
		goto end
	}
	err = row.Scan(&entity.PricingID)
	if err != nil {
		goto end
	}
	if entity.PricingID == 0 {
		err = errors.New("Primary key unexpectedly nil")
	}
end:
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

const itemPricingUpdateStatement = `UPDATE %s.item_pricing SET (` +
	`item_id, item_uuid, quantity_min, quantity_max, price` +
	`) = ( ` +
	`$1, $2, $3, $4, $5` +
	`) WHERE pricing_id = $6`

// Update performs an SQL UPDATE operation using the ItemPricing struct fields as attributes.
func (entity *ItemPricing) Update(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(itemPricingUpdateStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.ItemID, entity.ItemUUID, entity.QuantityMin, entity.QuantityMax, entity.Price,
		entity.PricingID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation, using PricingID as the lookup key.
func (entity *ItemPricing) Delete(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("DELETE FROM %s.item_pricing WHERE pricing_id = $1", entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.PricingID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		entity.PricingID = 0
	}
	return
}

func (entity *ItemPricing) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(entity)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetItemPricingByPricingID performs a SELECT, using the passed PricingID as the lookup key
// Returns nil if the entity does not exist--err may/may not be nil in that case.
func GetItemPricingByPricingID(tx *sqlx.Tx, entityID int64, schema string) (entity *ItemPricing, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.item_pricing WHERE pricing_id = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(ItemPricing)
	if tx == nil {
		err = db.Get(entity, statement, entityID)
	} else {
		err = tx.Get(entity, statement, entityID)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	return
}

// GetItemPricingByUUID performs a SELECT, using the passed UUID as the lookup key.
// Returns nil if the entity does not exist—-err may/may not be nil in that case.
func GetItemPricingByUUID(tx *sqlx.Tx, uuid string, schema string) (entity *ItemPricing, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.item_pricing WHERE pricing_uuid = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(ItemPricing)
	if tx == nil {
		err = db.Get(entity, statement, uuid)
	} else {
		err = tx.Get(entity, statement, uuid)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	return
}

// GetAllItemPricingIDsByItem returns all pricingIDs for a given item.
func GetAllItemPricingIDsByItem(tx *sqlx.Tx, itemID int64, schema string) ([]int64, error) {
	if schema == "" {
		err := errors.New("schema not specified")
		return nil, err
	}
	db, err := GetDB()
	if err != nil {
		return nil, err
	}
	ids := []int64{}
	if tx == nil {
		err = db.Select(&ids, fmt.Sprintf("SELECT pricing_id FROM %s.item_pricing WHERE item_id = $1", schema), itemID)
	} else {
		err = tx.Select(&ids, fmt.Sprintf("SELECT pricing_id FROM %s.item_pricing WHERE item_id = $1", schema), itemID)
	}
	return ids, err
}

// GetAllItemPricingUUIDsByItem returns all pricingIDs for a given item.
func GetAllItemPricingUUIDsByItem(tx *sqlx.Tx, itemID int64, schema string) ([]string, error) {
	if schema == "" {
		err := errors.New("schema not specified")
		return nil, err
	}
	db, err := GetDB()
	if err != nil {
		return nil, err
	}
	ids := []string{}
	if tx == nil {
		err = db.Select(&ids, fmt.Sprintf("SELECT pricing_uuid FROM %s.item_pricing WHERE item_id = $1", schema), itemID)
	} else {
		err = tx.Select(&ids, fmt.Sprintf("SELECT pricing_uuid FROM %s.item_pricing WHERE item_id = $1", schema), itemID)
	}
	return ids, err
}

// GetAllItemPricingByItemUUID returns all item pricing for a given item UUID.
func GetAllItemPricingByItemUUID(tx *sqlx.Tx, itemUUID string, schema string) ([]ItemPricing, error) {
	if schema == "" {
		err := errors.New("schema not specified")
		return nil, err
	}
	db, err := GetDB()
	if err != nil {
		return nil, err
	}
	pricing := make([]ItemPricing, 0)
	if tx == nil {
		err = db.Select(&pricing, fmt.Sprintf("SELECT * FROM %s.item_pricing WHERE item_uuid = $1", schema), itemUUID)
	} else {
		err = tx.Select(&pricing, fmt.Sprintf("SELECT * FROM %s.item_pricing WHERE item_uuid = $1", schema), itemUUID)
	}
	if err == nil {
		for i := range pricing {
			pricing[i].Schema = schema
		}
	}
	return pricing, err
}
