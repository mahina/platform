// +build integration all

package models

import (
	"crypto/md5"
	"testing"

	"gitlab.com/mahina/platform/util"

	"github.com/guregu/null/zero"
	"github.com/jmoiron/sqlx/types"
	"github.com/stretchr/testify/assert"
)

func TestRegistrationHash(t *testing.T) {
	reg := &Registration{}
	reg.Username = zero.StringFrom("foo@bar.com")
	reg.IPAddress = zero.StringFrom("127.0.0.1")
	reg.Extended = types.JSONText(regJSON)

	hash := reg.Hash()
	assert.Len(t, hash, md5.Size, "Expected has to be correct size")

	reg2 := &Registration{}
	reg2.Username = zero.StringFrom("foo@bar.com")
	reg2.IPAddress = zero.StringFrom("127.0.0.1")
	reg2.Extended = types.JSONText(regSlightlyDifferentJSON)

	assert.False(t, reg2.CompareHash(hash), "Expected comparison of hash from different Reg to fail")
}

func TestInsertRegistration(t *testing.T) {
	reg := &Registration{}
	reg.Username = zero.StringFrom("foo@bar.com")
	reg.IPAddress = zero.StringFrom("127.0.0.1")
	reg.Extended = types.JSONText(regJSON)

	err := reg.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert registration", err)
	}
	assert.True(t, util.IsUUID(reg.RegUUID.String), "Expected valid UUID for reg")

	// clean up
	err = reg.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete registration (clean up)", err)
	}
}

func TestInsertRegistrationActions(t *testing.T) {
	username := "foo@bar.com"
	reg := &Registration{}
	reg.Username = zero.StringFrom(username)
	reg.IPAddress = zero.StringFrom("127.0.0.1")
	reg.Extended = types.JSONText(regJSON)

	err := reg.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert registration", err)
	}
	assert.True(t, util.IsUUID(reg.RegUUID.String), "Expected valid UUID for reg")

	err = reg.AddAction(nil, RegStateEmailSent, "log details")
	assert.NoError(t, err, "Error inserting registration action")
	err = reg.AddAction(nil, RegStateEmailConfirmed, "log details")
	assert.NoError(t, err, "Error inserting registration action")

	actions, err := GetAllRegistrationActionsForUsername(nil, username)
	assert.NoError(t, err, "Expected to get actions for username")
	assert.Equal(t, 2, len(actions), "Expected to find a number of actions")

	assert.Equal(t, RegStateEmailConfirmed, actions[0].Action.String, "Expected first returned action to be of a certain state")

	// clean up
	err = reg.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete registration (clean up)", err)
	}

	actions, err = GetAllRegistrationActionsForUsername(nil, username)
	assert.Equal(t, 0, len(actions), "Expecting no actions following cascade deletion")
	assert.NoError(t, err, "Expected no error locating unexistent actions")
}
