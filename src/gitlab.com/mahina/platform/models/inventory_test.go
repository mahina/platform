// +build integration all

package models

import (
	"testing"

	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

func TestInsertInventory(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	inventory, err := insertInventory(tx, item, 10.0, "From the field")
	if err != nil {
		t.Fatal("Unable to insert inventory", err)
	}
	assert.Equal(t, 10.0, inventory.Quantity, "Expected certain value in inventory insert")

	err = inventory.Get(tx)
	if err != nil {
		t.Fatal("Unable to find inventory", err)
	}

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction, db now inconsisient", err)
	}
}

func TestFindInventoryRecordsForItem(t *testing.T) {
	tx, err := BeginTX()
	if err != nil {
		t.Fatal("Unable to create transaction", err)
	}
	item, err := insertItem(tx)
	if err != nil {
		t.Fatal("Unable to insert item", err)
	}
	inventory, err := insertInventory(tx, item, 10.0, "From the field")
	if err != nil {
		t.Fatal("Unable to insert inventory", err)
	}
	assert.Equal(t, 10.0, inventory.Quantity, "Expected certain value in inventory insert")

	inventory, err = insertInventory(tx, item, 4.3, "From the field, again")
	if err != nil {
		t.Fatal("Unable to insert inventory", err)
	}
	assert.Equal(t, 4.3, inventory.Quantity, "Expected certain value in inventory insert")

	inventoryRecords, err := GetAllInventoryRecordsByItemUUID(tx, item.ItemUUID.String, schema)
	if err != nil {
		t.Fatal("Unable to find inventory records", err)
	}
	assert.Len(t, inventoryRecords, 2, "Expected a certain number of inventory records to be found")

	err = inventory.Get(tx)
	if err != nil {
		t.Fatal("Unable to find inventory record", err)
	}
	assert.Equal(t, 4.3, inventory.Quantity, "Expected certain value in inventory insert")

	// clean up
	err = tx.Rollback()
	if err != nil {
		t.Fatal("Unable to rollback transaction", err)
	}
}

const InventoryJSON = `{
	"itemUUID": "%s",
	"quantity": %f,
	"notes": "%s"
}`

// Helper Funcs
func insertInventory(tx *sqlx.Tx, item *Item, quantity float64, notes string) (*Inventory, error) {
	json := fmt.Sprintf(InventoryJSON, item.ItemUUID.String, quantity, notes)
	inventory, err := NewInventory(tx, []byte(json), schema)
	if err != nil {
		return nil, err
	}
	err = inventory.Insert(tx)
	if err != nil {
		return nil, err
	}
	return inventory, err
}
