// +build integration all

package models

import (
	"testing"

	"github.com/guregu/null"
	"github.com/stretchr/testify/assert"
)

func TestInsertOrganization(t *testing.T) {
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	assert.Equal(t, true, org.OrgID > 0, "Expected OrgID to be non-zero")
	assert.True(t, org.Exists(nil), "Expected to find organization")

	// clean up
	err = org.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete organization (clean up)", err)
	}
}

func TestTransactionalInsertOrganization(t *testing.T) {
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	tx, err := BeginTX()
	assert.NoError(t, err, "Failure starting transaction")

	err = org.Insert(tx)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	assert.Equal(t, true, org.OrgID > 0, "Expected OrgID to be non-zero")
	assert.True(t, org.Exists(tx), "Expected to find organization")

	err = tx.Commit()
	assert.NoError(t, err, "Failure committing transaction")

	// clean up
	err = org.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete organization (clean up)", err)
	}
}

func TestDuplicateOrganizationNameInsert(t *testing.T) {
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	assert.Equal(t, true, org.OrgID > 0, "Expected OrgID to be non-zero")

	err = org.Insert(nil)
	assert.NotNil(t, err, "Expected error when inserting duplicate key")

	// clean up
	err = org.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete organization (clean up)", err)
	}
}

func TestUpdateOrganization(t *testing.T) {
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	assert.Equal(t, true, org.OrgID > 0)

	legalName := org.LegalName
	org.LegalName = null.StringFrom("Vandalay Industries")
	err = org.Update(nil)
	if err != nil {
		t.Fatal("Unable to update organization", err)
	}
	assert.NotEqual(t, legalName, org.LegalName)

	// clean up
	err = org.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete organization (clean up)", err)
	}
}

func TestFindUnknownOrganization(t *testing.T) {
	nonExistentUUID := "fdd93e34-c87d-11e6-804b-06f1521cd849"
	org, _ := GetOrganizationByUUID(nil, nonExistentUUID)
	assert.Nil(t, org, "Expected to not find an organization")
}

func TestFindOrganizationByUUID(t *testing.T) {
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}
	assert.Equal(t, true, org.OrgID > 0)

	org, err = GetOrganizationByUUID(nil, org.OrgUUID.String)
	if org == nil {
		t.Fatal("Unexpected null user")
	}
	assert.Nil(t, err, "Error in find by UUID")

	// clean up
	err = org.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete organization (clean up)", err)
	}
}

func TestGetAllOrganization(t *testing.T) {
	org, err := NewOrganization([]byte(orgJSON))
	if err != nil {
		t.Fatal("Unable to create organization", err)
	}
	err = org.Insert(nil)
	if err != nil {
		t.Fatal("Unable to insert organization", err)
	}

	users, err := GetAllOrganizations(-1, -1)
	assert.NoError(t, err, "Expected to be able to find all users")
	assert.NotEmpty(t, users, "Expected result from find all users to be not empty")

	// clean up
	err = org.Delete(nil)
	if err != nil {
		t.Fatal("Unable to delete organization (clean up)", err)
	}
}

const orgJSON = `{
	"status": "active",
	"legalName": "Kruger Industrial Smoothing",
	"shortName": "Kruger",
	"orgType": "corp"
}`

const org2JSON = `{
	"status": "active",
	"legalName": "Vandalay Industries",
	"shortName": "Vandalay",
	"orgType": "corp"	
}`
