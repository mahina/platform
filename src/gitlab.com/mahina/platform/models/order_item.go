package models

import (
	"bytes"
	"encoding/json"
	"errors"

	"github.com/jmoiron/sqlx"
	"github.com/jmoiron/sqlx/types"
	_ "github.com/lib/pq"

	"fmt"

	"github.com/guregu/null"
	"github.com/guregu/null/zero"
)

// OrderItem defines an line item in an order.
type OrderItem struct {
	OrderItemID   int64          `db:"order_item_id" json:"-"`
	OrderItemUUID null.String    `db:"order_item_uuid" json:"orderItemUUID"`
	OrderID       int64          `db:"order_id" json:"-"`
	OrderUUID     null.String    `db:"order_uuid" json:"orderUUID"`
	SupplierID    int64          `db:"supplier_id" json:"-"`
	SupplierUUID  null.String    `db:"supplier_uuid" json:"supplierUUID"`
	ItemID        int64          `db:"item_id" json:"-"`
	ItemUUID      null.String    `db:"item_uuid" json:"itemUUID"`
	OfferID       null.Int       `db:"offer_id" json:"-"`
	OfferUUID     zero.String    `db:"offer_uuid" json:"offerUUID"`
	LineItem      int64          `db:"line_item" json:"lineItem"`
	Quantity      float64        `db:"quantity" json:"quantity"`
	Price         float64        `db:"price" json:"price"`
	Notes         zero.String    `db:"notes" json:"notes"`
	Created       zero.Time      `db:"created" json:"created"`
	Extended      types.JSONText `db:"extended" json:"extended,omitempty"`

	Schema string `db:"-" json:"-"`
}

// NewOrderItem creates a OrderItem struct from a passed JSON string. No database
// update events occur from this call.
func NewOrderItem(tx *sqlx.Tx, jsonStr []byte, schema string) (*OrderItem, error) {
	entity := OrderItem{}
	entity.Schema = schema
	err := json.Unmarshal(jsonStr, &entity)
	if err != nil {
		return nil, err
	}
	if entity.OrderID == 0 {
		order, err := GetOrderByUUID(tx, entity.OrderUUID.String, schema)
		if err != nil {
			return nil, err
		}
		entity.OrderID = order.OrderID
	}
	if entity.SupplierID == 0 {
		org, err := GetOrganizationByUUID(tx, entity.SupplierUUID.String)
		if err != nil {
			return nil, err
		}
		entity.SupplierID = org.OrgID
	}
	if entity.ItemID == 0 {
		item, err := GetItemByUUID(tx, entity.ItemUUID.String, schema)
		if err != nil {
			return nil, err
		}
		entity.ItemID = item.ItemID
	}
	if entity.OfferID.Int64 == 0 && entity.OfferUUID.String != "" {
		offer, err := GetOfferByUUID(tx, entity.OfferUUID.String, schema)
		if err != nil {
			return nil, err
		}
		entity.OfferID = null.IntFrom(offer.OfferID)
	}
	return &entity, nil
}

// Get performs a SELECT, using OrderItemID as the lookup key. The OrderItem object
// is updated with the attributes of the found OrderItem.
func (entity *OrderItem) Get(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.order_item WHERE order_item_id = $1", entity.Schema)
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(entity, statement, entity.OrderItemID)
	} else {
		err = tx.Get(entity, statement, entity.OrderItemID)
	}
	if err == nil {
		if entity.OfferID.IsZero() {
			entity.OfferUUID = zero.StringFrom("")
		}
	}
	return
}

// Valid returns the validity of the OrderItem object
func (entity *OrderItem) Valid() bool {
	return len(entity.OrderItemUUID.String) > 0
}

// Exists tests for the existence of a OrderItem record corresponding to the OrderItemID value
func (entity *OrderItem) Exists(tx *sqlx.Tx) bool {
	if entity.OrderItemID <= 0 {
		return false
	}
	err := entity.Get(tx)
	if err != nil {
		return false
	}
	return entity.OrderItemID > 0
}

const orderItemInsertStatement = `INSERT INTO %s.order_item (` +
	`order_id, order_uuid, supplier_id, supplier_uuid, item_id, item_uuid, offer_id, offer_uuid, line_item, quantity, price, notes, extended` +
	`) VALUES (` +
	`$1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13` +
	`) RETURNING order_item_id`

// Insert performs an SQL INSERT statement using the OrderItem struct fields as attributes. The struct
// will contain all DB-generated defaults, IDs, etc.
func (entity *OrderItem) Insert(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(orderItemInsertStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	row := tx.QueryRowx(statement, entity.OrderID, entity.OrderUUID, entity.SupplierID, entity.SupplierUUID,
		entity.ItemID, entity.ItemUUID, entity.OfferID, entity.OfferUUID, entity.LineItem, entity.Quantity, entity.Price, entity.Notes, entity.Extended)
	err = row.Err()
	if err != nil {
		goto end
	}
	err = row.Scan(&entity.OrderItemID)
	if err != nil {
		goto end
	}
	if entity.OrderItemID == 0 {
		err = errors.New("Primary key unexpectedly nil")
	}
end:
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

const orderItemUpdateStatement = `UPDATE %s.order_item SET (` +
	`order_id, order_uuid, supplier_id, supplier_uuid, item_id, item_uuid, offer_id, offer_uuid, line_item, quantity, price, notes, extended` +
	`) = ( ` +
	`$1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13` +
	`) WHERE order_item_id = $14`

// Update performs an SQL UPDATE operation using the OrderItem struct fields as attributes.
func (entity *OrderItem) Update(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf(orderItemUpdateStatement, entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.OrderID, entity.OrderUUID, entity.SupplierID, entity.SupplierUUID,
		entity.ItemID, entity.ItemUUID, entity.OfferID, entity.OfferUUID, entity.LineItem, entity.Quantity,
		entity.Price, entity.Notes, entity.Extended,
		entity.OrderItemID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = entity.Get(tx)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation, using OrderItemID as the lookup key.
func (entity *OrderItem) Delete(tx *sqlx.Tx) (err error) {
	if entity.Schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("DELETE FROM %s.order_item WHERE order_item_id = $1", entity.Schema)
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(statement, entity.OrderItemID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		entity.OrderItemID = 0
	}
	return
}

func (entity *OrderItem) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(entity)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetOrderItemByOrderItemID performs a SELECT, using the passed OrderItemID as the lookup key
// Returns nil if the entity does not exist--err may/may not be nil in that case.
func GetOrderItemByOrderItemID(tx *sqlx.Tx, entityID int64, schema string) (entity *OrderItem, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.order_item WHERE order_item_id = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(OrderItem)
	if tx == nil {
		err = db.Get(entity, statement, entityID)
	} else {
		err = tx.Get(entity, statement, entityID)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	return
}

// GetOrderItemByUUID performs a SELECT, using the passed UUID as the lookup key.
// Returns nil if the entity does not exist—-err may/may not be nil in that case.
func GetOrderItemByUUID(tx *sqlx.Tx, uuid string, schema string) (entity *OrderItem, err error) {
	if schema == "" {
		err = errors.New("schema not specified")
		return
	}
	statement := fmt.Sprintf("SELECT * FROM %s.order_item WHERE order_item_uuid = $1", schema)
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(OrderItem)
	if tx == nil {
		err = db.Get(entity, statement, uuid)
	} else {
		err = tx.Get(entity, statement, uuid)
	}
	if err != nil {
		entity = nil
	} else {
		entity.Schema = schema
	}
	return
}

// GetAllOrderItemsForOrder returns all pricingIDs for a given item.
func GetAllOrderItemsForOrder(tx *sqlx.Tx, orderUUID string, schema string) ([]OrderItem, error) {
	if schema == "" {
		err := errors.New("schema not specified")
		return nil, err
	}
	db, err := GetDB()
	if err != nil {
		return nil, err
	}
	items := []OrderItem{}
	if tx == nil {
		err = db.Select(&items, fmt.Sprintf("SELECT * FROM %s.order_item WHERE order_uuid = $1 ORDER BY line_item", schema), orderUUID)
	} else {
		err = tx.Select(&items, fmt.Sprintf("SELECT * FROM %s.order_item WHERE order_uuid = $1 ORDER BY line_item", schema), orderUUID)
	}
	return items, err
}
