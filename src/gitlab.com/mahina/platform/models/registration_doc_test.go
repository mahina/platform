// +build integration all

package models

import (
	"testing"

	"gitlab.com/mahina/platform/util"

	"github.com/stretchr/testify/assert"
)

func TestAcceptRegistration(t *testing.T) {
	reg, err := NewRegistrationDoc([]byte(regJSON))
	if err != nil {
		t.Fatal("Unable to create registration:", err)
	}
	err = reg.EncryptPassword()
	assert.NoError(t, err, "Failed encrypting passwords")

	privateKey, err := util.GetPrivateRSAKey()
	password, err := util.Decrypt(privateKey, reg.Password)
	passwordConfirm, err := util.Decrypt(privateKey, reg.PasswordConfirm)
	assert.Equal(t, password, passwordConfirm, "Expected parsed unicode passwords to be equal")

	_, err = reg.Accept()
	assert.NoError(t, err, "Error accepting registration")

	registration, err := GetRegistrationForUsername(nil, "potus@whitehouse.gov")
	assert.NoError(t, err, "Error finding accepting registration")

	// clean up
	err = registration.Delete(nil)
	assert.NoError(t, err, "Error deleting registration")
}

func TestProcessRegistration(t *testing.T) {
	reg, err := NewRegistrationDoc([]byte(regJSON))
	if err != nil {
		t.Fatal("Unable to create registration:", err)
	}
	err = reg.EncryptPassword()
	assert.NoError(t, err, "Failed encrypting passwords")

	privateKey, err := util.GetPrivateRSAKey()
	password, err := util.Decrypt(privateKey, reg.Password)
	passwordConfirm, err := util.Decrypt(privateKey, reg.PasswordConfirm)
	assert.Equal(t, password, passwordConfirm, "Expected parsed unicode passwords to be equal")

	_, err = reg.Accept()
	assert.NoError(t, err, "Error accepting registration")

	registration, err := GetRegistrationForUsername(nil, "potus@whitehouse.gov")
	assert.NoError(t, err, "Error finding accepting registration")

	// pull out the Registration Document from the registration's extended field
	reg, err = NewRegistrationDoc([]byte(registration.Extended.String()))
	assert.NoError(t, err, "Expected to be able to revive registration doc")

	err = reg.Process()
	assert.NoError(t, err, "Expected successful processing of registration")

	_, err = VerifyCredential(nil, reg.User.UserUUID.String,
		CredentialTypePassword, []byte(reg.User.Username.String), []byte("이념의 암호"))
	assert.NoError(t, err, "Error verifying credential")

	_, err = VerifyCredential(nil, reg.User.UserUUID.String,
		CredentialTypePassword, []byte(reg.User.Username.String), []byte("clearlynotthepassword"))
	assert.Error(t, err, "Expected error verifying credential with incorrect password")

	// clean up
	err = registration.Delete(nil)
	assert.NoError(t, err, "Error deleting registration")
	err = reg.User.Delete(nil)
	assert.NoError(t, err, "Expected successful deletion of user")
	err = reg.Organization.Delete(nil)
	assert.NoError(t, err, "Expected successful deletion of organization")
}

const regJSON = `{
    "user": {
        "username": "potus@whitehouse.gov",
        "firstName": "Gerald",
        "lastName": "Ford",
        "title": "President",
        "email": "potus@whitehouse.gov",
        "birthDate": "1913-07-14"
    },
    "password": "이념의 암호",
    "passwordConfirm": "이념의 암호",
    "roles": [
        "buyer",
        "supplier"
    ],
    "organization": {
	    "legalName": "Kruger Industrial Smoothing",
	    "shortName": "Kruger",
	    "orgType": "corp"
    },
    "location": {
        "name": "uptown",
        "addressLine1": "123 Main Street",
        "addressLine2": "Unit 21",
        "locality": "Fairmount",
        "region": "New Hampshire",
        "postcode": "03042",
        "country": "USA",
        "phone": "603-555-1212"
    },
	"ipAddress": "127.0.0.1"
}`

const regSlightlyDifferentJSON = `{
    "user": {
        "username": "potus@whitehouse.gov",
        "firstName": "Gerald",
        "lastName": "Ford",
        "title": "President",
        "email": "potus@whitehouse.gov",
        "birthDate": "1913-07-14"
    },
    "password": "이념의 암",
    "passwordConfirm": "이념의 암",
    "roles": [
        "buyer",
        "supplier"
    ],
    "organization": {
	    "legalName": "Kruger Industrial Smoothing",
	    "shortName": "Kruger",
	    "orgType": "corp"
    },
    "location": {
        "name": "uptown",
        "addressLine1": "123 Main Street",
        "addressLine2": "Unit 21",
        "locality": "Fairmount",
        "region": "New Hampshire",
        "postcode": "03042",
        "country": "USA",
        "phone": "603-555-1212"
    },
	"ipAddress": "127.0.0.1"
}`
