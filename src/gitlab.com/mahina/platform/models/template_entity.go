package models

import (
	"bytes"
	"encoding/json"
	"errors"

	"gitlab.com/mahina/platform/util"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"github.com/guregu/null"
	"github.com/guregu/null/zero"
	"github.com/jmoiron/sqlx/types"
)

// Constants for the template entity
const (
	EntityStatusActive  = "active"
	EntityStatusDeleted = "deleted"
)

// Entity defines a template entity. This module is intended to be duplicated.
type Entity struct {
	EntityID   int64          `db:"entity_id" json:"-"` // serial ID's are internal and not shared with external systems
	EntityUUID null.String    `db:"entity_uuid" json:"entityUUID"`
	Status     zero.String    `db:"status" json:"status,omitempty"`
	Entityname null.String    `db:"entityname" json:"entityname" binding:"exists"`
	Date       null.Time      `db:"birth_date" json:"birthDate,omitempty"`
	ImageIcon  []byte         `db:"image_icon" json:"imageIcon,omitempty"`
	Created    zero.Time      `db:"created" json:"created"`
	Extended   types.JSONText `db:"extended" json:"extended,omitempty"`
}

// NewEntity creates a Entity struct from a passed JSON string. No database
// events occur from this call.
func NewEntity(jsonStr []byte) (*Entity, error) {
	entity := Entity{}
	err := json.Unmarshal(jsonStr, &entity)
	if err != nil {
		return nil, err
	}
	return &entity, nil
}

// Get performs a SELECT, using EntityID as the lookup key. The Entity object
// is updated with the attributes of the found Entity.
func (entity *Entity) Get(tx *sqlx.Tx) (err error) {
	var db *sqlx.DB
	if tx == nil {
		db, err = GetDB()
		if err != nil {
			return
		}
		err = db.Get(entity, "SELECT * FROM entity.entity WHERE entity_id = $1", entity.EntityID)
	} else {
		err = tx.Get(entity, "SELECT * FROM entity.entity WHERE entity_id = $1", entity.EntityID)
	}
	return
}

// Valid returns the validity of the Entity object
func (entity *Entity) Valid() bool {
	return len(entity.Entityname.String) > 0
}

// Exists tests for the existence of a Entity record corresponding to the EntityID value
func (entity *Entity) Exists(tx *sqlx.Tx) bool {
	if entity.EntityID <= 0 {
		return false
	}
	err := entity.Get(tx)
	if err != nil {
		return false
	}
	return entity.EntityID > 0
}

const entityInsertStatement = `INSERT INTO entity.entity (` +
	`status, entityname, birth_date, image_icon, extended` +
	`) VALUES (` +
	`$1, $2, $3, $4, $5` +
	`) RETURNING entity_id`

// Insert performs an SQL INSERT statement using the Entity struct fields as attributes. The struct
// will contain all DB-generated defaults, IDs, etc.
func (entity *Entity) Insert(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	row := tx.QueryRowx(entityInsertStatement, entity.Status, entity.Entityname, entity.Date,
		entity.ImageIcon, entity.Extended)
	err = row.Err()
	if err != nil {
		goto end
	}
	err = row.Scan(&entity.EntityID)
	if err != nil {
		goto end
	}
	if entity.EntityID == 0 {
		err = errors.New("Primary key unexpectedly nil")
	}
end:
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = tx.Get(entity, "SELECT * FROM entity.entity WHERE entity_id = $1", entity.EntityID)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

const entityUpdateStatement = `UPDATE entity.entity SET (` +
	`status, entityname, birth_date, image_icon, extended` +
	`) = ( ` +
	`$1, $2, $3, $4, $5` +
	`) WHERE entity_id = $6`

// Update performs an SQL UPDATE operation using the Entity struct fields as attributes.
func (entity *Entity) Update(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec(entityUpdateStatement, entity.Status, entity.Entityname, entity.Date, entity.ImageIcon, entity.Extended,
		entity.EntityID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		err = tx.Get(entity, "SELECT * FROM entity.entity WHERE entity_id = $1", entity.EntityID)
		if isolated {
			err = tx.Commit()
		}
	}
	return
}

// Delete performs an SQL DELETE operation, using EntityID as the lookup key.
func (entity *Entity) Delete(tx *sqlx.Tx) (err error) {
	isolated := tx == nil
	if isolated {
		tx, err = BeginTX()
		if err != nil {
			return
		}
	}
	_, err = tx.Exec("DELETE FROM entity.entity WHERE entity_id = $1", entity.EntityID)
	if err != nil {
		if isolated {
			tx.Rollback()
		}
	} else {
		if isolated {
			err = tx.Commit()
		}
		entity.EntityID = 0
	}
	return
}

func (entity *Entity) String() string {
	var prettyJSON bytes.Buffer
	b, err := json.Marshal(entity)
	err = json.Indent(&prettyJSON, b, "", "\t")
	if err != nil {
		return "Error encoding"
	}
	return string(prettyJSON.Bytes())
}

// GetEntityByUUIDOrEntityname locates a Entity record by either a UUID or Entityname
func GetEntityByUUIDOrEntityname(tx *sqlx.Tx, entityUUIDOrEntityname string) (*Entity, error) {
	if util.IsUUID(entityUUIDOrEntityname) {
		return GetEntityByUUID(tx, entityUUIDOrEntityname)
	}
	return GetEntityByEntityname(tx, entityUUIDOrEntityname)
}

// GetEntityByEntityID performs a SELECT, using the passed EntityID as the lookup key
// Returns nil if the entity does not exist--err may/may not be nil in that case.
func GetEntityByEntityID(tx *sqlx.Tx, entityID int64) (entity *Entity, err error) {
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(Entity)
	if tx == nil {
		err = db.Get(entity, "SELECT * FROM entity.entity WHERE entity_id = $1", entityID)
	} else {
		err = tx.Get(entity, "SELECT * FROM entity.entity WHERE entity_id = $1", entityID)
	}
	if err != nil {
		entity = nil
	}
	return
}

// GetEntityByUUID performs a SELECT, using the passed UUID as the lookup key.
// Returns nil if the entity does not exist—-err may/may not be nil in that case.
func GetEntityByUUID(tx *sqlx.Tx, uuid string) (entity *Entity, err error) {
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(Entity)
	if tx == nil {
		err = db.Get(entity, "SELECT * FROM entity.entity WHERE entity_uuid = $1", uuid)
	} else {
		err = tx.Get(entity, "SELECT * FROM entity.entity WHERE entity_uuid = $1", uuid)
	}
	if err != nil {
		entity = nil
	}
	return
}

// GetEntityByEntityname performs a SELECT, using the passed entityname as the lookup key
// Returns nil if the entity does not exist—-err may/may not be nil in that case.
func GetEntityByEntityname(tx *sqlx.Tx, entityname string) (entity *Entity, err error) {
	db, err := GetDB()
	if err != nil {
		return
	}
	entity = new(Entity)
	if tx == nil {
		err = db.Get(entity, "SELECT * FROM entity.entity WHERE entityname = $1", entityname)
	} else {
		err = tx.Get(entity, "SELECT * FROM entity.entity WHERE entityname = $1", entityname)
	}
	if err != nil {
		entity = nil
	}
	return
}

// GetAllEntitys returns all entitys. Pass -1 for either limit or offset
// to bypass query limits and/or offset position
func GetAllEntitys(limit int, offset int) ([]Entity, error) {
	db, err := GetDB()
	if err != nil {
		return nil, err
	}
	entitys := []Entity{}
	if limit > 0 && offset > 0 {
		err = db.Select(&entitys, "SELECT * FROM entity.entity LIMIT $1 OFFSET $2", limit, offset)
	} else if limit > 0 {
		err = db.Select(&entitys, "SELECT * FROM entity.entity LIMIT $1", limit)
	} else if offset > 0 {
		err = db.Select(&entitys, "SELECT * FROM entity.entity OFFSET $1", offset)
	} else {
		err = db.Select(&entitys, "SELECT * FROM entity.entity")
	}
	return entitys, err
}
