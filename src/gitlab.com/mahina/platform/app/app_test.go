// +build automated all

package app

import (
	"net/http"
	"strings"
	"testing"

	"os"

	"fmt"

	"time"

	selenium "github.com/rghose/go-selenium"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// Selenium & SauceLabs-based testing for the Platform application
// IMPROVEMENT: Abstract the Selenium-driven code in a service interface,
// which would allow easier integrations with other Selenium-based services

// StockTestTargets identifies the five browser-os combinations to test on.
var StockTestTargets = []map[string]interface{}{
	{"browserName": "chrome", "platform": "windows 10"},
	{"browserName": "firefox", "platform": "windows 10"},
	{"browserName": "MicrosoftEdge", "platform": "windows 10"},
	{"browserName": "chrome", "platform": "macOS 10.12", "version": "latest"},
	{"browserName": "safari", "platform": "macOS 10.12", "version": "latest"},
}

func TestSignIn(t *testing.T) {
	for _, target := range StockTestTargets {
		_testSignIn(t, target)
	}
}

func _testSignIn(t *testing.T, target map[string]interface{}) {
	caps := selenium.Capabilities(target)
	caps["name"] = "Sign In"
	wd, err := selenium.NewRemote(caps, executorURL)
	if err != nil {
		t.Fatal("Expected to be able to retrieve remote web driver: ", err.Error())
	}
	defer wd.Quit()
	err = wd.SetTimeout("page load", 3000)
	assert.NoError(t, err)
	err = wd.SetImplicitWaitTimeout(3000)
	assert.NoError(t, err)

	err = wd.Get(TestURL)
	require.NoError(t, err, "Expected to be able to get login page")

	element, err := wd.FindElement(selenium.ByXPATH, "//paper-input[@id='field_username']//input")
	require.NoError(t, err, "Expected to be able to get input username")
	require.NoError(t, element.SendKeys("matthew.mcneely+testing@gmail.com"), "Expected to be able to add input")
	element, err = wd.FindElement(selenium.ByXPATH, "//paper-input[@id='field_password']//input")
	require.NoError(t, err, "Expected to be able to get input field_password")
	require.NoError(t, element.SendKeys("foobar"), "Expected to be able to add input")
	element, err = wd.FindElement(selenium.ById, "button_login")
	require.NoError(t, err, "Expected to be able to get button button_login")
	err = element.Click()
	require.NoError(t, err, "Expected to be able to click button")

	time.Sleep(1000 * time.Millisecond)
	expectedTitle := "Marketplace: Dashboard"
	title, err := wd.Title()
	require.NoError(t, err, "Expected to get for title page")
	require.Equal(t, expectedTitle, title, "Expected title not correct")

	reportPassedTest(wd)
}

var executorURL string
var SauceLabsUsername string
var SauceLabsAccessKey string
var TestURL string
var Commit string

func init() {
	TestURL = os.Getenv("TEST_URL")
	if len(TestURL) == 0 {
		panic("No TEST_URL defined in the environment")
	}
	Commit = os.Getenv("COMMIT")
	reportPassedData = fmt.Sprintf(reportPassedData, Commit)
	SauceLabsUsername = os.Getenv("SAUCELABS_USERNAME")
	SauceLabsAccessKey = os.Getenv("SAUCELABS_ACCESSKEY")
	if len(SauceLabsAccessKey) == 0 || len(SauceLabsUsername) == 0 {
		panic("No SAUCELABS_USERNAME or SAUCELABS_ACCESSKEY defined in the environment")
	}
	executorURL = fmt.Sprintf("http://%s:%s@ondemand.saucelabs.com:80/wd/hub", SauceLabsUsername, SauceLabsAccessKey)
}

var reportPassedData = `
	{
		"passed": true,
		"custom-data": {
			"commit": "https://gitlab.com/mahina/platform/commit/%s"
		}
	}
`

func reportPassedTest(wd selenium.WebDriver) error {
	caps, err := wd.Capabilities()
	sessionID := caps["webdriver.remote.sessionid"].(string)
	url := fmt.Sprintf("https://%s:%s@saucelabs.com/rest/v1/%s/jobs/%s", SauceLabsUsername, SauceLabsAccessKey, SauceLabsUsername, sessionID)
	req, _ := http.NewRequest("PUT", url, strings.NewReader(reportPassedData))
	client := &http.Client{}
	_, err = client.Do(req)
	return err
}
