package util

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"reflect"
	"time"
)

// ZeroDate is a zero-able time.Time.
// JSON marshals to the zero value for time.Time if null.
// Considered to be null to SQL if zero.
type ZeroDate struct {
	Time  time.Time
	Valid bool
}

// Scan implements Scanner interface.
func (t *ZeroDate) Scan(value interface{}) error {
	var err error
	switch x := value.(type) {
	case time.Time:
		t.Time = x
	case nil:
		t.Valid = false
		return nil
	default:
		err = fmt.Errorf("null: cannot scan type %T into ZeroDate: %v", value, value)
	}
	t.Valid = err == nil
	return err
}

// Value implements the driver Valuer interface.
func (t ZeroDate) Value() (driver.Value, error) {
	if !t.Valid {
		return nil, nil
	}
	return t.Time, nil
}

// NewZeroDate creates a new ZeroDate.
func NewZeroDate(t time.Time, valid bool) ZeroDate {
	return ZeroDate{
		Time:  t,
		Valid: valid,
	}
}

// ZeroDateFrom creates a new ZeroDate that will
// be null if t is the zero value.
func ZeroDateFrom(t time.Time) ZeroDate {
	return NewZeroDate(t, !t.IsZero())
}

// ZeroDateFromPtr creates a new ZeroDate that will
// be null if t is nil or *t is the zero value.
func ZeroDateFromPtr(t *time.Time) ZeroDate {
	if t == nil {
		return NewZeroDate(time.Time{}, false)
	}
	return ZeroDateFrom(*t)
}

// MarshalJSON implements json.Marshaler.
// It will encode the zero value of time.Time
// if this time is invalid.
func (t ZeroDate) MarshalJSON() ([]byte, error) {
	ti := time.Time{}
	if t.Valid {
		ti = t.Time
	}
	b := make([]byte, 0, len(dateFormat)+2)
	b = append(b, '"')
	b = ti.AppendFormat(b, dateFormat)
	b = append(b, '"')
	return b, nil
}

// UnmarshalJSON implements json.Unmarshaler.
// It supports string, object (e.g. pq.NullTime and friends)
// and null input.
func (t *ZeroDate) UnmarshalJSON(data []byte) error {
	var err error
	var v interface{}
	if err = json.Unmarshal(data, &v); err != nil {
		return err
	}
	switch x := v.(type) {
	case string:
		var ti time.Time
		//if err = ti.UnmarshalJSON(data); err != nil {
		//	return err
		//}
		ti, err = time.Parse(`"`+dateFormat+`"`, string(data))
		if err != nil {
			return err
		}
		*t = ZeroDateFrom(ti)
		return nil
	case map[string]interface{}:
		ti, tiOK := x["ZeroDate"].(string)
		valid, validOK := x["Valid"].(bool)
		if !tiOK || !validOK {
			return fmt.Errorf(`json: unmarshalling object into Go value of type null.ZeroDate requires key "Time" to be of type string and key "Valid" to be of type bool; found %T and %T, respectively`, x["ZeroDate"], x["Valid"])
		}
		//err = t.Time.UnmarshalText([]byte(ti))
		t.Time, err = time.Parse(dateFormat, ti)
		t.Valid = valid
		return err
	case nil:
		t.Valid = false
		return nil
	default:
		return fmt.Errorf("json: cannot unmarshal %v into Go value of type null.ZeroDate", reflect.TypeOf(v).Name())
	}
}

// MarshalText converts the internal date value to a byte slice
func (t ZeroDate) MarshalText() ([]byte, error) {
	ti := t.Time
	if !t.Valid {
		ti = time.Time{}
	}
	//return ti.MarshalText()
	b := make([]byte, 0, len(dateFormat))
	return ti.AppendFormat(b, dateFormat), nil
}

// UnmarshalText converts the byte slice to a proper date
func (t *ZeroDate) UnmarshalText(text []byte) error {
	str := string(text)
	if str == "" || str == "null" {
		t.Valid = false
		return nil
	}
	//if err := t.Time.UnmarshalText(text); err != nil {
	//	return err
	//}
	var err error
	t.Time, err = time.Parse(dateFormat, str)
	if err != nil {
		return err
	}
	t.Valid = true
	return nil
}

// SetValid changes this Time's value and
// sets it to be non-null.
func (t *ZeroDate) SetValid(v time.Time) {
	t.Time = v
	t.Valid = true
}

// Ptr returns a pointer to this ZeroDate's value,
// or a nil pointer if this ZeroDate is zero.
func (t ZeroDate) Ptr() *time.Time {
	if !t.Valid {
		return nil
	}
	return &t.Time
}
