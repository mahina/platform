package util

import (
	"io"
	"strings"
	"time"

	"github.com/spf13/afero"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

// Config utilities. We half-heartedly abstract the interface here. A future
// IMPROVEMENT might involve providing an abstraction via an interface.
//
// Until then... 'viper' has the following precedence orders for configuration:
// 1. explicit call to Set inside codebase
// 2. flag
// 3. env
// 4. config
// 5. key/value store
// 6. default value

// ConfigGet returns an anonymous config value
func ConfigGet(key string) interface{} { return viper.Get(key) }

// ConfigGetString returns an string value
func ConfigGetString(key string) string { return viper.GetString(key) }

// ConfigGetBool returns a boolean value
func ConfigGetBool(key string) bool { return viper.GetBool(key) }

// ConfigGetInt returns an integer value
func ConfigGetInt(key string) int { return viper.GetInt(key) }

// ConfigGetInt64 returns an 64bit integer
func ConfigGetInt64(key string) int64 { return viper.GetInt64(key) }

// ConfigGetFloat64 returns a 64bit float
func ConfigGetFloat64(key string) float64 { return viper.GetFloat64(key) }

// ConfigGetTime returns a time value
func ConfigGetTime(key string) time.Time { return viper.GetTime(key) }

// ConfigGetDuration returns a duration value
func ConfigGetDuration(key string) time.Duration { return viper.GetDuration(key) }

// ConfigGetStringMap returns a map of anonymous values keyed to a string
func ConfigGetStringMap(key string) map[string]interface{} { return viper.GetStringMap(key) }

// ConfigGetStringMapString returns a map of string values keyed to a string
func ConfigGetStringMapString(key string) map[string]string { return viper.GetStringMapString(key) }

// ConfigGetStringMapStringSlice returns a map of string slices keyted to a string
func ConfigGetStringMapStringSlice(key string) map[string][]string {
	return viper.GetStringMapStringSlice(key)
}

// ConfigGetStringSlice is overridden to test for correct conversion to []string.
// See https://github.com/spf13/viper/issues/200
func ConfigGetStringSlice(key string) []string {
	val := viper.GetStringSlice(key)
	if len(val) == 1 && strings.Contains(val[0], ",") {
		val = strings.Split(val[0], ",")
	}
	return val
}

// ConfigGetSizeInBytes returns the size of a value keyed to a string
func ConfigGetSizeInBytes(key string) uint { return viper.GetSizeInBytes(key) }

// ConfigUnmarshalKey takes a single key and unmarshals it into a struct
func ConfigUnmarshalKey(key string, rawVal interface{}) error { return viper.UnmarshalKey(key, rawVal) }

// ConfigUnmarshal unmarshals the config into a Struct. Make sure that the tags
// on the fields of the structure are properly set
func ConfigUnmarshal(rawVal interface{}) error { return viper.Unmarshal(rawVal) }

// ConfigBindEnv binds a Viper key to a ENV variable
func ConfigBindEnv(input ...string) error { return viper.BindEnv(input...) }

// ConfigIsSet checks to see if the key has been set in any of the data locations
func ConfigIsSet(key string) bool { return viper.IsSet(key) }

// ConfigAutomaticEnv has Viper check ENV variables for all keys set in config, default & flags
func ConfigAutomaticEnv() { viper.AutomaticEnv() }

// ConfigInConfig checks to see if the given key (or an alias) is in the config file
func ConfigInConfig(key string) bool { return viper.InConfig(key) }

// ConfigSetDefault sets the default value for this key
func ConfigSetDefault(key string, value interface{}) { viper.SetDefault(key, value) }

// ConfigSet sets the value for the key in the override register
func ConfigSet(key string, value interface{}) { viper.Set(key, value) }

// ConfigBindPFlag binds a specific key to a pflag (as used by cobra)
func ConfigBindPFlag(key string, flag *pflag.Flag) { viper.BindPFlag(key, flag) }

// ConfigReadInConfig will discover and load the configuration file from disk
// and key/value stores, searching in one of the defined paths
func ConfigReadInConfig() error { return viper.ReadInConfig() }

// ConfigMergeInConfig merges a new configuration with an existing config
func ConfigMergeInConfig() error { return viper.MergeInConfig() }

// ConfigReadConfig will read a configuration file, setting existing keys to nil if the
// key does not exist in the file
func ConfigReadConfig(in io.Reader) error { return viper.ReadConfig(in) }

// ConfigMergeConfig merges a new configuration with an existing config
func ConfigMergeConfig(in io.Reader) error { return viper.MergeConfig(in) }

// ConfigAllKeys returns all keys holding a value, regardless of where they are set
func ConfigAllKeys() []string { return viper.AllKeys() }

// ConfigAllSettings merges all settings and returns them as a map[string]interface{}
func ConfigAllSettings() map[string]interface{} { return viper.AllSettings() }

// ConfigSetFs sets the filesystem to use to read configuration
func ConfigSetFs(fs afero.Fs) { viper.SetFs(fs) }

// ConfigSetConfigFile explicitly defines the path, name and extension of the config file
func ConfigSetConfigFile(in string) { viper.SetConfigFile(in) }

// ConfigSetConfigName sets name for the config file
func ConfigSetConfigName(in string) { viper.SetConfigName(in) }

// ConfigAddConfigPath adds a path for Viper to search for the config file in
func ConfigAddConfigPath(in string) { viper.AddConfigPath(in) }

// ConfigSetConfigType sets the type of the configuration returned by the remote source, e.g. "json"
func ConfigSetConfigType(in string) { viper.SetConfigType(in) }
