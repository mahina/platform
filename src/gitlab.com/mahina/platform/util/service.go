package util

import (
	"math"
	"math/rand"
	"time"

	log "github.com/Sirupsen/logrus"
)

// Service is goroutine-based mechanism that provides a framework for recoverable services. See unit test for an
// example of usage. The Service employs exponential back-off functionality in respect of system resources.
type Service struct {
	Name           string
	State          int
	StartDelay     time.Duration
	WorkInterval   time.Duration
	IntervalWorker func() error
	ErrorWorker    func(err error)
	CloseWorker    func(err error) error
	Backoff        Backoff

	stop                 chan bool
	originalWorkInterval time.Duration
}

// Init initializes the base service. Be sure to call this method first in your Service's Init() method.
func (s *Service) Init() error {
	s.Name = "Uninitialized Service"
	s.stop = make(chan bool, 1)
	return nil
}

// Serve is intended to be run in a goroutine. See the unit test for an example
func (s *Service) Serve() {
	if s.Name == "Uninitialized Service" {
		log.Warning("Uninitialized Service warns, did you forget to Init() your structure?")
	}
	if s.IntervalWorker == nil {
		log.Errorf("Service %s stops, no assigned worker function.", s.Name)
		return
	}
	if s.ErrorWorker == nil {
		log.Errorf("Service %s stops, no assigned error function.", s.Name)
		return
	}
	log.Infof("Service %s starts", s.Name)
	if s.WorkInterval == 0 {
		s.WorkInterval = 1 * time.Second
	}
	s.originalWorkInterval = s.WorkInterval
	time.Sleep(s.StartDelay)
	for {
		select {
		case <-s.stop:
			return
		case <-time.After(s.WorkInterval):
			err := s.IntervalWorker()
			if err != nil {
				s.ErrorWorker(err)
				backoff := s.Backoff.Duration()
				if backoff > s.WorkInterval {
					s.WorkInterval = backoff
				}
				log.Infof("Service %s restarting in %s", s.Name, s.WorkInterval)
			} else {
				s.WorkInterval = s.originalWorkInterval
				s.Backoff.Reset()
			}
		}
	}
}

// Stop stops the running service
func (s *Service) Stop() {
	log.Infof("Service %s stops", s.Name)
	var err error
	if s.CloseWorker != nil {
		err = s.CloseWorker(nil)
	}
	if err != nil {
		log.WithError(err).Errorf("%s: error closing service", s.Name)
	}
	s.stop <- true
}

func (s *Service) String() string {
	return s.Name
}

// 'Backoff' is Copyright (c) 2017 Jaime Pillora under the MIT License
//
// Backoff is a time.Duration counter, starting at Min. After every call to
// the Duration method the current timing is multiplied by Factor, but it
// never exceeds Max.

// Backoff is not generally concurrent-safe, but the ForAttempt method can
// be used concurrently.
type Backoff struct {
	//Factor is the multiplying factor for each increment step
	attempt, Factor float64
	//Jitter eases contention by randomizing backoff steps
	Jitter bool
	//Min and Max are the minimum and maximum values of the counter
	Min, Max time.Duration
}

// Duration returns the duration for the current attempt before incrementing
// the attempt counter. See ForAttempt.
func (b *Backoff) Duration() time.Duration {
	d := b.ForAttempt(b.attempt)
	b.attempt++
	return d
}

const maxInt64 = float64(math.MaxInt64 - 512)

// ForAttempt returns the duration for a specific attempt. This is useful if
// you have a large number of independent Backoffs, but don't want use
// unnecessary memory storing the Backoff parameters per Backoff. The first
// attempt should be 0.
//
// ForAttempt is concurrent-safe.
func (b *Backoff) ForAttempt(attempt float64) time.Duration {
	// Zero-values are nonsensical, so we use
	// them to apply defaults
	min := b.Min
	if min <= 0 {
		min = 100 * time.Millisecond
	}
	max := b.Max
	if max <= 0 {
		max = 10 * time.Second
	}
	if min >= max {
		// short-circuit
		return max
	}
	factor := b.Factor
	if factor <= 0 {
		factor = 2
	}
	//calculate this duration
	minf := float64(min)
	durf := minf * math.Pow(factor, attempt)
	if b.Jitter {
		durf = rand.Float64()*(durf-minf) + minf
	}
	//ensure float64 wont overflow int64
	if durf > maxInt64 {
		return max
	}
	dur := time.Duration(durf)
	//keep within bounds
	if dur < min {
		return min
	} else if dur > max {
		return max
	}
	return dur
}

// Reset restarts the current attempt counter at zero.
func (b *Backoff) Reset() {
	b.attempt = 0
}

// Attempt returns the current attempt counter value.
func (b *Backoff) Attempt() float64 {
	return b.attempt
}
