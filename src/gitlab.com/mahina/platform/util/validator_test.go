// +build unit all

package util

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidateUUID(t *testing.T) {
	validUUID := "ae1d44a9-6be2-4885-8389-d4bd0b3a3368"
	invalidUUID := "ae1d44a9-6be2-4885-8389-d4bd0b3a336" // one digit short
	text := "hello validator"

	assert.Equal(t, true, IsUUID(validUUID))
	assert.Equal(t, false, IsUUID(invalidUUID))
	assert.Equal(t, false, IsUUID(text))
}
