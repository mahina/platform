package util

import "regexp"

const uuidExpr string = "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"

var rxUUID = regexp.MustCompile(uuidExpr)

// IsUUID determines the validity of the passed UUID
func IsUUID(value string) bool {
	return rxUUID.MatchString(value)
}
