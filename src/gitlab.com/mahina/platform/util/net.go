package util

import (
	"io/ioutil"
	"net/http"
)

// GetImageBytesFromURL returns the raw bytes from a URL
func GetImageBytesFromURL(url string) ([]byte, error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	return data, err
}
