// +build unit all

package util

import (
	"errors"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type TestService struct {
	Service

	count      int
	errorCount int
	closed     bool
	serviceMu  sync.Mutex
}

func (s *TestService) Init() {
	s.serviceMu.Lock()
	defer s.serviceMu.Unlock()
	s.Service.Init()
	s.Name = "TestService"
	s.StartDelay = 50 * time.Millisecond
	s.IntervalWorker = s.handleIntervalWork
	s.WorkInterval = 500 * time.Millisecond
	s.ErrorWorker = s.handleError
	s.CloseWorker = s.handleClose
}

func (s *TestService) handleIntervalWork() error {
	s.serviceMu.Lock()
	defer s.serviceMu.Unlock()
	s.count++
	if s.count < 4 {
		return errors.New("Foo")
	} else {
		return nil
	}
}

func (s *TestService) handleError(err error) {
	s.serviceMu.Lock()
	defer s.serviceMu.Unlock()
	s.errorCount++
}

func (s *TestService) handleClose(err error) error {
	s.serviceMu.Lock()
	defer s.serviceMu.Unlock()
	s.closed = true
	return errors.New("Hey here's an error")
}

func TestCreateService(t *testing.T) {
	svc := &TestService{}
	svc.Init()
	go svc.Serve()

	time.Sleep(1500 * time.Millisecond)
	svc.Stop()
	time.Sleep(1000 * time.Millisecond)
	svc.serviceMu.Lock()
	defer svc.serviceMu.Unlock()
	assert.Equal(t, 2, svc.count, "Expected a number of worker calls")
	assert.Equal(t, 2, svc.errorCount, "Expected a number of error calls")
	assert.True(t, svc.closed, "Expected close call")
}
