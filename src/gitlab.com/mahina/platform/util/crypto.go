package util

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"io"
	"io/ioutil"

	"gitlab.com/mahina/platform/defines"
)

func trimKey(key []byte) ([]byte, error) {
	keyLength := len(key)
	switch {
	case keyLength >= 32:
		key = key[0:32]
	case keyLength >= 16:
		key = key[0:16]
	default:
		return nil, errors.New("Key length must be 16 or greater")
	}
	return key, nil
}

// Encrypt enciphers the passed text. If key is nil, we use the
// platform's default, trimmed private key.
func Encrypt(key, text []byte) (encoded string, err error) {
	if key == nil {
		key, err = GetPrivateRSAKey()
		if err != nil {
			return
		}
	}
	key, err = trimKey(key)
	if err != nil {
		return
	}
	block, err := aes.NewCipher(key)
	if err != nil {
		return
	}
	cipherText := make([]byte, aes.BlockSize+len(text))
	iv := cipherText[:aes.BlockSize]
	if _, err = io.ReadFull(rand.Reader, iv); err != nil {
		return
	}
	cfb := cipher.NewCFBEncrypter(block, iv)
	cfb.XORKeyStream(cipherText[aes.BlockSize:], text)
	encoded = base64.StdEncoding.EncodeToString([]byte(cipherText))
	return
}

// Decrypt decipher the passed base64-encoded cipher text. If key is nil,
// we use the platform's default, trimmed private key.
func Decrypt(key []byte, b64 string) (plaintext string, err error) {
	if key == nil {
		key, err = GetPrivateRSAKey()
		if err != nil {
			return
		}
	}
	key, err = trimKey(key)
	if err != nil {
		return
	}
	text, err := base64.StdEncoding.DecodeString(b64)
	block, err := aes.NewCipher(key)
	if err != nil {
		return
	}
	if len(text) < aes.BlockSize {
		err = errors.New("ciphertext too short")
		return
	}
	iv := text[:aes.BlockSize]
	text = text[aes.BlockSize:]
	cfb := cipher.NewCFBDecrypter(block, iv)
	cfb.XORKeyStream(text, text)
	plaintext = string(text)
	return
}

// GetPrivateRSAKey returns the platform's configured private key
func GetPrivateRSAKey() ([]byte, error) {
	if !ConfigIsSet(defines.PrivateKeyFlag) {
		// testing
		return []byte("FhEG2f0WS0RjTFkO"), nil
	}
	data, err := ioutil.ReadFile(ConfigGetString(defines.PrivateKeyFlag))
	if err != nil {
		return nil, err
	}
	block, _ := pem.Decode(data)
	if block == nil {
		return nil, errors.New("failed to decode PEM block containing key")
	}
	return block.Bytes, err
}
