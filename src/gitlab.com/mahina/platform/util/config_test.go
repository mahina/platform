// +build unit all

package util

import (
	"bytes"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfigSetGet(t *testing.T) {
	ConfigSet("foo", "bar")
	bar := ConfigGetString("foo")
	assert.Equal(t, "bar", bar, "Expected to be able to Set/Get configuration values")
}

// Config Precedence Order
// 1. explicit call to Set inside codebase
// 2. flag
// 3. env
// 4. config
// 5. key/value store
// 6. default value

func TestConfigPrecedenceOrderSetValue(t *testing.T) {
	key := "keySETORDER"
	ConfigSet(key, "setvalue")              // explicit call to Set
	os.Setenv("KEYSETORDER", "environment") // from the environment
	ConfigBindEnv(key)
	ConfigSetDefault(key, "setdefault") // from the default
	ConfigSetConfigType("toml")
	config := []byte(`
	key = "configfile"
	`)
	ConfigReadConfig(bytes.NewBuffer(config)) // from the config

	assert.Equal(t, "setvalue", ConfigGetString(key), "Expected set value to take precedence")
}

func TestConfigPrecedenceOrderEnvironmentValue(t *testing.T) {
	key := "keyENV"
	os.Setenv("KEYENV", "environment") // from the environment
	ConfigBindEnv(key)
	ConfigSetDefault(key, "setdefault") // from the default
	ConfigSetConfigType("toml")
	config := []byte(`
	key = "configfile"
	`)
	ConfigReadConfig(bytes.NewBuffer(config)) // from the config

	assert.Equal(t, "environment", ConfigGetString(key), "Expected environment value to take precedence")
}

func TestConfigPrecedenceOrderConfigFileValue(t *testing.T) {
	key := "keyFILE"
	ConfigSetDefault(key, "setdefault") // from the default
	ConfigSetConfigType("toml")
	config := []byte(`
	keyFILE = "configfile"
	`)
	ConfigReadConfig(bytes.NewBuffer(config)) // from the config

	assert.Equal(t, "configfile", ConfigGetString(key), "Expected config value to take precedence")
}

func TestConfigPrecedenceOrderDefaultValue(t *testing.T) {
	key := "keyDEFAULT"
	ConfigSetDefault(key, "setdefault") // from the default

	assert.Equal(t, "setdefault", ConfigGetString(key), "Expected set default value to take precedence")
}
