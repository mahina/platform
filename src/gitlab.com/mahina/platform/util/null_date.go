package util

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"time"
)

const dateFormat = "2006-01-02"

// NullDate is a nullable NullDate. It supports SQL and JSON serialization.
// It will marshal to null if null.
type NullDate struct {
	Time  time.Time
	Valid bool
}

// Scan implements the Scanner interface.
func (t *NullDate) Scan(value interface{}) error {
	var err error
	switch x := value.(type) {
	case time.Time:
		t.Time = x
	case nil:
		t.Valid = false
		return nil
	default:
		err = fmt.Errorf("null: cannot scan type %T into null.Time: %v", value, value)
	}
	t.Valid = err == nil
	return err
}

// Value implements the driver Valuer interface.
func (t NullDate) Value() (driver.Value, error) {
	if !t.Valid {
		return nil, nil
	}
	return t.Time, nil
}

// IsZero returns true if the date has a zero year (a null date)
func (t NullDate) IsZero() bool {
	if !t.Valid {
		return false
	}
	return t.Time.Year() > 1
}

// NewNullDate creates a new NullDate.
func NewNullDate(t time.Time, valid bool) NullDate {
	return NullDate{
		Time:  t,
		Valid: valid,
	}
}

// NullDateFrom creates a new NullDate that will always be valid.
func NullDateFrom(t time.Time) NullDate {
	return NewNullDate(t, true)
}

// NullDateFromPtr creates a new NullDate that will be null if t is nil.
func NullDateFromPtr(t *time.Time) NullDate {
	if t == nil {
		return NewNullDate(time.Time{}, false)
	}
	return NewNullDate(*t, true)
}

// MarshalJSON implements json.Marshaler.
// It will encode null if this time is null.
func (t NullDate) MarshalJSON() ([]byte, error) {
	if !t.Valid {
		return []byte("null"), nil
	}
	if y := t.Time.Year(); y < 0 || y >= 10000 {
		// RFC 3339 is clear that years are 4 digits exactly.
		// See golang.org/issue/4556#c15 for more discussion.
		return nil, errors.New("NullDate.MarshalJSON: year outside of range [0,9999]")
	}
	b := make([]byte, 0, len(dateFormat)+2)
	b = append(b, '"')
	b = t.Time.AppendFormat(b, dateFormat)
	b = append(b, '"')
	return b, nil
}

// UnmarshalJSON implements json.Unmarshaler.
// It supports string, object (e.g. pq.NullTime and friends)
// and null input.
func (t *NullDate) UnmarshalJSON(data []byte) error {
	var err error
	var v interface{}
	if err = json.Unmarshal(data, &v); err != nil {
		return err
	}
	switch x := v.(type) {
	case string:
		//err = t.Time.UnmarshalJSON(data)
		t.Time, err = time.Parse(`"`+dateFormat+`"`, string(data))
	case map[string]interface{}:
		ti, tiOK := x["NullDate"].(string)
		valid, validOK := x["Valid"].(bool)
		if !tiOK || !validOK {
			return fmt.Errorf(`json: unmarshalling object into Go value of type null.NullDate requires key "NullDate" to be of type string and key "Valid" to be of type bool; found %T and %T, respectively`, x["NullDate"], x["Valid"])
		}
		//err = t.Time.UnmarshalText([]byte(ti))
		t.Time, err = time.Parse(dateFormat, ti)
		t.Valid = valid
		return err
	case nil:
		t.Valid = false
		return nil
	default:
		err = fmt.Errorf("json: cannot unmarshal %v into Go value of type null.Time", reflect.TypeOf(v).Name())
	}
	t.Valid = err == nil
	return err
}

// MarshalText converts the internal date value to a byte slice
func (t NullDate) MarshalText() ([]byte, error) {
	if !t.Valid {
		return []byte("null"), nil
	}
	b := make([]byte, 0, len(dateFormat))
	return t.Time.AppendFormat(b, dateFormat), nil
}

// UnmarshalText converts the byte slice to a proper date
func (t *NullDate) UnmarshalText(text []byte) error {
	str := string(text)
	if str == "" || str == "null" {
		t.Valid = false
		return nil
	}
	//if err := t.Time.UnmarshalText(text); err != nil {
	//	return err
	//}
	var err error
	t.Time, err = time.Parse(dateFormat, str)
	if err != nil {
		return err
	}
	t.Valid = true
	return nil
}

// SetValid changes this NullDate's value and sets it to be non-null.
func (t *NullDate) SetValid(v time.Time) {
	t.Time = v
	t.Valid = true
}

// Ptr returns a pointer to this NullDate's value, or a nil pointer if this NullDate is null.
func (t NullDate) Ptr() *time.Time {
	if !t.Valid {
		return nil
	}
	return &t.Time
}
