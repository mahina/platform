// +build unit all

package util

import (
	"runtime"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mahina/platform/defines"
)

func TestCipher(t *testing.T) {
	key := "76247b64-fa39-11e6-9bd4-10ddb1b87a66"
	text := "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."

	cipherText, err := Encrypt([]byte(key), []byte(text))
	assert.NoError(t, err, "Expected successful encryption")
	assert.True(t, len(cipherText) > 0, "Expected valid ciphertext from ecryption")

	clearText, err := Decrypt([]byte(key), cipherText)
	assert.NoError(t, err, "Expected successful decryption")
	assert.Equal(t, text, clearText, "Expected decrypted value to equal original")

	key = "tooshort"
	cipherText, err = Encrypt([]byte(key), []byte(text))
	assert.Error(t, err, "Expected key too short error")
}

func TestCipherUnicode(t *testing.T) {
	key := "去年三月，由Google工程师团队打造的一台电脑"
	text := "هي يدي صغيرة جدا؟"

	cipherText, err := Encrypt([]byte(key), []byte(text))
	assert.NoError(t, err, "Expected successful encryption")
	assert.True(t, len(cipherText) > 0, "Expected valid ciphertext from ecryption")

	clearText, err := Decrypt([]byte(key), cipherText)
	assert.NoError(t, err, "Expected successful decryption")
	assert.Equal(t, text, clearText, "Expected decrypted value to equal original")
}

func TestCipherNilKey(t *testing.T) {
	text := "matthew.mcneely+buyer@gmail.com"
	encoded, err := Encrypt(nil, []byte(text))
	assert.NoError(t, err, "Expected successful encryption")
	decrypted, err := Decrypt(nil, encoded)
	assert.NoError(t, err, "Expected successful decryption")
	assert.Equal(t, text, decrypted, "Expected values to be equal")
}

func TestGetPrivateRSAKey(t *testing.T) {
	if runtime.GOOS != "linux" { // local testing only
		ConfigSet(defines.PrivateKeyFlag, "../certs/privkey.pem")
		key, err := GetPrivateRSAKey()
		assert.True(t, len(key) > 0, "Expected to get the private key")
		assert.NoError(t, err, "Expected no error in getting key")
	} else {
		t.Log("Skipping private RSA key test on linux")
	}
}
