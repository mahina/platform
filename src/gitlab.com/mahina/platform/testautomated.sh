#!/bin/bash

TEST_URL=$1 COMMIT=$2 SAUCELABS_USERNAME=$3 SAUCELABS_ACCESSKEY=$4 go test -v $(go list ./... | grep -v /vendor/) -tags automated
