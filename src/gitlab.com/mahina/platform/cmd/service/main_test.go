// +build unit all

package main

import (
	"testing"

	"io/ioutil"

	"github.com/spf13/pflag"
	"github.com/stretchr/testify/assert"
	"gitlab.com/mahina/platform/defines"
	"gitlab.com/mahina/platform/util"
)

func TestBadFlag(t *testing.T) {
	f := pflag.NewFlagSet("testing", pflag.ContinueOnError)
	f.SetOutput(ioutil.Discard)
	err := setupFlags(f, []string{"--invalidarg"})
	assert.Error(t, err, "Expected error parsing bad flag")
}

func TestPortFlags(t *testing.T) {
	f := pflag.NewFlagSet("testing", pflag.ContinueOnError)
	f.SetOutput(ioutil.Discard)
	err := setupFlags(f, []string{"--port=81", "--ssl_port=555"})
	assert.NoError(t, err, "Expected to parse ports")
	assert.Equal(t, 81, util.ConfigGetInt(defines.PortFlag), "Expected to get correct port value")
	assert.Equal(t, 555, util.ConfigGetInt(defines.SSLPortFlag), "Expected to get correct ssl_port value")
}

func TestBadPortFlags(t *testing.T) {
	f := pflag.NewFlagSet("testing", pflag.ContinueOnError)
	f.SetOutput(ioutil.Discard)
	err := setupFlags(f, []string{"--port=whaaa?", "--ssl_port=555"})
	assert.Error(t, err, "Expected error parsing bad port flag")
}

func TestServiceMultiFlag(t *testing.T) {
	f := pflag.NewFlagSet("testing", pflag.ContinueOnError)
	f.SetOutput(ioutil.Discard)
	err := setupFlags(f, []string{"--service=web,api"})
	assert.NoError(t, err, "Expected to parse services")
	assert.Len(t, util.ConfigGetStringSlice(defines.ServiceFlag), 2, "Expected to find two services")
}
