package main

import (
	goflag "flag"
	"log/syslog"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/mahina/platform/defines"
	"gitlab.com/mahina/platform/services"
	"gitlab.com/mahina/platform/util"
	"github.com/gin-gonic/gin"

	log "github.com/Sirupsen/logrus"
	"github.com/spf13/pflag"
)

// The main entry for the Mahina platform.

var defaultServices = []string{
	"api",
	"web",
	"registration",
}

func setupDefaults() {
	util.ConfigSetDefault(defines.SyslogFlag, false)
	util.ConfigSetDefault(defines.ModeFlag, gin.ReleaseMode)
	util.ConfigSetDefault(defines.PortFlag, 80)
	util.ConfigSetDefault(defines.SSLPortFlag, 443)
	util.ConfigSetDefault(defines.DBHostFlag, "localhost")
	util.ConfigSetDefault(defines.DBPortFlag, 5432)
	util.ConfigSetDefault(defines.DBDatabaseFlag, "marketplace")
	util.ConfigSetDefault(defines.DBUserFlag, "mahina")
	util.ConfigSetDefault(defines.DBUserPasswordFlag, "m@hInA")
	util.ConfigSetDefault(defines.SSLCertificateFlag, "")
	util.ConfigSetDefault(defines.PrivateKeyFlag, "")
	util.ConfigSetDefault(defines.ServiceFlag, defaultServices)
	util.ConfigSetDefault(defines.MailServiceProviderFlag, "sendinblue")
}

func setupEnvironmentVariables() {
	util.ConfigBindEnv(defines.SyslogFlag)
	util.ConfigBindEnv(defines.ModeFlag)
	util.ConfigBindEnv(defines.ServiceFlag)
	util.ConfigBindEnv(defines.PortFlag)
	util.ConfigBindEnv(defines.SSLPortFlag)
	util.ConfigBindEnv(defines.DBHostFlag)
	util.ConfigBindEnv(defines.DBPortFlag)
	util.ConfigBindEnv(defines.DBUserFlag)
	util.ConfigBindEnv(defines.DBUserPasswordFlag)
	util.ConfigBindEnv(defines.DBDatabaseFlag)
	util.ConfigBindEnv(defines.SSLCertificateFlag)
	util.ConfigBindEnv(defines.PrivateKeyFlag)
	util.ConfigBindEnv(defines.MailServiceProviderFlag)
	util.ConfigBindEnv(defines.MailServiceProviderAPIKeyFlag)
}

func setupFlags(f *pflag.FlagSet, args []string) error {
	f.String(defines.ConfigFlag, "", defines.ConfigFlagDesc)
	f.Bool(defines.SyslogFlag, false, defines.SyslogFlagDesc)
	f.String(defines.ModeFlag, gin.ReleaseMode, defines.ModeFlagDesc)
	f.Int(defines.PortFlag, 80, defines.PortFlagDesc)
	f.Int(defines.SSLPortFlag, 443, defines.SSLPortFlagDesc)
	f.String(defines.DBHostFlag, "localhost", defines.DBHostFlagDesc)
	f.Int(defines.DBPortFlag, 5432, defines.DBPortFlagDesc)
	f.String(defines.DBDatabaseFlag, "mahina", defines.DBDatabaseFlagDesc)
	f.String(defines.DBUserFlag, "mahina", defines.DBUserFlagDesc)
	f.String(defines.DBUserPasswordFlag, "m@hInA", defines.DBUserPasswordFlagDesc)
	f.String(defines.SSLCertificateFlag, "", defines.SSLCertificateFlagDesc)
	f.String(defines.PrivateKeyFlag, "", defines.PrivateKeyFlagDesc)
	f.StringSlice(defines.ServiceFlag, defaultServices, defines.ServiceFlagDesc)
	f.String(defines.WebAppLocationFlag, "", defines.WebAppLocationFlagDesc)
	f.String(defines.MailServiceProviderFlag, "sendinblue", defines.MailServiceProviderFlagDesc)
	f.String(defines.MailServiceProviderAPIKeyFlag, "", defines.MailServiceProviderAPIKeyFlagDesc)

	f.AddGoFlagSet(goflag.CommandLine)
	err := f.Parse(args)

	util.ConfigBindPFlag(defines.ConfigFlag, f.Lookup(defines.ConfigFlag))
	util.ConfigBindPFlag(defines.SyslogFlag, f.Lookup(defines.SyslogFlag))
	util.ConfigBindPFlag(defines.ModeFlag, f.Lookup(defines.ModeFlag))
	util.ConfigBindPFlag(defines.PortFlag, f.Lookup(defines.PortFlag))
	util.ConfigBindPFlag(defines.SSLPortFlag, f.Lookup(defines.SSLPortFlag))
	util.ConfigBindPFlag(defines.DBHostFlag, f.Lookup(defines.DBHostFlag))
	util.ConfigBindPFlag(defines.DBPortFlag, f.Lookup(defines.DBPortFlag))
	util.ConfigBindPFlag(defines.DBDatabaseFlag, f.Lookup(defines.DBDatabaseFlag))
	util.ConfigBindPFlag(defines.DBUserFlag, f.Lookup(defines.DBUserFlag))
	util.ConfigBindPFlag(defines.DBUserPasswordFlag, f.Lookup(defines.DBUserPasswordFlag))
	util.ConfigBindPFlag(defines.SSLCertificateFlag, f.Lookup(defines.SSLCertificateFlag))
	util.ConfigBindPFlag(defines.PrivateKeyFlag, f.Lookup(defines.PrivateKeyFlag))
	util.ConfigBindPFlag(defines.ServiceFlag, f.Lookup(defines.ServiceFlag))
	util.ConfigBindPFlag(defines.WebAppLocationFlag, f.Lookup(defines.WebAppLocationFlag))
	util.ConfigBindPFlag(defines.MailServiceProviderFlag, f.Lookup(defines.MailServiceProviderFlag))
	util.ConfigBindPFlag(defines.MailServiceProviderAPIKeyFlag, f.Lookup(defines.MailServiceProviderAPIKeyFlag))

	return err
}

func main() {
	setupDefaults()
	setupEnvironmentVariables()
	f := pflag.NewFlagSet(defines.SystemName, pflag.ExitOnError)
	setupFlags(f, os.Args[1:])

	if len(util.ConfigGetString(defines.ConfigFlag)) > 0 {
		util.ConfigSetConfigFile(util.ConfigGetString(defines.ConfigFlag))
	} else {
		util.ConfigSetConfigName("mahina")
		util.ConfigAddConfigPath("conf")
	}
	err := util.ConfigReadInConfig()
	if err != nil {
		log.Fatalf("Unable to process config file: %s", err.Error())
	}

	if util.ConfigGetBool(defines.SyslogFlag) {
		logWriter, err := syslog.New(syslog.LOG_NOTICE, defines.SystemName)
		if err == nil {
			log.SetOutput(logWriter)
		}
	}

	servicesSlice := util.ConfigGetStringSlice(defines.ServiceFlag)
	web := findInSlice("web", servicesSlice)
	api := findInSlice("api", servicesSlice)
	if web || api {
		services.StartAPIService(web, api)
	}

	for n := range servicesSlice {
		switch servicesSlice[n] {
		case "registration":
			services.StartRegistrationService()
		case "web":
			// ignore
		case "api":
			// ignore
		default:
			log.Warnf("Ignoring unknown service name %s", servicesSlice[n])
		}
	}

	done := make(chan bool, 1)
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs)
	go func() {
		for {
			sig := <-sigs
			log.Info(sig.String())
			switch sig {
			case syscall.SIGINT:
				fallthrough
			case syscall.SIGTERM:
				fallthrough
			case syscall.SIGSTOP:
				fallthrough
			case syscall.SIGQUIT:
				done <- true
			}
		}
	}()

	<-done
}

func findInSlice(key string, s []string) bool {
	for n := range s {
		if s[n] == key {
			return true
		}
	}
	return false
}
