package main

import (
	goflag "flag"
	"fmt"
	"os"

	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/spf13/pflag"
	"gitlab.com/mahina/platform/controllers"
	"gitlab.com/mahina/platform/defines"
	"gitlab.com/mahina/platform/util"
)

// This code generates JWT-based access token, needed to execute REST calls.
// Note that the private key file that is used to generate the token needs
// to be the same one that the platform service is using when validating JWT
// tokens.

func main() {
	var userUUID *string
	var roles *[]string
	var validDays *int
	var keyFile *string
	var market *string

	f := pflag.NewFlagSet("token gen", pflag.ExitOnError)
	roles = f.StringSlice("role", nil, "the Roles")
	userUUID = f.String("uuid", "", "the user UUID")
	validDays = f.Int("valid", 1, "the number of days from now that the token is valid")
	keyFile = f.String("keyfile", "", "the location of the private RSA key file")
	market = f.String("market", "market", "the market the token has access to")
	f.AddGoFlagSet(goflag.CommandLine)
	err := f.Parse(os.Args[1:])
	if err != nil {
		panic(err)
	}

	if *roles == nil || *userUUID == "" || *keyFile == "" {
		fmt.Println("Required flags missing.")
		f.PrintDefaults()
		os.Exit(1)
	}
	if !util.IsUUID(*userUUID) && *userUUID != "admin" {
		fmt.Println("Invalid UUID: ", *userUUID)
		f.PrintDefaults()
		os.Exit(1)
	}
	fmt.Println("Generating access token:")
	fmt.Println("–㊙––––––––––––––––––––––––––––––––––––")
	fmt.Println("UUID: ", *userUUID)
	fmt.Println("Roles: ", *roles)
	fmt.Println("ValidDays: ", *validDays)
	expiration := time.Now().Add(time.Hour * 24 * time.Duration(*validDays))
	fmt.Println("Expiration date: ", expiration)

	claim := &controllers.AuthClaims{}
	claim.Id = *userUUID
	claim.ExpiresAt = expiration.Unix()
	claim.Roles = *roles
	claim.Market = *market
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	util.ConfigSet(defines.PrivateKeyFlag, *keyFile)

	key, err := util.GetPrivateRSAKey()
	if err != nil {
		panic(err)
	}
	ss, err := token.SignedString(key)
	if err != nil {
		panic(err)
	}
	fmt.Println("Signed token for use in REST requests: ", ss)
}
