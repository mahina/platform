## Mahina Food Platform — CI Instance Setup

The Mahina Platform uses continuous integration (CI) to verify builds and conduct 
testing when files are committed to the repository.

Gitlab has a nice CI integration, with multiple _builds_ triggered in a _pipeline_ when files
are committed to the repository. _Runners_ are responsible to handing these pipelines. Gitlab makes
available shared runners for this purpose, but unfortunately they don't meet our needs. Fortunately it's easy enough
to set up a private CI Runner.

The [Gitlab-supplied CI Runner](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner) is used to handle Mahina builds and tests. Presently a CI Runner 
is configured on an AWS Micro EC2 instance at 54.213.101.254. Installation instructions for the Runner can be found [here](https://docs.gitlab.com/runner/install/).

Start the CI Runner thusly:

```bash
$ /usr/bin/gitlab-ci-multi-runner run --working-directory /home/gitlab-runner --config /etc/gitlab-runner/config.toml --service gitlab-runner --syslog --use
r gitlab-runner
```

The current CI Runner configuration file (/etc/gitlab-runner/config.toml):

```toml
concurrent = 1
check_interval = 0

[[runners]]
  name = "aws-micro-runner"
  url = "https://gitlab.com"
  token = "0f15fcc0d54681d4c10d8429ee2fa0"
  executor = "ssh"
  [runners.ssh]
    user = "ci-runner"
    password = "YOURCHOSENPASSWORD"
    host = "lab0.mahina.org"
    port = "22"
  [runners.cache]
```

The builds and tests are controlled using a [yaml file that is part of this repository](https://gitlab.com/mahina/platform/blob/master/.gitlab-ci.yml). 

### Additional Dependencies

The EC2 instance needs a few other applications in order to build and test the platform.

* Golang 1.7+ [https://golang.org/dl/](https://golang.org/dl/)
* Postgres 9.6+ Recommended installation [instructions in our wiki](https://gitlab.com/mahina/platform/wikis/installing-postgresql)
* Sqitch (used for DB deployment and migration). Instructions for installation in the [same Postgres installation document](https://gitlab.com/mahina/platform/wikis/installing-postgresql).