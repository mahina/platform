package defines

// System-wide constants
const (
	SystemName = "mahina"

	ConfigFlag             = "config"
	ConfigFlagDesc         = "Location of the config file (mahina.toml)"
	SyslogFlag             = "syslog"
	SyslogFlagDesc         = "Send logging output to syslog"
	SSLCertificateFlag     = "ssl_certificate"
	SSLCertificateFlagDesc = "Location of SSL certificate file"
	PrivateKeyFlag         = "ssl_certificate_key"
	PrivateKeyFlagDesc     = "Location of SSL certificate key file"
	ModeFlag               = "mode"
	ModeFlagDesc           = "Set the run mode: release | debug | test"
	ServiceFlag            = "service"
	ServiceFlagDesc        = "Set which services (list) should be started. For instance api,registration"
	PortFlag               = "port"
	PortFlagDesc           = "The HTTP port to use"
	SSLPortFlag            = "ssl_port"
	SSLPortFlagDesc        = "The SSL port to use"
	DBHostFlag             = "db_host"
	DBHostFlagDesc         = "The database host name"
	DBPortFlag             = "db_port"
	DBPortFlagDesc         = "The database port number"
	DBUserFlag             = "db_user"
	DBUserFlagDesc         = "The database user name"
	DBUserPasswordFlag     = "db_password"
	DBUserPasswordFlagDesc = "The database user password"
	DBDatabaseFlag         = "database"
	DBDatabaseFlagDesc     = "The database which to connect"

	WebAppLocationFlag     = "webapp.location"
	WebAppLocationFlagDesc = "Location of the webapp directory"

	MailServiceProviderFlag           = "mail_service.provider"
	MailServiceProviderFlagDesc       = "SMTP service provider"
	MailServiceProviderAPIKeyFlag     = "mail_service.api_key"
	MailServiceProviderAPIKeyFlagDesc = "SMTP service provider api key"
)
