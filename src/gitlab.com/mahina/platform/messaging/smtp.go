package messaging

import (
	"errors"

	"fmt"

	"gitlab.com/mahina/platform/defines"
	"gitlab.com/mahina/platform/util"
)

// EMailMessage stores the attributes of an email
type EMailMessage struct {
	To          map[string]string `json:"to"`
	Subject     string            `json:"subject"`
	FromName    string            `json:"fromName"`
	FromEmail   string            `json:"fromEMail"`
	HTML        string            `json:"html"`
	Text        string            `json:"text"`
	CC          map[string]string `json:"cc"`
	BCC         map[string]string `json:"bcc"`
	ReplyTo     map[string]string `json:"replyto"`
	Attachment  map[string]string `json:"attachment"` // must be URL
	Headers     map[string]string `json:"headers"`
	InlineImage map[string]string `json:"inline_image"`
}

// Valid tests the correctness of a EMailMessage
func (msg *EMailMessage) Valid() error {
	if len(msg.To) == 0 {
		return errors.New("No recipients")
	}
	if msg.Subject == "" {
		return errors.New("No subject")
	}
	if msg.FromEmail == "" {
		return errors.New("No from email")
	}
	if msg.FromName == "" {
		return errors.New("No from name")
	}
	if msg.HTML == "" && msg.Text == "" {
		return errors.New("No Text or HTML body")
	}
	return nil
}

// SMTPProvider abstracts the service for sending email
type SMTPProvider interface {
	SetAPICredentials(key string)
	SendEmail(msg *EMailMessage) (string, error)
}

// GetSMTPService returns the MailService provider as defined
// in either the command line, environment variable or toml file.
func GetSMTPService() (SMTPProvider, error) {
	provider := util.ConfigGetString(defines.MailServiceProviderFlag)
	key := util.ConfigGetString(defines.MailServiceProviderAPIKeyFlag)
	switch provider {
	case "sendinblue":
		return &SMTPSendInBlue{APIKey: key}, nil
	default:
		err := fmt.Errorf("GetSMTPService fails, unknown STMP provider '%s'", provider)
		return nil, err
	}
}
