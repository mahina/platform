package messaging

import (
	"errors"
	"fmt"

	sib "github.com/JKhawaja/sendinblue"
)

type SMTPSendInBlue struct {
	APIKey string
}

// SetAPICredentials stores the SendInBlue API Key. Subsequent calls to the
// service use this key in the requests.
func (svc *SMTPSendInBlue) SetAPICredentials(key string) {
	svc.APIKey = key
}

// SendEmail attempts to send the passed EMailMessage. If successful,
// a service report is returned
func (svc *SMTPSendInBlue) SendEmail(msg *EMailMessage) (string, error) {
	err := msg.Valid()
	if err != nil {
		return "", err
	}
	if svc.APIKey == "" {
		return "", errors.New("Cannot interface with SendInBlue service, no APIKey specified")
	}
	client, err := sib.NewClient(svc.APIKey)
	if err != nil {
		return "", err
	}
	email := sib.NewEmail()
	email.From = [2]string{msg.FromEmail, msg.FromName}
	email.Subject = msg.Subject
	email.Text = msg.Text
	email.HTML = msg.HTML
	for k, v := range msg.To {
		email.To[k] = v
	}
	for k, v := range msg.CC {
		email.CC[k] = v
	}
	for k, v := range msg.BCC {
		email.Bcc[k] = v
	}
	for k, v := range msg.ReplyTo {
		email.ReplyTo[k] = v
	}
	for k, v := range msg.Attachment {
		email.Attachment[k] = v
	}
	for k, v := range msg.Headers {
		email.Headers[k] = v
	}
	for k, v := range msg.InlineImage {
		email.Inline_image[k] = v
	}

	// Send Email
	response, err := client.SendEmail(email)
	return fmt.Sprintf("Email Transaction Code: %s, Message: %s", response.Code, response.Message), err
}
