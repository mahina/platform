package services

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"

	"gitlab.com/mahina/platform/controllers"
	"gitlab.com/mahina/platform/defines"
	"gitlab.com/mahina/platform/models"
	"gitlab.com/mahina/platform/util"

	log "github.com/Sirupsen/logrus"
	"github.com/unrolled/secure"
)

// APIService defines the service associated with managing API endpoints
type APIService struct {
	util.Service

	web    bool
	api    bool
	router *gin.Engine
}

// StartAPIService begins the APIService
func StartAPIService(web bool, api bool) {
	svc := &APIService{web: web, api: api}
	svc.Init()
	go svc.Serve()
}

// Init sets the internal parameters of the APIService
func (s *APIService) Init() {
	s.Service.Init()
	s.Name = "APIService"
	if s.web && s.api {
		s.Name += " (web + rest)"
	} else if s.web {
		s.Name += " (web)"
	} else if s.api {
		s.Name += " (rest)"
	}
	s.StartDelay = 50 * time.Millisecond
	s.WorkInterval = 500 * time.Millisecond
	s.IntervalWorker = s.handleIntervalWork
	s.ErrorWorker = s.handleError
	s.CloseWorker = s.handleClose

	s.InitializeRouter()
	if s.api {
		controllers.InitializeAPIRoutes(s.router)
	}
	if s.web {
		controllers.InitializeWebRoutes(s.router)
	}
}

func (s *APIService) handleIntervalWork() error {
	// DB is required for the API Service
	_, err := models.GetDB()
	if err != nil {
		return err
	}
	if s.web {
		// HTTP endpoint, redirects to SSL via secureMiddleware
		go s.router.Run(fmt.Sprintf(":%d", util.ConfigGetInt(defines.PortFlag)))
	}
	// HTTPS (blocking)
	return s.router.RunTLS(fmt.Sprintf(":%d", util.ConfigGetInt(defines.SSLPortFlag)), util.ConfigGetString(defines.SSLCertificateFlag), util.ConfigGetString(defines.PrivateKeyFlag))
}

func (s *APIService) handleError(err error) {
	log.Errorf("%s reports error: %s", s.Name, err.Error())
}

func (s *APIService) handleClose(err error) error {
	// TODO: Awaiting Go 1.8 where we can close the http server
	return nil
}

// InitializeRouter sets the internal parameters for the API service
// and commences the functions for the APIService
func (s *APIService) InitializeRouter() *gin.Engine {
	gin.SetMode(util.ConfigGetString(defines.ModeFlag))

	secureFunc := func() gin.HandlerFunc {
		return func(c *gin.Context) {
			secureMiddleware := secure.New(secure.Options{
				SSLRedirect: true,
				//TODO: Host here is problematic if not running on 443
				//SSLHost:     fmt.Sprintf("10.0.1.4:%s", util.Config[sslPortFlag]),
				SSLHost: "",
			})
			err := secureMiddleware.Process(c.Writer, c.Request)

			// If there was an error, do not continue.
			if err != nil {
				log.Warnf("Secure middleware reports an error processing request: %s", err.Error())
				return
			}
			c.Next()
		}
	}()

	s.router = gin.Default()
	if s.web {
		s.router.Use(secureFunc)
	}
	return s.router
}
