package services

import (
	"bytes"
	"html/template"
	"time"

	"gitlab.com/mahina/platform/messaging"
	"gitlab.com/mahina/platform/models"
	"gitlab.com/mahina/platform/util"

	"fmt"

	log "github.com/Sirupsen/logrus"
	"github.com/guregu/null/zero"
)

// RegistrationService manages the processing of registrations within the Platform
type RegistrationService struct {
	util.Service
}

// StartRegistrationService launches the registration service
func StartRegistrationService() {
	svc := &RegistrationService{}
	svc.Init()
	go svc.Serve()
}

// Init sets the internal parameters of the RegistrationService
func (s *RegistrationService) Init() {
	s.Service.Init()
	s.Name = "RegistrationService"
	s.StartDelay = 1000 * time.Millisecond
	s.WorkInterval = 15 * time.Second
	s.IntervalWorker = s.handleIntervalWork
	s.ErrorWorker = s.handleError
	s.CloseWorker = s.handleClose
}

// Look for 'received' registrations -> send confirm email -> set to 'email-confirm-sent'
// Look for 'email-confirmed' registrations -> set to 'awaiting-approval' -> send email to manager
// Look for 'approved' -> send welcome email -> set to 'active', set user to 'active'
func (s *RegistrationService) handleIntervalWork() error {
	// DB is required for the Registration Service, ensure we have a good connection
	_, err := models.GetDB()
	if err != nil {
		return err
	}
	err = s.processApplications()
	if err != nil {
		return err
	}
	err = s.processNewApplicantNotices()
	return err
}

func (s *RegistrationService) processApplications() error {
	regs, err := models.GetRegistrationsByStatus(nil, models.RegStateAppReceived)
	if err != nil {
		return err
	}
	for _, reg := range regs {
		log.Infof("Processing new application for %s", reg.Username.String)
		mailProvider, err := messaging.GetSMTPService()
		if err != nil {
			log.Errorf("Unable to load SMTP service, not sending email for %s", reg.Username.String)
			continue
		}
		regDoc, err := models.NewRegistrationDoc([]byte(reg.Extended.String()))
		if err != nil {
			log.Errorf("Unable to load registration doc for user %s, not sending email. Error: %s", reg.Username.String, err.Error())
			continue
		}
		msg := &messaging.EMailMessage{}
		msg.FromEmail = util.ConfigGetString("marketplace.no_reply_to_email")
		msg.FromName = util.ConfigGetString("marketplace.messaging_account_name")
		msg.To = make(map[string]string)
		msg.To[regDoc.User.Email.String] = string(regDoc.User.FirstName.String + " " + regDoc.User.LastName.String)
		msg.Subject = "Please confirm your email"
		msg.Text = fmt.Sprintf("Hello %s, please confirm your email", regDoc.User.FirstName.String)
		templateData := struct {
			FirstName             string
			Hostname              string
			MarketplaceName       string
			ConfirmEmailURL       string
			CompanyNameAndAddress string
		}{
			FirstName:             regDoc.User.FirstName.String,
			Hostname:              util.ConfigGetString("hostname"),
			MarketplaceName:       util.ConfigGetString("marketplace.full_name"),
			ConfirmEmailURL:       fmt.Sprintf("%s/v1/registration/%s/confirm-email?key=%x", util.ConfigGetString("hostname"), reg.RegUUID.String, reg.Hash()),
			CompanyNameAndAddress: fmt.Sprintf("%s %s %s", util.ConfigGetString("marketplace.full_name"), util.ConfigGetString("marketplace.address_line1"), util.ConfigGetString("marketplace.address_city_state_zip")),
		}

		t, err := template.ParseFiles(util.ConfigGetString("webapp.location") + "/email-templates/confirm-email.html")
		if err != nil {
			log.Errorf("Unable to parse email-confirm template for user %s. Error: %s", reg.Username.String, err)
			continue
		}
		buf := new(bytes.Buffer)
		err = t.Execute(buf, templateData)
		if err != nil {
			log.Errorf("Unable to execute email-confirm template for user %s. Error: %s", reg.Username.String, err)
			continue
		}
		msg.HTML = buf.String()

		resp, err := mailProvider.SendEmail(msg)
		if err != nil {
			log.Errorf("Unable to send confirm email message to user %s. Error: %s", reg.Username.String, err)
			continue
		}
		tx, err := models.BeginTX()
		err = reg.AddAction(tx, models.RegStateEmailSent, resp)
		if err != nil {
			log.Errorf("Unable to add action to registration %s. Error: %s", reg.RegUUID.String, err)
			tx.Rollback()
			continue
		}
		reg.Status = zero.StringFrom(models.RegStateEmailSent)
		err = reg.Update(tx)
		if err != nil {
			log.Errorf("Unable to update registration %s. Error: %s", reg.RegUUID.String, err)
			tx.Rollback()
			continue
		}
		err = tx.Commit()
		if err != nil {
			log.Errorf("Unable to finalize registration update for %s. Error: %s", reg.RegUUID.String, err)
			continue
		}
	}
	return nil
}

func (s *RegistrationService) processNewApplicantNotices() error {
	regs, err := models.GetRegistrationsByStatus(nil, models.RegStateEmailConfirmed)
	if err != nil {
		return err
	}
	for _, reg := range regs {
		log.Infof("Processing approval notice for %s", reg.Username.String)
		mailProvider, err := messaging.GetSMTPService()
		if err != nil {
			log.Errorf("Unable to load SMTP service, not sending email for %s", reg.Username.String)
			continue
		}
		regDoc, err := models.NewRegistrationDoc([]byte(reg.Extended.String()))
		if err != nil {
			log.Errorf("Unable to load registration doc for user %s, not sending email. Error: %s", reg.Username.String, err.Error())
			continue
		}
		msg := &messaging.EMailMessage{}
		msg.FromEmail = util.ConfigGetString("marketplace.no_reply_to_email")
		msg.FromName = util.ConfigGetString("marketplace.messaging_account_name")
		msg.To = make(map[string]string)
		msg.To[util.ConfigGetString("marketplace.manager_email")] = util.ConfigGetString("marketplace.manager_name")
		msg.Subject = "Please review new applicant: " + regDoc.User.Email.String + " at " + regDoc.Organization.LegalName.String
		msg.Text = fmt.Sprintf("Please review new applicant: %s at %s", regDoc.User.Email.String, regDoc.Organization.LegalName.String)
		templateData := struct {
			FirstName             string
			ApplicantName         string
			ApplicantCompany      string
			Hostname              string
			MarketplaceName       string
			LoginURL              string
			CompanyNameAndAddress string
		}{
			FirstName:             util.ConfigGetString("marketplace.manager_name"),
			ApplicantName:         regDoc.User.FirstName.String + " " + regDoc.User.LastName.String,
			ApplicantCompany:      regDoc.Organization.LegalName.String,
			Hostname:              util.ConfigGetString("hostname"),
			MarketplaceName:       util.ConfigGetString("marketplace.full_name"),
			LoginURL:              fmt.Sprintf("%s", util.ConfigGetString("hostname")),
			CompanyNameAndAddress: fmt.Sprintf("%s %s %s", util.ConfigGetString("marketplace.full_name"), util.ConfigGetString("marketplace.address_line1"), util.ConfigGetString("marketplace.address_city_state_zip")),
		}

		t, err := template.ParseFiles(util.ConfigGetString("webapp.location") + "/email-templates/applicant-notice-email.html")
		if err != nil {
			log.Errorf("Unable to parse applicant-notice email template for user %s. Error: %s", reg.Username.String, err)
			continue
		}
		buf := new(bytes.Buffer)
		err = t.Execute(buf, templateData)
		if err != nil {
			log.Errorf("Unable to execute applicant-notice email template for user %s. Error: %s", reg.Username.String, err)
			continue
		}
		msg.HTML = buf.String()

		_, err = mailProvider.SendEmail(msg)
		if err != nil {
			log.Errorf("Unable to send applicant-notice email message to user %s. Error: %s", util.ConfigGetString("marketplace.manager_email"), err)
			continue
		}
		reg.Status = zero.StringFrom(models.RegStateAwaitApproval)
		err = reg.Update(nil)
		if err != nil {
			// TODO: remove the notice from the queue, email flooding will occur otherwise
			log.Errorf("Unable to update registration %s. Error: %s", reg.RegUUID.String, err)
			continue
		}
	}
	return nil
}

func (s *RegistrationService) handleError(err error) {
}

func (s *RegistrationService) handleClose(err error) error {
	return nil
}
