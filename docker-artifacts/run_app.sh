#!/bin/bash

su-exec root /usr/local/bin/mahina --db_user $DB_USER --db_password $DB_PASSWORD --database $DB_NAME --config /var/lib/mahina/mahina.toml --ssl_certificate=/var/lib/mahina/fullchain.pem --ssl_certificate_key=/var/lib/mahina/privkey.pem --webapp.location /var/lib/mahina/app