FROM alpine:latest

EXPOSE 80 443

# App Setup
# ------------------------------------------------------------------------------

ADD docker-artifacts/mahina-testing.toml /var/lib/mahina/mahina.toml
ADD src/gitlab.com/mahina/platform/certs/fullchain.pem /var/lib/mahina/
ADD src/gitlab.com/mahina/platform/certs/privkey.pem /var/lib/mahina/

# copy the mahina image to /usr/local/bin
ADD mahina-linux /usr/local/bin/mahina

ENTRYPOINT ["/usr/local/bin/mahina", "--config", "/var/lib/mahina/mahina.toml"]





