# Mahina Project: Platform Services

[![codecov](https://codecov.io/gl/mahina/platform/branch/master/graph/badge.svg?token=WSLzObcc9v)](https://codecov.io/gl/mahina/platform)

The mission of the Mahina Project is to create intuitive, open-source technology to increase local food production and sales. This repository is one of several that make up the entire platform stack, see [the project page](https://gitlab.com/mahina).

## Mahina Stack Overview

The Platform uses the tried and true [three tier web architecture](https://en.wikipedia.org/wiki/Multitier_architecture).

![](https://docs.google.com/uc?id=0B_j1Cc9fvVC-Q1l4OXBMRHI1OFU)

The Platform implements a [responsive](https://en.wikipedia.org/wiki/Responsive_web_design) **web application** through which growers and buyers interact with the Platform's functionality. Typescript, Angular 4 and Bootstrap 4 are used. The web application repo has full details on its design: [https://gitlab.com/mahina/mahina-app](https://gitlab.com/mahina/mahina-app/tree).

A **services layer** (this repo) handles users requests and other marketplace functionality. This repo is the home for this layer.

The **db layer** handles the Platform storage via Postgresql. The db repo has full details on its design: [https://gitlab.com/mahina/db](https://gitlab.com/mahina/db).

----

# Platform Services
This repository implements a number of services to support operations in the Mahina Food Hub Marketplace. It's written primarily in Go (golang). Both unit and integration tests for most major services are implemented.

Communication with the web application is accomplished using a REST API, which is documented [here](https://mahina.restlet.io). By default, all services redirect open http traffic (port 80) to SSL-encrypted traffic (port 443). 

## Code Organization

The golang code is organized under src/gitlab.com/mahina/platform directory, as:

| Directory   | Purpose                                  |
| ----------- | ---------------------------------------- |
| app         | Selenium testing (Saucelabs-based)       |
| certs       | dummy certs for local testing            |
| cmd         | utilities                                |
| cmd/service | services initialization                  |
| conf        | default configuration for local testing  |
| controllers | services controllers                     |
| defines     | platform-wide definitions                |
| messaging   | email (and other future) messaging       |
| models      | database access                          |
| services    | discrete services                        |
| util        | utilities                                |


## Dependencies

The Platform Services have the following dependencies:

* [Golang](https://golang.org), version 1.8+. Installers and instructions for all platforms [here](https://golang.org/dl/)
(https://gitlab.com/mahina/platform/wikis/installing-postgresql).
* [dep](https://github.com/golang/dep), _the_ golang dependency management tool. Installation instructions [here](https://github.com/golang/dep)
* [Selenium](http://www.seleniumhq.org/), (for automated testing only) a browser automation tool used for automated testing. Download the *standalone driver* from [here](http://www.seleniumhq.org/download/).

The `dep` tool manages go-specific libraries that have been used; they represent the only other dependencies.

## Local Build
After cloning the repo...

```
$ cd platform
$ export GOPATH=`pwd`
$ cd ${GOPATH}/src/gitlab.com/mahina/platform
$ dep ensure
$ go build -o mahina cmd/service/main.go
```

## Docker Build
The [app.dockerfile](https://gitlab.com/mahina/platform/blob/master/app.dockerfile) can be used to create an Alpine Linux-based Platform container. 

```
$ docker build --pull -f app.dockerfile
```

The gitlab CI automatically builds a docker container for the **platform services** and installs it in the Gitlab container registry. Running that one is probably your best bet:

```
$ export DATABASE_HOST=mydatabasehost
$ export DATABASE_USER=mydatabaseuser
$ export DATABASE_PASSWORD=mydatabasepassword
$ export DATABASE_NAME=mydatabasename
$ docker run -d -t --rm -p 80:80 -p 443:443 --network mahina --name mahina-app registry.gitlab.com/mahina/platform mahina \
  --db_host $DATABASE_HOST --db_user $DATABASE_USER --db_password $DATABASE_PASSWORD --database $DATABASE_NAME
```

## Continuous Integration on Gitlab
Commits to this repo kick off a Gitlab CI task that builds the repo, runs unit and integation tests and creates a Docker container. Check out the [.gitlab-ci.yml](https://gitlab.com/mahina/platform/blob/master/.gitlab-ci.yml) file for the details.

## Testing

### Unit Tests

To run all unit tests:
```
$ cd ${GOPATH}/src/gitlab.com/mahina/platform
$ go test -v $(go list ./... | grep -v /vendor/) -tags unit
```
There's also a shell script in that directory (testunit.sh) that performs same command.

### Integration Tests

A running Postgres DB is required to execute all integration tests.
```
$ cd ${GOPATH}/src/gitlab.com/mahina/platform
$ go test -v $(go list ./... | grep -v /vendor/) -tags integration
```

Note that the Gitlab CI runs these tests. Check out how the [.gitlab-ci.yml](https://gitlab.com/mahina/platform/blob/master/.gitlab-ci.yml) runs tests for more details.

## Selenium (automated browser) Tests
These tests are still in their infancy, and they should/will be implemented in the web application repo.

